// jQuery.sap.require("sap.banking.ui.services.App.Settings");

sap.ui.controller("view.Authentication.Authentication", {
	
	
	onBeforeShow : function(evt){

		var obView = this.getView();

		this.adjust_View( obView );
		this.clear_all_value_state( obView );
		
    },
	
	/* ======= FRAGMENTS ======= */
	adjust_View : function( obView ){
		
		obView.div_menu.removeAllItems();
		obView.div_menu.addItem( obDesktopMenu );
		
		myCurrentData.DEVICE.adjust_View_Width(obView);
		
		// this.adjustSubHeadersAndTitle(obView);
		
		// obView.txt_HaltService_Reason.setValue("");
		
	},
	
	adjustSubHeadersAndTitle: function (obView) {
		
		
	},
	
	clear_all_value_state:function( obView ){
		// obView.txt_HaltService_Reason.setValueState( sap.ui.core.ValueState.None );
	},
	
	
	buttonTap_Halt : function(evt){
		
    },

    
    send_Halt_Request: function( stReason ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.send_Halt_Request_SUCCESS;

    	AXX_MESSAGE_CONFIRM( 
			fn_APP_CONF_GET_TEXT( "messageFile", "MESSAGE.ALERT.TITLE" ), 
			fn_APP_CONF_GET_TEXT( "messageFile", "03_F19" ), 
			[	
			 	fn_APP_CONF_GET_TEXT( "messageFile", "Settings.HaltService.Warning.ButtonCANCEL" ),
			 	fn_APP_CONF_GET_TEXT( "messageFile", "Settings.HaltService.Warning.ButtonOK" )
			], 
			function(){

				var wsParameterHaltService = new WSCaller().setSuccessCallback(callBack_Success);
				var wsHaltService = new WS(SETTINGS_Halt_Service, wsParameterHaltService );

				var wsWrapper = new WSPackageWrapper().setOperation(wsHaltService);
				wsWrapper.startWS();
			},
			function(){
				Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_HOME });
			}, 
			false
		);
    	
    },

    send_Halt_Request_SUCCESS: function(jsonData) {

		var stMessage = jsonData.Status.value;

		AXX_MESSAGE_SUCCESS(
			"",
			stMessage || fn_APP_CONF_GET_TEXT("messageFile", "03_F20")
		);

		if (jsonData.Status.code == 0) {
			myCurrentData.INFO.logOut_Force();
		}
	}
	
});