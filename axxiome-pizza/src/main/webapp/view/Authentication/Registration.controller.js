// jQuery.sap.require("sap.banking.ui.services.App.Settings");

sap.ui.controller("view.Authentication.Registration", {
	
	
	onBeforeShow : function(evt){

		var obView = this.getView();

		this.clear_all_fields( obView );
		this.clear_all_value_state( obView );
		
    },
	
	/* ======= FRAGMENTS ======= */
	adjust_View : function( obView ){
		
		obView.div_menu.removeAllItems();
		obView.div_menu.addItem( obDesktopMenu );
		
		myCurrentData.DEVICE.adjust_View_Width(obView);
		
	},
	
	adjustSubHeadersAndTitle: function (obView) {
		
		
	},
	
	clear_all_fields:function( obView ){
		
		obView.txt_FirstName.setValue("");
		obView.txt_LastName.setValue("");
		obView.txt_Street.setValue("");
		obView.txt_City.setValue("");
		obView.txt_Province.setValue("");
		obView.txt_Postal.setValue("");
		obView.txt_Email.setValue("");

		obView.txt_Password1.setValue("");
		obView.txt_Password2.setValue("");

	},
	
	clear_all_value_state:function( obView ){

		obView.txt_FirstName.setValueState( sap.ui.core.ValueState.None );
		obView.txt_LastName.setValueState( sap.ui.core.ValueState.None );
		obView.txt_Street.setValueState( sap.ui.core.ValueState.None );
		obView.txt_City.setValueState( sap.ui.core.ValueState.None );
		obView.txt_Province.setValueState( sap.ui.core.ValueState.None );
		obView.txt_Postal.setValueState( sap.ui.core.ValueState.None );
		obView.txt_Email.setValueState( sap.ui.core.ValueState.None );
		
		obView.txt_Password1.setValueState( sap.ui.core.ValueState.None );
		obView.txt_Password2.setValueState( sap.ui.core.ValueState.None );

	},
	

    
    send_Request: function( obData ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		app.setCurrentViewBusy(true);

		var callBack_Success = app.getCurrentController().send_Request_SUCCESS;

		// myCurrentData.TEMP.customer = obData;
		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);

		wsParameter.setCustomData( obData );
		var wsService = new WS( AUTHENTICATION_WS.Create_User , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();
			
    },

    send_Request_SUCCESS: function(jsonData) {

    	console_log([ "send_Request_SUCCESS", jsonData ]);
    	app.setCurrentViewBusy(false);

		// var stMessage = jsonData.Status.value;
		var obParameters = {
			fn_callBack_YES : function(){

				var obData = [];
				obData.push( myCurrentData.TEMP.customer );

				var NEWjsonData = AUTHENTICATION_MOCK.Authenticate_with_MOCK_info( obData );
				SIGNIN.execute_Pos_Login( NEWjsonData );

				// VIEWS.update_Current_Content();

				myCurrentData.TEMP.customer = null;
				Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_NEW_ORDER });

			}
		};
		DISPLAY_MESSAGE_SUCCESS(
			"Register Created !",
			"Ready to order your pizza ?",
			obParameters
		);

	},
	
	buttonTap_Register : function() {
		
		console_log("REGISTER NOW TAP");
        //TODO: Navigation based on previous page context

        var obView = app.getCurrentView();

        app.setCurrentViewBusy( true );

        obView.getController().clear_all_value_state( obView );

        var obError = { error: false, title : [], message: [], component: [] };


		if ( obView.txt_FirstName.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The First Name was not provided." );
			obError.component.push( obView.txt_FirstName );

		} 
		if ( obView.txt_LastName.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The Last Name was not provided." );
			obError.component.push( obView.txt_LastName );

		} 
		if ( obView.txt_Street.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The Street Name was not provided." );
			obError.component.push( obView.txt_Street );

		}

		if ( obView.txt_City.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The City  Name was not provided." );
			obError.component.push( obView.txt_City );

		}

		if ( obView.txt_Province.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The Province Name was not provided." );
			obError.component.push( obView.txt_Province );

		}

		if ( obView.txt_Postal.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The Postal Code was not provided." );
			obError.component.push( obView.txt_Postal );

		}

		if ( obView.txt_Email.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The E-mail was not provided." );
			obError.component.push( obView.txt_Email );

		} else if ( !isValidEmail( obView.txt_Email.getValue() ) ){

			obError.error = true;
			obError.message.push( "The E-mail provided was not valid." );
			obError.component.push( obView.txt_Email );

		}

		if ( obView.txt_Password1.getValue() === "" || ( obView.txt_Password2.getValue() === "" ) ){
			if ( obView.txt_Password1.getValue() === "" ) {

				obError.error = true;
				obError.message.push( "The Password was not provided." );
				obError.component.push( obView.txt_Password1 );

			}

			if ( obView.txt_Password2.getValue() === "" ) {

				obError.error = true;
				obError.message.push( "The Password Confirmation was not provided." );
				obError.component.push( obView.txt_Password2 );

			}
		} else if ( obView.txt_Password1.getValue() !== obView.txt_Password2.getValue() ){

			obError.error = true;
			obError.message.push( "The Password Confirmation does not match." );
			obError.component.push( obView.txt_Password2 );

		}


		if ( obError.error === true ){

			AXX_EXCEPTION_USER(
				"Oops.."
				, obError.message
				, true
				, "error"
				, obError.component
			);

		} else {

			obData = {
				FirstName 	: obView.txt_FirstName.getValue(),
				LastName 	: obView.txt_LastName.getValue(),
				Email 		: obView.txt_Email.getValue(),
				Password 	: obView.txt_Password1.getValue(),
				Street 		: obView.txt_Street.getValue(),
				City 		: obView.txt_City.getValue(),
				Province 	: obView.txt_Province.getValue(),
				PostalCode 	: obView.txt_Postal.getValue()
			};

			obView.getController().send_Request( obData );

		}

		app.setCurrentViewBusy(false);

	}
	
});