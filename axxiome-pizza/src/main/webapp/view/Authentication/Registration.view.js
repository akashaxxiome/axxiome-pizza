sap.ui.jsview("view.Authentication.Registration", {
              
    getControllerName : function() {
    	return "view.Authentication.Registration";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
    
		this.create_fragments(oCon);
		
		this.create_components(oCon);
		
		//==============================================
        //			PAGE
        //==============================================
		var obParameters = {
    		stViewName : "REGISTRATION_Page",
    		boShowFooter : false
    	}
		VIEWS_Create_Regular_Page( this, obParameters ); 
		
    	return this.thePage;
    	
    },
    
    
    create_fragments : function(oCon){
    	
    },
    
    
    create_components : function(oCon){
/*
 *   	TODO: Add Controls here, placed in "div"s (HBox or VBox)
 */   	
    	this.lbl_Register = new sap.m.Label({
    		text: "Please provide the following details to register in ATB Pizza."
    	}).addStyleClass("Text_Big centered color_navy");
        this.div_Register =  new sap.m.HBox ({
            items:[
                this.lbl_Register
            ]
        }).addStyleClass("Content_Fields_DIV margin-bottom-2 ");


    	
    	this.lbl_FirstName = new sap.m.Label ({
    		text: "First Name:"
    	}).addStyleClass("");
    	this.txt_FirstName = new sap.m.Input({maxLength:50}).addStyleClass("");
    	this.div_FirstName = new sap.m.HBox ({
    		items:[
				this.lbl_FirstName,
				this.txt_FirstName
    		]
    	}).addStyleClass("Content_Fields_DIV");
    	
    	this.lbl_LastName = new sap.m.Label ({
    		text: "Last Name:"
    	}).addStyleClass("");
    	this.txt_LastName = new sap.m.Input({maxLength:50}).addStyleClass("");
    	this.div_LastName = new sap.m.HBox ({
    		items:[
				this.lbl_LastName,
				this.txt_LastName
    		]
    	}).addStyleClass("Content_Fields_DIV ");

    	
    	this.lbl_Street = new sap.m.Label ({
    		text: "Street Address:"
    	}).addStyleClass("");
    	this.txt_Street = new sap.m.Input({maxLength:50}).addStyleClass("");
    	this.div_Street = new sap.m.HBox ({
    		items:[
				this.lbl_Street,
				this.txt_Street
    		]
    	}).addStyleClass("Content_Fields_DIV");
    	
    	this.lbl_City = new sap.m.Label ({
    		text: "City:"
    	}).addStyleClass("");
    	this.txt_City = new sap.m.Input({maxLength:50}).addStyleClass("");
    	this.div_City = new sap.m.HBox ({
    		items:[
				this.lbl_City,
				this.txt_City
    		]
    	}).addStyleClass("Content_Fields_DIV");
    	
    	this.lbl_Province = new sap.m.Label ({
    		text: "Province:",
            maxLength: 2
    	}).addStyleClass("");
    	this.txt_Province = new sap.m.Input({maxLength:2}).addStyleClass("");
    	this.div_Province = new sap.m.HBox ({
    		items:[
				this.lbl_Province,
				this.txt_Province
    		]
    	}).addStyleClass("Content_Fields_DIV");
    	
    	this.lbl_Postal = new sap.m.Label ({
    		text: "Postal Code:"
    	}).addStyleClass("");
    	this.txt_Postal = new sap.m.Input({maxLength:6}).addStyleClass("");
    	this.div_Postal = new sap.m.HBox ({
    		items:[
				this.lbl_Postal,
				this.txt_Postal
    		]
    	}).addStyleClass("Content_Fields_DIV ");
    	
    	this.lbl_Email = new sap.m.Label ({
    		text: "Email Address (used to log in):"
    	}).addStyleClass("");
    	this.txt_Email = new sap.m.Input({maxLength:50}).addStyleClass("");
    	this.div_Email = new sap.m.HBox ({
    		items:[
				this.lbl_Email,
				this.txt_Email
    		]
    	}).addStyleClass("Content_Fields_DIV ");
    	
    	this.lbl_Password1 = new sap.m.Label ({
    		text: "Password:"
    	}).addStyleClass("");
    	this.txt_Password1 = new sap.m.Input({ type: "Password" }).addStyleClass("");
    	this.div_Password1 = new sap.m.HBox ({
    		items:[
				this.lbl_Password1,
				this.txt_Password1
    		]
    	}).addStyleClass("Content_Fields_DIV ");
    	
    	this.lbl_Password2 = new sap.m.Label ({
    		text: "Re-enter Password:"
    	}).addStyleClass("");
    	this.txt_Password2 = new sap.m.Input({ type: "Password" }).addStyleClass("");
    	this.div_Password2 = new sap.m.HBox ({
    		items:[
				this.lbl_Password2,
				this.txt_Password2
    		]
    	}).addStyleClass("Content_Fields_DIV ");

        /*  ===================================================================
         *                          BUTTONS SECTION 
         *  */

        this.btn_Register = new sap.m.Button({
            text: "Register Now",
            tap:[ oCon.buttonTap_Register ],
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Accept  margin-top-1");

        this.div_btn_Register = VIEWS_Create_Buttons_Container([ this.btn_Register ]);
        /*
         *                          BUTTONS SECTION 
         *  =================================================================== */


         this.divZebra = new sap.m.VBox({
            items:[
                this.div_FirstName,
                this.div_LastName,
                this.div_Street,
                this.div_City,
                this.div_Province,
                this.div_Postal,
                this.div_Email,
                this.div_Password1,
                this.div_Password2,
            ]
         }).addStyleClass("ZEBRA_CONTAINER_DIV centered");
    	
    	VIEWS_Add_Main_Content( oCon.getView(), [
/*
 *		TODO: list "div"s here  
 */    		
			this.div_Register,
			this.divZebra,
			this.div_btn_Register

    	]);
    	
    }
   
});