sap.ui.jsview("view.Authentication.Authentication", {
              
    getControllerName : function() {
    	return "view.Authentication.Authentication";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
    
		this.create_fragments(oCon);
		
		this.create_components(oCon);
		
		//==============================================
        //			PAGE
        //==============================================
		var obParameters = {
    		stViewName : "AUTHENTICATION_Page",
    		boEnableScroll: false,
    		boShowTitle: false,
    		cssClasses: "Authentication_Page",
    		boShowFooter : true,
    		footerStyle: "WHITE",
    	}
		VIEWS_Create_Regular_Page( this, obParameters ); 
		
    	return this.thePage;
    	
    },
    
    
    create_fragments : function(oCon){
    	
    },
    
    
    create_components : function(oCon){
    	

		this.userIdInputField = new sap.m.Input(this.createId('authenticationUserId'), { 
			type: sap.m.InputType.Tel,
			// maxLength: Enrollment.InputMasks.CLIENT_NUMBER.length,
			layoutData: new sap.m.FlexItemData({
				growFactor: 1
			})
		}).addStyleClass("loginFormInput");
		this.userIdInputField.onsapenter = $.proxy(function(){ 
			myTempTimeout = setTimeout(function() {
				clearTimeout(myTempTimeout);
				myTempTimeout = null;
				oCon.buttonTap_SignIn();
			}, 10);
		}, oCon);
		
		this.pinInputField =  new sap.m.Input(this.createId('authenticationPin'), { 
			type: sap.m.InputType.Password, 
			layoutData: new sap.m.FlexItemData({
				growFactor: 1
			}),
			valueLiveUpdate : true,
			liveChange : function(oEvent) {
				var value = oEvent.getSource().getValue();
				var pattern = /^[a-zA-Z0-9]$/;
				var newString = validateInputLiveChangeValue(value, pattern);
				this.setValue(newString);
			},

		}).addStyleClass("loginFormInput");
		this.pinInputField.onsapenter = $.proxy(function(){ 
			myTempTimeout = setTimeout(function() {
				clearTimeout(myTempTimeout);
				myTempTimeout = null;
				oCon.buttonTap_SignIn();
			}, 10);
		}, oCon);

		this.lblUsername = new sap.m.Label({
			text:"{i18N>login.label.userId}",
		}).addStyleClass("Text_B ");
		
		this.lblPassword = new sap.m.Label({
			text:"{i18N>login.label.password}",
			layoutData: new sap.m.FlexItemData({ growFactor: 2	})
		}).addStyleClass("Text_B ");
		

		this.div_revision = new sap.m.Label({
			visible: (myCurrentData.IS_MOBILE ? AXX_SETTINGS_getProperty("APP_INFO.VERSION.SHOW_IN_LOGIN") : false),
			text: "v." + myCurrentData.appVersion
		}).addStyleClass("Text_C align_center width-100");



		this.divUsername = new sap.m.VBox({ 
			items:[
				this.lblUsername,
				this.userIdInputField
			]
		}).addStyleClass("width-100");
		this.divPassword = new sap.m.VBox({ 
			items:[
				this.lblPassword,
				this.pinInputField
			]
		}).addStyleClass("width-100");


    	/*	===================================================================
		 *  						FOOTER SECTION 
		 *  */
		this.footer_controls = new sap.m.HBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center,
			items:[
				new sap.m.VBox({
					items:[
						new sap.m.Button({
							text:"{i18N>login.button.login}",
							tap:[ oCon.buttonTap_Halt, oCon ],
						}).addStyleClass("AxxiomeRegularButton "),
					]
				}).addStyleClass("Page_Buttons_DIV_V"),
			]
		}).addStyleClass("PAGE_Buttons_DIV margin-top-4");
		/*
		 *  						FOOTER SECTION 
		 * 	=================================================================== */
		
    	
    	this.div_all = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
		}).addStyleClass("PAGE_Content_DIV_ALL");
    	
    	
    	VIEWS_Add_Main_Content( oCon.getView(), [ this.divUsername, this.divPassword, this.footer_controls  ]  );
    	

        /* ------------- FINALLY --------------- */
    	this.div_content = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Start ,
			items:[
			    this.div_all,
			]
		}).addStyleClass("PAGE_Content_DIV");
    	
    	
    }
   
});