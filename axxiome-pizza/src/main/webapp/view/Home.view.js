//jQuery.sap.require("sap.ui.core.IconPool");
jQuery.sap.require("control.commons.LoadingIndicator");
jQuery.sap.require("control.commons.Token_Control");

//Variable to keep track of Swipe action
var isSwipeInAction = false;

sap.ui.jsview("view.Home", {

	arBlocks : [],
	
	getControllerName: function() {
		return "view.Home";
	},
	
	onBeforeShow:function(evt){
		this.getController().onBeforeShow(evt);
	},
	onAfterShow:function(evt){
		this.getController().onAfterShow(evt);
	},


	createContent : function(oCon) {
		
		this.activityLoadingIndicator = new control.commons.LoadingIndicator({});
		
		// if (myCurrentData.IS_MOBILE) {
		// 	this.myAppHeader = sap.ui.jsfragment("home_header", "fragment.headers.mobile.APP_HEADER_HOME", this.getController());
		// 	this.myAppFooter = sap.ui.jsfragment("home_footer", "fragment.footers.mobile.APP_FOOTER", this.getController());
		// } else {
		// 	this.myAppHeader = sap.ui.jsfragment("home_header","fragment.headers.desktop.APP_HEADER", this.getController());
		// 	this.myAppFooter = sap.ui.jsfragment("home_footer","fragment.footers.desktop.APP_FOOTER", this.getController());
		// }
		
		this.create_mobile_components();
		this.render_content();
	    	
    	this.thePage = new sap.m.Page("homePage_Compartamos",{
			enableScrolling: false,
			customHeader: this.myAppHeader,
			footer: this.myAppFooter,
			showFooter: myCurrentData.IS_MOBILE ? AXX_SETTINGS_getProperty( "FOOTER.visible" ) : true,
			content:[
			         
			    new sap.m.HBox({
					alignItems : sap.m.FlexAlignItems.Start,
					justifyContent : sap.m.FlexJustifyContent.Center ,
					items:[
						new sap.m.VBox({
							alignItems : sap.m.FlexAlignItems.Center,
							justifyContent : sap.m.FlexJustifyContent.Center ,
							items:[
							    this.div_profile.addStyleClass("width-80"),
							    this.myHome_Options,
							]
						}).addStyleClass("width-100"),
					]
				}).addStyleClass("Axxiome_Page_content homePage "),
				
				this.activityLoadingIndicator,
			]
		}).removeStyleClass("Compartamos_Page");
		
		return this.thePage;
		
	},
	
	
	create_mobile_components: function(obView){
		
		this.userName = new sap.m.Text({
			textAlign: "Center",
			text: "{USER_INFO>/displayName}",
			wrapping: true,
			width: "90%",
		}).addStyleClass("Text_Big color_white align_center");
	    
	    this.userLastVisit = new sap.m.Text({
			text: "{USER_INFO>/lastLoginDate}",
			textAlign: "Left",
			wrapping: true
		});
	    
	    
	    this.div_profile_lastLogin =  new sap.m.HBox({
	    	alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
	    	    new sap.m.Text({ 
	    	    	width:"100%", textAlign:"Right", 
	    	    	text: "{i18N>home.label.lastAccess}"
	    	    }).addStyleClass("Text_C color_white "),
	    	    this.userLastVisit.addStyleClass("Text_C color_orange ")
			]
		}).addStyleClass("width-100");
	    
		this.div_profile = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			visible: myCurrentData.IS_MOBILE,
			items:[
			    this.userName,
			    this.div_profile_lastLogin
			]
		}).addStyleClass("Home_UserProfile");
		
	},
	
	render_content :function(){
		
		this.row_A = new sap.m.HBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
		}).addStyleClass("Home_MainDIV_ROW");
		this.row_B = new sap.m.HBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
		}).addStyleClass("Home_MainDIV_ROW");
		this.row_C = new sap.m.HBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
		}).addStyleClass("Home_MainDIV_ROW");
		
		this.create_Blocks();
		
		
		var oCon = sap.ui.getCore().byId( Common.Navigations.PAGE_HOME ).getController();
		oCon.manage_Blocks( oCon.getView() );
		
		
		this.myHome_Options = new sap.m.VBox({
		    alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				this.row_A,
				this.row_B,
				this.row_C,
			]
		}).addStyleClass("Home_MainDIV");
		
	},
	
	
	create_Blocks: function(){
		
		this.Home_Block = new sap.m.VBox({
			id: "Home_Icon_HOME",
			tooltip: "HOME",
			visible: false,
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				new sap.ui.core.HTML({ 
					content: "" + 
						"<div class='Home_MainDIV_Image_DIV'>" + 
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.HOME +"' />" +
						"</div>"
				}),
				new sap.m.Text({ text:"{i18N>menu.label.home}" }).addStyleClass("Home_MainDIV_Text font_black"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_HOME ");
		this.arBlocks.push( this.Home_Block );
		
		this.Account_Savings_Block = new sap.m.VBox({
			id: "Home_Icon_SAVINGS",
			tooltip: "ACCOUNT_SAVINGS",
			visible: AXX_SETTINGS_getProperty( "MODULES.ACCOUNTS_SAVINGS.visible" ),
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				new sap.ui.core.HTML({ 	
					content: "" + 
						"<div class='Home_MainDIV_Image_DIV'>" +
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.ACCOUNT_SAVINGS +"' />" +
						"</div>"
				}),
				new sap.m.Text({ text:"{i18N>menu.label.accountSavings}" }).addStyleClass("Home_MainDIV_Text"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_ACCOUNT_SAVINGS");
		this.arBlocks.push( this.Account_Savings_Block );
		
		this.Account_Insurance_Block = new sap.m.VBox({
			id: "Home_Icon_INSURANCES",
			tooltip: "ACCOUNT_INSURANCES",
			visible: AXX_SETTINGS_getProperty( "MODULES.ACCOUNTS_INSURANCES.visible" ),
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				new sap.ui.core.HTML({ 
					content: "" + 
						"<div class='Home_MainDIV_Image_DIV'>" +
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.ACCOUNT_INSURANCES +"' />" +
						"</div>"
				}),
				new sap.m.Text({ text:"{i18N>menu.label.accountInsurances}" }).addStyleClass("Home_MainDIV_Text"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_ACCOUNT_INSURANCES");
		this.arBlocks.push( this.Account_Insurance_Block );
		
		this.Account_Loans_Block = new sap.m.VBox({
			id: "Home_Icon_LOANS",
			tooltip: "ACCOUNT_LOANS",
			visible: AXX_SETTINGS_getProperty( "MODULES.ACCOUNTS_LOANS.visible" ),
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				new sap.ui.core.HTML({ 
					content: "" + 
						"<div class='Home_MainDIV_Image_DIV'>" +
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.ACCOUNT_LOANS +"' />" +
						"</div>"
				}),
				new sap.m.Text({ text:"{i18N>menu.label.accountLoans}" }).addStyleClass("Home_MainDIV_Text"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_ACCOUNT_LOANS");
		this.arBlocks.push( this.Account_Loans_Block );
		
		this.Account_Cards_Block = new sap.m.VBox({
			id: "Home_Icon_CARDS",
			tooltip: "CARDS",
			visible: AXX_SETTINGS_getProperty( "MODULES.CARDS.visible" ),
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				new sap.ui.core.HTML({ 
					content: "" + 
						"<div class='Home_MainDIV_Image_DIV'>" +
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.CARDS +"' />" +
							//"<div class='noti_bubble'>0</div>" +
						"</div>"
				}),
				new sap.m.Text({ text:"{i18N>menu.label.cards}" }).addStyleClass("Home_MainDIV_Text"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_CARDS");
		this.arBlocks.push( this.Account_Cards_Block );

		this.MobileMoney_Block = new sap.m.VBox({
			id: "Home_Icon_MOBILE_MONEY",
			tooltip: "MOBILE_MONEY",
			visible: AXX_SETTINGS_getProperty( "MODULES.MOBILE_MONEY.visible" ),
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				new sap.ui.core.HTML({ 
					content: "" + 
						"<div class='Home_MainDIV_Image_DIV'>" +
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.MOBILE_MONEY +"' />" +
						"</div>"
				}),
				new sap.m.Text({ text:"{i18N>menu.label.mobileMoney}" }).addStyleClass("Home_MainDIV_Text"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_MOBILE_MONEY");
		this.arBlocks.push( this.MobileMoney_Block );
				
		this.Payments_Block = new sap.m.VBox({
			id: "Home_Icon_PAYMENTS",
			tooltip: "PAYMENTS",
			visible: AXX_SETTINGS_getProperty( "MODULES.PAYMENTS.visible" ),
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				new sap.ui.core.HTML({ 
					content: "" + 
						"<div class='Home_MainDIV_Image_DIV'>" +
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.PAYMENTS +"' />" +
						"</div>"
				}),
				new sap.m.Text({ text:"{i18N>menu.label.payments}" }).addStyleClass("Home_MainDIV_Text"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_PAYMENTS");
		this.arBlocks.push( this.Payments_Block );
		
		this.Transfers_Block = new sap.m.VBox({
			id: "Home_Icon_TRANSFERS",
			tooltip: "TRANSFERS",
			visible: AXX_SETTINGS_getProperty( "MODULES.TRANSFERS.visible" ),
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
			    new sap.ui.core.HTML({ 
			    	content: "" +
			    		"<div class='Home_MainDIV_Image_DIV'>" +
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.TRANSFERS +"' />" +
						"</div>"
				}),
				new sap.m.Text({ text:"{i18N>menu.label.transfers}" }).addStyleClass("Home_MainDIV_Text"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_TRANSFERS");
		this.arBlocks.push( this.Transfers_Block );
		
		this.Contact_Block = new sap.m.VBox({
			id: "Home_Icon_CONTACT",
			tooltip: "SUPPORT",
			visible: AXX_SETTINGS_getProperty( "MODULES.SUPPORT.visible" ),
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				new sap.ui.core.HTML({ 
					content: "" + 
						"<div class='Home_MainDIV_Image_DIV'>" +
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.SUPPORT +"' />" +
						"</div>"
				}),
				new sap.m.Text({ text:"{i18N>menu.label.contact}" }).addStyleClass("Home_MainDIV_Text"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_CONTACT");
		this.arBlocks.push( this.Contact_Block );
		
		this.Administration_Block = new sap.m.VBox({
			id: "Home_Icon_ADMINISTRATION",
			tooltip: "SETTINGS",
			visible: AXX_SETTINGS_getProperty( "MODULES.SETTINGS.visible" ),
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				new sap.ui.core.HTML({ 
					content: "" +
						"<div class='Home_MainDIV_Image_DIV'>" +
							"<img class='Home_MainDIV_Image_DIV_IMG' src='"+ IMAGES.MAIN.SETTINGS +"' />" +
						"</div>"
				}),
				new sap.m.Text( {text:"{i18N>menu.label.adminInfo}" }).addStyleClass("Home_MainDIV_Text"),
			]
		}).addStyleClass("Home_MainDIV_ROW_TD BACKGROUND_SETTINGS");
		this.arBlocks.push( this.Administration_Block );
		
	},
	

});