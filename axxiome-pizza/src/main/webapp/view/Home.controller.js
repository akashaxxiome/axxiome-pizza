jQuery.sap.require("sap.ui.base.Event");


sap.ui.controller("view.Home", {
	
	globalEntitlementsObj: null,
	requestTracker:null,
	mock_mode : false,
	last_wallet_check: 0,
	
	onInit: function() {
	},
	
	backTap: function(evt){
		console_log("Home BACK TAP");
		CPTMS_logOut();
    },
	
	onBeforeShow : function(evt) {

		var obView = this.getView();
		app.setBusy( obView, false );
		
		var isFirstTimeAfterLogin = ( evt.data && evt.data.PageFrom  === Common.Navigations.PAGE_SIGN_IN );

	},
	
	onAfterShow : function(evt){
		
		var obView = this.getView();
		app.setBusy( obView, false );

		if ( myCurrentData.INFO == null || myCurrentData.INFO.get_Session_ID() == null ){
			obView.getController().backTap(evt);
		} else {
			this.render_Profile_Display( obView );
		}
		
	},
	
	onBeforeRendering: function (evt) {

	},
	
	
	onAfterRendering: function(evt){
	
		console_log(" HOME onAfterRendering ");
		var oCon = sap.ui.getCore().byId( Common.Navigations.PAGE_HOME ).getController();
		
		//When an ICON in HOME is clicked
		jQuery(".Home_MainDIV_ROW_TD").on("click", function(evt) {
			
			console_log("HOME -> ICON CLICKED");
        	
        	oCon.fireICON_Click({
                iconHome_Clicked : evt.currentTarget.id,
                from: Common.Navigations.PAGE_HOME
            });
        	
        });

	},

	fireICON_Click: function(evt){
		
		console_log([ "=========== fireICON_Click", typeof evt, evt ]);

		Menu_Navigation_GoTo(evt);
		
	},

	checkHomeLoading : function()
	{
		if(!this.getView().isBusy()){
			this.getView().activityLoadingIndicator.hide();
		}
	},
	
	render_Profile_Display: function( obView ){
				
		var boShow_LastAccessDate = false;
		
		if( myCurrentData.INFO && myCurrentData.INFO.get_User_LastSession() != null) 
		{
			boShow_LastAccessDate = true;
		}
		
		if ( myCurrentData.IS_MOBILE ){
			obView.div_profile_lastLogin.setVisible( boShow_LastAccessDate );
			
			$("#ProfileDisplay_DisplayName").html( myCurrentData.INFO.get_User_DisplayName() );
			$("#ProfileDisplay_DisplayDate").html( myCurrentData.INFO.get_User_LastSession() );
		} else {
			if ( !boShow_LastAccessDate ){
				$(".FRAG_Header_User div.FRAG_Header_User_LastLogin ").css("display", "none");
			} else {
				$(".FRAG_Header_User div.FRAG_Header_User_LastLogin ").css("display", "flex");
			}
		}
		
	},
	
	manage_Blocks : function ( obView ){
		
		var theBlocks = obView.arBlocks;
		
		var itemCount = 1;
		$.each( theBlocks, function( blockIndex, blockItem ){
			if ( blockItem.getVisible() ){
				addBlock_to_Screen( obView, blockItem, itemCount );
				itemCount++;
			}
		});
			
		
		function addBlock_to_Screen(obView, theBlock, itemCount){
			if ( itemCount <= 3 ){
				obView.row_A.addItem( theBlock );
			} else if ( itemCount <= 6 ){
				obView.row_B.addItem( theBlock );
			} else {
				obView.row_C.addItem( theBlock );
			}
		}
		
	},

	checkWallet: function( obView, isFirstTimeAfterLogin ){

		if ( isFirstTimeAfterLogin ){
				
			this.last_wallet_check = 0;
			this.last_wallet_check = Date.now();
			
		} else {

			app.setBusy( obView, true );

			if ( (this.last_wallet_check == 0) || (Date.now() - this.last_wallet_check > Common.Validations.HOME_last_wallet_check_PERIOD ) ){
				var wsParameterGetWallet = new WSCaller().setSuccessCallback(this.getWalletOK);
				var wsGetWallet = new WS(ACCOUNTS_Get_Wallet, wsParameterGetWallet );

				var wsArray = [wsGetWallet];
				var wsWrapper = new WSPackageWrapper().setOperation(wsGetWallet).setController(this);
				wsWrapper.startWS();
			} else {
				app.setBusy( obView, false );
			}

		}

	},
	
	
    getWalletFAIL : function(jsonData){
		
    	console_log([ " ---- getWalletFAIL CUSTOM ---- ", jsonData ]);

		var obView = sap.ui.getCore().byId( Common.Navigations.PAGE_HOME );
    	app.setBusy( obView, false );
    	
    },
    
    getWalletOK : function(jsonData){
    	
    	console_log([ ">> getWalletOK ", jsonData ]);

    	var oModel = new sap.ui.model.json.JSONModel();
		oModel.setData(jsonData.customerWallet);
		sap.ui.getCore().setModel( oModel, Common.ModelNames.CUSTOMER_WALLET );
    	
    	var obView = sap.ui.getCore().byId( Common.Navigations.PAGE_HOME );
    	app.setBusy( obView, false );
    	
    },
  
	
	/**
     * This method logs the user out on pressing device's back button while on the home page.
     */
    logoutOnBackKeyPress : function(oAction){
    	if(oAction === sap.m.MessageBox.Action.OK){
	    	//If user selects OK, then proceed with logout action and redirect the user to login page.    	
	    	Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_SIGN_IN, action: "logout" });
	    }
    },
	
});