sap.ui.jsview("view.Signin", {
	
	onInit: function(){	
		
	},

	getControllerName: function() {
		return "view.Signin";
	},
	onAfterShow:function(evt){
		var oCon = sap.ui.getCore().byId(Common.Navigations.PAGE_SIGN_IN).getController();
		// oCon.onAfterShow(evt);
	},

	createContent : function(oCon) {
		
		// -- HEADER
		this.set_Header(oCon);

		// -- FOOT
		this.set_Footer(oCon);

		this.render_Content( this.getController() );
				
		this.thePage = new sap.m.Page( {
			enableScrolling: true,
			showHeader: true,
			customHeader: this.header,
			footer: this.footer,
			showFooter: true,
			content: [
				this.div_content,
			]
		}).addStyleClass("SignIN_PAGE");
		
		return this.thePage;
		
	},

	set_Header : function(oCon){

		// if (myCurrentData.IS_MOBILE) {
			// this.header = sap.ui.jsfragment("home_NEW_Header","fragment.headers.mobile.APP_HEADER_CLEAR", oCon);
		// } else {
			this.header = sap.ui.jsfragment("home_NEW_Header","fragment.headers.desktop.APP_HEADER_SIGNIN", oCon);
		// }

	},

	set_Footer : function(oCon){

		this.footer = sap.ui.jsfragment("home_NEW_Header","fragment.footers.desktop.APP_FOOTER_SIGNIN", oCon);
		// this.crate_desktop_footer(oCon);

	},
	
	
	render_Content: function( oCon ){
		
		this.create_structure( oCon );
		
		this.div_content = new sap.m.ScrollContainer({
			horizontal: false,
			vertical: true,
			content:[
				this.dv_PizzaBody,
				// this.div_popUp
			]
		}).addStyleClass("loginScrollContainer loginPageMargin");
		
	},
	

	create_structure : function( oCon ){


		this.div_ItemA = new sap.m.VBox({
			items:[
				new sap.m.VBox({
					items:[
						new sap.m.Image({
							src: "img/pizzas/itemA.jpg"
						})
					]
				}),
				new sap.m.VBox({
					items:[
						new sap.m.Label({
							text: "Words under Ingredients"
						}).addStyleClass("color_white wrap_text centered"),
						new sap.m.Label({
							text: "ATB Pizzas are made with only the freshest ingredients!"
						}).addStyleClass("color_white Text_C wrap_text justified ")
					]
				}),
			]
		}).addStyleClass("width-30 itemA centered cursor_pointer float_left");
		this.div_ItemB = new sap.m.VBox({
			items:[
				new sap.m.VBox({
					items:[
						new sap.m.Image({
							src: "img/pizzas/itemB.jpg", 
							tap: function(){
								console_log("TAP");
								NAVIGATION.goTo_GoogleMaps();
							}
						})
					]
				}),
				new sap.m.VBox({
					items:[
						new sap.m.Label({
							text: "Find a location"
						}).addStyleClass("color_white wrap_text centered"),
						new sap.m.Label({
							text: "Need pizza right now? Click here to find a location!"
						}).addStyleClass("color_white Text_C wrap_text justified ")
					]
				}),
			]
		}).addStyleClass("width-30 itemB centered cursor_pointer float_left");
		this.div_ItemC = new sap.m.VBox({
			items:[
				new sap.m.VBox({
					items:[
						new sap.m.Image({
							src: "img/pizzas/itemC.jpg"
						})
					]
				}),
				new sap.m.VBox({
					items:[
						new sap.m.Label({
							text: "We deliver"
						}).addStyleClass("color_white wrap_text centered"),
						new sap.m.Label({
							text: "Relax and let us bring your pizza to you. If it's not hot and delivered in 30 minutes, it's free!"
						}).addStyleClass("color_white Text_C wrap_text justified ")
					]
				}),
			]
		}).addStyleClass("width-30 itemC centered cursor_pointer float_left");

		this.div_Items = new sap.m.FlexBox({
			displayInline: true,
			items:[
				this.div_ItemA,
				this.div_ItemB,
				this.div_ItemC
			]
		}).addStyleClass("div_Items centered margin-bottom-2");

		this.div_buttons_line1 = new sap.m.HBox({
			items:[
				new sap.m.VBox({
					items:[
						new sap.m.Button({
							text: "My Previous Orders",
							press:[ oCon.buttonTap_OrderHistory ],
							visible: false
						}).addStyleClass("btnPromo btnSimple  width-80")
					]
				}).addStyleClass("div-table_column centered width-50"),
				new sap.m.VBox({
					items:[
						new sap.m.Button({
							text: "Create New Order",
							press:[ oCon.buttonTap_NewOrder ],
						}).addStyleClass("btnPromo btnSimple width-80")
					]
				}).addStyleClass("div-table_column centered width-50"),
			]
		}).addStyleClass("padding-top-2"),

	
		this.div_buttons = new sap.m.VBox({
			items: [ 
				this.div_buttons_line1,
				this.div_buttons_line2
			]
		}).addStyleClass("width-90 centered ");

		this.dv_PizzaBody = new sap.m.VBox({
			items: [
				// this.div_Special,
				this.div_Items,
				this.div_buttons 
			]
		}).addStyleClass("Main_Body width-90 centered");



	},

	crate_desktop_footer : function( oCon ){

	}
	
});
