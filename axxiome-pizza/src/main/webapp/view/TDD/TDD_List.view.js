jQuery.sap.require("control.tdd.TDD_List_Header_Control");

sap.ui.jsview("view.TDD.TDD_List", {
	
    getControllerName : function() {
    	return "view.TDD.TDD_List";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
        
		this.create_fragments( oCon );
		this.create_components( oCon );
		
        //==============================================
        //			PAGE
        //==============================================
		var obParameters = {
    		stViewName : "TDD_List_Page",
    		boEnableScroll: true,
    		boShowFooter: false,
    		boShowTitle: true,
    	}
		VIEWS_Create_Regular_Page( this, obParameters ); 
		
		return this.thePage;
    	
    },
    
    
    
    create_fragments: function(oCon){
    	
    },
    
    
    create_components: function( oCon ){
    	
    	this.theList = new sap.m.List({
			showSeparators: sap.m.ListSeparators.Inner,
			showNoData: true,
			noDataText: "{i18N>common.lable.list.loading}"
		});
		
		var myViewPath = "fragment.tdd.mobile.TDD_Cards_List";
		var myNewTemplate = sap.ui.jsfragment( "TDD_Cards_List", myViewPath, oCon );
		
		var arFilters = [];
		// arFilters.push( 
		//     new sap.ui.model.Filter([
		//         new sap.ui.model.Filter( "active", sap.ui.model.FilterOperator.EQ, false ),
		//     ]),
		// );
	
		this.theList.bindAggregation("items", {
			path : "/cards",
			template : myNewTemplate,
			filters : arFilters
		});
		
		/* ------------------- DIV ALL ------------ */
		
		this.div_all = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Start ,
		}).addStyleClass("PAGE_Content_DIV_ALL");
		
		VIEWS_Add_Main_Content( oCon.getView(), [ this.theList ]  );
		
		
		/* ------------- FINALLY --------------- */
		
		this.div_content = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Start ,
			items:[
				this.div_all
			]
		}).addStyleClass("PAGE_Content_DIV_Listing");
    	
    }
   
});