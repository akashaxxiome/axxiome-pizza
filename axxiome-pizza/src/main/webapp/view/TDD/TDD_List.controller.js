sap.ui.controller("view.TDD.TDD_List", {
	
	backTap: function(evt){
  	   Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_HOME }); 
    },
	
    isEditMode:function(){
    	return false;
    },
    
    
    onBeforeShow : function(evt){
    	
    	var obView = this.getView();
		app.setBusy( obView, false);

		console_log([ "TDD ACTIVATION PAGE - BEFORE SHOW ", evt.data ]);
        
		this.adjust_View( obView );

		this.get_Cards( obView );
		
    },
	
	
	adjust_View : function( obView ) {
		
		obView.div_menu.removeAllItems();
		obView.div_menu.addItem( obDesktopMenu );
		
		myCurrentData.DEVICE.adjust_View_Width(obView);
		
		var obViewSettings = {
			subHeader_TEXT : fn_APP_CONF_GET_TEXT( "i18N", "tdd.subHeader.title" ),
			subHeader_IMG : IMAGES.MAIN.CARDS,
			title_TEXT : fn_APP_CONF_GET_TEXT( "i18N", "tdd.list.title" ),
			arCSS_REM : [],
			arCSS_ADD : [ "BACKGROUND_SUBHEADER_CARDS" ],
		};
		
		apply_Header_Title_CSS( obView, obViewSettings );
		
	},
	
    
    
	
	
	
	
	/***********************************************************
	 * 
	 * FUNCTION: 	getAllAccounts
	 * DESCRIPTION:	 
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: 
	 * 
	 * ----------------   */
	get_Cards : function( obView ){
		
		var fnCallBack_SUCCESS = this.get_Cards_SUCCESS ;
		var fnCallBack_FAILURE = this.get_Cards_FAILURE ;
		
		app.setBusy( obView, true );


		/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */

		var customData = {
			customerId : 6090
		};

		var wsParameterGetList = new WSCaller();
		wsParameterGetList.setSuccessCallback( fnCallBack_SUCCESS )
		wsParameterGetList.setFailureCallback( fnCallBack_FAILURE )
		wsParameterGetList.setCustomData( customData );


		// var wsGetList = new WS( TDD_Get_Cards_MOCK,  wsParameterGetList );
		var wsGetList = new WS( TDD_Get_Cards,  wsParameterGetList );
		var wsWrapper = new WSPackageWrapper().setWsArray([wsGetList]);

		wsWrapper.startWS();
		
	},
	
	
	
	
	/***********************************************************
	 * 
	 * FUNCTION: 	get_Inactive_Cards_SUCCESS
	 * DESCRIPTION:	 
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: 
	 * 
	 * ----------------   */
	get_Cards_SUCCESS: function(jsonData){
		
		console_log([ "-=========== get_Cards_SUCCESS", jsonData ]);
		
		try {

			var obView = sap.ui.getCore().byId( Common.Navigations.PAGE_TDD_LIST );
			obView.theList.destroyItems(); 
				
			var stModelName = Common.ModelNames.TDD_CARDS;
			var oModel = sap.ui.getCore().getModel(stModelName);
			
			obView.theList.setModel(oModel);
			oModel.refresh(true);

			if ( sap.ui.getCore().getModel( Common.ModelNames.TDD_CARDS ).getData().cards.length == 0 ){
				obView.getController().set_TDD_List_Message_NoData();
			}

		} catch(e){
			
		} finally{
			app.setCurrentViewBusy(false);
		}
			
	},
	

	/***********************************************************
	 * 
	 * FUNCTION: 	get_Inactive_Cards_FAILURE
	 * DESCRIPTION:	 
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: 
	 * 
	 * ----------------   */
	get_Cards_FAILURE: function(jsonData, stFilter){

		var obView = sap.ui.getCore().byId( Common.Navigations.PAGE_TDD_LIST );
		app.setBusy( obView, false );

		obView.theList.setShowNoData(false);

	},
	

	/***********************************************************
	 * 
	 * FUNCTION: 	set_TDD_List_Message_NoData
	 * DESCRIPTION:	The customer has no TDD. So the app displays a message.
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: messages
	 * 
	 * ----------------   */
	set_TDD_List_Message_NoData: function( jsonData ){

		var obView = sap.ui.getCore().byId( Common.Navigations.PAGE_TDD_LIST );

		obView.theList.setShowNoData(true).setNoDataText( fn_APP_CONF_GET_TEXT( "i18N", "tdd.list.nodata" ) );

	},

	
	navigateTo_Details : function(evt) {
		
		var fromPage = Common.Navigations.PAGE_TDD_LIST;
		
		if ( evt.getSource().data("active") ){
			var toPage = Common.Navigations.PAGE_TDD_NIP_CHANGE;
		} else {
			var toPage = Common.Navigations.PAGE_TDD_ACTIVATION;
		}
		
		Navigation_Invoke_Page({
			PageTo : toPage, 
			theCard : { 
				credit_card_id : evt.getSource().data("credit_card_id"),
				productId: evt.getSource().data("productId"),
				cardNumber : evt.getSource().data("cardNumber"),
				lastDigits : evt.getSource().data("lastDigits"),
				flag : evt.getSource().data("flag"),
				active : evt.getSource().data("active"),
			},
			PageFrom : fromPage
		});
        
    },
    
});