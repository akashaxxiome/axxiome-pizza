sap.ui.jsview("view.App", {

	getControllerName: function() {
		return "view.App";
	},
	
	createContent : function(oCon) {		
			
		this.Retailapp = new sap.m.App();
		sap.ui.core.Core().loadLibrary( "sap.m" );
		this.Retailapp.addPage( sap.ui.jsview( "Login", "view.Login" ) );
	

		return this.Retailapp;
	},
	
});