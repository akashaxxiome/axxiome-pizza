//jQuery.sap.require("js.utils.legacy.ErrorHandler");
var util = {};
sap.ui.controller("view.App", {

	onInit : function() {
		
		// init history mgmt
		jQuery.sap.require("jquery.sap.history");
		jQuery.sap.history({
			routes: [{ 
				path : "page", handler : jQuery.proxy(this.historyPageHandler, this) 
			}],
			defaultHandler: jQuery.proxy(this.historyDefaultHandler, this)
		});
		
		// subscribe to event bus                  
		var bus = sap.ui.getCore().getEventBus();
		bus.subscribe("nav", "to", this.navToHandler, this);
		bus.subscribe("nav", "back", this.navBackHandler, this);
		
		// subscribe onfocusin event for input and select controls
		/*window.sap.m.DateTimeInput.prototype.onfocusin = util.ErrorHandler.captureElementFocus;*/
			
		//Handling MFA
		var onclickHandler = function(oBrowserEvent) {
			pressedControl = this;
			this.focus(); // Set focus so that values will be updated.
		 }; 
		 
		var afterRenderingHandler = function(browserEvent) {
		
			var controlType = this.getMetadata().getName().toUpperCase();
			if(controlType.indexOf('BUTTON') != -1 || controlType.indexOf('LISTITEM') != -1) {
				this.$().bind("touchstart", jQuery.proxy(onclickHandler, this));
			} else if (controlType.indexOf('DATETIMEINPUT') != -1 || controlType.indexOf('INPUT') != -1) {
				this.$().find("INPUT").bind("touchstart", util.ErrorHandler.captureElementFocus);
			} else if (controlType.indexOf('SELECT') != -1) {
				this.$().bind("touchstart", util.ErrorHandler.captureElementFocus);
			}
			
	    }; 

	    
	    var beforeRenderingHandler = function() {
	    	
	    	var controlType = this.getMetadata().getName().toUpperCase();
	    	if(controlType.indexOf('BUTTON') != -1 || controlType.indexOf('LISTITEM') != -1) {
				this.$().unbind("touchstart", onclickHandler);
	    	} else if (controlType.indexOf('DATETIMEINPUT') != -1 || controlType.indexOf('INPUT') != -1) {
	    		this.$().find("INPUT").unbind("touchstart", util.ErrorHandler.captureElementFocus);
	    	} else if (controlType.indexOf('SELECT') != -1) {
				this.$().unbind("touchstart", util.ErrorHandler.captureElementFocus);
			}
	     };
	     
	    var exitHandler =  function() {
	    
	    	var controlType = this.getMetadata().getName().toUpperCase();
	    	if (controlType.indexOf('BUTTON') != -1 || controlType.indexOf('LISTITEM') != -1) {
				this.$().unbind("touchstart", onclickHandler);
	    	} else if (controlType.indexOf('DATETIMEINPUT') != -1 || controlType.indexOf('INPUT') != -1) {
	    		this.$().find("INPUT").unbind("touchstart",util.ErrorHandler.captureElementFocus);
	    	} else if (controlType.indexOf('SELECT') != -1) {
				this.$().unbind("touchstart", util.ErrorHandler.captureElementFocus);
	    	}
	    	
	    };
	    
	     
	     
	    //Handling MFA 
		window.sap.m.Button.prototype.onAfterRendering = afterRenderingHandler;
		window.sap.m.Button.prototype.onBeforeRendering = beforeRenderingHandler;
		window.sap.m.Button.prototype.exit = exitHandler;
		window.sap.m.ListItemBase.prototype.onAfterRendering = afterRenderingHandler;
		window.sap.m.ListItemBase.prototype.onBeforeRendering = beforeRenderingHandler;
		window.sap.m.ListItemBase.prototype.exit = exitHandler;
		
		//Handling validation error
	/*	window.sap.m.DateTimeInput.prototype.onAfterRendering = afterRenderingHandler;
		window.sap.m.DateTimeInput.prototype.onBeforeRendering = beforeRenderingHandler;
		window.sap.m.DateTimeInput.prototype.exit = exitHandler;*/
		
		/*window.sap.m.Input.prototype.onAfterRendering = afterRenderingHandler;
//		window.sap.m.Input.prototype.onBeforeRendering = beforeRenderingHandler;
//		window.sap.m.Input.prototype.exit = exitHandler;*/
//		window.sap.m.DateTimeInput.prototype.ontouchstart = util.ErrorHandler.captureElementFocus;
//		window.sap.m.Input.prototype.ontouchstart = util.ErrorHandler.captureElementFocus;
//		
//		window.sap.m.Select.prototype.onAfterRendering = afterRenderingHandler;
//		window.sap.m.Select.prototype.onBeforeRendering = beforeRenderingHandler;
//		window.sap.m.Select.prototype.exit = exitHandler;
//		window.sap.m.Select.prototype.ontouchstart = util.ErrorHandler.captureElementFocus;

		/*************** Netowrk online offline tracking ***************************/
		var fnNetWorkStatusMessageDone = function(){
			//TODO: Things to be done on offline network status after users confirmation
		}
//		var fnNetworkStatusChangeHandler = function(oEvent){
//			//oEvent.type
//			var bOnline = navigator.onLine ? true : false;
//			if(!bOnline){
//				var oOfflineError = {
//					response: {
//							statusCode: Common.ErrorTypes.NETWORK_OFFLINE_ERROR,
//							message: "Your are offline at moment"
//						}
//					}					
//				util.ErrorHandler.handleError(oOfflineError,undefined, undefined, fnNetWorkStatusMessageDone);
//			}
//		}

//		window.addEventListener('online',  fnNetworkStatusChangeHandler);
//  		window.addEventListener('offline', fnNetworkStatusChangeHandler);


  		window.addEventListener('offline', NETWORK_Went_Offline);
  		// window.addEventListener('online', NETWORK_Went_Online);

  		
  		/*************** END: Netowrk online offline tracking **********************/
	},
	
	historyPageHandler : function(params, navType) {
		if (params && params.id) {
			this.navTo(params.id, false, navType, null);
		} else {
			jQuery.sap.log.error("invalid page parameter: " + params);
		}
	},
	
	historyDefaultHandler : function(navType) {
		this.navTo("Login", false, navType, null);
	},
	
	navToHandler : function(channelId, eventId, data) {
		if (data && data.id) {
			this.navTo(data.id, true, null, data.data);
		} else {
			jQuery.sap.log.error("nav-to event cannot be processed. Invalid data: " + data);
		}
	},
	
	
	navBackHandler : function(channelId, eventId, data) {
		jQuery.sap.history.back();
		jQuery.sap.log.info("navBack");
	},
	
	navTo : function(id, writeHistory, navType, data) {
		
		// check param
		if (id === undefined) {
			jQuery.sap.log.error("navTo failed due to missing id");
			return;
		}
		
		// navigate on app
        var app = this.getView().Retailapp;
		if (navType === jQuery.sap.history.NavType.Back) {
			if (sap.m.InstanceManager.hasOpenDialog()) {
				sap.m.InstanceManager.closeAllDialogs();
			} else {
				app.backToPage(id);
			}
		} else {
			// lazy load view
			if (app.getPage(id) === null) {
				jQuery.sap.log.info("now loading page '" + id + "'");
				app.addPage(sap.ui.jsview(id, "view." + id));
			} 
			app.to(id, data);
		}
		
		// write history
		if (writeHistory === undefined || writeHistory) {
			jQuery.sap.history.addHistory("page", {id: id}, false);
		}
		
		// log
		jQuery.sap.log.info("navTo '" + id + "' (" + writeHistory + "," + navType + ")");
	}
	
	
});