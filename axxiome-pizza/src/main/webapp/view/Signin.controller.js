// jQuery.sap.require("sap.services.AUTHENTICATION");
jQuery.sap.require("sap.services.AUTHENTICATION");
jQuery.sap.require("sap.services.MOCK_AUTHENTICATION");
jQuery.sap.require("sap.services.PIZZA");
jQuery.sap.require("sap.services.MOCK_PIZZA");
jQuery.sap.require("sap.services.ORDERS");
jQuery.sap.require("sap.services.ORDER_STATUS");
jQuery.sap.require("sap.services.PIZZA");
jQuery.sap.require("sap.services.MOCK_ORDERS");

jQuery.sap.require("sap.ui.base.Event");

var viewSignin;
var isFirstTime = true;

this.token_tap_Message = '';
this.tokenServiceActivationStatusCode = '';

this.boExecuteValidateDevice = true;

sap.ui.controller("view.Signin", {

	busyIndicator : null,
	timer : null,

	backTap: function(evt){
		app.closeApp();
	},

	onInit : function() {

		viewSignin = this.getView();

		myCurrentData.stView = Common.Navigations.PAGE_SIGN_IN;

		viewSignin.addDelegate({ onBeforeShow: this.onBeforeShow });
		viewSignin.setBusyIndicatorDelay(0);

	},
	
	buttonTap_OrderHistory : function() {
		console_log("ORDER HISTORY TAP");

		if ( myCurrentData.isLoggedIn === false ){
			obParameters = {
				fn_callBack_YES : function(){
					Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_NEW_ORDER });		
				}
			};

			DISPLAY_MESSAGE_CONFIRM("Attention", "You must to be logged in the application \n to access the previous orders.\n\n Would you like to create a new Order ?", obParameters);
			
		}else{
			if(myCurrentData.isStaff){
				Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_STAFF_ORDER_HISTORY });
			}else {
				Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_ORDER_HISTORY });
			}
		}

	},
	
	buttonTap_NewOrder : function() {
		console_log("NEW ORDER TAP");
        Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_NEW_ORDER });
	},

	onAfterRendering : function( evt ){

		$( ".div_Items .itemA" ).on( "click", function() {
		 	// Navigation_Invoke_Page({ });
		});

		$( ".div_Items .itemC" ).on( "click", function() {
		 	// Navigation_Invoke_Page({ });
		});
		
	},


	onBeforeShow : function(evt){

		console_log("onBeforeShow");

		var oCon = app.getCurrentController();

		myCurrentData.stView = Common.Navigations.PAGE_SIGN_IN;
		app.setCurrentViewBusy(false);

		oCon.setENV( AXX_SETTINGS_getConnectionID() );
		
		if ( isFirstTime ){
			isFirstTime = false;
			viewSignin.getController().load_data( viewSignin );
		}

	},


	load_data: function (obView) {

    	try{

    		app.setBusy( obView, true );

			this.get_PizzaCrusts();
			this.get_PizzaSauces();
			this.get_PizzaToppings();

		} catch(e){

		} finally{
			app.setBusy( obView, false );
		}
		
	},


	setENV : function( inConnection ){

		var newPos = 0;
		var currentURL = myCurrentData.URL;
		var numOfENVS = arENVS.length;

		if ( inConnection != null && inConnection > - 1 ){

			newPos = inConnection;

		} else {

			if ( currentURL[0] == arENVS.length - 1){
				newPos = 0;
			} else {
				newPos = currentURL[0] + 1;
			}

		}

		myCurrentData.URL = arENVS[newPos];

	},

	
	adjust_View : function(obView){

		// obView.getController().resetCoreModels();
	},

	clear_all_value_state:function(  ){
	},

	onAfterShow : function( evt ){
		isFirstTime = false;
	},

	clear_session : function(){
		SESSION_clear_TimeOut();
		SESSION_clear_TimeOut_Clock();
		clearTimeout(wsTimeOutTimer);
	},

	clear_existing_session : function(){

		var oCon = sap.ui.getCore().byId(Common.Navigations.PAGE_SIGN_IN).getController();

		oCon.boExecuteValidateDevice = false;
		myCurrentData.activeWebService = null;
		myCurrentData.DEVICE.removeCookie();
		myCurrentData.INFO.CLEAR();
		app.setCurrentViewBusy(false);
		clearTimeout(wsTimeOutTimer);

	},

	read_RememberMe : function(){

		console_log("> read_RememberMe");

		viewSignin.userIdInputField.setEditable(true);

		if ( myCurrentData.IS_MOBILE ){

			console_log("> read_RememberMe A");

			if ( myCurrentData.DEVICE.get_RememberMe() === true && !isEmpty( myCurrentData.DEVICE.get_registered_user() ) ) {

				console_log("> read_RememberMe B");

				try {
					
					var myUserId = myCurrentData.DEVICE.get_registered_user();

					var boUseMask = true;

					if ( boUseMask === true ){
						// keep 
						var myUserId_PartA = myUserId.substr( 0, ( myUserId.length - 4 ) ) ;
						myUserId_PartA = myUserId_PartA.replace(/\d/gi, "*");
					
						var myUserId_PartB =  myUserId.substr( ( myUserId.length - 4 ), 4 ) ;

						$('#Signin--authenticationUserId-inner').unmask();
			
						viewSignin.userIdInputField.setValue( myUserId_PartA + "" + myUserId_PartB );

					} else {
						viewSignin.userIdInputField.setValue( myUserId );
					}

					viewSignin.userIdInputField.setEditable(false);

				} catch(e){
					console_log("Error when showing the masked user id >> " + e);
					viewSignin.userIdInputField.setEditable(true);
				}

			} else {
				console_log("get_registered_user EMPTY");
				$('#Signin--authenticationUserId-inner').mask( Enrollment.InputMasks.CLIENT_NUMBER, { placeholder: "" } );

			}

		}

	},

	read_userId : function(){

		if ( myCurrentData.IS_MOBILE ){

			if ( myCurrentData.DEVICE.get_RememberMe() === true || !isEmpty( myCurrentData.DEVICE.get_registered_user() ) ) {

				try {
					
					var myUserId = myCurrentData.DEVICE.get_registered_user();
					return myUserId;

				} catch(e){}

			}

		} else {

			return viewSignin.userIdInputField.getValue();

		}

	},



	/**
	 * This function finds for the models attached with ModelNames
	 * defined in constant.js and resets with empty data
	 */
	resetCoreModels : function(){

		console_log("resetCoreModels");
		if(bankingModules){
			var NO_OF_MODULES = bankingModules.length;
			var i, module, modelNames, modelName, boundModel;
			for (i = 0; i < NO_OF_MODULES; i++) {
				module = bankingModules[i];
				if(module.ModelNames){
					modelNames = module.ModelNames;
					for(var key in modelNames){
						modelName = modelNames[key];
						boundModel = sap.ui.getCore().getModel(modelName);
						if(boundModel){
							//console_log("before->",modelName, boundModel.getData());
							boundModel.setData({});
							//console_log("after->", modelName, boundModel.getData());
							boundModel.refresh();
						}
					}

				}
			};
		}
		console_log("resetCoreModels completed");

		app.setBusy( viewSignin, false );
		homeNavContainer.setBusyIndicatorDelay(0);

	},

	get_PizzaCrusts: function(  ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */

		app.setCurrentViewBusy(true);

		var callBack_Success = this.get_PizzaToppings_SUCCESS;


		app.setCurrentViewBusy(true);

		var callBack_Success = this.get_PizzaCrusts_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		var wsService = new WS( PIZZAS_WS.Get_Crusts , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();


    },

    get_PizzaCrusts_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaCrusts_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

	},

	get_PizzaSauces: function(  ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_PizzaSauces_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		var wsService = new WS( PIZZAS_WS.Get_Sauces , wsParameter );
		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaSauces_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaSauces_SUCCESS ", jsonData ]);

	},


	get_PizzaToppings: function(  ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_PizzaToppings_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		var wsService = new WS( PIZZAS_WS.Get_Toppings , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaToppings_SUCCESS: function(jsonData) {
		console_log([ ">> get_PizzaToppings_SUCCESS ", jsonData ]);
		// app.setBusy( obView, false );
	},


});