// jQuery.sap.require("sap.banking.ui.services.App.Settings");

sap.ui.controller("view.Example.Simple", {
	
	
	onBeforeShow : function(evt){

		var obView = this.getView();

		this.adjust_View( obView );
		this.clear_all_value_state( obView );
		
    },
	
	/* ======= FRAGMENTS ======= */
	adjust_View : function( obView ){
		
		obView.div_menu.removeAllItems();
		obView.div_menu.addItem( obDesktopMenu );
		
		myCurrentData.DEVICE.adjust_View_Width(obView);
		
	},
	
	adjustSubHeadersAndTitle: function (obView) {
		
		
	},
	
	clear_all_value_state:function( obView ){

		obView.txt_UserName.setValueState( sap.ui.core.ValueState.None );
		obView.txt_Password.setValueState( sap.ui.core.ValueState.None );

	},
	
	
	buttonTap_MyFunction : function(evt){
		
		var oCon = app.getCurrentController();

		var obError = {
			error: false, 
			title : [], 
			message: [], 
			component: []
		};
		this.perform_validations( obError, oCon.getView() );
		
		if ( obError.error == true ){

			app.setCurrentViewBusy(false);

			AXX_EXCEPTION_USER(
				obError.title
				, obError.message
				, true  
				, "error"
				, obError.component
			);
			
		} else {

			var stUserName = oCon.getView().txt_UserName.getValue();
			var stPassword = oCon.getView().txt_Password.getValue();
			this.send_Request( stUserName, stPassword );

		}

    },

    perform_validations : function( obError, obView ){
    	
    	this.clear_all_value_state( obView );
			
		var stUserName = obView.txt_UserName.getValue();
		var stPassword = obView.txt_Password.getValue();
		
		app.setCurrentViewBusy(true);
		
		if( isEmpty(stUserName) ){

			obError.error = true;
			obError.title.push( "Username error" );
			obError.message.push( "Username not provided" );
			obError.component.push( obView.txt_UserName );

		}

		if( isEmpty(stPassword) ){

			obError.error = true;
			obError.title.push( "Password error" );
			obError.message.push( "Password not provided" );
			obError.component.push( obView.txt_Password );

		}
		
    },

    
    send_Request: function( stUserName, stPassword ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.send_Request_SUCCESS;
		var callBack_Failure = this.send_Request_FAILURE;

		var wsParameter = new WSCaller();
		wsParameter.setSuccessCallback( callBack_Success );
		wsParameter.setFailureCallback( callBack_Failure );
		// var wsService = new WS( PIZZA_Get_Options, wsParameter );
		var wsService = new WS( PIZZA_Get_Options_MOCK, wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation( wsService );
		wsWrapper.startWS();

    },

    send_Request_SUCCESS: function(jsonData) {

		var stMessage = jsonData.Status.value;

		AXX_MESSAGE_SUCCESS(
			"Great",
			"You've made it :)"
		);

	},

	send_Request_FAILURE: function(jsonData) {

		DISPLAY_MESSAGE_ERROR("Oops", "Something went wrong" );

	},

	buttonTap_DisplayMessage : function(){

		DISPLAY_MESSAGE_SUCCESS(
			"Hi",
			"This is the success message"
		);

	},


	buttonTap_POST : function(){

		var callBack_Success = this.buttonTap_POST_SUCCESS;
		var callBack_Failure = this.buttonTap_POST_FAILURE;

		var wsParameter = new WSCaller();
		wsParameter.setSuccessCallback( callBack_Success );
		wsParameter.setFailureCallback( callBack_Failure );
		// var wsService = new WS( PIZZA_Get_Options, wsParameter );
		var wsService = new WS( ORDERS_WS.Create_Order, wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation( wsService );
		wsWrapper.startWS();

	},

	buttonTap_POST_SUCCESS : function(jsonData){

		console_log([ "buttonTap_POST_SUCCESS" , jsonData ]);

		DISPLAY_MESSAGE_SUCCESS( "", " ORDER CREATED ");

	},

	buttonTap_POST_FAILURE : function(){


	},
	
});