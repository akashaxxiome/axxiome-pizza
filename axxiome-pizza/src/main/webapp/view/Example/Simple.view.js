sap.ui.jsview("view.Example.Simple", {
              
    getControllerName : function() {
    	return "view.Example.Simple";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
    
		this.create_fragments(oCon);
		
		this.create_components(oCon);
		
		//==============================================
        //			PAGE
        //==============================================
		var obParameters = {
    		stViewName : "EXAMPLE_Simple_Page",
            // boShowFooter: false
    	}
		VIEWS_Create_Regular_Page( this, obParameters ); 
		
    	return this.thePage;
    	
    },
    
    
    create_fragments : function(oCon){
    	
    },
    
    
    create_components : function(oCon){

    	//DIV FOR FIRST ITEMS
    	this.lbl_UserName = new sap.m.Label({
    		text: "Please, inform your username"
    	}).addStyleClass("font_blue color_navy align_left");
    	this.txt_UserName = new sap.m.Input({ }).addStyleClass("");

    	this.div_Username = new sap.m.VBox({
    		items:[
    			this.lbl_UserName,
    			this.txt_UserName
    		]
    	}).addStyleClass("Content_Fields_DIV  align_left");



		//DIV FOR FIRST ITEMS
    	this.lbl_Password = new sap.m.Label({
    		text: "Please, inform your password"
    	}).addStyleClass("color_green centered");
    	this.txt_Password = new sap.m.Input({ }).addStyleClass("");

    	this.div_Password = new sap.m.VBox({
    		items:[
    			this.lbl_Password,
    			this.txt_Password
    		]
    	}).addStyleClass("Content_Fields_DIV centered width-60  margin-top-1");


    	/*	===================================================================
		 *  						BUTTONS SECTION 
		 *  */
		this.btnConfirm = new sap.m.Button({
			text: "{i18N>changePassword.label.confirmChange}",
			tap:[ oCon.buttonTap_MyFunction, oCon ],
		}).addStyleClass("AxxiomeRegularButton");

		this.div_btn_Confirm = VIEWS_Create_Buttons_Container([ this.btnConfirm ]);


		this.btnTest_A = new sap.m.Button({
			text: "Return",
			tap:[ function(){ homeNavContainer.back(); }, oCon ],
		}).addStyleClass("AxxiomeRegularButton Axxiome_Button_Reject");
		this.btnTest_B = new sap.m.Button({
			text: "Display Message",
			tap:[ oCon.buttonTap_DisplayMessage, oCon ],
		}).addStyleClass("AxxiomeRegularButton Axxiome_Button_Accept");

		this.div_btn_Test = VIEWS_Create_Buttons_Container([ this.btnTest_A, this.btnTest_B ]);

        this.btnPost = new sap.m.Button({
            text: "POST",
            tap:[ oCon.buttonTap_POST, oCon ],
        }).addStyleClass("AxxiomeRegularButton");

        this.div_btn_Post = VIEWS_Create_Buttons_Container([ this.btnPost ]);
		
		/*
		 *  						BUTTONS SECTION 
		 * 	=================================================================== */
    	


    	VIEWS_Add_Main_Content( this, [
    		this.div_Username,
    		this.div_Password,
    		this.footer_controls,
    		this.div_btn_Confirm,
    		this.div_btn_Test,
            this.div_btn_Post
    	]);
    	    	
    }
   
});