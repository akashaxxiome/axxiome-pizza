var applicationMenu;
var obDesktopMenu;
var homeNavContainer;
var signinDialog;
var loginButton;
var devicePixel;
var theAnimation;

jQuery.sap.require("sap.ui.core.IconPool");
jQuery.sap.require("control.menu.mobile.MenuControl");
jQuery.sap.require("control.menu.desktop.MenuControl");
jQuery.sap.require("control.OCBNavContainer");

sap.ui.jsview("view.Login", {
	
	getControllerName: function() {
		return "view.Login";
	},

	createContent : function(oCon) {
		
		( window.devicePixelRatio >= 2 ) ? devicePixel = "2x" : devicePixel = "1x";
		
		if ( myCurrentData.IS_MOBILE ) {
			applicationMenu = new control.menu.mobile.MenuControl({
				menuClick : function(evt){
					Menu_Navigation_GoTo(evt.mParameters.menuClicked);
				}
			});
		} 
		
		obDesktopMenu = new control.menu.desktop.MenuControl({
			menuClick : function(evt){
				Menu_Navigation_GoTo(evt.mParameters.menuClicked);
			}
		});
		
		theAnimation = (myCurrentData.IS_MOBILE) ? Common.Animation.SHOW_ANIMATION : Common.Animation.SHOW_ANIMATION;
		
		homeNavContainer = new control.OCBNavContainer({
			height:"100%",
			defaultTransitionName :  theAnimation,
		  	pages:[
		  	    sap.ui.jsview( Common.Navigations.PAGE_SIGN_IN, "view.Signin" ),
		  	]
				
		});
				  
	
		this.page = new sap.m.Page({
		   enableScrolling:false,
		   showHeader:false,
		   content:[
				homeNavContainer
			]
		});
		
		if ( myCurrentData.IS_MOBILE ){
			this.page.addContent(applicationMenu);
		}
		
		return this.page;
	}
});
