// jQuery.sap.require("sap.banking.ui.services.App.Settings");

sap.ui.controller("view.Orders.NewOrderDiego", {
	
	
	onBeforeShow : function(evt){

		var obView = this.getView();

		this.load_data( obView );
		this.clear_all_value_state( obView );
		
    },
	
	/* ======= FRAGMENTS ======= */
	load_data: function (obView) {

		this.get_Pizzas();
		this.get_PizzaCrusts();
		this.get_PizzaSauces();
		this.get_PizzaToppings();
		
	},
	
	clear_all_value_state:function( obView ){
		// obView.txtService_Reason.setValueState( sap.ui.core.ValueState.None );
	},
	
	
	buttonTap : function(evt){
		
    },

    
    get_Pizzas: function( stReason ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_Pizzas_SUCCESS;

		var wsParameter = new WSCaller();
		wsParameter.setSuccessCallback(callBack_Success);
		// var wsService = new WS( PIZZAS_MOCK.Get_Options , wsParameter );
		var wsService = new WS( PIZZAS_WS.Get_Pizzas , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_Pizzas_SUCCESS: function(jsonData) {

		console_log([ ">> get_Pizzas_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		obView.slt_Pizza.setModel( oModel );
		// obView.slt_Pizza.refreshModel();

		
	},

	get_PizzaCrusts: function( stReason ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		app.setCurrentViewBusy(true);

		var callBack_Success = this.get_PizzaCrusts_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		wsParameter.setCustomData({
			// id: 1
		});
		var wsService = new WS( PIZZAS_WS.Get_Crusts , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaCrusts_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaCrusts_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		obView.slt_Crust.setModel( oModel );
		// obView.slt_Crust.refreshModel();

		app.setCurrentViewBusy( false );

	},

	get_PizzaSauces: function( stReason ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_PizzaSauces_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		// var wsService = new WS( PIZZAS_MOCK.Get_Sauces , wsParameter );
		var wsService = new WS( PIZZAS_WS.Get_Sauces , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaSauces_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaSauces_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		obView.slt_Sauce.setModel( oModel );
		// obView.slt_Crust.refreshModel();

	},


	get_PizzaToppings: function( stReason ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_PizzaToppings_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		// var wsService = new WS( PIZZAS_MOCK.Get_Toppings , wsParameter );
		var wsService = new WS( PIZZAS_WS.Get_Toppings , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaToppings_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaToppings_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		var myDiv = obView.dv_Toppings_List;
		myDiv.destroyItems();

		$.each(jsonData, function(i, item){

			if ( item.ToppingTypeID !== "0" ){

				var checkBox = new sap.m.CheckBox(
					obView.createId("checkBox_Topping_" + item.ToppingTypeID ),
					{
						text: item.ToppingName,

					}
				);
				myDiv.addItem( checkBox );

			}

		});

	},
	



	buttonTap_OrderNow : function() {
		console.log("ORDER NOW TAP");
        Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_ORDER_SUMMARY });
	}
	
});