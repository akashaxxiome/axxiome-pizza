sap.ui.jsview("view.Orders.NewOrderDiego", {
              
    getControllerName : function() {
        return "view.Orders.NewOrderDiego";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
    
        this.create_fragments(oCon);
        
        this.create_components(oCon);
        
        //==============================================
        //          PAGE
        //==============================================
        var obParameters = {
            stViewName : "NEW_ORDER_Diego_Page",
            boShowFooter : true
        }
        VIEWS_Create_Regular_Page( this, obParameters ); 
        
        return this.thePage;
        
    },
    
    
    create_fragments : function(oCon){
        
    },
    
    
    create_components : function(oCon){
/*
 *      TODO: Add Controls here, placed in "div"s (HBox or VBox)
 */     
        this.lbl_Order = new sap.m.Label({
            text: "Your Order"
        }).addStyleClass("Text_Big");
        this.div_Order = new sap.m.HBox ({
            items:[
                this.lbl_Order
            ]
        }).addStyleClass("Content_Fields_DIV margin-bottom-1 ");
        
        this.lbl_Pizza = new sap.m.Label ({
            text: "Pizza:"
        }).addStyleClass("");
        this.slt_Pizza = new sap.m.Select ( 
            this.createId("slt_Pizza"),
            {
                change : function(evt) {
                    // oCon.onChooseAccountComboBox(evt);
                },
                items: { 
                    path: "/", // + "StandarPizzas",
                    template: new sap.ui.core.ListItem({
                        key : "{StandardPizzaID}",
                        text: "{PizzaName}"
                    }) 
                },
            }
        ).addStyleClass("");

        this.div_Pizza = new sap.m.HBox ({
            items:[
                this.lbl_Pizza,
                this.slt_Pizza
            ]
        }).addStyleClass("Content_Fields_DIV");

        this.lbl_Crust = new sap.m.Label ({
            text: "Crust:"
        });
        this.slt_Crust = new sap.m.Select ( 
            this.createId("slt_Crust"),
            {
                change : function(evt) {
                    // oCon.onChooseAccountComboBox(evt);
                },
                items: { 
                    path: "/" , // + "CrustTypes",
                    template: new sap.ui.core.ListItem({
                        key : "{CrustTypeID}",
                        text: "{CrustName}"
                    }) 
                },
            }
        ).addStyleClass("");
        this.div_Crust = new sap.m.HBox ({
            items:[
                this.lbl_Crust,
                this.slt_Crust
            ]
        }).addStyleClass("Content_Fields_DIV");

        this.lbl_Sauce = new sap.m.Label ({
            text: "Sauce:"
        });
        this.slt_Sauce = new sap.m.Select ( 
            this.createId("slt_Sauce"),
            {
                change : function(evt) {
                    // oCon.onChooseAccountComboBox(evt);
                },
                items: { 
                    path: "/" , //+ "SauceTypes",
                    template: new sap.ui.core.ListItem({
                        key : "{SauceTypeID}",
                        text: "{SauceName}"
                    }) 
                },
            }
        ).addStyleClass("");
        this.div_Sauce = new sap.m.HBox ({
            items:[
                this.lbl_Sauce,
                this.slt_Sauce
            ]
        }).addStyleClass("Content_Fields_DIV");
        
        this.lbl_Toppings = new sap.m.Label ({
            text: "Toppings:"
        });
        this.dv_Toppings_List = new sap.m.VBox({

        });
        this.div_Toppings = new sap.m.HBox ({
            items:[
                this.lbl_Toppings,
                this.dv_Toppings_List
            ]
        }).addStyleClass("Content_Fields_DIV");


        this.div_NewOrderDetails = new sap.m.VBox ({
            items:[
                   this.div_Pizza,
                   this.div_Crust,
                   this.div_Sauce,
                   this.div_Toppings
            ]
        }).addStyleClass("ZEBRA_CONTAINER_DIV centered");
        

        /*  ===================================================================
         *                          BUTTONS SECTION 
         *  */

        this.btn_Return = new sap.m.Button({
            text: "Return to Home",
            tap: [ Navigation_GoHome, oCon ]
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Back");
        this.btn_OrderNow = new sap.m.Button({
            text: "Order Now!",
            tap:[ oCon.buttonTap_OrderNow ],
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Accept ");

        this.div_btn_OrderNow = VIEWS_Create_Buttons_Container([ this.btn_Return, this.btn_OrderNow ]);
        this.div_btn_OrderNow.addStyleClass(" margin-top-3 ");

        
        /*
         *                          BUTTONS SECTION 
         *  =================================================================== */

        
        VIEWS_Add_Main_Content( oCon.getView(), [
/*
 *      TODO: list "div"s here  
 */         
            this.div_Order,
            this.div_NewOrderDetails,
            this.div_btn_OrderNow
        ]);
        
    }
   
});