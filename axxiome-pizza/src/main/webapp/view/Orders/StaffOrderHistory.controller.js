sap.ui.controller("view.Orders.StaffOrderHistory", {

	onBeforeShow : function(evt) {
		var obView = this.getView();
		this.load_data(obView);
		this.clear_all_value_state(obView);
	},

	/* ======= FRAGMENTS ======= */
	load_data : function(obView) {

		/* ================================================================= */
		/* ------- CALL ODATA SERVICE ------------ */
		/* ================================================================= */
		var callBack_Success = this.get_Orders_SUCCESS;

		var wsParameter = new WSCaller();
		wsParameter.setSuccessCallback(callBack_Success);
		var wsService = new WS(ORDERS_WS.Get_Orders, wsParameter);
		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

	},

	get_Orders_SUCCESS : function(jsonData) {
		console_log([ ">> get_Orders_SUCCESS ", jsonData ]);

		function filterByStatus(obj){
			 return obj.StatusID !== 5;
		};

		var oModel = new sap.ui.model.json.JSONModel();
		var tempDate = null;

		if (jsonData.length > 0) {
			// filter the orders already delivered
			jsonData = jsonData.filter(filterByStatus);
			// format the output of the columns
			jsonData.forEach(function(a){
				a.Details = "Name: "+a.PizzaName+ "\nCrust: "+a.CrustName+
						(a.SauceName ? "\nSauce: "+a.SauceName : "")+
						(a.Cheese ? "\nCheese: "+a.Cheese : "")+
						(a.Meats ? "\nMeat: "+a.Meats : "")+
						(a.Others ? "\nToppings: "+a.Others : "");

				if(a.OrderTstmp){
					tempDate = new Date(parseInt(a.OrderTstmp.substring(6, [19])));
					a.OrderTstmp = tempDate.toLocaleString();					
				}
			});

			oModel.setData(jsonData);

			var obView = app.getCurrentView();
			obView.oTable.setModel(oModel);
		} else {

			var obParameters = {
				fn_callBack_YES : function() {
					Navigation_Invoke_Page({
						PageTo : Common.Views.PAGE_NEW_ORDER
					});
				}
			};
			DISPLAY_MESSAGE_ERROR("Data Error",
					"The user doesn't have available information.",
					obParameters);

		}
		app.setCurrentViewBusy(false);

	},

	clear_all_value_state : function(obView) {

	},

	buttonTap_Create : function() {
		Navigation_Invoke_Page({
			PageTo : Common.Views.PAGE_STAFF_COMPLETED_ORDER_HISTORY
		});
	},

	buttonTap_OrderNow : function(algo) {
		console_log("ORDER NOW TAP");

		var obView = app.getCurrentView();
		var pizzaId = 0;
		var orderId;
		var orderTimestamp;
		var selectedCustomerID;
		if (obView.oTable.getSelectedItem() != null) {

			pizzaId = obView.oTable.getSelectedItem().getBindingContext()
					.getObject().PizzaID;
			orderId = obView.oTable.getSelectedItem().getBindingContext()
			.getObject().OrderID;
			orderTimestamp = obView.oTable.getSelectedItem().getBindingContext()
			.getObject().OrderTstmp;
			selectedCustomerID = obView.oTable.getSelectedItem().getBindingContext()
			.getObject().CustomerID;
	
	
			

			var boStandard = true;
			StandardPizzaID = obView.oTable.getSelectedItem()
					.getBindingContext().getObject().StandardPizzaID;
			if (isEmpty(StandardPizzaID)) {
				boStandard = false;
			} else {
				pizzaId = StandardPizzaID;
			}

			Navigation_Invoke_Page({
				PageTo : Common.Views.PAGE_STAFF_ORDER_SUMMARY,
				pizzaId : pizzaId,
				specialPizza : boStandard,
				orderId : orderId,
				orderTimestamp : orderTimestamp,
				selectedCustomerID : selectedCustomerID
			});

		} else {
			DISPLAY_MESSAGE_ERROR("Selection Error",
					"You must to choose one item.");
		}

	}

});