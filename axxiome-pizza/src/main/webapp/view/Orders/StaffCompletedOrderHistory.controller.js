sap.ui.controller("view.Orders.StaffCompletedOrderHistory", {
	
	onBeforeShow : function(evt){

		var obView = this.getView();

		this.load_data( obView );
		this.clear_all_value_state( obView );

    },
	
	/* ======= FRAGMENTS ======= */
	load_data : function( obView ){
		
		/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_Orders_SUCCESS;

		var wsParameter = new WSCaller();
		wsParameter.setCustomData( { StatusID : 5 } );
		wsParameter.setSuccessCallback(callBack_Success);
		var wsService = new WS( ORDERS_WS.Get_Orders , wsParameter );
		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

	},

	get_Orders_SUCCESS: function(jsonData) {

		console_log([ ">> get_Orders_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();

		if (jsonData.length > 0) {
			// format the output of the columns
			jsonData.forEach(function(a){
				if(a.OrderTstmp){
					tempDate = new Date(parseInt(a.OrderTstmp.substring(6, [19])));
					a.OrderTstmp = tempDate.toLocaleString();					
				}
			});

			
	        oModel.setData(jsonData);

			var obView = app.getCurrentView();
			obView.oTable.setModel(oModel);	
		}else{
			DISPLAY_MESSAGE_ERROR("Data Error", "There are no Orders matching such criteria", obParameters);
			
		}

	},
	
	clear_all_value_state:function( obView ){

	},

	buttonTap_Create : function(){
		Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_STAFF_ORDER_HISTORY });
	},
	
	buttonTap_OrderNow : function(algo) {
		console_log("ORDER NOW TAP");

		var obView = app.getCurrentView();
		var pizzaId = 0;

		if (obView.oTable.getSelectedItem() != null) {
			
			pizzaId = obView.oTable.getSelectedItem().getBindingContext().getObject().PizzaID;

			var boStandard = true;
			StandardPizzaID = obView.oTable.getSelectedItem().getBindingContext().getObject().StandardPizzaID;
			if ( isEmpty( StandardPizzaID ) ){
				boStandard = false;
			} else {
				pizzaId = StandardPizzaID;
			}
			
			Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_STAFF_ORDER_SUMMARY, pizzaId: pizzaId, specialPizza: boStandard });

		}else{
			DISPLAY_MESSAGE_ERROR("Selection Error", "You must to choose one item.");
		}
       
	}
	
	
});