// jQuery.sap.require("sap.banking.ui.services.App.Settings");

sap.ui.controller("view.Orders.OrderSummary", {

	thePizza : {},

	onBeforeShow : function(evt) {

		var obView = this.getView();

		this.thePizza = {};
		this.clear_fields(obView);
		this.order = evt.data.order;

		try {
			if ( !isEmpty( evt.data.pizzaId ) ){
				if(evt.data.specialPizza){
					this.thePizza.isStandard = true;
					this.thePizza.StandardPizzaID = evt.data.pizzaId;
					this.load_special_pizza(obView, { PizzaID : evt.data.pizzaId });

				}else{
					this.pizzaId = evt.data.pizzaId;
					this.load_order_pizza(obView, { OrderID : evt.data.order.OrderID });					
				}
			} else {
				if (!isEmpty(evt.data.CustomPizza)) {
					this.thePizza = evt.data.CustomPizza;
					this.thePizza.arToppings.push(parseInt(evt.data.CustomPizza.Cheese));					
					this.display_Pizza_Fields(obView);
				}
			}
		} catch (e) {
		}

		this.reload_Session();

	},

	/* ======= FRAGMENTS ======= */
	display_Pizza_Fields : function(obView) {

		var oCon = obView.getController();

		obView.txt_Pizza.setText(oCon.thePizza.Name);
		obView.txt_Size
				.setText(UTILS_PIZZA.Translate_Sizes(oCon.thePizza.Size));
		obView.txt_Cheese.setText(UTILS_PIZZA
				.Translate_Toppings(parseInt(oCon.thePizza.Cheese)));
		obView.txt_Crust.setText(UTILS_PIZZA
				.Translate_Crusts(parseInt(oCon.thePizza.Crust)));
		obView.txt_Sauce.setText(UTILS_PIZZA
				.Translate_Sauces(parseInt(oCon.thePizza.Sauce)));
		obView.txt_Toppings.setText(UTILS_PIZZA
				.Translate_Toppings(oCon.thePizza.arToppings));
		if(oCon.thePizza.OfferID && !oCon.thePizza.RedeemOrderID){
			obView.txt_PromCode.setText(oCon.thePizza.OfferID);
			obView.div_PromCode.setVisible(true);
		}

	},

	reload_Session : function() {

		var obView = app.getCurrentView();

		if (myCurrentData.isLoggedIn) {

			obView.setModel(sap.ui.getCore().getModel(
					Common.ModelNames.USER_INFO));
			var myData = obView.getModel().getData();

			obView.txt_Street.setText(myData.customer.Street);
			obView.txt_City.setText(myData.customer.City + " / "
					+ myData.customer.Province);
			obView.txt_PostalCode.setText(myData.customer.PostalCode);
			// obView.txt_Province.setText( myData.customer.Province );
		}

	},

	clear_fields : function(obView) {

		if (!myCurrentData.isLoggedIn) {
			obView.txt_Street.setText();
			obView.txt_City.setText();
			obView.txt_PostalCode.setText();
		}

		obView.txt_Pizza.setText();
		obView.txt_Size.setText();
		obView.txt_Cheese.setText();
		obView.txt_Crust.setText();
		obView.txt_Sauce.setText();
		obView.txt_Toppings.setText();
		obView.txt_PromCode.setText();
		obView.div_PromCode.setVisible(false);

	},
	clear_all_value_state : function(obView) {
		// obView.txtService_Reason.setValueState( sap.ui.core.ValueState.None
		// );
	},

	load_special_pizza : function(obView, obParameters) {

		app.setCurrentViewBusy(true);

		var callBack_Success = this.load_special_pizza_SUCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		wsParameter.setCustomData({
			StandardPizzaID : obParameters.PizzaID
		});
		var wsService = new WS(PIZZAS_WS.Get_Pizzas_Details, wsParameter);

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

	},

	load_special_pizza_SUCESS : function(jsonData) {

		console_log([ "load_specificf_pizza_SUCESS", jsonData ]);
		app.setCurrentViewBusy(false);

		var oCon = app.getCurrentController();

		var arToppingsMeats = [];

		var arToppingsOthers = [];

		if (!isEmpty(jsonData[0].OtherIDs)) {
			arToppingsOthers = jsonData[0].OtherIDs.split(",");
		}
		if (!isEmpty(jsonData[0].MeatIDs)) {
			arToppingsMeats = jsonData[0].MeatIDs.split(",");
		}

		var arToppings = arToppingsMeats.concat(arToppingsOthers);
		arToppings.push(jsonData[0].CheeseID);

		oCon.thePizza.arToppings = arToppings;
		oCon.thePizza.Sauce = jsonData[0].SauceTypeID;
		oCon.thePizza.Cheese = jsonData[0].CheeseID;
		oCon.thePizza.Size = (isEmpty(jsonData[0].Size) ? "L"
				: jsonData[0].Size);
		oCon.thePizza.Name = jsonData[0].PizzaName;
		oCon.thePizza.Crust = jsonData[0].CrustTypeID;
		oCon.thePizza.OfferID = jsonData[0].OfferID;
		oCon.thePizza.RedeemOrderID = jsonData[0].RedeemOrderID;

		oCon.display_Pizza_Fields(oCon.getView());

	},

	load_order_pizza : function(obView, obParameters) {

		app.setCurrentViewBusy(true);

		var callBack_Success = this.load_special_pizza_SUCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		wsParameter.setCustomData({ OrderID : obParameters.OrderID } );
		var wsService = new WS(ORDERS_WS.Get_Orders, wsParameter);

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

	},

	load_order_pizza_SUCCESS : function(jsonData) {

		console_log(">> load_order_pizza_SUCCESS", jsonData);
		app.setCurrentViewBusy(false);

		// TODO: Display all the information about the Standard Pizza
		// oCon.thePizza.Name = ( isEmpty( jsonData[0].StandardPizzaName ) ?
		// "Custom" : jsonData[0].StandardPizzaName );

	},

	buttonTap_ModifyOrder : function() {
		Navigation_Invoke_Page({
			PageTo : "BACK"
		});
		// Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_NEW_ORDER });
	},

	buttonTap_CreateOrder : function() {

		var obView = app.getCurrentView();

		if (myCurrentData.isLoggedIn === false) {

			DISPLAY_MESSAGE_WARNING("Sorry",
					"You need to be logged in to Order.");

		} else {
			// VALIDATIONS OF THE 3 INPUT TYPES

			if (!isEmpty(this.order)) {
				obView.getController().initialize_Re_Order();
			} else {
				obView.getController().initialize_Create_Order();
			}
		}

	},

	initialize_Create_Order : function() {

		var oCon = app.getCurrentController();

		var obOrderData = {
			"UserData" : {
				CustomerID : 1,
			},
			"PizzaData" : oCon.thePizza,
			"DeliveryData" : {

			}
		};

		UTILS_ORDER.Create_Order(obOrderData);
	},

	initialize_Re_Order : function() {

		var oCon = app.getCurrentController();

		var obOrderData = {
			"UserData" : {
				CustomerID : 1,
			},
			"PizzaData" : oCon.thePizza,
			"SelectedOrder" : oCon.order,
			"DeliveryData" : {

			}
		};

		UTILS_ORDER.Create_Order(obOrderData);
		if (!isEmpty(obOrderData.SelectedOrder.OfferID)) {
			if (obOrderData.SelectedOrder.StatusID == 5) {
				UTILS_ORDER.Update_Offer(obOrderData);
			}
		}

	}
});