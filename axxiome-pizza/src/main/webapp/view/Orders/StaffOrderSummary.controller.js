// jQuery.sap.require("sap.banking.ui.services.App.Settings");

sap.ui.controller("view.Orders.StaffOrderSummary", {
	
	thePizza : {},

	onInit: function(){
		this.get_OrdersStatus();
	},	

	onBeforeShow : function(evt){

		var obView = this.getView();

		this.thePizza = {};
		this.clear_fields( obView );
		this.selectedOrderId = evt.data.orderId;
		this.selectedOrderTimestamp = evt.data.orderTimestamp;
		this.selectedCustomerID = evt.data.selectedCustomerID;
		

		try{
			if ( !isEmpty( evt.data.pizzaId ) ){
				this.pizzaId = evt.data.pizzaId;
					this.load_order_pizza(obView, { OrderID : evt.data.orderId });
			} else {
				if ( !isEmpty(evt.data.CustomPizza) ){
					this.thePizza = evt.data.CustomPizza;
					this.display_Pizza_Fields(obView);	
				}			
			}
		} catch(e){}
		
		this.reload_Session();

    },

	get_OrdersStatus: function(  ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */

		app.setCurrentViewBusy(true);

		var callBack_Success = this.get_OrdersStatus_SUCCESS;

		if ( typeof sap.ui.getCore().getModel( Common.ModelNames.ORDERS_STATUS  ) !== "undefined" ){

			var jsonData = sap.ui.getCore().getModel( Common.ModelNames.ORDERS_STATUS  ).getData();
			callBack_Success( jsonData );

		} else {

			app.setCurrentViewBusy(true);

			var callBack_Success = this.get_OrdersStatus_SUCCESS;

			var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
			wsParameter.setCustomData({
				// id: 1
			});
			var wsService = new WS( ORDER_STATUS_WS.Get_Status , wsParameter );

			var wsWrapper = new WSPackageWrapper().setOperation(wsService);
			wsWrapper.startWS();

		}

    },

    get_OrdersStatus_SUCCESS: function(jsonData) {

		console_log([ ">> get_OrdersStatus_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		obView.orderStatus.setModel( oModel );

		app.setCurrentViewBusy( false );

	},
	
	/* ======= FRAGMENTS ======= */
	display_Pizza_Fields : function( obView ){

		var oCon = obView.getController();

		obView.orderStatus.setSelectedKey(oCon.thePizza.StatusID);
		obView.txt_Pizza.setText( oCon.thePizza.Name );
		obView.txt_Size.setText( UTILS_PIZZA.Translate_Sizes( oCon.thePizza.Size ) );
		obView.txt_Cheese.setText( UTILS_PIZZA.Translate_Toppings( parseInt( oCon.thePizza.Cheese ) )  );
		obView.txt_Crust.setText( UTILS_PIZZA.Translate_Crusts( parseInt( oCon.thePizza.Crust ) ) );
		obView.txt_Sauce.setText( UTILS_PIZZA.Translate_Sauces( parseInt( oCon.thePizza.Sauce ) ) );
		obView.txt_Toppings.setText( UTILS_PIZZA.Translate_Toppings( oCon.thePizza.arToppings ) );
		obView.txt_Order_Number.setText(this.selectedOrderId);
		obView.txt_Order_Date.setText(this.selectedOrderTimestamp);

	},

	
	
	reload_Session: function () {

		var obView = app.getCurrentView();

		if ( myCurrentData.isLoggedIn ){

			obView.setModel( sap.ui.getCore().getModel( Common.ModelNames.USER_INFO ) );
			var myData = obView.getModel().getData();

			obView.txt_Street.setText( myData.customer.Street );
			obView.txt_City.setText( myData.customer.City + " / " + myData.customer.Province  );
			obView.txt_PostalCode.setText( myData.customer.PostalCode );
			// obView.txt_Province.setText( myData.customer.Province );
		}

	},
	
	clear_fields : function(obView){

		if ( !myCurrentData.isLoggedIn ){
			obView.txt_Street.setText();
			obView.txt_City.setText();
			obView.txt_PostalCode.setText();
		}

		obView.txt_Pizza.setText();
		obView.txt_Size.setText();
		obView.txt_Cheese.setText();
		obView.txt_Crust.setText();
		obView.txt_Sauce.setText();
		obView.txt_Toppings.setText();

	},
	clear_all_value_state:function( obView ){
		// obView.txtService_Reason.setValueState( sap.ui.core.ValueState.None );
	},
	
	
	load_special_pizza: function(obView , obParameters ){

		app.setCurrentViewBusy(true);

		var callBack_Success = this.load_special_pizza_SUCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		wsParameter.setCustomData({ StandardPizzaID : obParameters.PizzaID } );
		var wsService = new WS( PIZZAS_WS.Get_Pizzas_Details , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

	},

	load_special_pizza_SUCESS: function( jsonData ){

		console_log([ "load_specificf_pizza_SUCESS", jsonData ]);
		app.setCurrentViewBusy(false);
		
		var oCon = app.getCurrentController();
		

		var arToppingsMeats = [];

		var arToppingsOthers = [];
		
		if ( !isEmpty( jsonData[0].OtherIDs ) ){
			arToppingsOthers = jsonData[0].OtherIDs.split(",");
		}
		if ( !isEmpty( jsonData[0].MeatIDs ) ){
			arToppingsMeats = jsonData[0].MeatIDs.split(",");
		}

		var arToppings = arToppingsMeats.concat( arToppingsOthers );

		oCon.thePizza.StatusID = jsonData[0].StatusID;

		var binding = app.getCurrentView().orderStatus.getBinding("items");
		var filters = [];
		for(var i = 1; i < oCon.thePizza.StatusID; i++) {
	    	var statusFilter = new sap.ui.model.Filter("StatusID", sap.ui.model.FilterOperator.NE, i);
	        filters.push(statusFilter);
		}
		if(filters.length != 0){
			var finalFilter = new sap.ui.model.Filter({filters: filters, and: true})
		    binding.filter( finalFilter );			
		}

		oCon.thePizza.arToppings = arToppings;
		oCon.thePizza.Sauce = 	jsonData[0].SauceTypeID;
		oCon.thePizza.Cheese = 	jsonData[0].CheeseID;
		oCon.thePizza.Size 	= 	( isEmpty( jsonData[0].Size ) ? "L" : jsonData[0].Size );
		oCon.thePizza.Name 	=  	jsonData[0].PizzaName;
		oCon.thePizza.Crust = 	jsonData[0].CrustTypeID;

		oCon.display_Pizza_Fields( oCon.getView() );

	},

	load_order_pizza : function (obView, obParameters){

		app.setCurrentViewBusy(true);

		var callBack_Success = this.load_special_pizza_SUCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		wsParameter.setCustomData({ OrderID : obParameters.OrderID } );
		var wsService = new WS( ORDERS_WS.Get_Orders , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

	},

	load_order_pizza_SUCCESS : function ( jsonData ){

		console_log(">> load_order_pizza_SUCCESS", jsonData );
		app.setCurrentViewBusy(false);

		//TODO: Display all the information about the Standard Pizza
		// oCon.thePizza.Name = ( isEmpty( jsonData[0].StandardPizzaName ) ? "Custom" : jsonData[0].StandardPizzaName );

	},

    
	buttonTap_ModifyOrder : function(){
		Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_STAFF_ORDER_HISTORY });
	},

	buttonTap_CreateOrder : function(  ){

		var obView = app.getCurrentView();

		if ( myCurrentData.isLoggedIn === false ) {

			DISPLAY_MESSAGE_WARNING( "Sorry", "You need to be logged in to Order." );

		} else {
			if ( obView.orderStatus.getSelectedKey() == this.thePizza.StatusID ) {

				DISPLAY_MESSAGE_WARNING( "Sorry", "You need to change the status to update the order." );

			} else {
				obView.getController().initialize_Update_Order();
			}
		}

	},

	initialize_Update_Order : function(){

		var oCon = app.getCurrentController();

		var obOrderData = {
			"UserData" :{
				CustomerID : oCon.selectedCustomerID,
			},
			"StatusID" : this.oView.orderStatus.getSelectedKey() || 1,
			"OrderID" : this.selectedOrderId,
			"PizzaData": oCon.thePizza,
			"DeliveryData" :{

			}
		};

		UTILS_ORDER.Update_Order( obOrderData );
		Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_STAFF_ORDER_HISTORY });
		app.setCurrentViewBusy(false);
	}
	
});