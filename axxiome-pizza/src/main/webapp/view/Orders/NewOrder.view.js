sap.ui.jsview("view.Orders.NewOrder", {
              
    getControllerName : function() {
        return "view.Orders.NewOrder";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
    
        this.create_fragments(oCon);
        
        this.create_components(oCon);
        
        //==============================================
        //          PAGE
        //==============================================
        var obParameters = {
            stViewName : "NEW_ORDER_Page",
            boShowFooter : true
        }
        VIEWS_Create_Regular_Page( this, obParameters ); 
        
        return this.thePage;
        
    },
    
    
    create_fragments : function(oCon){
        
    },
    
    
    create_components : function(oCon){

        var obView = this;
/*
 *      TODO: Add Controls here, placed in "div"s (HBox or VBox)
 */     

        this.lbl_OrderCustom = new sap.m.Label({
            text: "Creating a new Order"
        }).addStyleClass("Text_Big");
        this.div_OrderCustom = new sap.m.VBox({
            items:[
                this.lbl_OrderCustom
            ]
        }).addStyleClass("Content_Fields_DIV margin-bottom-1 ");

        this.lbl_Order = new sap.m.Label({
            text: "Your Order"
        }).addStyleClass("");
        
        this.lbl_Pizza = new sap.m.Label ({
            text: "Pizza:"
        }).addStyleClass("");
        this.txt_Pizza = new sap.m.Text ({
            text: "TODO-Bind with Pizza selection (standard name or 'Custom Pizza')"
        }).addStyleClass("");
        
        /* Choice combo*/
        this.txt_Pizzas = new sap.m.Text ({
            text: "Choose from following selections:"
        }).addStyleClass("");
        //Combos
        this.combo_Pizzas1 =    new sap.m.RadioButton({groupName:"pizzaChoose",
                                        text:"Hawaiian"
                                });
        this.combo_Pizzas2 =    new sap.m.RadioButton({groupName:"pizzaChoose",
                                      text:"ATB Special",
                                      select:function(evt){
                                      }
                                });
        this.combo_Pizzas3 =    new sap.m.RadioButton({groupName:"pizzaChoose",
                                      text:"Meat Lovers",
                                      select:function(evt){
                                      }
                                });
        this.combo_Pizzas4 =    new sap.m.RadioButton({groupName:"pizzaChoose",
                                      text:"Classic Pepperoni",
                                      select:function(evt){
                                      }
                                });
        this.combo_Pizzas5 =    new sap.m.RadioButton({groupName:"pizzaChoose",
                                    text:"Build Your Own",
                                        select:function (evt){
                                            if ( evt.mParameters.selected == true ){
                                                Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_NEW_ORDER_CUSTOM }); 
                                                obView.combo_Pizzas5.setSelected(false);
                                            }
                                      }
                                });
        //DIV for combos
        this.div_PizzasTxt = new sap.m.FlexBox ({
            direction: "Column",
            alignItems: "Start",
            displayInline: true,
            items:[
                   this.txt_Pizzas,
            ]
        });

        // this.slt_Pizza = new sap.m.Select ( 
        //     this.createId("slt_Pizza"),
        //     {
        //         change : function(evt) {
        //             // oCon.onChooseAccountComboBox(evt);
        //         },
        //         items: { 
        //             path: "/" + "StandarPizzas",
        //             template: new sap.ui.core.ListItem({
        //                 key : "{StandardPizzaID}",
        //                 text: "{PizzaName}"
        //             }) 
        //         },
        //     }
        // ).addStyleClass("");

        this.slt_Pizza = new sap.m.FlexBox({
            direction: "Row",
            alignItems: "Center",
        });
        this.slt_Pizza2 = new sap.m.FlexBox({
            direction: "Row",
            alignItems: "Center",
        });

        // this.div_Pizzas1 = new sap.m.FlexBox ({
        //     direction: "Column",
        //     alignItems: "Start",
        //     displayInline: true,
        //     items:[
        //         //this.txt_Pizzas,
        //         this.combo_Pizzas1,
        //         new sap.m.Image ({src: "./img/pizzas/Hawaiian.jpg",width:"200pt",height:"150pt"})
        //     ]
        // });
        // this.div_Pizzas2 = new sap.m.FlexBox ({
        //     direction: "Column",
        //     alignItems: "Start",
        //     displayInline: true,
        //     items:[
                
        //         this.combo_Pizzas2,
        //         new sap.m.Image ({src: "./img/pizzas/atbSpecial.jpg",width:"200pt",height:"150pt"})
        //     ]
        // });
        // this.div_Pizzas3 = new sap.m.FlexBox ({
        //     direction: "Column",
        //     alignItems: "Start",
        //     displayInline: true,
        //     items:[
                
        //         this.combo_Pizzas3,
        //         new sap.m.Image ({src: "./img/pizzas/meatLover.jpeg",width:"200pt",height:"150pt"})
        //     ]
        // });
        // this.div_Pizzas4 = new sap.m.FlexBox ({
        //     direction: "Column",
        //     alignItems: "Start",
        //     displayInline: true,
        //     items:[
                
        //         this.combo_Pizzas4,
        //         new sap.m.Image ({src: "./img/pizzas/pepperonix.jpg",width:"200pt",height:"150pt"})
        //     ]
        // });
        // this.div_Pizzas5 = new sap.m.FlexBox ({
        //     direction: "Column",
        //     alignItems: "Start",
        //     displayInline: true,
        //     items:[
                
        //         this.combo_Pizzas5,
        //         new sap.m.Image ({src: "./img/pizzas/own.png",width:"200pt",height:"150pt"})
        //     ]
        // });

        this.div_Pizza = new sap.m.HBox ({
            alignItems: "Center",
            items:[
                this.slt_Pizza,
            ]
        }).addStyleClass("displayInline");
        this.div_Pizza2 = new sap.m.HBox ({
            alignItems: "Center",
            items:[
                this.slt_Pizza2
            ]
        }).addStyleClass("displayInline");
        
        this.lbl_Register = new sap.m.Label({
            text: "Choose from following selections:"
        }).addStyleClass("Text_Big centered color_navy");
        this.div_Register =  new sap.m.HBox ({
            items:[
                this.lbl_Register
            ]
        }).addStyleClass("Content_Fields_DIV margin-bottom-2 ");


        /*  ===================================================================
         *                          BUTTONS SECTION 
         *  */

        this.btn_Return = new sap.m.Button({
            text: "Main Screen",
            tap: [ Navigation_GoHome, oCon ]
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Back");
        this.btn_OrderNow = new sap.m.Button({
            text: "Order Now!",
            tap:[ oCon.buttonTap_OrderNow, oCon ],
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Accept ");

        this.div_btn_OrderNow = VIEWS_Create_Buttons_Container([ this.btn_Return, this.btn_OrderNow ]);
        /*
         *                          BUTTONS SECTION 
         *  =================================================================== */

        
        VIEWS_Add_Main_Content( oCon.getView(), [
/*
 *      TODO: list "div"s here  
 */         this.div_OrderCustom,
            this.div_Register,
            this.div_Pizza,
            this.div_Pizza2,
            this.div_btn_OrderNow
        ]);
        
    }
   
});