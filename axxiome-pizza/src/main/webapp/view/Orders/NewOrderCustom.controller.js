// jQuery.sap.require("sap.banking.ui.services.App.Settings");

sap.ui.controller("view.Orders.NewOrderCustom", {
	
	CustomPizza : {},
	
	onBeforeShow : function(evt){

		var obView = this.getView();

		this.load_data( obView );

		this.clear_all_value_state( obView );
		
    },
	
	clear_all_value_state:function( obView ){
		obView.pizzaSize.setValueState( sap.ui.core.ValueState.None );
		obView.crustStyle.setValueState( sap.ui.core.ValueState.None );
		obView.baseSauce.setValueState( sap.ui.core.ValueState.None );
		obView.cheese.setValueState( sap.ui.core.ValueState.None );
	},
	
    
    load_data: function (obView) {

    	try{

    		app.setCurrentViewBusy(true);

			this.get_PizzaCrusts();
			this.get_PizzaSauces();
			this.get_PizzaToppings();

		} catch(e){

		} finally{
			app.setCurrentViewBusy(false);
		}
		
	},


	get_PizzaCrusts: function(  ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */

		app.setCurrentViewBusy(true);

		var callBack_Success = this.get_PizzaCrusts_SUCCESS;

		if ( typeof sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_CRUSTS  ) !== "undefined" ){

			var jsonData = sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_CRUSTS  ).getData();
			callBack_Success( jsonData );

		} else {

			app.setCurrentViewBusy(true);

			var callBack_Success = this.get_PizzaCrusts_SUCCESS;

			var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
			wsParameter.setCustomData({
				// id: 1
			});
			var wsService = new WS( PIZZAS_WS.Get_Crusts , wsParameter );

			var wsWrapper = new WSPackageWrapper().setOperation(wsService);
			wsWrapper.startWS();

		}

    },

    get_PizzaCrusts_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaCrusts_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		obView.crustStyle.setModel( oModel );
		// obView.slt_Crust.refreshModel();

		app.setCurrentViewBusy( false );

	},

	get_PizzaSauces: function(  ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_PizzaSauces_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		// var wsService = new WS( PIZZAS_MOCK.Get_Sauces , wsParameter );
		var wsService = new WS( PIZZAS_WS.Get_Sauces , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaSauces_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaSauces_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

        var obView = app.getCurrentView();
        obView.baseSauce.setModel( oModel );

		obView.slt_Sauce.setModel( oModel );

	},


	get_PizzaToppings: function(  ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_PizzaToppings_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		// var wsService = new WS( PIZZAS_MOCK.Get_Toppings , wsParameter );
		var wsService = new WS( PIZZAS_WS.Get_Toppings , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaToppings_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaToppings_SUCCESS ", jsonData ]);
		var oModelCheese = new sap.ui.model.json.JSONModel();
        
		jsonDataCheese = [];

		var obView = app.getCurrentView();

		var myDivToppings = obView.div_Toppings;
		var myDivMeats = obView.div_Meats;
		// myDivToppings.destroyItems();
		// myDivMeats.destroyItems();



		$.each(jsonData, function(i, item){

			//CHEESE
			if ( item.ToppingCategoryID === 1 ){

				jsonDataCheese.push( item );

			} else if ( item.ToppingCategoryID === 2 ){

				//MEATS
				var checkBox = new sap.m.CheckBox(
					obView.createId("checkBox_Topping_X" + item.ToppingTypeID ),
					{
						text: item.ToppingName,
						select: function() {
			                if(!obView.CheckBoxValidationLeft()){
			                    this.setSelected(false);
			                }
			            },
					}
				);
				myDivMeats.addItem( checkBox );

			} else if ( item.ToppingCategoryID === 3 ){


				var checkBox = new sap.m.CheckBox(
					obView.createId("checkBox_Topping_X" + item.ToppingTypeID ),
					{
						text: item.ToppingName,
					}
				);
				myDivToppings.addItem( checkBox );

			}

		});

		oModelCheese.setData(jsonDataCheese);
		obView.cheese.setModel( oModelCheese );

	},


	
	buttonTap_OrderNow : function() {

		console_log("ORDER NOW TAP");

		var obView = app.getCurrentView();

        app.setCurrentViewBusy( true );

        obView.getController().clear_all_value_state( obView );

        var obError = { error: false, title : [], message: [], component: [] };

        try {
	        obView.getController().perform_validations( obView, obError );

	        if ( obError.error === true ){

				AXX_EXCEPTION_USER(
					"Oops.."
					, obError.message
					, true
					, "error"
					, obError.component
				);
				
	        } else {
	        
	        	obView.getController().create_Data_Confirmation();
	        	
	        }
	    } catch(e){

	    } finally{
			app.setCurrentViewBusy( false );
	    }

	},

	perform_validations : function( obView, obError ){

		if ( obView.pizzaSize.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The Pizza Size was not provided." );
			obError.component.push( obView.pizzaSize );

		} 
		if ( obView.crustStyle.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The Pizza Crust was not provided." );
			obError.component.push( obView.crustStyle );

		} 
		if ( obView.baseSauce.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The Pizza Sauce was not provided." );
			obError.component.push( obView.baseSauce );

		} 
		if ( obView.cheese.getValue() === "" ) {

			obError.error = true;
			obError.message.push( "The Pizza Cheese was not provided." );
			obError.component.push( obView.cheese );

		} 

		return obError;

	},

	create_Data_Confirmation: function(){

		var obView = app.getCurrentView();

		var arMeatsToppings = obView.getController().get_Toppings( "meats" ).concat( obView.getController().get_Toppings( "general" ) );

		var myCustomPizza = {
			Name: "Custom",
			Size: obView.pizzaSize.getSelectedKey(),
			Crust: obView.crustStyle.getSelectedKey(),
			Cheese: obView.cheese.getSelectedKey(),
			Sauce: obView.baseSauce.getSelectedKey(),
			// arMeats: obView.getController().get_Toppings( "meats" ),
			arToppings : arMeatsToppings
		};

		Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_ORDER_SUMMARY, CustomPizza : myCustomPizza });

	},

	get_Toppings:function( stType ){

		var obView = app.getCurrentView();

		if ( stType.toLowerCase() == "meats" ){
			var myDiv = obView.div_Meats;
		} else {
			var myDiv = obView.div_Toppings;
		}

		var arToppings = [];

		var myDivItems = myDiv.getItems();
		$.each( myDivItems, function(i, item){
			// console_log(item);
			if ( item.mProperties.selected == true ){
				var myId = item.sId;
				myId = parseInt( myId.split("X")[1] );
				arToppings.push( myId );
			}
			
		});

		return arToppings;

	},

	validateMeatsSelection : function() {
		console_log("ORDER NOW TAP");
        //Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_ORDER_SUMMARY });
	}	
	
});