sap.ui.jsview("view.Orders.OrderHistory", {
              
    getControllerName : function() {
    	return "view.Orders.OrderHistory";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
    
		this.create_fragments(oCon);
		
		this.create_components(oCon);
		
		//==============================================
        //			PAGE
        //==============================================
		var obParameters = {
    		stViewName : "ORDER_HISTORY_Page",
    	}
		VIEWS_Create_Regular_Page( this, obParameters ); 
		
    	return this.thePage;
    	
    },
    
    
    create_fragments : function(oCon){
    	
    },
    
    create_components : function(oCon){

        this.lbl_OrderCustom = new sap.m.Label({
            text: "Choose one of your previous Orders !"
        }).addStyleClass("Text_Big");
        this.div_OrderCustom = new sap.m.VBox({
            items:[
                this.lbl_OrderCustom
            ]
        }).addStyleClass("Content_Fields_DIV margin-bottom-1 ");


        this.oTable = new sap.m.Table("table",{
            mode: sap.m.ListMode.SingleSelect,
            headerText: "Order History",
            growing: true,
            growingThreshold: 5,
            columns : [ new sap.m.Column({width: "10%", header :[ new sap.m.Label({text : "Order N°"}) ]}),
                        new sap.m.Column({width: "15%", header :[ new sap.m.Label({text : "Name"}) ]}),
                        new sap.m.Column({width: "10%", header :[ new sap.m.Label({text : "Size"}) ]}),
                        new sap.m.Column({width: "30%", header :[ new sap.m.Label({text : "Pizza Details"}) ]}),
                        new sap.m.Column({width: "15%", header :[ new sap.m.Label({text : "Status"}) ]}),
                        new sap.m.Column({width: "20%", header :[ new sap.m.Label({text : "Promotion"}) ]})
                      ]
            });

        this.oTable.bindItems("/", new sap.m.ColumnListItem("listItem",{
            cells : [ new sap.m.Text({text : "{OrderID}"}),
                      new sap.m.Text({text : "{PizzaName}"}),
                      new sap.m.Text({text : "{Size}"}),
                      new sap.m.Text({text : "{Details}"}),
                      new sap.m.Text({text : "{StatusName}"}),
                      new sap.m.Text({text : "{OfferDesc}"})
                    ]
        }));

    	/*	===================================================================
		 *  						BUTTONS SECTION 
		 *  */
		this.btnNew = new sap.m.Button({
            text: "No, I want to create a new order.",
            tap:[ oCon.buttonTap_Create, oCon ],
        }).addStyleClass("AxxiomeRegularButton");
        this.btnConfirm = new sap.m.Button({
            text: "Order Now!",
            tap:[ oCon.buttonTap_OrderNow, oCon ],
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Accept");

		this.div_btn_Confirm = VIEWS_Create_Buttons_Container([ this.btnConfirm ]);

        this.div_btn_New = VIEWS_Create_Buttons_Container([ this.btnNew ]);

        this.div_Table = new sap.m.FlexBox ({
            direction: "Column",
            alignItems: "Start",
            displayInline: true,
            width: "90%",
            items:[
                this.oTable,
           ]
        });
		/*
		 *  						BUTTONS SECTION 
		 * 	=================================================================== */
    	
    	VIEWS_Add_Main_Content( this, [
            this.div_OrderCustom,
            this.div_Table,
    		this.div_btn_Confirm,
            this.div_btn_New
    	]);
    	    	
    }
   
});