sap.ui.jsview("view.Orders.StaffCompletedOrderHistory", {
              
    getControllerName : function() {
    	return "view.Orders.StaffCompletedOrderHistory";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
    
		this.create_fragments(oCon);
		
		this.create_components(oCon);
		
		//==============================================
        //			PAGE
        //==============================================
		var obParameters = {
    		stViewName : "STAFF_COMPLETED_ORDER_HISTORY_Page",
    	}
		VIEWS_Create_Regular_Page( this, obParameters ); 
		
    	return this.thePage;
    	
    },
    
    
    create_fragments : function(oCon){
    	
    },
    
    create_components : function(oCon){

        this.lbl_OrderCustom = new sap.m.Label({
            text: "Completed Orders History!"
        }).addStyleClass("Text_Big");
        this.div_OrderCustom = new sap.m.VBox({
            items:[
                this.lbl_OrderCustom
            ]
        }).addStyleClass("Content_Fields_DIV margin-bottom-1 ");


        this.oTable = new sap.m.Table("Completedtable",{
            headerText: "Completed Orders",
            growing: true,
            growingThreshold: 5,
            columns : [ new sap.m.Column({header :[ new sap.m.Label({text : "Order Number"}) ]}),
                        new sap.m.Column({header :[ new sap.m.Label({text : "Order Date"}) ]}),
                        new sap.m.Column({header :[ new sap.m.Label({text : "Order Details"}) ]}),
                        new sap.m.Column({header :[ new sap.m.Label({text : "Status"}) ]})
                      ]
            });
        
        this.oTable.bindItems("/", new sap.m.ColumnListItem("completedListItem",{
                                cells : [ new sap.m.Text({text : "{OrderID}"}),
                                          new sap.m.Text({text : "{OrderTstmp}"}),
                                          new sap.m.Text({text : "Name : {PizzaName}, Crust : {CrustName}, Sauce : {SauceName}, Cheese : {Cheese}, Meat : {Meats}, Toppings : {Others}"}),
                                          new sap.m.Text({text : "{StatusName}"})
                      ]
        }));

    	/*	===================================================================
		 *  						BUTTONS SECTION 
		 *  */
        
		this.btnNew = new sap.m.Button({
            text: "Go back to Open Orders",
            tap:[ oCon.buttonTap_Create, oCon ],
        }).addStyleClass("AxxiomeRegularButton");

        this.div_btn_New = VIEWS_Create_Buttons_Container([ this.btnNew ]);

        this.div_Table = new sap.m.FlexBox ({
            direction: "Column",
            alignItems: "Start",
            displayInline: true,
            width: "90%",
            items:[
                this.oTable,
           ]
        });
		/*
		 *  						BUTTONS SECTION 
		 * 	=================================================================== */
    	
    	VIEWS_Add_Main_Content( this, [
            this.div_OrderCustom,
            this.div_Table,
            this.div_btn_New
    	]);
    	    	
    }
   
});