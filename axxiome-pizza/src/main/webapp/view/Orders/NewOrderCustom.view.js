sap.ui.jsview("view.Orders.NewOrderCustom", {
              
    getControllerName : function() {
    	return "view.Orders.NewOrderCustom";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
    
		this.create_fragments(oCon);
		
		this.create_components(oCon);
		
		//==============================================
        //			PAGE
        //==============================================
		var obParameters = {
    		stViewName : "NEW_ORDER_Page2",
    		boShowFooter : true
    	}
		VIEWS_Create_Regular_Page( this, obParameters ); 
		
    	return this.thePage;
    	
    },
    
    
    create_fragments : function(oCon){
    	
    },
    
    
    create_components : function(oCon){
/*
 *   	TODO: Add Controls here, placed in "div"s (HBox or VBox)
 */   	

        this.lbl_OrderCustom = new sap.m.Label({
            text: "Create your own Pizza !"
        }).addStyleClass("Text_Big");
        this.div_OrderCustom = new sap.m.VBox({
            items:[
                this.lbl_OrderCustom
            ]
        }).addStyleClass("Content_Fields_DIV margin-bottom-1 ");

        obView = this;
    	this.lbl_Order = new sap.m.Label({
    		text: "Your Order"
    	}).addStyleClass("");
    	
    	this.lbl_Pizza = new sap.m.Label ({
    		text: "Pizza:"
    	}).addStyleClass("");

    	this.txt_Pizza = new sap.m.Text ({
    		text: "TODO-Bind with Pizza selection (standard name or 'Custom Pizza')"
    	}).addStyleClass("");
    	
    	/* Choice combo*/
    	this.txt_Size = new sap.m.Text ({
    		text: "Pizza Size (select one):"
    	}).addStyleClass("");

        /* Choice combo*/
        this.txt_Crust = new sap.m.Text ({
            text: "Crust Style (select one):"
        }).addStyleClass("");

        /* Choice combo*/
        this.txt_Sauce = new sap.m.Text ({
            text: "Base Sauce (select one):"
        }).addStyleClass("");

        /* Choice combo*/
        this.txt_Cheese = new sap.m.Text ({
            text: "Cheese (select one):"
        }).addStyleClass("");

        /* Choice combo*/
        this.txt_Meats = new sap.m.Text ({
            text: "Meats (select as many as three):"
        }).addStyleClass("");

        /* Choice combo*/
        this.txt_Toppings = new sap.m.Text ({
            text: "Other Toppings (select as many as desired):"
        }).addStyleClass("");

    	this.pizzaSize = // Create a Size
    		new sap.m.ComboBox("cbSize",{
		        tooltip: "Pizza Size",
				items: [
                    new sap.ui.core.ListItem("sizeS",{text: "Small", key: "S"}),
				    new sap.ui.core.ListItem("sizeM",{text: "Medium", key: "M"}),
				    new sap.ui.core.ListItem("sizeL",{text: "Large", key: "L"})
				]
			});

        this.crustStyle = // Create a Crust
            new sap.m.ComboBox("cbCrust",{
                tooltip: "Crust Style",
                items: { 
                    path: "/" , // + "CrustTypes",
                    template: new sap.ui.core.ListItem({
                        key : "{CrustTypeID}",
                        text: "{CrustName}"
                    }) 
                }
            });

        this.baseSauce = // Create a Base Sauce
            new sap.m.ComboBox("cbSauce",{
                tooltip: "Base Sauce",
                items: { 
                    path: "/" , //+ "SauceTypes",
                    template: new sap.ui.core.ListItem({
                        key : "{SauceTypeID}",
                        text: "{SauceName}"
                    }) 
                }
            });

        this.cheese = // Create a Cheese
            new sap.m.ComboBox("cbCheese",{
                tooltip: "Cheese",
                items: { 
                    path: "/" , //+ "SauceTypes",
                    template: new sap.ui.core.ListItem({
                        key : "{ToppingTypeID}",
                        text: "{ToppingName}"
                    }) 
                }
            });

//------Meats
        this.meatsBacon = new sap.m.CheckBox({
            id:"bacon",
            visible:true,
            enabled: true,
            text: "Bacon",
            select: function (value) {
                if(!obView.CheckBoxValidationLeft()){
                    obView.meatsBacon.setSelected(false);
                }
            }
        }),

        this.meatsChicken = new sap.m.CheckBox({
            id:"chicken",
            visible:true,
            enabled: true,
            text: "Chicken",
            select: function () {
                if(!obView.CheckBoxValidationLeft()){
                    obView.meatsChicken.setSelected(false);
                }
            }
        }),

        this.meatsHam = new sap.m.CheckBox({
            id:"ham",
            visible:true,
            enabled: true,
            text: "Ham",
            select: function () {
                if(!obView.CheckBoxValidationLeft()){
                    obView.meatsHam.setSelected(false);
                }
            }
        }),

        this.meatsItalianSausage = new sap.m.CheckBox({
            id:"italiansausage",
            visible:true,
            enabled: true,
            text: "Italian Sausage",
            select: function () {
                if(!obView.CheckBoxValidationLeft()){
                    obView.meatsItalianSausage.setSelected(false);
                }
            }
        }),

        this.meatsPepperoni = new sap.m.CheckBox({
            id:"pepperoni",
            visible:true,
            enabled: true,
            text: "Pepperoni",
            select: function () {
                if(!obView.CheckBoxValidationLeft()){
                    obView.meatsPepperoni.setSelected(false);
                }
            }
        }),

//------Toppings
        this.toppingsBananaPeppers = new sap.m.CheckBox({
            id:"bananapeppers",
            visible:true,
            enabled: true,
            text: "Banana Peppers",
            select: function () {
            }
        }),

        this.toppingsBasil = new sap.m.CheckBox({
            id:"basil",
            visible:true,
            enabled: true,
            text: "Basil",
            select: function () {
            }
        }),

        this.toppingsBlackOlives = new sap.m.CheckBox({
            id:"blackolives",
            visible:true,
            enabled: true,
            text: "Black Olives",
            select: function () {
            }
        }),

        this.toppingsCaramelizedOnions = new sap.m.CheckBox({
            id:"caramelizedonions",
            visible:true,
            enabled: true,
            text: "Caramelized Onions",
            select: function () {
            }
        }),

        this.toppingsFreshTomato = new sap.m.CheckBox({
            id:"freshtomato",
            visible:true,
            enabled: true,
            text: "Fresh Tomato",
            select: function () {
            }
        }),

        

        this.toppingsFriedEgg = new sap.m.CheckBox({
            id:"FriedEgg",
            visible:true,
            enabled: true,
            text: "Mushrooms",
            select: function () {
            }
        }),


        this.toppingsMushrooms = new sap.m.CheckBox({
            id:"mushrooms",
            visible:true,
            enabled: true,
            text: "Mushrooms",
            select: function () {
            }
        }),
        
        this.toppingsPineapple = new sap.m.CheckBox({
            id:"pineapple",
            visible:true,
            enabled: true,
            text: "Pineapple",
            select: function () {
            }
        }),

        this.toppingsRoastedRedPepper = new sap.m.CheckBox({
            id:"roastedredpepper",
            visible:true,
            enabled: true,
            text: "Roasted Red Pepper",
            select: function () {
            }
        }),

//------DIVs
    	this.div_Details = new sap.m.FlexBox ({
            direction: "Column",
            alignItems: "Start",
            displayInline: true,
            width: "40%",
    		items:[
                this.txt_Size,
    			this.pizzaSize,
                this.txt_Crust.addStyleClass("margin-top-1"),
                this.crustStyle
	       ]
    	}).addStyleClass("margin-top-1");

        this.div_Details2 = new sap.m.FlexBox ({
            direction: "Column",
            alignItems: "Start",
            displayInline: true,
            width: "40%",
            items:[
                this.txt_Sauce,
                this.baseSauce,
                this.txt_Cheese.addStyleClass("margin-top-1"),
                this.cheese
           ]
        }).addStyleClass("margin-top-1");

        this.div_Meats = new sap.m.FlexBox ({
            direction: "Column",
            alignItems: "Start",
            displayInline: true,
            width: "40%",
            items:[
                this.txt_Meats,
                // this.meatsHam,
                // this.meatsBacon,
                // this.meatsChicken,
                // this.meatsItalianSausage,
                // this.meatsPepperoni       
            ]
        }).addStyleClass("margin-top-1");

        this.div_Toppings = new sap.m.FlexBox ({
            direction: "Column",
            alignItems: "Start",
            displayInline: true,
            width: "40%",
            items:[
                this.txt_Toppings,
                // this.toppingsBananaPeppers,
                // this.toppingsBasil,
                // this.toppingsBlackOlives,
                // this.toppingsCaramelizedOnions,
                // this.toppingsFreshTomato,
                // this.toppingsFriedEgg,
                // this.toppingsMushrooms,
                // this.toppingsPineapple,
                // this.toppingsRoastedRedPepper                
            ]
        }).addStyleClass("margin-top-1");
    	
    	this.div_Pizza = new sap.m.HBox ({
    		items:[
				this.lbl_Pizza,
				this.txt_Pizza
    		]
    	});
    	

        /*  ===================================================================
         *                          BUTTONS SECTION 
         *  */

        this.btn_Return = new sap.m.Button({
            text: "Modify Order",
            tap: [ app.onBackKeyPress, oCon ]
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Back");

        this.btn_OrderNow = new sap.m.Button({
            text: "Order Now!",
            tap:[ oCon.buttonTap_OrderNow ],
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Accept");

        this.div_btn_OrderNow = VIEWS_Create_Buttons_Container([ this.btn_Return, this.btn_OrderNow ]).addStyleClass("");

        /*
         *                          BUTTONS SECTION 
         *  =================================================================== */

    	
    	VIEWS_Add_Main_Content( oCon.getView(), [
/*
 *		TODO: list "div"s here  
 */    		this.div_OrderCustom,
            this.div_Details,
            this.div_Details2,
            this.div_Meats,
			this.div_Toppings,
			this.div_btn_OrderNow
    	]);
    	
    },

    CheckBoxValidationLeft : function( thisItem ){

        var arItemsSelected = obView.getController().get_Toppings( "meats" );

        if (arItemsSelected.length > 3){
            
            DISPLAY_MESSAGE_ERROR(
                "Selected Items",
                "You are not allowed to choose more than 3!"
            );

            return false;

        }else{
            return true;
        }
       
    },


       
   
});