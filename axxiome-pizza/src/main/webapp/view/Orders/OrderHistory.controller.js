sap.ui.controller("view.Orders.OrderHistory", {

	// onInit: function(){
	// 	this.get_Offers();
	// },	
	
	onBeforeShow : function(evt){

		var obView = this.getView();

		this.load_data( obView );
		this.clear_all_value_state( obView );

    },

 //    get_Offers: function(evt){
 //    	app.setCurrentViewBusy(true);

	// 	var callBack_Success = this.get_Offers_SUCCESS;
	// 	var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
	// 	wsParameter.setCustomData( { CustomerID : myCurrentData.INFO.get_User_ID() } );
	// 	var wsService = new WS( ORDERS_WS.Get_Offers , wsParameter );
	// 	var wsWrapper = new WSPackageWrapper().setOperation(wsService);
	// 	wsWrapper.startWS();
	// },

	// get_Offers_SUCCESS: function(jsonData) {
	// 	console_log([ ">> get_Offers_SUCCESS ", jsonData ]);
	// 	var oModel = new sap.ui.model.json.JSONModel();
 //        oModel.setData(jsonData);

	// 	var obView = app.getCurrentView();
	// 	obView.offersModel = oModel;
	// 	// obView.oTable.setModel(oModel);	
	// 	app.setCurrentViewBusy( false );
	// },
	
	/* ======= FRAGMENTS ======= */
	load_data : function( obView ){
		
		/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_Orders_SUCCESS;

		var wsParameter = new WSCaller();
		wsParameter.setCustomData( { CustomerID : myCurrentData.INFO.get_User_ID() } );
		wsParameter.setSuccessCallback(callBack_Success);
		var wsService = new WS( ORDERS_WS.Get_Orders , wsParameter );
		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

	},

	get_Orders_SUCCESS: function(jsonData) {

		console_log([ ">> get_Orders_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();

		if (jsonData.length > 0) {
			var obView = app.getCurrentView();

			// format the output of the columns
			jsonData.forEach(function(a){
				a.Details = "Crust: "+a.CrustName+
						(a.SauceName ? "\nSauce: "+a.SauceName : "")+
						(a.Cheese ? "\nCheese: "+a.Cheese : "")+
						(a.Meats ? "\nMeat: "+a.Meats : "")+
						(a.Others ? "\nToppings: "+a.Others : "");
				
				// check for redeemOrderId field, if it is not null, promotion(OfferDesc) should say "Redeemed"
				if(a.RedeemOrderID){
					a.OfferDesc = "Redeemed";
				} else if(a.OfferID){
					a.OfferDesc = "Order this again and receive 20% OFF - Coupon # " + a.OfferID;
				}
			});

	        oModel.setData(jsonData);

			obView.oTable.setModel(oModel);	
		}else{

			var obParameters = {
				fn_callBack_YES : function(){
					Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_NEW_ORDER });
				}
			};
			DISPLAY_MESSAGE_ERROR("Data Error", "The user doesn't have available information.", obParameters);
			
		}

	},
	
	clear_all_value_state:function( obView ){

	},

	buttonTap_Create : function(){
		Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_NEW_ORDER });
	},

	buttonTap_OrderNow : function(algo) {
		console_log("ORDER NOW TAP");

		var obView = app.getCurrentView();
		var pizzaId = 0;
		var order;

		if (obView.oTable.getSelectedItem() != null) {
			
			pizzaId = obView.oTable.getSelectedItem().getBindingContext().getObject().PizzaID;
			order = obView.oTable.getSelectedItem().getBindingContext().getObject();

			var boStandard = true;
			StandardPizzaID = obView.oTable.getSelectedItem().getBindingContext().getObject().StandardPizzaID;
			if ( isEmpty( StandardPizzaID ) ){
				boStandard = false;
			} else {
				pizzaId = StandardPizzaID;
			}
			
			// stPizzaName = obView.oTable.getSelectedItem().getBindingContext().getObject().PizzaName;

			Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_ORDER_SUMMARY, pizzaId: pizzaId, specialPizza: boStandard, order: order});

		}else{
			DISPLAY_MESSAGE_ERROR("Selection Error", "You must to choose one item.");
		}
       
	}
	
	
});