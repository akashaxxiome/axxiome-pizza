sap.ui.jsview("view.Orders.StaffOrderSummary", {
              
    getControllerName : function() {
    	return "view.Orders.StaffOrderSummary";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
		this.create_fragments(oCon);
		this.create_components(oCon);
		var obParameters = {
    		stViewName : "STAFF_ORDER_SUMMARY_Page",
    		boShowFooter : true
    	}
		VIEWS_Create_Regular_Page( this, obParameters ); 
		
    	return this.thePage;
    	
    },

    create_fragments : function(oCon){

    },
    
    
    create_components : function(oCon){
        
        /*  ===================================================================
         *                          ORDER STATUS SECTION 
         *  */

        this.lbl_Status = new sap.m.Label({
            text: "Change Order Status"
        }).addStyleClass("Text_Big");
        this.div_Status = new sap.m.VBox({
            items:[
                this.lbl_Status
            ]
        }).addStyleClass("Content_Fields_DIV margin-bottom-1 ");
        
        this.lbl_StatusCombo = new sap.m.Label ({
            text: "Status:"
        }).addStyleClass("");

        this.orderStatus =
            new sap.m.ComboBox({
                tooltip: "Order Status",
                items: { 
                    path: "/" ,
                    template: new sap.ui.core.ListItem({
                        key : "{StatusID}",
                        text: "{StatusName}"
                    }) 
                },
                change: function(oEvent){
                    if(app.getCurrentController().thePizza.StatusID > oEvent.oSource.mProperties.selectedKey){
                        DISPLAY_MESSAGE_ERROR("Wrong Status","This is not a valid status. Please select a different Status");
                        this.setSelectedKey(app.getCurrentController().thePizza.StatusID);
                    }
                }
            }).addStyleClass("orderStatusSummary");

        this.div_StatusCombo = new sap.m.HBox ({
            items:[
                this.lbl_StatusCombo,
                this.orderStatus
            ]
        }).addStyleClass("Content_Fields_DIV");
        
        this.div_StatusDetail = new sap.m.VBox ({
            items:[
                   this.div_StatusCombo
            ]
        }).addStyleClass("ZEBRA_CONTAINER_DIV centered");

        
        /*  ===================================================================
         *                          ORDER DETAILS SECTION 
         *  */


    	this.lbl_Order = new sap.m.Label({
    		text: "Selected Order Details"
    	}).addStyleClass("Text_Big");
        this.div_Order = new sap.m.VBox({
            items:[
                this.lbl_Order
            ]
        }).addStyleClass("Content_Fields_DIV margin-bottom-1 ");
    	
    	this.lbl_Pizza = new sap.m.Label ({
            text: "Pizza:"
        }).addStyleClass("");
        this.txt_Pizza = new sap.m.Label ({
            text: "TODO-Bind with Pizza selection (standard name or 'Custom Pizza')"
        }).addStyleClass("Text_B wordwrap color_highlight");
        this.div_Pizza = new sap.m.HBox ({
            items:[
                this.lbl_Pizza,
                this.txt_Pizza
            ]
        }).addStyleClass("Content_Fields_DIV");
        
        this.lbl_Order_Number = new sap.m.Label ({
            text: "Order Number:"
        }).addStyleClass("");
        this.txt_Order_Number = new sap.m.Label ({
            text: "TODO-Bind with Order selection"
        }).addStyleClass("Text_B wordwrap color_highlight");
        
        this.div_Order_Number = new sap.m.HBox ({
            items:[
                this.lbl_Order_Number,
                this.txt_Order_Number
            ]
        }).addStyleClass("Content_Fields_DIV");
        
        
        this.lbl_Order_Date = new sap.m.Label ({
            text: "Order Date:"
        }).addStyleClass("");
        this.txt_Order_Date = new sap.m.Label ({
            text: "TODO-Bind with Order selection Date"
        }).addStyleClass("Text_B wordwrap color_highlight");
        
        this.div_Order_Date = new sap.m.HBox ({
            items:[
                this.lbl_Order_Date,
                this.txt_Order_Date
            ]
        }).addStyleClass("Content_Fields_DIV");

        this.lbl_Size = new sap.m.Label ({
            text: "Size:"
        }).addStyleClass("");
        this.txt_Size = new sap.m.Label ({
            text: "TODO-Bind with Pizza selection (standard name or 'Custom Pizza')"
        }).addStyleClass("Text_B wordwrap color_highlight");
        this.div_Size = new sap.m.HBox ({
            items:[
                this.lbl_Size,
                this.txt_Size
            ]
        }).addStyleClass("Content_Fields_DIV");
    	
    	this.lbl_Cheese = new sap.m.Label ({
            text: "Cheese:"
        });
        this.txt_Cheese = new sap.m.Label({
            text: "TODO-Bind with Cheese selection"
        }).addStyleClass("Text_B wordwrap color_highlight");
        this.div_Cheese = new sap.m.HBox ({
            items:[
                this.lbl_Cheese,
                this.txt_Cheese
            ]
        }).addStyleClass("Content_Fields_DIV");

        this.lbl_Crust = new sap.m.Label ({
            text: "Crust:"
        });
        this.txt_Crust = new sap.m.Label({
            text: "TODO-Bind with Crust selection"
        }).addStyleClass("Text_B wordwrap color_highlight");
        this.div_Crust = new sap.m.HBox ({
            items:[
                this.lbl_Crust,
                this.txt_Crust
            ]
        }).addStyleClass("Content_Fields_DIV");
    	
    	this.lbl_Sauce = new sap.m.Label ({
            text: "Sauce:"
        });
        this.txt_Sauce = new sap.m.Label ({
            text: "TODO-Bind with Sauces selection. Maybe make this a table?"
        }).addStyleClass("Text_B wordwrap color_highlight");
        this.div_Sauce = new sap.m.HBox ({
            items:[
                this.lbl_Sauce,
                this.txt_Sauce
            ]
        }).addStyleClass("Content_Fields_DIV");

        this.lbl_Toppings = new sap.m.Label ({
            text: "Toppings:"
        });
        this.txt_Toppings = new sap.m.Label ({
            text: "TODO-Bind with Toppings selection. Maybe make this a table?"
        }).addStyleClass("Text_B wordwrap color_highlight");
        this.div_Toppings = new sap.m.HBox ({
            items:[
                this.lbl_Toppings,
                this.txt_Toppings
            ]
        }).addStyleClass("Content_Fields_DIV");
    	
    	this.div_OrderDetails = new sap.m.VBox ({
    		items:[
    		       this.div_Order_Number,
    		       this.div_Order_Date,
    		       this.div_Pizza,
                   this.div_Size,
                   this.div_Cheese,
    		       this.div_Crust,
    		       this.div_Sauce,
    		       this.div_Toppings
    		]
    	}).addStyleClass("ZEBRA_CONTAINER_DIV centered");
    	
        /*  ===================================================================
         *                          CUSTOMER DETAILS SECTION 
         *  */

    	this.lbl_Address = new sap.m.Label ({
    		text: "Customer Details"
    	}).addStyleClass("Text_Big");
        this.div_Address = new sap.m.VBox({
            items:[ 
                this.lbl_Address
            ]
        }).addStyleClass("Content_Fields_DIV margin-top-2 margin-bottom-1 ");
    	
    	this.lbl_Street = new sap.m.Label ({
    		text: "Street:"
    	});
    	this.txt_Street = new sap.m.Label ({
    	}).addStyleClass("Text_B color_highlight");
    	this.div_Street = new sap.m.HBox ({
    		items: [
		        this.lbl_Street,
		        this.txt_Street
	        ]
    	}).addStyleClass("Content_Fields_DIV");
    	
    	this.lbl_City = new sap.m.Label ({
    		text: "City / Province:"
    	});
    	this.txt_City = new sap.m.Label ({
    	}).addStyleClass("Text_B color_highlight");
    	this.div_City = new sap.m.HBox ({
    		items: [
    		        this.lbl_City,
    		        this.txt_City
	        ]
    	}).addStyleClass("Content_Fields_DIV");
    	
    	this.lbl_Province = new sap.m.Label ({
    		text: "Province:"
    	});
    	this.txt_Province = new sap.m.Label ({
    	}).addStyleClass("Text_B color_highlight");
    	this.div_Province = new sap.m.HBox ({
    		items: [
    		        this.lbl_Province,
    		        this.txt_Province
	        ]
    	}).addStyleClass("Content_Fields_DIV");
    	
    	this.lbl_PostalCode = new sap.m.Label ({
    		text: "Postal Code:"
    	});
    	this.txt_PostalCode = new sap.m.Label ({
    	}).addStyleClass("Text_B color_highlight");
    	this.div_PostalCode = new sap.m.HBox ({
    		items: [
    		        this.lbl_PostalCode,
    		        this.txt_PostalCode
	        ]
    	}).addStyleClass("Content_Fields_DIV");
    	
    	this.div_AddressDetails = new sap.m.VBox ({
    		items: [
		        this.div_Street, 
		        this.div_City, 
		        this.div_PostalCode
	        ]
    	}).addStyleClass("ZEBRA_CONTAINER_DIV centered");
    	


    	this.lbl_Confirm = new sap.m.Label ({
    		text: "Confirmation"
    	}).addStyleClass("Text_Big");
        this.div_Confirm = new sap.m.VBox({
            items:[ 
                this.lbl_Confirm
            ]
        }).addStyleClass("Content_Fields_DIV margin-top-1 margin-bottom-1 ");
    	
    	this.txt_Message = new sap.m.Label ({
    		text: "Please review your order and click Update Order!"
    	}).addStyleClass("Text_A color_navy centered");
    	
    	this.div_ConfirmMessage = new sap.m.HBox ({
    		items: [
    		    this.txt_Message, 
	        ]
    	}).addStyleClass(" Content_Fields_DIV");
    	/*	===================================================================
		 *  						BUTTONS SECTION 
		 *  */

        this.btn_Return = new sap.m.Button({
            text: "Go Back",
            tap: [oCon.buttonTap_ModifyOrder, oCon ]
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Back");
        this.btn_Confirm = new sap.m.Button({
            text: "Update Order",
            tap: [oCon.buttonTap_CreateOrder, oCon ]
        }).addStyleClass("AxxiomeRegularButton Axxiome_Button_Accept");
		
        this.div_btn_OrderConfirm = VIEWS_Create_Buttons_Container([ this.btn_Return, this.btn_Confirm ]);
        this.div_btn_OrderConfirm.addStyleClass(" margin-top-1 ");
		/*
		 *  						BUTTONS SECTION 
		 * 	=================================================================== */
		
    	

    	VIEWS_Add_Main_Content( oCon.getView(), [
            this.div_Status,
            this.div_StatusDetail,
    		this.div_Order,
            this.div_OrderDetails,
    		//this.div_Address,
            //this.div_AddressDetails,
    		this.div_Confirm,
            this.div_ConfirmMessage,
            this.div_btn_OrderConfirm
    	]);
    	
    }
   
});