// jQuery.sap.require("sap.banking.ui.services.App.Settings");

sap.ui.controller("view.Orders.NewOrder", {
	
	
	onBeforeShow : function(evt){

		var obView = this.getView();

		this.load_data( obView );
		this.clear_all_value_state( obView );
		
    },
	
	/* ======= FRAGMENTS ======= */
	load_data: function (obView) {

		this.get_Pizzas();
		// this.get_PizzaCrusts();
		// this.get_PizzaSauces();
		// this.get_PizzaToppings();
		
	},
	
	clear_all_value_state:function( obView ){
		// obView.txtService_Reason.setValueState( sap.ui.core.ValueState.None );
	},
	
	
	buttonTap : function(evt){
		
    },

    
    get_Pizzas: function( stReason ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_Pizzas_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		var wsService = new WS( PIZZAS_WS.Get_Pizzas, wsParameter ); 

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_Pizzas_SUCCESS: function(jsonData) {

		console_log([ ">> get_Pizzas_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		var myDiv1 = obView.slt_Pizza;
		var myDiv2 = obView.slt_Pizza2;
		myDiv1.destroyItems();
		myDiv2.destroyItems();

		var cont = 1;

		$.each(jsonData, function(i, item){

			if ( item.StandardPizzaID !== 0 ){

				var checkBox = new sap.m.RadioButton(
					obView.createId("checkBox_Pizza_" + item.StandardPizzaID ),
					{	groupName:"pizzaChooseTmp",
						text: item.PizzaName,


					}
				);

				var imageForCheck = new sap.m.Image ({src: "./img/pizzas/checkBox_Pizza_" + item.StandardPizzaID + ".jpg",width:"200pt",height:"150pt"});

				var divNexTo = new sap.m.FlexBox ({
		            direction: "Column",
		            alignItems: "Start",
		            items:[
		                checkBox,
		                imageForCheck
		            ]
		        });

				if ( cont <= 3 ){
					myDiv1.addItem( divNexTo );
				} else {
					myDiv2.addItem( divNexTo );
				}
				//myDiv.addItem( imageForCheck );

				cont++;

			}

		});

		var checkBox = new sap.m.RadioButton(
			obView.createId("checkBox_Pizza_" + "Custom" ),
			{	groupName:"pizzaChooseTmp",
				text:"Build Your Own",
	                    select:function (evt){
	                        if ( evt.mParameters.selected == true ){
	                            Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_NEW_ORDER_CUSTOM }); 
	                            obView.combo_Pizzas5.setSelected(false);
	                        }
	                  }
	            });


			var imageForCheck = new sap.m.Image ({src: "./img/pizzas/checkBox_Pizza_" + "5" + ".jpg", width:"200pt",height:"150pt"});

			var divNexTo = new sap.m.FlexBox ({
	            direction: "Column",
	            alignItems: "Start",
	            displayInline: true,
	            items:[
	                checkBox,
	                imageForCheck
	            ]
	        });

			myDiv2.addItem( divNexTo );		
		
	},

	get_PizzaCrusts: function( stReason ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_PizzaCrusts_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		var wsService = new WS( PIZZAS_MOCK.Get_Crusts , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaCrusts_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaCrusts_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		obView.slt_Crust.setModel( oModel );
		// obView.slt_Crust.refreshModel();

	},

	get_PizzaSauces: function( stReason ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_PizzaSauces_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		var wsService = new WS( PIZZAS_MOCK.Get_Sauces , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaSauces_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaSauces_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		obView.slt_Sauce.setModel( oModel );
		// obView.slt_Crust.refreshModel();

	},


	get_PizzaToppings: function( stReason ){
    	
    	/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		var callBack_Success = this.get_PizzaToppings_SUCCESS;

		var wsParameter = new WSCaller().setSuccessCallback(callBack_Success);
		var wsService = new WS( PIZZAS_MOCK.Get_Toppings , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

    },

    get_PizzaToppings_SUCCESS: function(jsonData) {

		console_log([ ">> get_PizzaToppings_SUCCESS ", jsonData ]);
		var oModel = new sap.ui.model.json.JSONModel();
        oModel.setData(jsonData);

		var obView = app.getCurrentView();

		var myDiv = obView.dv_Toppings_List;
		myDiv.destroyItems();

		$.each(jsonData.ToppingTypes, function(i, item){

			if ( item.ToppingTypeID !== "0" ){

				var checkBox = new sap.m.CheckBox(
					obView.createId("checkBox_Topping_" + item.ToppingTypeID ),
					{
						text: item.ToppingName,

					}
				);
				myDiv.addItem( checkBox );

			}

		});

	},
	
	buttonTap_OrderNow : function() {
		console_log("ORDER NOW TAP");

		var view = this.getView();
		var flag = false;

		var pizzaIdTmp;
		var count = 0;
		// See how many radios are
		for(count = 1; view.byId("checkBox_Pizza_" + count) != undefined ; count = count + 1);
		console_log("count:  " +  count);
		//check radio per radio if selected
		for(var i=1; i < count ; i = i + 1){
			if(view.byId("checkBox_Pizza_" + i).getSelected()){
				flag = true;
				pizzaIdTmp = i;

			}
		}

		if(flag) {
	        Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_ORDER_SUMMARY, pizzaId: pizzaIdTmp, specialPizza: true });
		} else{
	    	DISPLAY_MESSAGE_WARNING(
                "Selected Item",
                "Please select at least one choice!"
            );
         }
	}
	
});