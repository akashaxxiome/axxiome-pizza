jQuery.sap.require("sap.ui.base.Event");
jQuery.sap.require("js.utils.legacy.Formatter");
var oCarouselU = null;   // This will hold the carousel component
var loopTimeU = null;    // This will hold the carousel loop time value in millisecond
var bShowNextU = null;  // This will hold the value in boolean whether user wants to loop nextwise or previouswise
var timeoutID = null;


sap.ui.controller( "view.Login", {                  
			
	onInit : function(){
		//jQuery.sap.require("view.ConnectionSetting");
		jQuery.sap.require("js.settings.ConnectionSettings");
		//connectionSettingController.initializeConnectionSettings();
	},
	
    getCurrencyCodes : function() {
        var model = this.getView().getModel();
        var data = model.getData();
        return data.currencycodes;
    },
    
});