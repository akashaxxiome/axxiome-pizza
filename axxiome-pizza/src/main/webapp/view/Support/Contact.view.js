﻿sap.ui.jsview("view.Support.Contact", {
              
    getControllerName : function() {
    	return "view.Support.Contact";
    },
              
    onBeforeShow:function(evt){
        this.getController().onBeforeShow(evt);
    },
    
    createContent : function(oCon) {
    
		this.create_fragments(oCon);

		this.create_components(oCon);
		
		//==============================================
		//			PAGE
        //==============================================
    	var obParameters = {
    		stViewName : "CONTACT_Page",
    		boEnableScroll: true,
    		boHeaderIsLogout: false,
    		boShowTitle: false,
    		boShowFooter : true,
    		footerStyle: "WHITE",
    	}
		VIEWS_Create_Regular_Page( this, obParameters ); 
		
    	return this.thePage;
    	
    },
    
    
    create_fragments: function(oCon){
    
    },
    
    create_components: function(oCon){
    	
		if( myCurrentData.IS_MOBILE ){
			this.create_components_mobile(oCon);
		} else {
			this.create_components_desktop(oCon);
		}

    	/* ------------- FINALLY --------------- */
    	
    	this.div_all = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Start,
			items:[]
		}).addStyleClass("PAGE_Content_DIV_ALL");
    	
    	VIEWS_Add_Main_Content( oCon.getView(), [ this.divPhone, this.divMail, this.div_contact_presentation,
    	                                          this.div_contact_phone, this.div_contact_email,
    	                                          this.footer_controls ] );
    	
    	if ( myCurrentData.IS_MOBILE ){
    		this.div_all.addStyleClass("margin-top-1");
    	} else {
    		this.div_all.addStyleClass("width-90");
    	}
    	
    	
    	this.div_content = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Start,
			items:[
				this.div_all,
			]
		}).addStyleClass("PAGE_Content_DIV");
    	
    	
    },
    
    
    create_components_mobile: function(){
    	
    	this.link_MailTo = new sap.ui.core.HTML({});
		this.btnCall_Phone = new sap.ui.core.HTML({});
		
		this.btn_Mail = new sap.m.Button({
			text: "{i18N>contact.button.envelop}",
			press: function(){ $("#CONTACT_DIV_EMAIL img").click(); }
		}).addStyleClass("AxxiomeRegularButton width-60 margin-bottom-1 ");
		
		this.divMail = new sap.m.VBox({
			id: "CONTACT_DIV_EMAIL",
			visible: false,
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				this.link_MailTo,
				this.btn_Mail
			]
		}).addStyleClass("Content_NoMargin_DIV margin-top-4"),
		
		this.btn_Phone = new sap.m.Button({
			text: "{i18N>contact.button.phone}",
			press: function(){ call_Phone(); }
		}).addStyleClass("AxxiomeRegularButton width-60 margin-bottom-1 ");
		
		this.divPhone = new sap.m.VBox({
			id: "CONTACT_DIV_PHONE",
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
				this.btnCall_Phone,
				this.btn_Phone 
			]
		}).addStyleClass("Content_NoMargin_DIV margin-top-2 ");
		
    },
    
    create_components_desktop: function(oCon){
    	
    	this.txt_Contact_Email_To = new sap.m.Input({ 
			type: sap.m.InputType.Email,
			enabled: false,
			textAlign: "Center",
			layoutData: new sap.m.FlexItemData({
				growFactor: 1
			})
		}).addStyleClass("Compartamos_TextField width-80");
    	
    	this.lbl_Contact_Email_To = new sap.m.Label({ 
			type: sap.m.InputType.Email,
			enabled: false,
			textAlign: "Left",
			layoutData: new sap.m.FlexItemData({
				growFactor: 1
			})
		}).addStyleClass("Text_Big align_left color_compartamos width-80");
    	
    	this.txt_Contact_Comments = new sap.m.TextArea({
			rows: 7,
			cols: 150,
			width: "100%",
		}).addStyleClass("Compartamos_TextField");
    	
    	
		
    	this.div_contact_presentation = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Start ,
			items:[
				
			    new sap.m.VBox({
					alignItems : sap.m.FlexAlignItems.Start,
					justifyContent : sap.m.FlexJustifyContent.Start ,
					items:[
						new sap.m.Label({text: "{i18N>support.contact.label.top}"}).addStyleClass("Text_Big font_compartamos"),
					]
				}).addStyleClass("Content_Text_DIV "),
				
				new sap.m.VBox({
					alignItems : sap.m.FlexAlignItems.Start,
					justifyContent : sap.m.FlexJustifyContent.Start ,
					items:[
						new sap.m.Label({
							text: "{i18N>support.contact.label.top_a}"
						}).addStyleClass("Text_B align_left wordwrap"),
						new sap.m.Label({
							text: "{i18N>support.contact.label.top_b}"
						}).addStyleClass("Text_B align_left wordwrap"),
					]
				}).addStyleClass("Content_Text_DIV "),
				
			]
		}).addStyleClass("Content_NoMargin_DIV "), 
    	
		this.div_contact_phone = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
			    new sap.m.VBox({
					alignItems : sap.m.FlexAlignItems.Start,
					justifyContent : sap.m.FlexJustifyContent.Center ,
					items:[
						new sap.m.Label({text: "{i18N>support.contact.phone.topic}"}).addStyleClass("Text_Big font_compartamos"),
					]
				}).addStyleClass("Content_Text_DIV "),
				
				new sap.m.HBox({
					alignItems : sap.m.FlexAlignItems.Start,
					justifyContent : sap.m.FlexJustifyContent.Center ,
					items:[
						new sap.m.Label({text: "{i18N>support.contact.label.comparTel}"}).addStyleClass("Contact_Text_PhoneNumber font_compartamos"),
						new sap.m.Text({
							text: Common.Contact.PHONE_NUMBER,
							tap: [ call_Phone ],
						}).addStyleClass("Contact_Text_PhoneNumber font_compartamos font_bold"),
					]
				}).addStyleClass("Content_NoMargin_DIV "),
			]
		}).addStyleClass("Content_NoMargin_DIV Contact_DIV_Separator margin-top-2"),
		
		this.div_contact_email = new sap.m.VBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center ,
			items:[
			    new sap.m.VBox({
					alignItems : sap.m.FlexAlignItems.Start,
					justifyContent : sap.m.FlexJustifyContent.Center ,
					items:[
						new sap.m.Label({text: "{i18N>support.contact.email.topic}"}).addStyleClass("Text_Big font_compartamos"),
					]
				}).addStyleClass("Content_Text_DIV "),
				
				new sap.m.HBox({
					alignItems : sap.m.FlexAlignItems.Center,
					justifyContent : sap.m.FlexJustifyContent.Center ,
					items:[
					    new sap.m.HBox({
					    	width: "25%",
							alignItems : sap.m.FlexAlignItems.Center,
							justifyContent : sap.m.FlexJustifyContent.Center ,
							items:[
							    new sap.m.Label({text: "{i18N>support.contact.email.label.To}:"}).addStyleClass("Text_B "),
							]
					    }),
					    new sap.m.HBox({
					    	width: "75%",
							alignItems : sap.m.FlexAlignItems.Start,
							justifyContent : sap.m.FlexJustifyContent.Start ,
							items:[
							    this.lbl_Contact_Email_To
							]
					    }),
					]
				}).addStyleClass("Content_Text_DIV "),
				
				new sap.m.HBox({
					alignItems : sap.m.FlexAlignItems.Center,
					justifyContent : sap.m.FlexJustifyContent.Center ,
					items:[
					    new sap.m.HBox({
					    	width: "25%",
							alignItems : sap.m.FlexAlignItems.Start,
							justifyContent : sap.m.FlexJustifyContent.Center,
							items:[
							    new sap.m.Label({text: "{i18N>support.contact.email.label.Comments}:"}).addStyleClass("Text_B "),
							]
					    }),
					    new sap.m.HBox({
					    	width: "75%",
							alignItems : sap.m.FlexAlignItems.Center,
							justifyContent : sap.m.FlexJustifyContent.Start ,
							items:[
							    this.txt_Contact_Comments
							]
					    }),
					]
				}).addStyleClass("Content_Text_DIV "),
				
			]
		}).addStyleClass("Content_NoMargin_DIV margin-top-2 "),
		
		
    	
    	this.btn_SendContact = new sap.m.Button({
			text: "Enviar",
			tap:[ oCon.buttonTap_Send, oCon ],
		}).addStyleClass("AxxiomeRegularButton "),
		
		 /*	===================================================================
		 *  						FOOTER SECTION 
		 *  */
		this.footer_controls = new sap.m.HBox({
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Center,
			items:[
				new sap.m.VBox({
					items:[
						this.btn_SendContact
					]
				}).addStyleClass("Page_Buttons_DIV_H"),
			]
		}).addStyleClass("PAGE_Buttons_DIV");
		/*
		 *  						FOOTER SECTION 
		 * 	=================================================================== */
		
    	
    }
   
});
