jQuery.sap.require("sap.ui.base.Event");
jQuery.sap.require("sap.banking.ui.services.App.Support");
jQuery.sap.require("sap.banking.ui.services.App.MOCK_Support");

sap.ui.controller("view.Support.Contact", {
	
	isEditMode: false,
	
    backTap: function(evt){
  	   Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_HOME }); 
    },
    
    onBeforeShow : function(evt){

		var obView = this.getView();
		
		this.get_Contact_Email( obView );
		
		this.adjust_View( obView );
		this.logic_validations(obView);
		this.clear_all_value_state(obView);
		
    },
    
    onBeforeRendering: function(evt){
    	
    	var obView = this.getView();
    	
    	if ( myCurrentData.IS_MOBILE ){
    		
	    	obView.link_MailTo.setContent( "" );
	    	obView.divMail.setVisible(false);
			
			obView.btnCall_Phone.setContent( "" +
				"<a href='#' onclick='call_Phone()' >" +
					"<img class='Contact_Img' src='" + IMAGES.SUPPORT.PHONE + "' />" +
				"</a>"
			);
			
			$("#Div_Contact_Phone").on( "click", function(evt) {
				call_Phone();
			});
			
    	}
    	
    },
    
    onAfterRendering: function(evt){
    	
    	if ( myCurrentData.IS_MOBILE ){
	    	$("#Div_Contact_Phone").on( "click", function(evt) {
				call_Phone();
			});
    	}
    	
    },
    
    adjust_View : function( obView ){
    	    			
		obView.div_menu.removeAllItems();
		obView.div_menu.addItem( obDesktopMenu );
		
    	myCurrentData.DEVICE.adjust_View_Width(obView);
    	
    	var obViewSettings = {
			subHeader_TEXT : fn_APP_CONF_GET_TEXT( "i18N", "contact.subHeader.label" ),
			subHeader_IMG : IMAGES.MAIN.SUPPORT,
			title_TEXT : fn_APP_CONF_GET_TEXT( "i18N", "contact.title.label" ),
			arCSS_REM : [],
			arCSS_ADD : [ "BACKGROUND_SUBHEADER_CONTACT" ],
		};
		
		apply_Header_Title_CSS( obView, obViewSettings );
    	
    },
    
    
    logic_validations : function(obView){
    	
    	if ( myCurrentData.IS_MOBILE ){
    		if ( Common.Contact.EMAIL_ADDRESS == "" || Common.Contact.EMAIL_SUBJECT == "" ){
    			obView.divMail.setVisible(false);
    		}
    	} else {
    		obView.txt_Contact_Comments.setValue();
    		obView.txt_Contact_Comments.setEnabled( true );
    		obView.btn_SendContact.setEnabled( true );
    	}
    	
    },
    
    clear_all_value_state: function( obView ){
    	
		if ( !myCurrentData.IS_MOBILE ){
			obView.txt_Contact_Email_To.setValueState( sap.ui.core.ValueState.None );
			obView.txt_Contact_Comments.setValueState( sap.ui.core.ValueState.None );
		}
		
	},
	
	get_Contact_Email: function( obView ){
		
		console_log(" >> get_Contact_Email ");
		
		if ( Common.Contact.EMAIL_ADDRESS == "" || Common.Contact.EMAIL_SUBJECT == "" ){
		
			app.setCurrentViewBusy( true );
			
			var callBackFunction_SUCCESS = this.get_Contact_Email_SUCCESS;

			var wsParameterGetEmail = new WSCaller().setSuccessCallback(callBackFunction_SUCCESS);
			var wsGetEmail = new WS(CONTACT_Get_Contact_Email, wsParameterGetEmail );

			var wsWrapper = new WSPackageWrapper().setOperation(wsGetEmail).setController(this);
			wsWrapper.startWS();
			
		} else {
			this.show_components( obView );
		}
		
	},

	get_Contact_Email_SUCCESS: function(jsonData){
		
		console_log( "get_Contact_Email_SUCCESS" );

		Common.Contact.EMAIL_ADDRESS = jsonData.contactEmailAddress;
		Common.Contact.EMAIL_SUBJECT = jsonData.contactEmailSubject;
		
		var oCon = sap.ui.getCore().byId( Common.Navigations.PAGE_CONTACT ).getController();
		oCon.show_components( oCon.getView() );
		
		app.setCurrentViewBusy( false );
		
	},
    
	show_components : function( obView ){
		
		if ( myCurrentData.IS_MOBILE ) {
			obView.link_MailTo.setContent( "" +
				"<a href='mailto:" + Common.Contact.EMAIL_ADDRESS + "?" +
				"subject=" + Common.Contact.EMAIL_SUBJECT + "' target='_blank' >" +
					"<img class='Contact_Img' src='" + IMAGES.SUPPORT.ENVELOP + "' />" +
				"</a>"
			);
			obView.divMail.setVisible(true);
		} else {
			obView.btn_SendContact.setEnabled( true );
			obView.lbl_Contact_Email_To.setText( Common.Contact.EMAIL_ADDRESS );
			obView.txt_Contact_Email_To.setValue( Common.Contact.EMAIL_ADDRESS );
		}
		
		app.setCurrentViewBusy( false );
		
	},
	
    buttonTap_Send : function(){
    	
    	console_log([ " --------------------------- ", "CONTACT PAGE - buttonTap_Send " ]);
		
    	var obView = this.getView();
    	app.setCurrentViewBusy( true );
    	
		var obError = {error: false, title : [], message: [], component: []};
		try{
			this.perform_validations(obError, obView );
		} catch(e){
			app.setCurrentViewBusy( false );
		}
		
		if ( obError.error == true ){
				
			AXX_EXCEPTION_USER( 
				obError.title
				, obError.message
				, true  
				, "error"
				, obError.component
			);
			app.setCurrentViewBusy( false );
			
		} else {
			
			try{

				var theEmail = obView.txt_Contact_Email_To;
				var theComment = obView.txt_Contact_Comments;

				this.send_Contact_Email( theEmail.getValue(), theComment.getValue() );

			} catch(e){
				obErrorTEMP = AXX_EXCEPTION_Translate_ERROR(e);

				DISPLAY_MESSAGE_ERROR(
					obErrorTEMP.title,
					obErrorTEMP.message
				);
			} finally{
				app.setCurrentViewBusy( false );
			}
		}
		
    },
    
    
    perform_validations: function(obError, obView){
    	
    	this.clear_all_value_state(obView);
    	
    	var theEmail = obView.txt_Contact_Email_To;
		var theComment = obView.txt_Contact_Comments;
		
		if ( !isValidEmail( theEmail.getValue() ) ){
			obError.error = true;
			obError.title.push( fn_APP_CONF_GET_TEXT( "messageFile", "03_B21_T_N" ) );
			obError.message.push( fn_APP_CONF_GET_TEXT( "messageFile", "03_B21_N_1" ) );
			obError.component.push( theEmail );
		} 
		if ( theComment.getValue().length < 5 ){
			obError.error = true;
			obError.title.push( fn_APP_CONF_GET_TEXT( "messageFile", "03_B21_T_N" ) );
			obError.message.push( fn_APP_CONF_GET_TEXT( "messageFile", "03_B21_N_2" ) );
			obError.component.push( theComment );
		}
    	
    },
    
    
    
    
    send_Contact_Email: function( stEmail, stComment ){

    	console_log(" >> send_Contact_Email ");	
    	
    	var oCon = sap.ui.getCore().byId(Common.Navigations.PAGE_CONTACT).getController();
    	app.setCurrentViewBusy( true );
		
		var callBackFunction_SUCCESS = this.send_Contact_Email_SUCCESS;

		var wsParameterSendEmail = new WSCaller().setSuccessCallback(callBackFunction_SUCCESS).setCustomData({stText: replaceNewLinesWithSpace(stComment)});
		var wsSendEmail = new WS(CONTACT_Send_Contact, wsParameterSendEmail );

		var wsWrapper = new WSPackageWrapper().setOperation(wsSendEmail).setController(this);
		wsWrapper.startWS();
		
    },
    
	send_Contact_Email_SUCCESS : function( jsonData ){
		
		console_log(" >> send_Contact_Email_SUCCESS ");
		
		var oCon = sap.ui.getCore().byId(Common.Navigations.PAGE_CONTACT).getController();
		app.setCurrentViewBusy( false );
		
		var obParameters = {};
		obParameters.fn_callBack_YES = oCon.navigate_After_Success;
		
		DISPLAY_MESSAGE_SUCCESS(
			fn_APP_CONF_GET_TEXT( "messageFile", "03_B21_T" ) ,
			fn_APP_CONF_GET_TEXT( "messageFile", "03_B21" ) ,
			obParameters
		);
	},
    
    navigate_After_Success: function(){
    	var oCon = sap.ui.getCore().byId(Common.Navigations.PAGE_CONTACT).getController();
    	oCon.onBeforeShow({	});
    }
   
});