jQuery.sap.declare("control.OCBNavContainerRenderer");
control.OCBNavContainerRenderer = {
	render : function(rm, oControl) {
		sap.m.NavContainerRenderer.render(rm, oControl);
	}
};

/*
control.OCBNavContainerRenderer.render = function(rm, oControl) {
	sap.m.NavContainerRenderer.render(rm, oControl);
};
*/ 