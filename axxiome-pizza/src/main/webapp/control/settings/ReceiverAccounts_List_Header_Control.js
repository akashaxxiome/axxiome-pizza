sap.ui.core.Control.extend("control.settings.ReceiverAccounts_List_Header_Control", {

    metadata : {                              
        properties : {
        	//"displayName" 	: "string",
        }
    },

    renderer : function(oRm, oControl) {
    	
    	oRm.write("<div class='Settings_ReceiverAccounts_List_Header_Control' style=' ' >");
    	
    		oRm.write("<div class='Settings_ReceiverAccounts_List_Header_ROW' style=' ' >");
    		
    			//oRm.write( "<div class='Settings_ReceiverAccounts_List_Header_COLUMN_A' style=' ' >" );
    				oRm.write( "<div class='Settings_ReceiverAccounts_List_Header_CELL Settings_ReceiverAccounts_List_Header_CELL_A' style=' ' >" );
    					oRm.write( fn_APP_CONF_GET_TEXT("i18N","settings.receiverAccounts.list.header.name") );
    				oRm.write( "</div>" );
    				    				
    			//oRm.write( "</div>" );
    			
    			//oRm.write( "<div class='Settings_ReceiverAccounts_List_Header_COLUMN_B' style=' ' >" );
    				oRm.write( "<div class='Settings_ReceiverAccounts_List_Header_CELL Settings_ReceiverAccounts_List_Header_CELL_B' style=' ' >" );
    				oRm.write( fn_APP_CONF_GET_TEXT("i18N","settings.receiverAccounts.list.header.account") );
    				oRm.write( "</div>" );
    			//oRm.write( "</div>" );
    			
    			//oRm.write( "<div class='Settings_ReceiverAccounts_List_Header_COLUMN_C' style=' ' >" );
    				oRm.write( "<div class='Settings_ReceiverAccounts_List_Header_CELL Settings_ReceiverAccounts_List_Header_CELL_B' style=' ' >" );
    				oRm.write( fn_APP_CONF_GET_TEXT("i18N","settings.receiverAccounts.list.header.type") );
    				oRm.write( "</div>" );
    			//oRm.write( "</div>" );
    			
        			//oRm.write( "<div class='Settings_ReceiverAccounts_List_Header_COLUMN_C' style=' ' >" );
    				oRm.write( "<div class='Settings_ReceiverAccounts_List_Header_CELL Settings_ReceiverAccounts_List_Header_CELL_D' style=' ' >" );
    				oRm.write( fn_APP_CONF_GET_TEXT("i18N","settings.receiverAccounts.list.header.action") );
    				oRm.write( "</div>" );
    			//oRm.write( "</div>" );
    				
    				
    		oRm.write("</div>");
    		
    	
    	oRm.write("</div>");
    }

});
