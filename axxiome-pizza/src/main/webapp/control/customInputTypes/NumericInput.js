jQuery.sap.declare("control.customInputTypes.NumericInput");

sap.ui.define(['jquery.sap.global', 'sap/m/Input'],
    function(jQuery, BaseInput) {
        "use strict";

        /**
         * Constructor for a new Input.
         *
         * @param {string} [sId] id for the new control, generated automatically if no id is given 
         * @param {object} [mSettings] initial settings for the new control
         *
         * @class
         * The Input control builds the container for a header layout. 
         * Its is a convenience control as it is just a specialized Input control.
         */
        var commonControlInput = BaseInput.extend('control.customInputTypes.NumericInput', /** @lends sap.m.Input.prototype */ {
            metadata: {},
            renderer : {
                render : function(oRm, oControl) {
                    sap.m.InputRenderer.render(oRm, oControl); 
                }
            }
        });


        commonControlInput.prototype.onkeypress = function(oEvent) {

                // if ( ( sap.ui.Device.os.name.toUpperCase() === "IOS" ) &&
            //     this.getType() == sap.m.InputType.Number) {

                var regexKey = /[\d]|\./;
                var regexValue = /[\d]{1,9}|[\d]{1,9}(\.[\d]{1,2}){1}/;

                var theEvent = oEvent || window.event;

                var charValue = theEvent.keyCode || theEvent.which;
                charValue = String.fromCharCode(charValue);

                var key = charValue;

                // var charValue = String.fromCharCode(oEvent.charCode);

                // if (/\d/.test(charValue) == false &&
                //     '.' != charValue) {
                //     oEvent.preventDefault();
                //     return false;
                // }


                //First REGEX validation, in the character only
                var boRegex = regexKey.test(key);
                if ( !boRegex ) {

                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();

                } else {
                    
                    var valueTemp = this.getValue() + "" + key;

                    //Second REGEX Validation, in the whole value
                    var boRegexValue = regexValue.test( valueTemp );

                    if ( !boRegexValue ){

                        theEvent.returnValue = false;
                        if (theEvent.preventDefault) theEvent.preventDefault();

                    }

                }


            // }
        }

        return commonControlInput;

    }, /* bExport= */ true);


        // commonControlInput.prototype.onkeydown = function(oEvent) {
        //         var theEvent = oEvent || window.event;

        //         var charValue = theEvent.keyCode || theEvent.which;
        //         charValue = String.fromCharCode(charValue);

        // };

        // commonControlInput.prototype.onkeyup = function(oEvent) {
        //         var theEvent = oEvent || window.event;

        //         var charValue = theEvent.keyCode || theEvent.which;
        //         charValue = String.fromCharCode(charValue);

        // };


 // txt_Currency  = new common.control.Input({
    
 // });