jQuery.sap.declare("control.customInputTypes.LettersOnly");
sap.ui.model.SimpleType.extend("control.customInputTypes.LettersOnly", {
    formatValue : function(oValue) {
        return oValue;
    },

    parseValue : function(oValue) {
        return oValue;
    },

    validateValue: function (oValue) {
        if (!oValue.match(RegularExpression.ACCEPTS_ONLY_LETTERS)) {
            var messageString = fn_APP_CONF_GET_TEXT( "messages", "MESSAGE_ALLOW_LETTERS_ONLY");
            throw new sap.ui.model.ValidateException(messageString);
        }
    }
})