jQuery.sap.declare("control.customInputTypes.Float");
sap.ui.model.SimpleType.extend("control.customInputTypes.Float", {
    formatValue : function(oValue) {
        return oValue;
    },

    parseValue : function(oValue) {
        return oValue;
    },

    validateValue: function (oValue) {
        if (!oValue.match(RegularExpression.ACCEPT_UP_TO_TWO_DECIMAL_DIGITS)) {
            var messageString = fn_APP_CONF_GET_TEXT( "messages", "mobileMoney.transferInformation.message.invalidAmount");
            throw new sap.ui.model.ValidateException(messageString);
        }
    }
})