sap.ui.core.Control.extend("control.menu.mobile.MenuControl", {
 
	metadata : {
	
		properties : {
			name: "string"
		},
		events:{
			menuClick:{enablePreventDefault : true}
		}

	},

	renderer : function(oRm, oControl) {
		
		
		
		var menuHtml = ""+
		"<div id='secureMenu' class='secureMenuCls'>" +
			"<div id='topBrand' class='menuTopBranding'>" +
				"<span class='productTitleSpan'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.ocb")+"</span>" +
			"</div>"+
			"<ul class='menuLiHolder secureMenuLiGap'>" + 
				"<li class='menuLi_Column_Left MENU_ICON_HOME BACKGROUND_HOME'><span id='menuHome'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.home")+"</span></li>" ;
				
				if ( AXX_SETTINGS_getProperty( "MODULES.ACCOUNTS_SAVINGS.visible" ) ){
					menuHtml += "<li class=' menuLi_Column_Left MENU_ICON_SAVINGS BACKGROUND_ACCOUNT_SAVINGS'><span id='menuAccountSavings'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.accountSavings")+"</span></li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.ACCOUNTS_INSURANCES.visible" ) ){
					menuHtml += "<li class=' menuLi_Column_Left MENU_ICON_INSURANCES BACKGROUND_ACCOUNT_INSURANCES'><span id='menuAccountInsurances'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.accountInsurances")+"</span></li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.ACCOUNTS_LOANS.visible" ) ){
					menuHtml += "<li class=' menuLi_Column_Left MENU_ICON_LOANS BACKGROUND_ACCOUNT_LOANS'><span id='menuAccountLoans'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.accountLoans")+"</span></li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.CARDS.visible" ) ){
					menuHtml += "<li class=' menuLi_Column_Left MENU_ICON_CARDS BACKGROUND_CARDS'><span id='menuCards'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.cards")+"</span></li>";
				}
				if ( AXX_SETTINGS_getProperty( "MODULES.MOBILE_MONEY.visible" ) ){
					menuHtml += "<li class=' menuLi_Column_Left MENU_ICON_MOBILE_MONEY BACKGROUND_MOBILE_MONEY'><span id='menuMobileMoney'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.mobileMoney")+"</span></li>";
				}
				if ( AXX_SETTINGS_getProperty( "MODULES.PAYMENTS.visible" ) ){
					menuHtml += "<li class=' menuLi_Column_Left MENU_ICON_PAYMENTS BACKGROUND_PAYMENTS'><span id='menuPayments'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.payments")+"</span></li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.TRANSFERS.visible" ) ){
					menuHtml += "<li class=' menuLi_Column_Left MENU_ICON_TRANSFERS BACKGROUND_TRANSFERS'><span id='menuTransfers'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.transfers")+"</span></li>";
				}
				if ( AXX_SETTINGS_getProperty( "MODULES.SUPPORT.visible" ) ){
					menuHtml += "<li class=' menuLi_Column_Left MENU_ICON_CONTACT BACKGROUND_CONTACT'><span id='menuContact'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.contact")+"</span></li>";
				}
				if ( AXX_SETTINGS_getProperty( "MODULES.SETTINGS.visible" ) ){
					menuHtml += "<li class=' menuLi_Column_Left MENU_ICON_SETTINGS BACKGROUND_SETTINGS'><span id='menuSettings'>"+sap.ui.getCore().getModel(Common.GlobalModelNames.I18N).getResourceBundle().getText("menu.label.adminInfo")+"</span></li>";
				}
				
			menuHtml += "</ul>" + 
			"<div class='moreMenuOverlayDiv'>&nbsp;</div>" +
		"</div>";
		
		
		//Create wrapper div for the menu
		oRm.write("<div id='mainMenu'"); 
        oRm.writeControlData(oControl);  // writes the Control ID and enables event handling - important!
        // oRm.addClass("menu");        // add a CSS class for styles common to all Control instances
        oRm.writeClasses();              // this call writes the above class plus enables support 
        oRm.write(">");
        
        //Add menu content
        oRm.write(menuHtml);
        oRm.write("</div>");
	},
	
	onAfterRendering:function(){
		
        //Get all li to register click event 
        var oControl = this;
        
        jQuery(".secureMenuLiGap li").removeClass("menuSelectedItem");

        jQuery(".secureMenuLiGap li,#settings.menuHeaderItem").on( "click", function(evt) {
			
            jQuery("#secureMenu").hide();
            
           	$(this).addClass("menuSelectedItem");
        	$(this).siblings().removeClass("menuSelectedItem");
            
            oControl.fireMenuClick({
                menuClicked : evt.target.id
            });
            
        });
        
        jQuery(".moreMenuOverlayDiv").click(function() {
        	jQuery("#secureMenu").hide();
        });
            
	},

	
	toggleSecureMenu: function(isVisible){
		console_log(" toggleSecureMenu triggered...");
		jQuery("#secureMenu").toggle();
		//jQuery("#secureMenu").toggle( "fast", function() {	/* Animation complete. */	});
	},
	
	showNonSecureMenu: function(isVisible){
		console_log("showNonSecureMenu");
		if(isVisible){
			jQuery("#nonSecureMenu").show();
			
			// Removing selected menu class from secure menu
			$("#secureMenu ul li").removeClass("menuSelectedItem");
			jQuery("#secureMenu").hide();
		} else {
			jQuery("#nonSecureMenu").hide();
			jQuery("#nonSecureMenu ul li").removeClass("menuSelectedItem");
		}		
	},
	
	
	hideMenu:function(name){
		var menuLi = jQuery("."+name);
		if(menuLi.length > 0){
			menuLi.hide();
		}
	},
	
	showMenu:function(name){
		var menuLi = jQuery("."+name);
		if(menuLi.length > 0){
			//menuLi.show();
			menuLi.show( "slow", function() {
				// Animation complete.
			});
		}
		
	}	
});
