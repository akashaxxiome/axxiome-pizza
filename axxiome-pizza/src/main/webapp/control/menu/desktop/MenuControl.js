sap.ui.core.Control.extend("control.menu.desktop.MenuControl", {
 
	metadata : {
	
		properties : {
			name: "string",
			view_name: "string"
		},
		events:{
			menuClick:{enablePreventDefault : true}
		}

	},

	renderer : function(oRm, oControl) {
		
		$( "." + oControl.getView_name() + " .menuDesktop").remove();
		
		var menuHtml = ""+
		"<div class='menuDesktop'>" +
			"<ul class='menuDesktopLi'>" + 
				
				"<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_HOME'>" +
					"<a href='#' id='menuHome'>" +
						"<div>" +
							"<div>" +
								"<img src='"+ IMAGES.MAIN.HOME +"' />" +
							"</div><div>" +
								"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.home" ) + "</span>" +
							"</div>" +
						"</div>" +
					"</a>" +
				"</li>";

				if ( AXX_SETTINGS_getProperty( "MODULES.ACCOUNTS_SAVINGS.visible" ) ){
					menuHtml += "<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_ACCOUNT_SAVINGS'>" +
						"<a href='#' id='menuAccountSavings'>" +
							"<div>" +
								"<div>" +
									"<img src='"+ IMAGES.MAIN.ACCOUNT_SAVINGS +"' />" +
								"</div><div>" +
									"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.accountSavings" ) + "</span>" +
								"</div>" +
							"</div>" +
						"</a>" +
					"</li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.ACCOUNTS_INSURANCES.visible" ) ){
					menuHtml += "<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_ACCOUNT_INSURANCES'>" +
						"<a href='#' id='menuAccountInsurances'>" +
							"<div>" +
								"<div>" +
									"<img src='"+ IMAGES.MAIN.ACCOUNT_INSURANCES +"' />" +
								"</div><div>" +
									"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.accountInsurances" ) + "</span>" +
								"</div>" +
							"</div>" +
						"</a>" +
					"</li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.ACCOUNTS_LOANS.visible" ) ){
					menuHtml += "<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_ACCOUNT_LOANS'>" +
						"<a href='#' id='menuAccountLoans'>" +
							"<div>" +
								"<div>" +
									"<img src='"+ IMAGES.MAIN.ACCOUNT_LOANS +"' />" +
								"</div><div>" +
									"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.accountLoans" ) + "</span>" + 
								"</div>" +
							"</div>" +
						"</a>" +
					"</li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.CARDS.visible" ) ){
					menuHtml += "<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_CARDS'>" +
						"<a href='#' id='menuCards'>" +
							"<div>" +
								"<div>" +
									"<img src='"+ IMAGES.MAIN.CARDS +"' />" +
								"</div><div>" +
									"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.cards" ) + "</span>" +
								"</div>" +
							"</div>" +
						"</a>" +
					"</li>";
				}
				
				
				if ( AXX_SETTINGS_getProperty( "MODULES.MOBILE_MONEY.visible" ) ){
					menuHtml += "<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_MOBILE_MONEY'>" +
						"<a href='#' id='menuMobileMoney'>" +
							"<div>" +
								"<div>" +
									"<img src='"+ IMAGES.MAIN.MOBILE_MONEY +"' />" +
								"</div><div>" +
									"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.mobileMoney" ) + "</span>" +
								"</div>" +
							"</div>" +
						"</a>" +
					"</li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.PAYMENTS.visible" ) ){
					menuHtml += "<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_PAYMENTS'>" +
						"<a href='#' id='menuPayments'>" +
							"<div>" +
								"<div>" +
									"<img src='"+ IMAGES.MAIN.PAYMENTS +"' />" +
								"</div><div>" +
									"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.payments" ) + "</span>" +
								"</div>" +
							"</div>" +
						"</a>" +
					"</li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.TRANSFERS.visible" ) ){
					menuHtml += "<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_TRANSFERS'>" +
						"<a href='#' id='menuTransfers'>" +
							"<div>" +
								"<div>" +
									"<img src='"+ IMAGES.MAIN.TRANSFERS +"' />" +
								"</div><div>" +
									"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.transfers" ) + "</span>" +
								"</div>" +
							"</div>" +
						"</a>" +
					"</li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.SUPPORT.visible" ) ){
					menuHtml += "<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_CONTACT'>" +
						"<a href='#' id='menuContact'>" +
							"<div>" +
								"<div>" +
									"<img src='"+ IMAGES.MAIN.SUPPORT +"' />" +
								"</div><div>" +
									"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.contact" ) + "</span>" +
								"</div>" +
							"</div>" +
						"</a>" +
					"</li>";
				}
				
				if ( AXX_SETTINGS_getProperty( "MODULES.SETTINGS.visible" ) ){
					menuHtml += "<li class='menuicon_account menuLi_Column_Left MENU_IMG_ICON BACKGROUND_SETTINGS'>" +
						"<a href='#' id='menuSettings'>" +
							"<div>" +
								"<div>" +
									"<img src='"+ IMAGES.MAIN.SETTINGS +"' />" +
								"</div><div>" +
									"<span class='spanImg'>" + fn_APP_CONF_GET_TEXT( "i18N", "menu.label.adminInfo" ) + "</span>" +
								"</div>" +
							"</div>" +
						"</a>" +
					"</li>";
				}
				
			menuHtml += "</ul>" + 
		"</div>";
		
        //Add menu content
        oRm.write(menuHtml);
	},
	
	onAfterRendering:function(){
		
        //Get all li to register click event 
        var oControl = this;
        
        jQuery(".secureMenuLiGap li").removeClass("menuSelectedItem");

        jQuery(".menuDesktopLi li").on( "click", function(evt) {
	
        	var myTarget_ID = evt.target.id;
        	if ( myTarget_ID == "" ){
        		myTarget_ID = $(this).children().context.lastChild.id;
        	}
        	
        	$(this).addClass("menuSelectedItem");
        	$(this).siblings().removeClass("menuSelectedItem");
            
            oControl.fireMenuClick({
                menuClicked : myTarget_ID
            });
            
        });
        
        
	},

});
