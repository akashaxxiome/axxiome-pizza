sap.ui.core.Control.extend("control.commons.Token_Control", {

	id:null,
	tokenValue:null,
	viewName:null,
	viewController:null,
	isVisible:true,

    metadata : {                              
        properties : {
        	"viewName" 	: "string",
        }
    },

    renderer : function(oRm, oControl) {
    	
    	oRm.write("<div class='Token_Control' id='Token_Wrapper_" + oControl.getViewName() + "' >");
    	
    		oRm.write("<div class='Token_Control_TEXT_DIV' style=\"\">");
    			
	    		oRm.write("<div style=\" margin:auto\">");
	    		
					oRm.write( "<span class='Token_Control_IMG_SPAN' style=\"float:left\">" );
					oRm.write( 		"<img src='"+ IMAGES.COMMON.TOKEN +"' class='Token_Control_IMG' />" );
					oRm.write( "</span>" );
		    		
		    		
		    		oRm.write( "<span class='Token_Control_TEXT_SPAN' style=\"float:left\">" );
		    			oRm.write(fn_APP_CONF_GET_TEXT("i18N", "tokenControl.token"));
		    		oRm.write( "</span>" );
		    		
		    	oRm.write("</div>");
    			
			oRm.write("</div>");
    		
	    		oRm.write("<div class='Token_Control_ITEMS_DIV' style=\" clear: both; \">");
	    			oRm.write( "<span class='Token_Control_INPUT_SPAN' style=''>" );
	    			oRm.write( 		"<input id='"+ oControl.getViewName() +"_Token_Control_TXT' " + 
	    							" type='text' value='' min='1' max='"+ Common.Validations.TOKEN_MAX_RANGE + "' " +
	    							" class='Token_Control_Input  " + 
	    							( !myCurrentData.IS_MOBILE ? " Token_Control_Input_Active " : "" )+
	    							"' " +
	    							" oninput='TOKEN_ACTION_NUMBERS(this)' " +
	    							" >" );
	    			oRm.write( "</span>" );
	    		oRm.write("</div>");
    	
    	oRm.write("</div>");
    },

	setGeneratedTokenValue: function (  ) {
		
		myCurrentData.TOKEN.viewName = this.getViewName();

		if (myCurrentData.IS_MOBILE) {
			this.setTokenReadOnlyProperty();
			this.generateTokenForMobile(  );
		}

	},

	setTokenValue : function (tokenValue) {
		var tokenControlText = $("#" + this.viewName +"_Token_Control_TXT" );
		this.setTokenReadOnlyProperty();
		tokenControlText.val(tokenValue);
	},

    onAfterRendering: function(){
    	var oControl = this;
    	var id = oControl.getViewName() +"_Token_Control_TXT";

		this.viewName = oControl.getViewName();
		this.viewController = sap.ui.getCore().byId(this.viewName).getController();
    	this.id = id;

		$("#" + this.viewName +"_Token_Control_TXT" ).mask( Common.Validations.TOKEN_MASK,{ placeholder: "" });
    	this.setTokenReadOnlyProperty();
    },
    
    setTokenReadOnlyProperty : function(){
		var tokenControlText = $("#" + this.viewName +"_Token_Control_TXT" );

        if (myCurrentData.IS_MOBILE){
    		tokenControlText.prop("readonly",true);
    	}
    	else {
    		tokenControlText.prop("readonly",false);
    	}    	        	
    },

    //TODO: Check how to make it work, so we don't need to call a function in every controller to clear the value
    onBeforeShow: function(){
    	console_log(" ::::: onBeforeShow of TOKEN CONTROL ::::: ");
    	var oControl = this;
    	$("#" + oControl.getViewName() +"_Token_Control_TXT" ).val("");
    },

    getValue: function(  ){
    	console_log(" :: TOKEN GET VALUE() :: ");
    	return $('#'+this.id).val();
    },
    
    clear: function(  ){
    	console_log(" :: TOKEN clear() :: ");
    	try{
    		this.setTokenReadOnlyProperty();
			this.setTokenValue('');
			this.markValid();
    	} catch (e){
    		AXX_EXCEPTION_APP("Token error", "Error in clearing the TOKEN");
    		return false;
    	}
    },

	validate: function (isValidateWithBackend, noUnbusyView) {

		if (this.isVisible) {
			var myValue = $('#' + this.id).val();
			if (myValue == "" || myValue.length < Common.Validations.TOKEN_MAX_RANGE.length) {
				var obError = {
					error: true,
					title: fn_APP_CONF_GET_TEXT("messageFile", "token.verify.invalid.title"),
					message: fn_APP_CONF_GET_TEXT("messageFile", "token.verify.invalid.message"),
					component: this
				};

				this.markValidationError();
				if (obError.error == true) {
					throwValidationError(obError);
				}

				return false;
			}
			if (isValidateWithBackend) {
				this.verifyToken(this.viewController.getView(), noUnbusyView);
			} else {
				return true;
			}
		}
		else {
			return true;
		}
	},

	validateForPassword : function () {
		
		var myValue = $('#' + this.id).val();
		if (myValue == "" || myValue.length < Common.Validations.TOKEN_MAX_RANGE.length) {
			
			var obError = {
				error: true, 
				title: fn_APP_CONF_GET_TEXT( "messageFile", "token.verify.invalid.title" ), 
				message: fn_APP_CONF_GET_TEXT( "messageFile", "token.verify.invalid.message" ), 
				component: this
			};
			// this.markValidationError();
			return obError;
		}

		return {error : false};
		
	},

	generateTokenForMobile : function ( obView ) {

		try{
			var myBpID = myCurrentData.DEVICE.get_registered_user().split("|")[0];

			var nonce = generateNonce();
			var encryptedBpId = encryptMessage(myBpID,nonce);

			var wsParameters = new WSCaller().setSuccessCallback( this.generateTokenMobileSuccess );
			wsParameters.setFailureCallback( this.generateTokenMobileFailure );
			wsParameters.setCustomData({
				controller : sap.ui.getCore().byId( myCurrentData.TOKEN.viewName ).getController(),
				bpId: encryptedBpId,
				optData : toHex(nonce),
				publicKey : getClientPublicKey()
			});
			var wsRequest_SMS = new WS( TOKEN_getMobileToken, wsParameters );

			var wsWrapper = new WSPackageWrapper().setWsArray([wsRequest_SMS]).setController(this.viewController);
			wsWrapper.startWS();

			//GENERAL_ENROLL_Token_Register_MOCK( myBpID, myActivation_Code, this.enroll_Token_Register_SUCCESS, this.enroll_Token_Register_FAILURE);

		} catch(e){
			app.setBusy(sap.ui.getCore().byId( myCurrentData.TOKEN.viewName ), false);
			AXX_EXCEPTION_CATCH(e);
		}

	},

	generateTokenMobileSuccess : function(data, customData){
		
		var oCon = sap.ui.getCore().byId( myCurrentData.TOKEN.viewName ).getController();
		//var oCon = customData.controller;
		oCon.getView().token_key.setTokenValue(data.mobileToken);

		//For recipients
		try{
			oCon.navigator.jsonData.oData.recipientModel.mobileToken = data.mobileToken;
		} catch(e){}

	},

	generateTokenMobileFailure : function(jsonData){

		try{
			var stMessage = jsonData.Status.value;
			if ( isEmpty( jsonData.Status.value ) ){
				throw 1;
			} 
		} catch(e){
			var stMessage = fn_APP_CONF_GET_TEXT( "messageFile", "token.register.check.fail_message" );
		}

		AXX_MESSAGE_WARNING(
			"",
			stMessage,
			null
		);

	},

	verifyToken : function( obView, noUnbusyView ){

		app.setBusy(obView, true);

		try{
			var myActivation_Code = obView.token_key.getValue();

			if (myCurrentData.IS_MOBILE)
				var myBpID = myCurrentData.DEVICE.get_registered_user().split("|")[0];
			else
				var myBpID = myCurrentData.INFO.get_User_ID();

			var nonce = generateNonce();
			var encryptedBpId = encryptMessage(myBpID,nonce);

			var wsParameters = new WSCaller().setSuccessCallback(this.verifyMobileTokenSuccess);
			wsParameters.setValidationCallback( this.verifyMobileTokenFailure );
			wsParameters.setCustomData({
				controller : this.viewController,
				bpId: encryptedBpId,
				mobileToken: myActivation_Code,
				noUnbusyView: noUnbusyView,
				optData : toHex(optData)
			});
			var wsRequest = [new WS( TOKEN_verifyMobileToken, wsParameters )];

			var wsWrapper = new WSPackageWrapper().setWsArray(wsRequest);

			if (isEmpty(noUnbusyView) || !noUnbusyView)
				wsWrapper.setController(this.viewController);
			
			wsWrapper.startWS();

		} catch(e){
			app.setBusy(obView, false);
			AXX_EXCEPTION_CATCH(e);
		}

	},

	verifyMobileTokenSuccess : function (jsonData, customData) {

		console_log("verifyMobileTokenSuccess: "+customData+ ' - ' + jsonData);

		var tokenKey = customData.controller.getView().token_key;

        tokenKey.markValid();
		var tokenValue = tokenKey.getValue();

		if (isEmpty(customData.noUnbusyView) || !customData.noUnbusyView)
			app.setBusy(customData.controller.getView(), false);

		//TOOD RWa change te response structure
		if(jsonData.isValidToken && jsonData.Status.code == TOKENS_CONST.TOKEN_IS_VALID){
			customData.controller.onTokenValidAction(tokenValue);
		} else if (jsonData.Status.code == TOKENS_CONST.TOKEN_IS_INVALID || jsonData.Status.code == TOKENS_CONST.TOKEN_IS_EXPIRED){
			displayTokenFailValidationMessage(jsonData);
		}

	},

	verifyMobileTokenFailure : function (jsonData, customData) {
		console_log("verifyMobileTokenFailure: " + jsonData)
		var tokenKey = customData.controller.getView().token_key;

		var errorMessage = ''

		// if(jsonData.Status.code = WSExceptions.UNDEFINED_ERROR){
		// 	errorMessage = fn_APP_CONF_GET_TEXT( "messageFile", "MESSAGE_WS_GENEREAL_EXCEPTION" );
		// } else {
			errorMessage = jsonData.Status.value;
		// }


		tokenKey.markValidationError();

		customData.controller.onTokenInValidAction(tokenKey, errorMessage);
		app.setBusy(customData.controller.getView(), false);
	},

    markValidationError:function(){
    	$('#'+this.id).removeClass( "Token_Control_Input" ).addClass( "Token_Control_Input_Error" );
    },
    
    markValid:function(){
    	$('#'+this.id).removeClass( "Token_Control_Input_Error" ).addClass( "Token_Control_Input" );
    },

	//TODO why necessary
	setNameOfView:function(viewName){
		this.viewName = viewName;
	},

	setVisible: function(visible){
		console_log('TOKEN setVisible '+'#Token_Wrapper_' + this.viewName + ' = '+visible);

		this.isVisible = visible;
		$('#Token_Wrapper_' + this.viewName).css('display',visible ? 'block' : 'none');
	},

	setEnabled :function(enabled){

	},


});

var displayTokenFailValidationMessage = function (jsonData) {

	AXX_MESSAGE_WARNING(
		'',
		jsonData.Status.value,
		function(){
			// Navigation_Invoke_Page({
			// 	PageFrom: Common.Navigations.PAGE_TOKEN_REGISTER,
			// 	PageTo: Common.Navigations.PAGE_SIGN_IN
			// });
		}
	);
};

var TOKEN_ACTION_NUMBERS = function ( object ) {
	if (object.value.length > object.max.length)
		object.value = object.value.slice(0, object.max.length)
};