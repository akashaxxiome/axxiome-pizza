sap.ui.core.Control.extend("control.commons.ProfileDisplay_Control", {

    metadata : {                              
        properties : {
        	"displayName" 	: "string",
	        "displayDate" 	: "string",
	        "boDisplayDate" : "boolean",
        }
    },

    renderer : function(oRm, oControl) {
    	
    	oRm.write("<div class='Home_MyProfileDisplay_DIV' >");
    	
    		oRm.write("<div style=\"float: left; \">");
    			//oRm.write( "Hola, " );
    			oRm.write( "<span id='ProfileDisplay_DisplayName' style='color: Navy; font-weight: bold; '>" );
    			oRm.write( oControl.getDisplayName() );
    			oRm.write( "</span>" );
    		oRm.write("</div>");
    		
    		if ( oControl.getBoDisplayDate() ) {
    		
	    		oRm.write("<div class='Home_MyProfileDisplay_DIV_LastDate' style=' width: 100% !important; ' >");
	    			oRm.write(fn_APP_CONF_GET_TEXT("i18N", "profileDisplayControl.lastAccess"));
	    			oRm.write( "<span id='ProfileDisplay_DisplayDate' style='color: Navy;'>" );
	    			oRm.write( oControl.getDisplayDate() );
	    			oRm.write( "</span>" );
	    		oRm.write("</div>");
	    		
    		}
    	
    	oRm.write("</div>");
    }

});
