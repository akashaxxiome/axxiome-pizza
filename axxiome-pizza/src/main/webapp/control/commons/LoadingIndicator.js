sap.ui.core.Control.extend("control.commons.LoadingIndicator", {
 
	metadata : {
	},

	renderer : function(oRm, oControl) {
		/*
		var controlHtml = "<svg id='__indicator0-svg' viewBox='0 0 100 100' class='sapMBusySvg' style='width:1.7em;height:1.7em'><g transform='translate(50,50)'><path d='M0,-36A36,36 0 1,0 36,0' stroke-width='20%' fill='none' class='sapMSpinSvg' transform='rotate(333.453)'><animateTransform attributeName='transform' attributeType='XML' type='rotate' from='0' to='360' dur='1.1s' repeatCount='indefinite'></animateTransform></path></g></svg>";
		//Create wrapper div for the indicator
		oRm.write("<div id='loadingIndicator' class='loadingIndicatorHolder'"); 
		oRm.writeControlData(oControl);  // writes the Control ID and enables event handling - important!
        oRm.writeClasses();              // this call writes the above class plus enables support
        oRm.write(">");
		//Add menu content
        oRm.write(controlHtml);
        oRm.write("</div>");
        */
	},
	
	onAfterRendering:function(){
        //Nothing
	},

	
	show : function(flag){
		jQuery("#loadingIndicator").show();	
	},
	
	hide : function(){
		jQuery("#loadingIndicator").hide();
	}
});
