/**
 * This file consist overridden NavContainer for OCB Mobile app.
 * Here, sap.m.NavContainer.to() method has been overridden.
 */
sap.m.NavContainer.extend("control.OCBNavContainer", {
	/**
	 * Overriden method to pass OCB specific default navigation animation / effect.
	 */
	to : function(sPageId, sTransitionName, oData, oTransitionParameters){
		//Check if sTransitionName passed in, is of type object
		if( sTransitionName && typeof sTransitionName === 'object' && !oData ){
			sap.m.NavContainer.prototype.to.apply(this, [sPageId, theAnimation, sTransitionName, oTransitionParameters]);
		} else if( sTransitionName && typeof sTransitionName === 'string' ){
			// Call Super class functionality.
			sap.m.NavContainer.prototype.to.apply(this, [sPageId, theAnimation, oData, oTransitionParameters]);
		} else {
			// Call Super class functionality.
			sap.m.NavContainer.prototype.to.apply(this, [sPageId, theAnimation, oData, oTransitionParameters]);			
		}		
	},
	
});


