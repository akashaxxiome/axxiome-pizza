sap.ui.core.Control.extend("control.tdd.TDD_List_Header_Control", {

    metadata : {                              
        properties : {
        	//"displayName" 	: "string",
        }
    },

    renderer : function(oRm, oControl) {
    	
    	oRm.write("<div class='CPTMS_HEADER_CONTROL' style=' ' >");
    	
    		oRm.write("<div class='CPTMS_HEADER_CONTROL_ROW' style=' ' >");
    		
    			//oRm.write( "<div class='TDD_List_Header_COLUMN_A' style=' ' >" );
    				oRm.write( "<div class='CPTMS_HEADER_CONTROL_CELL TDD_List_Header_CELL_A' style=' ' >" );
    					oRm.write(fn_APP_CONF_GET_TEXT("i18N","tddListHeaderControl.active"));
    				oRm.write( "</div>" );
    			//oRm.write( "</div>" );
    			
    			//oRm.write( "<div class='TDD_List_Header_COLUMN_B' style=' ' >" );
    				oRm.write( "<div class='CPTMS_HEADER_CONTROL_CELL TDD_List_Header_CELL_B' style=' ' >" );
    					oRm.write(fn_APP_CONF_GET_TEXT("i18N","tddListHeaderControl.cardNumber"));
    				oRm.write( "</div>" );
    			//oRm.write( "</div>" );
    			
    			//oRm.write( "<div class='TDD_List_Header_COLUMN_C' style=' ' >" );
    				oRm.write( "<div class='CPTMS_HEADER_CONTROL_CELL TDD_List_Header_CELL_C' style=' ' >" );
    					oRm.write(fn_APP_CONF_GET_TEXT("i18N","tddListHeaderControl.action"));
    				oRm.write( "</div>" );
    			//oRm.write( "</div>" );
    			
    		oRm.write("</div>");
    		
    	
    	oRm.write("</div>");
    }

});
