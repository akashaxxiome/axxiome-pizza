sap.ui.jsfragment("fragment.footers.desktop.APP_FOOTER_SIGNIN", {
	
	createContent: function(oCon) {

		this.divContent = new sap.m.HBox({
			width: "100%",
			height: "100%",
			justifyContent : sap.m.FlexJustifyContent.End ,
			items:[
				new sap.ui.core.HTML({
					content: 
						'<div id="pizzaFooter">' +
							'<div class="div-table centered">' +
								'<div class="div-table_row centered">' +
									'<div class="div-table_column footerLogo">' +
										'<img src="./img/axx_s.png">' +
									'</div>' +
									'<div class="div-table_column footerContact">' +

										'<div class="div-table centered">' +
											'<div class="div-table_row">' +
												'<div class="div-table_column width-70">' +
													'<div>Call Now</div>' +
													'<div class="font_fone cursor_pointer" onclick="call_Phone();">(780) 962-6000</div>' +
												'</div>' +
												'<div class="div-table_column div_cart width-30">' +
													'<div class="div-table centered">' +
														'<div class="div-table_row ">' +
															'<div class="div-table_column  ">' +
																"<div id='topFB'><a href='https://www.facebook.com/ATBFinancial/' target='_blank'>"+
																	"<img src='img/social/face.png' />" +
																"</a></div>" +
															'</div>' +
															'<div class="div-table_column  ">' +
																"<div id='topTwitter'><a href='https://twitter.com/atbfinancial' target='_blank'>" +
																	"<img src='img/social/twitter.png' />" +
																"</a></div>" +
															'</div>' +
															'<div class="div-table_column  ">' +
																"<div id='topYT'><a href='https://www.youtube.com/user/ATBFinancialVids' target='_blank'>" +
																	"<img src='img/social/youtube.png' />" +
																"</a></div>" +
															'</div>' +
														'</div>' +
													'</div>' +
												'</div>' +
											'</div>' +
										'</div>' +

									'</div>' +
								'</div>' +
							'</div>' +
						'</div>'
				})
			]
		});
		
	    // THE M.BAR
		this.footerBar = new sap.m.Bar({
			design : sap.m.BarDesign.Footer,
			contentLeft: [
				this.divContent
			],
			contentMiddle :[
				
				// new sap.ui.core.HTML({ content: "" +
				// 	"<div class=''>Version Beta" + 
				// 		//" <img src='" + IMAGES.FOOTER.DESKTOP.LOGO_GENTERA + "' />" +
				// 		"<img src='img/footer/temp.JPG'" + /*IMAGES.FOOTER.DESKTOP.LOGO_GENTERA*/ + " />" +
						
				// 		//" | Rev. " + myCurrentData.appVersion +
						
				// 	"</div>" 
				// }),

				// this.divContent

            ],
			
        }).addStyleClass("Compartamos_app_FOOTER ");

		return this.footerBar;

	}

});