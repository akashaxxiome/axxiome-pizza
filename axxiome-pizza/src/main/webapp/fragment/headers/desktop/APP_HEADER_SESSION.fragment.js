sap.ui.jsfragment("fragment.headers.desktop.APP_HEADER_SESSION", {
	
	createContent: function(oCon) {

		this.div_PizzaHeader_HeaderContent = new sap.m.HBox({
			items:[

				new sap.m.HBox({
					items:[

						new sap.m.VBox({
							items:[
								new sap.ui.core.HTML({
									content: '<a href="#" onclick="Navigation_GoHome();" class="font_white"><img src="./img/pizza-slice.png" />ATB Pizza</a>' 
								})
							]
						}).addStyleClass("div-table_column logo"),
						new sap.m.VBox({
							items:[
								new sap.m.Button({
									text: "",
									tap: function(){
										// Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_EXAMPLE_SIMPLE });
									}
								}).addStyleClass("width-100"),
							]
						}).addStyleClass("div-table_column user width-10"),
						new sap.m.HBox({
							items:[
								new sap.m.Label({
									text: "Welcome, " + myCurrentData.INFO.get_User_DisplayName()
								}).addStyleClass(" font_black align_right"),
							]
						}).addStyleClass("div-table_column  align_left width-50"),
						new sap.m.VBox({
							items:[
								// new sap.m.Label({
								// 	text: "[ My Account ]"
								// }).addStyleClass(" font_navy padding-right-1 cursor_pointer")
							]
						}).addStyleClass("div-table_column login align_right width-10 "),
						new sap.m.VBox({
							items:[
								// new sap.m.Image({
								// 	src: "img/common/logout.png"
								// })
								new sap.ui.core.HTML({
									content: '<a href="#" onclick="myCurrentData.INFO.logOut();" class=""><img src="./img/common/logout.png" /></a>' 
								})
							]
						}).addStyleClass("div-table_column width-10 logout") //empty column, to give some space
						
					]
				}).addStyleClass("div-table centered")

			]
		}).addStyleClass("headerContent");

		this.headerBarContent = new sap.m.VBox({
			items:[
				this.div_PizzaHeader_HeaderContent
			]
		}).addStyleClass("pizzaHeader");

	
	    // THE M.BAR
		this.headerBar = new sap.m.Bar({
			design : sap.m.BarDesign.Header,
			translucent: true,
            contentLeft: [
				this.headerBarContent
            ],
			// contentMiddle :[],
            // contentRight: []
        }).addStyleClass("");


        if ( sap.ui.Device.browser.firefox ){
        	this.headerBar.addStyleClass("header_firefox");
        }




		return this.headerBar;

	}

});