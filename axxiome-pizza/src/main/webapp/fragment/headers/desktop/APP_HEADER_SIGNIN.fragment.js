sap.ui.jsfragment("fragment.headers.desktop.APP_HEADER_SIGNIN", {
	
	createContent: function(oCon) {

		this.div_PPizzaHeader_HeaderContent = new sap.m.HBox({
			items:[

				new sap.m.HBox({
					items:[

//						new sap.m.VBox({
//							items:[
//								new sap.m.Button({
//									text: "HOME",
//									tap : function() {
//								        Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_SIGN_IN });
//								    }
//								})
//							]
//						}).addStyleClass("div-table_column"),
//						new sap.m.VBox({
//							items:[
//								new sap.m.Button({
//									text: "MY ORDER",
//									tap : function() {
//								        Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_ORDER_HISTORY });
//								    }
//								})
//							]
//						}).addStyleClass("div-table_column"),
						new sap.m.VBox({
							items:[
								new sap.ui.core.HTML({
									content: '<a href="#" onclick="Navigation_GoHome();" class="font_white"><img src="./img/pizza-slice.png" />ATB Pizza</a>' 
								})
							]
						}).addStyleClass("div-table_column logo"),
//						new sap.m.VBox({
//							items:[
//								new sap.m.Button({
//									text: "",
//									tap: function(){
//										Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_EXAMPLE_SIMPLE });
//									}
//								})
//							]
//						}).addStyleClass("div-table_column user"),
						new sap.m.VBox({
							items:[
								new sap.m.Label({

								}).addStyleClass("font_regular font_black padding-right-1")
								// new sap.m.Button({
								// 	text: "SIGN UP",
								// 	tap : function() {

								// 	   	var obParameters = {
								// 	   		allowOutsideClick : true,
								// 	   		boShowCancelButton: false,
								// 	   		fn_callBack_YES : function(){
								// 	   			// SIGNIN.authenticate_SUCCESS();
								// 	   		},
								// 	   		arButtonTexts : [
								// 	   			"Close", "Cancel"
								// 	   		]
								// 	   	};

								//         DISPLAY_MESSAGE_HTML( "SIGN UP", SIGNIN.stPopup_SIGNUP, obParameters );

								//     }
								// })
							]
						}).addStyleClass("div-table_column signup align_right"),
						new sap.m.HBox({
							items:[
								new sap.m.Button({
									text: "STAFF LOG IN",
									tap : function() {

									   	var obParameters = {
									   		allowOutsideClick : true,
									   		closeAfterOK : false,
									   		arButtonTexts : [
									   			"Cancel",
									   			"Log in"
									   		],
									   		fn_callBack_YES : function(){
									   			swal.resetInputError();
									   			SIGNIN.authenticate_tap_PopUp_Staff();
									   		},
									   	};

								        DISPLAY_MESSAGE_HTML( "STAFF LOG IN", SIGNIN.stPopup_LOGIN_STAFF, obParameters );

								    }
								}).addStyleClass(""),

								new sap.m.Button({
									text: "LOG IN",
									tap : function() {

									   	var obParameters = {
									   		allowOutsideClick : true,
									   		closeAfterOK : false,
									   		arButtonTexts : [
									   			"Cancel",
									   			"Log in"
									   		],
									   		fn_callBack_YES : function(){
									   			swal.resetInputError();
									   			SIGNIN.authenticate_tap_PopUp();
									   		},
									   	};

								        DISPLAY_MESSAGE_HTML( "WELCOME USERS", SIGNIN.stPopup_LOGIN, obParameters );

								    }
								}).addStyleClass("")
							]
						}).addStyleClass("div-table_column login align_right"),
						new sap.m.VBox({}).addStyleClass("div-table_column width-05") //empty column, to give some space
						
						
					]
				}).addStyleClass("div-table centered")

			]
		}).addStyleClass("headerContent");

		this.headerBarContent = new sap.m.VBox({

			items:[
				this.div_PPizzaHeader_HeaderContent
			]

		}).addStyleClass("pizzaHeader");

	
	    // THE M.BAR
		this.headerBar = new sap.m.Bar({
			design : sap.m.BarDesign.Header,
			translucent: true,
            contentLeft: [
				this.headerBarContent
            ],
			// contentMiddle :[],
            // contentRight: []
        }).addStyleClass("");


        if ( sap.ui.Device.browser.firefox ){
        	this.headerBar.addStyleClass("header_firefox");
        }


		return this.headerBar;

	}

});