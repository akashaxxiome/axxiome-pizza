sap.ui.jsfragment("fragment.generic.Generic_Title", { 
	
	createContent: function(oCon) {

		var mySubHeader = new sap.m.HBox({
			width:"100%",
			items: [
			    new sap.m.VBox({
			    	width: "0%",
			    	visible: false,
			    	justifyContent : sap.m.FlexJustifyContent.Center,
			    	alignItems : sap.m.FlexAlignItems.Center ,
			    	items: [
					    new sap.m.Image({ 
					    	densityAware: false,
					    	id: this.createId("Title_IMG"), 
					    	src: "", 
					    }).addStyleClass("FRAG_TITLE_IMG_DIV_IMG"), 
					]
			    }).addStyleClass("FRAG_TITLE_IMG_DIV"),
				
			    new sap.m.VBox({
			    	justifyContent : sap.m.FlexJustifyContent.Center,
			    	alignItems : sap.m.FlexAlignItems.Center ,
			    	items: [
						new sap.m.Text({ 
							id: this.createId("Title_TEXT"),
						}).addStyleClass("FRAG_TITLE_TEXT_DIV_TEXT")
					]
			    }).addStyleClass("FRAG_TITLE_TEXT_DIV")
			],
			
	    }).addStyleClass("FRAG_TITLE");
				
		return mySubHeader;
		
	} 

});