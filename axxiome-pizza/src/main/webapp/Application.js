jQuery.sap.declare("Application");
jQuery.sap.require("sap.ui.app.Application");
//jQuery.sap.require("sap.banking.ui.services.ServiceFactory");

sap.ui.app.Application.extend("Application", {
	
	init : function() {
		// set global models
		this.set_Value_Models();
		
		//Get the resources based on the current language
		this.set_Language_Models();

		setIdleForInternetExplorer();
	},
	
	main : function() {
		// create app view and put to html root element
		var root = this.getRoot();
//		console.log( root );
		var theView = sap.ui.jsview("app", "view.App");
		theView.placeAt(root);
		//sap.ui.jsview("app", "view.App").placeAt("app");
	},
	
	set_Value_Models: function(){
//		var resourceJSONModel = new sap.ui.model.json.JSONModel("model/resourceFile.json");
		var settingsImageModel = new sap.ui.model.json.JSONModel("model/settings.json");
		var paymentCategoryImageModel = new sap.ui.model.json.JSONModel("model/paymentCategory.json");
		
//		sap.ui.getCore().setModel(resourceJSONModel, Common.GlobalModelNames.StringResources);
		sap.ui.getCore().setModel(settingsImageModel, Common.GlobalModelNames.SETTINGS_IMAGES);
		sap.ui.getCore().setModel(paymentCategoryImageModel, Common.GlobalModelNames.PAYMENTS_CATEGORY_IMAGES);
	},
	
	set_Language_Models: function(){
		
		var resourceModel;
		var messageModel;
		
//		var locale = sap.ui.getCore().getConfiguration().getLanguage();
		
//		if( locale != undefined && locale != '' ){
//			try {
//				console.log("a1");
//				resourceModel = new sap.ui.model.resource.ResourceModel({bundleUrl:"model/resources.properties", bundleLocale: locale});
//				console.log("a2");
//				messageModel = new sap.ui.model.resource.ResourceModel({bundleUrl:"model/messages.properties", bundleLocale: locale});
//				console.log("a3");
//			} catch(e){
//				console.log("b");
//				resourceModel = new sap.ui.model.resource.ResourceModel({bundleUrl:"model/resources.properties", bundleLocale:"es"});
//				messageModel = new sap.ui.model.resource.ResourceModel({bundleUrl:"model/messages.properties", bundleLocale:"es"});
//			}
//		} else {
			resourceModel = new sap.ui.model.resource.ResourceModel({bundleUrl:"model/resources.properties", bundleLocale:"en"});
			messageModel = new sap.ui.model.resource.ResourceModel({bundleUrl:"model/messages.properties", bundleLocale:"en"});
//		}
		
		sap.ui.getCore().setModel(	messageModel, 	Common.GlobalModelNames.MESSAGE_FILE );
		sap.ui.getCore().setModel(	resourceModel, 	Common.GlobalModelNames.I18N );
		
	},
	
});