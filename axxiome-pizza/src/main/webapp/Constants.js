/**
* Javascript file containing all the constants.
*/
jQuery.sap.declare("Constants");
/** Common Constants used in Application */

var myCurrentData = {
	INFO: null, //Session Information
	isLoggedIn : false,
	IS_MOBILE: null,
	IS_SSL : false,
	DEVICE: {},
	activeWebService : {},
	appEnv: "Trunk",
	appVersion: ( 3 + 1 ),
	TEMP: {}
};

bankingModules = []; //module list



Common = {
	Actions : {
		logout : "logout",
		timeout: "timeout"
	},
	BankData:{
		BANK_NAME: "Compartamos",
		BANK_KEY: 40130
	},
	Branding: {
		BUTTON_COLOR: "#970050"
	},
	Contact:{
		PHONE_NUMBER: "780 962 6000",
		EMAIL_ADDRESS: "",
		EMAIL_SUBJECT: "",
	},
	DateFormat:{
		SHORT:'MM/dd/yyyy'
	},
	CurrentyFormat:{
		THOUSAND_SEPARATOR : ",",
		DECIMAL_SEPARATOR : ".",
		CURRENCY: "MXN",
	},
	Operations:{
		ADD : "Add",
		EDIT : "Edit",
	},
	ScreenWidths:{
		MOBILE_MENU: "0%",
		MOBILE_CONTENT: "100%",
		DESKTOP_MENU: "25%",
		DFESKTOP_CONTENT: "75%",
	},
	Session:{
		
		APP: {
			CHECK_PERIOD: 2000, //3 seconds between each check
			POPUP_IN_PERIOD: 11000, //10 seconds before expire
			POPUP_COUNTER: 5,
		},
		WEB : {
			CHECK_PERIOD: 5000, //5 seconds between each check 
			POPUP_IN_PERIOD: 1130000, //62 seconds before expire
			POPUP_COUNTER: 60,
		}
		
	},
	ModelNames:{
		CUSTOMER: "CUSTOMER",
		USER_INFO : "USER_INFO",
		PIZZAS_TOPPINGS : "PIZZA_TOPPINGS",
		PIZZAS_SAUCES : "PIZZAS_SAUCES",
		PIZZAS_CRUSTS : "PIZZAS_CRUSTS",
		PIZZAS_STANDARDS : "PIZZAS_STANDARDS",
		ORDERS_STATUS : "ORDERS_STATUS",
	},
	Validations : {
		TOKEN_MAX_RANGE: 999999,
		CAPTCHA_LENGTH: 6,
		TOKEN_MASK: "999999",
		HOME_WALLET_CHECK_PERIOD: 30000
	},
	Shares:{
		ACCOUNT:{
			HOW_TO_SHARE: "¿Como quieres compartir lar cuenta?",
			EMAIL_SUBJECT: "Cuenta {0} Compartamos Banco",
			EMAIL_BODY: "Cuenta: {0} %0D%0ACuenta CLABE: {1} %0D%0ATEST",
		}
	},
	GlobalModelNames:{
		PRODUCTS_IMAGES: "ProductImages",
		SETTINGS_IMAGES: "SettingsImages",
		PAYMENTS_CATEGORY_IMAGES: "PaymentImages",
		I18N: "i18N",
		IMAGES: "IMAGES",
		StringResources: "resourceFile",
		MESSAGE_FILE: "messageFile",
	},
	Navigations:{
		PAGE_LOG_IN: "view.Login",
		PAGE_SIGN_IN: "SignIn",
		PAGE_HOME: "Home",
		
		PAGE_CONTACT: "Contact",
	},



	Views: {
		
		PAGE_LOG_IN: 	{ name: "Login", 	path: "view.Login" },
		PAGE_SIGN_IN: 	{ name: "SignIn", 	path: "view.Signin" },
		PAGE_HOME: 		{ name: "Home", 	path: "view.Home" },

		PAGE_EXAMPLE_SIMPLE: 	{ name: "Example_Simple", 	path: "view.Example.Simple" },

		PAGE_AUTHENTICATION: 	{ name: "Authentication", 	path: "view.Authentication.Authentication" },
		PAGE_REGISTRATION: 	{ name: "Registration", 	path: "view.Authentication.Registration" },
		
		PAGE_NEW_ORDER:			{ name: "New_Order",		path: "view.Orders.NewOrder" },
		PAGE_NEW_ORDER_CUSTOM: { name: "New_OrderCustom", path: "view.Orders.NewOrderCustom" },

		PAGE_ORDER_HISTORY: 	{ name: "Order_History", 	path: "view.Orders.OrderHistory" },
		PAGE_STAFF_ORDER_HISTORY: 	{ name: "Staff_Order_History", 	path: "view.Orders.StaffOrderHistory" },
		
		PAGE_ORDER_SUMMARY:		{ name: "Order_Summary",	path: "view.Orders.OrderSummary" },	
		PAGE_STAFF_ORDER_SUMMARY:		{ name: "Staff_Order_Summary",	path: "view.Orders.StaffOrderSummary" },	
		
		PAGE_STAFF_COMPLETED_ORDER_HISTORY: 	{ name: "Staff_Completed_Order_History",	path: "view.Orders.StaffCompletedOrderHistory" },
		//Tests
		PAGE_NEW_ORDER_DIEGO:	{ name: "New_Order_Diego",		path: "view.Orders.NewOrderDiego" },
	
	},


	
	MASKS : {
		MONEY : "99999999.99",
		ACCOUNT_NUMBER: "99999999999",
		DEBIT_CARD: "9999999999999999",
		LOAN_NUMBER: "99999999999",
		PASSWORD: "AAAAAAAAAAAAAAAA",
	},
	TransactionStatus : {
		COMPLETED : "COMPLETED",
		PENDING : "SCHEDULED",
		DELETED : "DELETED"
	},
	Animation : {
		NAV_ANIMATION : "slide",
		FADE_ANIMATION: "fade",
		SLIDE_ANIMATION: "slide",
		FLIP_ANIMATION: "flip",
		SHOW_ANIMATION: "show",
	},
	NETWORK_TIMEOUT_HANDLER:{
		PRE_EXECUTION: 500,
		MAX_TENTATIVES: 3
	},
	ErrorTypes:{
		VALIDATION_ERROR:"VE",
		SERVICE_ERROR:"SE",
		BUSINESS_ERROR:"BE",
		MFA_ERROR:"MFA",
		ODATA_ERROR:"OE",
		UAF_ERROR:"UAF",
		ODATA_VALIDATION_ERROR:"OVE",
		HTTP_ERROR:"ECONNREFUSED",
		NETWORK_OFFLINE_ERROR:"NOE",
	},
	WebServices: {
		TIME_OUT: ( 40 ) * 1000,//TODO set timeout time
		ASYNCHRONOUS_SERVICES_TIME_CHECK: 1000,
	},
	MODULES : {
		ENROLLMENT : "ENROLLMENT",
		FORGOT_PASSWORD : "FORGOT_PASSWORD",
	},
	FOOTER : {
		URL_PREFIX		: "https://www.compartamos.com.mx",
	}
};
bankingModules.push(Common);



IMAGES = {

	MAIN : {
		HOME			: "./img/menu/Home.svg",
		SETTINGS		: "./img/menu/Settings.svg",
	},
	
	HEADER : {
		DESKTOP : {
			LOGO			: "./img/header/logoCompartamos.svg",
			LOGO_SIGN_IN 	: "./img/header/header_logo_signin_desktop.png",
			TELEPHONE 		: "./img/header/header_signin_telephono.svg",
		},
		MOBILE : {
			LOGO	: "./img/header/header_logo.svg",
			BACK	: "./img/header/header_back_button.svg",
			MENU	: "./img/header/header_menu_button.svg",
			LOGOUT	: "./img/header/header_logout_button.svg",
		},
	},
	
	FOOTER : {
		DESKTOP:{
			LOGO_GENTERA		: "./img/footer/desktop/gentera.png",
			LOGO_IPAB			: "./img/footer/desktop/ico_ipab.png",
			LOGO_CONDUSEF		: "./img/footer/desktop/ico_condusef.png",
			LOGO_BURO			: "./img/footer/desktop/ico_buro.png",
		},
		MOBILE:{
			FISHES_BG_WHITE 	: "./img/footer/fishes_bg_white.svg",
			FISHES_BG_MAGENTA 	: "./img/footer/fishes_bg_magenta.svg",
		}
	},

	POPUP : {
		SUCCESS: 	"./img/popup/success.png",
		WARNING: 	"./img/popup/warning.svg",
		ERROR: 		"./img/popup/error.svg",
	},

	ACCOUNTS : {
		EMPTY_LIST		: "./img/account/alert_no_account.svg",
	},
	
	COMMON : {
		TOKEN			: "./img/common/token_key.svg",
		USER			: "./img/common/user.svg",
		LOCK			: "./img/common/lockpad.svg",
		PRINTER			: "./img/common/printer.svg",
		EDIT			: "./img/common/edit.svg",
		DELETE			: "./img/common/delete.svg",
		LOADING			: "./img/common/loadingImage.gif",
		RESEND_CODE		: "./img/common/Refresh.svg",
		SMS				: "./img/common/sms.png",
		NETWORK_OFFLINE	: "./img/common/network.svg",
	},
	
	CUSTOMER : {
		QUESTION	: "./img/customers/frag_title_question.svg",
	},
	
	
	SETTINGS : {
		BENEFICIARY_ADD			: "",
		CHANGE_PASSWORD 		: "./img/settings/ChangePassword.svg",
		HALT_SERVICE		 	: "./img/settings/HaltService.svg",
	},
	
	SUPPORT : {
		PHONE		: "./img/contact/phone.svg",
		ENVELOP		: "./img/contact/envelop.svg",	
	},
	
	Encryption : {
		PUBLIC_KEY_SIZE_HEX : 64,
		PRIVATE_KEY_SIZE_HEX : 64,
		NONCE_SIZE_BYTES : 24,
		PUBLIC_KEY_SIZE_BYTES : 32,
		PRIVATE_KEY_SIZE_BYTES : 32,
	}

	
},

RegularExpression ={
	ACCEPT_UP_TO_TWO_DECIMAL_DIGITS : /^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/,
	ACCEPTS_ONLY_LETTERS : /^[A-z]+$/
},


Enrollment = {
	InputMasks:{
		MOBILE_NUMBER_2: "(999) 999-9999",
		DEBIT_CARD: "9999999999999999",
		BENEFICIENT_NAME_REGEXP:/^[a-zA-ZÁÉÍÓÚáéíóuñÑ\s]*$/
	},
	DOB_Types:{
		DDMM: { formatDisplay: "DDMM", formatSend: "ddMM" },
		MMAA: { formatDisplay: "MMAA", formatSend: "MMyy" },
		DDAA: { formatDisplay: "DDAA", formatSend: "ddyy" }
	},
	StepsNumbers:{
		AUTO_SIGN_UP_SERVICE: 4,
	},
	UseCases:{
		AUTO_SIGN_UP_SERVICE: "AUTO_SIGN_UP_SERVICE",
		SIGN_UP_CODE_SERVICE: "SIGN_UP_CODE_SERVICE",
	}
}
bankingModules.push(Enrollment);



