var tb_MAP_ERROR = {
		
	//CONNECTIVITY Error
	"0" : [ "MESSAGE_CONN_ERROR-TITLE", "MESSAGE_CONN_ERROR" ],
	"I" : [ "Internal Error", "There was an error \n Please, try again later." ],
	
	"NETWORK" : [ "Connection Error", "Please, check your network and try again" ],
	"USER_EXIT_APP_NETWORK" : [ "Cerrando el APP", "Esta aplicación sólo puede ser utilizado en conexión segura." ],
		
	//Authentication
	"B.19" : [ "MENSAJE B.19-TITLE", "MENSAJE B.19" ],
	"201" : [ "MESSAGE_C_002-TITLE", "MESSAGE_C_002" ],
	"329" : [ "MESSAGE_C_002-TITLE", "MESSAGE_C_002" ],
	
	"400" : [ "Not Found", "Service/Feature not found." ],
	"401" : [ "Error", "Acción no permitida. Sesión finalizado" ],
	"403" : [ "Error", "Acción no permitida. Sesión finalizado" ],
	"404" : [ "MESSAGE.ALERT.TITLE", "MESSAGE.ALERT.MESSAGE" ],
	"500" : [ "Error interno", "There was an error " ],

	"9998" : [ "ERR_TIMEOUT.title", "ERR_TIMEOUT.message" ],
	"9999" : [ "", "03_B35" ]
	
};