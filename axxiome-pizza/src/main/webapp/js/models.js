
function WSCaller(success_callback, failure_callback, validation_callback, mapOfFieldsOnView, customData){
	this.success_callback = success_callback;
	this.failure_callback = failure_callback;
	this.validation_callback = validation_callback;
	this.mapOfFieldsOnView = mapOfFieldsOnView;
	this.customData = customData;
}

WSCaller.prototype.setSuccessCallback = function(success){
	this.success_callback = success;
	
	return this;
}

WSCaller.prototype.setFailureCallback = function(failure){
	this.failure_callback = failure;

	return this;
}

WSCaller.prototype.setValidationCallback = function(validation_callback){
	this.validation_callback = validation_callback;

	return this;
}

WSCaller.prototype.setMapOfFieldsOnView = function(mapOfFieldsOnView){
	this.mapOfFieldsOnView = mapOfFieldsOnView;
	
	return this;
}

WSCaller.prototype.setCustomData = function(customData){
	this.customData = customData;
	
	return this;
}
