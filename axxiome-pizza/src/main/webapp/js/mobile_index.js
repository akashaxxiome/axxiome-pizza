function onResize() {
	
	
	console_log("==> onResize ..... ");
	var height = $('body').height();
	var width = $('body').width();
	
	try {
	    if (ios7){
	        if (height < 350){ // adjust this height value conforms to your layout
	            $('#demo4').hide();
	            $(".topInput :input").attr('readonly', true);
	        }
	        else {
	        	
	            $('#demo4').show();
	            var target = $('.sapMPageScroll');
	            target.animate({scrollTop: 0}, 300, "swing", function(){});
	            $(".topInput :input").attr('readonly', false);
	            $(".topInput :input").focus();
	            $("input").blur();
	            
	        }
	    } else {
	    	//CHECK THE RESOLUTION
	    	/* Commented because it was killing the MASKED FIELDS
	    	if ( height <= 1280 && width <= 720 ){
	    		myCurrentData.DEVICE.set_Screen_Menu_Width(15);
	    	} else {
	    		myCurrentData.DEVICE.set_Screen_Menu_Width(20);
	    	}
	    	*/
	    	var thePage = homeNavContainer.getCurrentPage();
	    	if ( (/iPhone|iPod|iPad|Android|BlackBerry/).test(navigator.userAgent) ){
				myCurrentData.IS_MOBILE = true;
			} else {
				myCurrentData.IS_MOBILE = false;
			}
	    	myCurrentData.DEVICE.set_DEVICE();

	    	$('#app').attr("width", width);
	    	
	    	
	    }
	    console_log("==> onResize ..... done");
	    
	} catch(e){
		console_log(e);
	}
    
}

function onKeypadScroll(elemId, scrollContainer){
	console_log("==> onKeypadScroll ..... ");
	if (ios7){
		setTimeout(function() {
			var p = $(elemId);
			var offset = p.offset();
			var target = $('#'+scrollContainer);
			target.animate({scrollTop: offset.top}, function(){
				setTimeout(function() {
					$(elemId).focus();
				}, 300);
			});
		}, 300);
	}
	console_log("==> onKeypadScroll ..... done");
}

function call_Phone(){
	sap.m.URLHelper.triggerTel( Common.Contact.PHONE_NUMBER.split(' ').join('') ); 
};