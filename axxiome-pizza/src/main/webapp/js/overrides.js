jQuery.sap.require("sap.ui.core.Control");
(function(){
	
	sap.ui.core.Control.prototype.setBusy = function(bBusy,sBusySection){
		this._sBusySection = sBusySection;
		var $this = this.$(this._sBusySection);

		//If the new state is already set, we don't need to do anything
		if (bBusy == this.getProperty("busy")) {
			return;
		}

		//No rerendering
		this.setProperty("busy", bBusy, true);
		
		if (bBusy) {
			this.addDelegate(oBusyIndicatorDelegate, false, this);
		} else {
			this.removeDelegate(oBusyIndicatorDelegate);
			//If there is a pending delayed call we clear it
			if (this._busyIndicatorDelayedCallId) {
				jQuery.sap.clearDelayedCall(this._busyIndicatorDelayedCallId);
				delete this._busyIndicatorDelayedCallId;
			}
		}
		
		//If no domref exists stop here.
		if (!this.getDomRef()) {
			return;
		}
		
		if (bBusy) {
			if (this.getBusyIndicatorDelay() <= 0) {
				fnAppendBusyIndicator.apply(this);
			} else {
				this._busyIndicatorDelayedCallId = jQuery.sap.delayedCall(this.getBusyIndicatorDelay(), this, fnAppendBusyIndicator);
			}
		} else {
			//Remove the busy indicator from the DOM
			this.$("busyIndicator").remove();
			this.$().removeClass('sapUiLocalBusy');
			
			//Reset the position style to its original state
			if (this._busyStoredPosition) {
				$this.css('position', this._busyStoredPosition);
				delete this._busyStoredPosition;
			}
			if (this._busyDelayedCallId) {
				jQuery.sap.clearDelayedCall(this._busyDelayedCallId);
				delete this._busyDelayedCallId;
			}
		}
	};
	
	oBusyIndicatorDelegate = {
		onAfterRendering: function() {
			if (this.getProperty("busy") === true && this.$()) {
				fnAppendBusyIndicator.apply(this);
			}
		}
	},
	fnAppendBusyIndicator = function() {
		var $this = this.$(this._sBusySection),
			aForbiddenTags = ["area", "base", "br", "col", "embed", "hr", "img", "input", "keygen", "link", "menuitem", "meta", "param", "source", "track", "wbr"];
		

		//If there is a pending delayed call to append the busy indicator, we can clear it now
		if (this._busyIndicatorDelayedCallId) {
			jQuery.sap.clearDelayedCall(this._busyIndicatorDelayedCallId);
			delete this._busyIndicatorDelayedCallId;
		}

		//Check if DOM Element where the busy indicator is supposed to be placed can handle content
		var sTag = $this.get(0) && $this.get(0).tagName;
		if (sTag && jQuery.inArray(sTag.toLowerCase(), aForbiddenTags) >= 0) {
			jQuery.sap.log.warning("Busy Indicator cannot be placed in elements with tag " + sTag);
			return;
		}
		
		//check if the control has static position, if this is the case we need to change it,
		//because we relay on relative/absolute/fixed positioning
		if ($this.css('position') == 'static') {
			this._busyStoredPosition = 'static';
			$this.css('position', 'relative');
		}

		var stMensagem = fn_APP_CONF_GET_TEXT("i18N", "common.lable.loading");
		if ( !isEmpty( myCurrentData.TEMP.loadingMessage ) ){
			stMensagem = myCurrentData.TEMP.loadingMessage;
		}

		//Append busy indicator to control DOM
		var $BusyIndicator = jQuery("<div class='sapUiLocalBusyIndicator' style='width: 100%;' >" +
			"<div class='sapUiLocalBusyIndicatorAnimation' style='display: table;'>" +
				"<div style='display: table-cell; align: center; text-align: center;' class='color_compartamos'>" +
					"<img height='50px' width='50px' src='" + IMAGES.COMMON.LOADING + "' />" + 
//					"<svg id='__indicator0-svg' viewBox='0 0 100 100' class='sapMBusySvg' style='width:1.7em;height:1.7em'><g transform='translate(50,50)'><path d='M0,-36A36,36 0 1,0 36,0' stroke-width='20%' fill='none' class='sapMSpinSvg' transform='rotate(333.453)'><animateTransform attributeName='transform' attributeType='XML' type='rotate' from='0' to='360' dur='1.1s' repeatCount='indefinite'></animateTransform></path></g>" +
//					"</svg>" +
					stMensagem +
				"</div>" +
			"</div>" +
		"</div>");
		$BusyIndicator.attr("id",this.getId() + "-busyIndicator");
		$this.append($BusyIndicator);
		$this.addClass('sapUiLocalBusy');
	};
})();