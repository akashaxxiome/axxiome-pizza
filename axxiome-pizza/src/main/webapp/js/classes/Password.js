function Password(inPassA, inPassB, inPassCurrent) {

	
	//PROPERTIES SECTION
	var myCurrrentPassword = inPassCurrent;
    var myPassword = inPassA;
    var myPasswordConfirm = inPassB;
	var clientNumber = ( myCurrentData.INFO != null ) ? myCurrentData.INFO.get_User_ID() : null; 
	var errorTitle = "";
	var errorMessage = "";
	var boError = false;
    var regEX = new RegExp("^[a-zA-Z0-9]+$", "g");
	
    
    
    //FUNCTION SECTION
    /***********************************************************
	 * 
	 * FUNCTION: 	validatePassword_Empty
	 * DESCRIPTION:	 
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: 		messages
	 * 
	 * ----------------   */
    var validatePassword_Empty = function( ){

    	if ( myPassword == "" || ( myPasswordConfirm != null && myPasswordConfirm == "" ) ){
    		boError = true;
    		errorTitle = "";
    		errorMessage = "BR-BE-0012";
    	}
    	
    };
  
    
    /***********************************************************
	 * 
	 * FUNCTION: 	validatePassword_Same
	 * DESCRIPTION:	 
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: 		messages
	 * 
	 * ----------------   */
     var validatePassword_Same = function( ){

    	if ( myPassword != myPasswordConfirm ){
    		boError = true;
    		errorTitle = "";
    		errorMessage = fn_APP_CONF_GET_TEXT( "messageFile" , "03_H2");
    	} else if ( myCurrrentPassword && ( myCurrrentPassword == myPassword ) ){
			boError = true;
			errorTitle = "";
			errorMessage = fn_APP_CONF_GET_TEXT( "messageFile" , "03_N16");
    	}
    	
    };
    
    /***********************************************************
	 * 
	 * FUNCTION: 	validatePassword_Characters
	 * DESCRIPTION:	According to the Category of the Product to be listed, there is a specific filter to be triggered 
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: 		messages
	 * -- THIS FUNCTION MY BE REPLACED BY THE WS CHECK
	 * ----------------   */
    var validatePassword_Characters = function(){
	  
    	var cont = 0;
    	var lastChar1 = "";
    	var lastChar2 = "";
	  
    	if ( myPassword.length < 8 ){
    		boError = true;
    		errorMessage = "BR-BE-0025";
    	} else if ( myPassword.length > 16 ) {
    		boError = true;
    		errorMessage = "BR-BE-0025";
    	} else {
		
    		while( cont < myPassword.length ){
		
    			var currentChar = myPassword[cont];
		
    			if ( lastChar1 == "" ){ 
    				lastChar1 = currentChar;
    				lastChar2 = "";
    			} else if ( lastChar2 == "" ){
    				lastChar2 = currentChar;
    			} else {
    				if ( lastChar1 == currentChar && lastChar2 == currentChar ){
    					boError = true;
    					errorMessage = "BR-BE-0026";
    				} else {
    					lastChar1 = lastChar2;
    					lastChar2 = currentChar;
    				}
    			}
		
    			if ( boError ){
    				break;
    			}
		
    			cont++;
    		}//end WHILE
		
    		if (!boError)
    			checkConsecutiveCharacters();
    	}
	  
	}.bind(this);
	
	
	var checkConsecutiveCharacters = function(){

    	var cont = 0;
    	var lastChar1 = -1;
    	var lastChar2 = -1;	
    	
		while( cont < myPassword.length ){
			
			var currentChar = myPassword.charCodeAt(cont);
	
			if ( lastChar1 == -1 ){ 
				lastChar1 = currentChar;
				lastChar2 = -1;
			} else if ( lastChar2 == -1 ){
				lastChar2 = currentChar;
			} else {
				if ( lastChar1 == lastChar2 -1 && lastChar2 == currentChar -1 ){
					boError = true;
					errorMessage = "BR-BE-0027";
				} else {
					lastChar1 = lastChar2;
					lastChar2 = currentChar;
				}
			}
	
			if ( boError ){
				break;
			}
	
			cont++;
		}//end WHILE    	
		
	}.bind(this);

	
	
	/***********************************************************
	 * 
	 * FUNCTION: 	validatePassword_Matches
	 * DESCRIPTION:	 
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: 		messages
	 * 
	 * ----------------   */
	var validatePassword_Matches = function( ){
	    
		clientNumber = ( myCurrentData.INFO == null ) ? null : myCurrentData.INFO.get_User_ID();
		
		if ( myPassword.indexOf(clientNumber) > -1 ){ 
			boError = true;
			errorTitle = "";
			errorMessage = "03_B13";
		} 
	  
		if ( this.boError ){
			//console_log( "[ERROR] " + this.errorMessage );
			return false;
		}
	  
		return true;
	  
	}.bind(this);

  
	
	
	
	/***********************************************************
	 * 
	 * FUNCTION: 	validatePassword_RegEX
	 * DESCRIPTION:	 
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: 		messages
	 * 
	 * ----------------   */
    var validatePassword_RegEX = function( ){
    	boError = !regEX.test( myPassword );
    	if ( boError ){
    		errorMessage = "BR-BE-0001";
    	}
    };
    
    
    /***********************************************************
	 * 
	 * FUNCTION: 	validate_with_WS
	 * DESCRIPTION:	 
	 * PARAMETERS:	
	 * RETURN:		-
	 * TAGS: 		messages
	 * 
	 * ----------------   */
    this.validate_Password_WS = function( bpId, callBack_SUCCESS, mapOfFieldsOnView ){
    	
    	/* ================================================================= */
		/* -------          		CALL WS SERVICE 			------------ */
		/* ================================================================= */

		var nonce = generateNonce();
		var encryptedPassword  = encryptMessage(myPassword,nonce);
		var encryptedBpId = getEncryptedBpId(nonce);

		var oCon = sap.ui.getCore().byId(Common.Navigations.PAGE_SIGN_IN).getController();

		myCurrentData.activeWebService = null;

		if (!isEmpty(encryptedPassword)) {

			var customData = {
				bpId: encryptedBpId,
				password: encryptedPassword,
				optData: toHex(nonce),
			};

			var wsParameterEnrollment = new WSCaller().setSuccessCallback(callBack_SUCCESS).setCustomData(customData);
			var wsValidatePassword = new WS(CREDENTIALS_Validate, wsParameterEnrollment);

			var wsWrapper = new WSPackageWrapper().setOperation(wsValidatePassword);
			wsWrapper.startWS();

		}



    }
    
    /***********************************************************
	 * 
	 * FUNCTION: 	validatePassword
	 * DESCRIPTION:	Handles the calls for the Password Validations functions 
	 * PARAMETERS:	
	 * RETURN:		TRUE if valid | MESSAGE_ID if invalid
	 * TAGS: 		messages
	 * 
	 * ----------------   */
	this.validate_Password = function() {
		
		console_log("Enter to validate password--->");
      
		errorMessage = "";
		errorTitle = "PASSWORD_ERROR_TITLE";
		boError = false;
		
		validatePassword_Empty();
		
		if ( !boError && myPasswordConfirm != null ){
			validatePassword_Same();
		}
		
		if ( !boError && myPasswordConfirm != null ) {
			validatePassword_Matches();
		}
		
		if ( boError ) {
			return [errorTitle, errorMessage];
		}  else {
			return true;
		}
	 
	};

}