function COMPARTAMOS_SMS( arNumber_To, stMessage ) {
	
	//PROPERTIES SECTION
	// TODO: Improve and Structure the system to load this info from device and settings files
    this.myNumbers = arNumber_To;
    this.myMessage = stMessage;
	
	
	/***********************************************************
	 * 
	 * FUNCTION: 	sendSMS
	 * DESCRIPTION:	 
	 * PARAMETERS:	
	 * RETURN:		
	 * TAGS: 		
	 * 
	 * ----------------   */
	this.send = function() {
      
		errorMessage = "";
		errorTitle = "PASSWORD_ERROR_TITLE";
		boError = false;

		//TODO: need to load the Mobile Number
		//TODO: need to load the Client Number
		//TODO: need to load the Bank Name
		var number = null;
		if ( typeof this.myNumbers == 'array' ){
			number = this.myNumbers.split(',');
		} else {
			number = this.myNumbers; 
		}
        var message = this.myMessage;
        
        //CONFIGURATION
        var options = {
            replaceLineBreaks: false, // true to replace \n by a new line, false by default
            android: {
                intent: 'INTENT'  // send SMS with the native android SMS messaging
                //intent: '' // send SMS without open any other app
            }
        };

        var success = function () { 
        	AXX_MESSAGE_SUCCESS( "SMS enviado!", "SMS enviado con successo", null );
        	//AXX_('Message sent successfully'); 
        };
        var error = function (e) { 
        	AXX_MESSAGE_SUCCESS( "SMS enviado!", "SMS no enviado \n Message Failed: [" + e + "]" , null );
        };
        
        try {
        	//sms.send(number, message, options, success, error);
        	
        	$cordovaSms.send(
        		number, 
        		message, 
        		options
        	).then(function() {
		        // Success! SMS was sent
        		success
		    }, function(error) {
		        // An error occurred
		    	error
		    });
        	 
        } catch(e){
        	throw { errorCode: "SMS_ERROR", errorText: e };
        }
	 
	};
	
};