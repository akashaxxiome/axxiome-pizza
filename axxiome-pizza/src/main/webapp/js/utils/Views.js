    
function VIEWS_Create_Regular_Page( obView, obParameters ){
	
	var oCon = obView.getController();
	
	VIEWS_Create_DivMenu();
	
	VIEWS_Create_Root( obView );
	
	var boErrorStop = false;
	var boEnableScroll = false;
	var boShowFooter = true;
	var boHeaderClear = obParameters.boHeaderClear;
	var stViewName = "";
	var cssClasses = "";
	var boShowTitle = true;
	var boShowSubHeader = true;
	var footerStyle = "";
	var boShowSubHeaderLOGO = false;
	
	if ( obParameters ){
		try { boEnableScroll = true; } catch(e){}
		try { cssClasses = obParameters.cssClasses; } catch(e){}
		try { stViewName = obParameters.stViewName; } catch(e){
			console_log(["View without NAME", obView]);
			boError_Stop = true;
		}
		try{ 
			boHeaderClear = obParameters.boHeaderClear;
			boShowTitle = !boHeaderClear; 
		} catch(e){ }
		try{
			boShowSubHeader = ( obParameters.boShowSubHeader != null ) ? obParameters.boShowSubHeader : true;
		} catch(e){
			boShowSubHeader = true;
		}
		try{
			boShowTitle = !myCurrentData.IS_MOBILE ? obParameters.boShowTitle : ( obParameters.boShowTitle != null ) ? obParameters.boShowTitle : true;
//			boShowTitle = myCurrentData.IS_MOBILE ? false : obParameters.boShowTitle;
		} catch(e){
			boShowTitle = !boHeaderClear; 
		}
		try {
			boShowFooter = isEmpty( obParameters.boShowFooter ) ? true : obParameters.boShowFooter;
		} catch(e){}
		try{
			boShowSubHeaderLOGO = ( !myCurrentData.IS_MOBILE ) ? false : ( isEmpty( obParameters.boShowSubHeaderLOGO ) ) ? false : obParameters.boShowSubHeaderLOGO;
		} catch(e){}
	}
	
	
	if ( !boErrorStop ){
		
	    /* Functions to finish the settins before creating the view */
		VIEWS_Create_HEADER( boHeaderClear );
		VIEWS_Create_FOOTER( );
		VIEWS_Create_SubHeader_Title( );
		
		obView.setBusyIndicatorDelay(0);
		
		obView.thePage = new sap.m.Page( stViewName, {
			customHeader: obView.header,
			footer: obView.footer,
			showFooter: boShowFooter,
			enableScrolling: boEnableScroll,
			content : [
			    boShowSubHeader ? obView.subHeader : null,
				obView.div_root,
	        ]
		}).addStyleClass("Compartamos_Page " + cssClasses);
		
		
	}
	
	function VIEWS_Create_HEADER( boHeaderClear ){
		
		if ( myCurrentData.isLoggedIn ){
			obView.header = sap.ui.jsfragment( "Header", "fragment.headers.desktop.APP_HEADER_SESSION", oCon );
		} else {
			obView.header = sap.ui.jsfragment( "Header", "fragment.headers.desktop.APP_HEADER_SIGNIN", oCon );
		}
		
		
	}
	
	function VIEWS_Create_FOOTER(  ){
		
		obView.footer = sap.ui.jsfragment("home_NEW_Header","fragment.footers.desktop.APP_FOOTER_SIGNIN", oCon);
		
	}
	
	function VIEWS_Create_DivMenu(  ){
		
		var boMenuVisible = false;
		
		obView.div_menu = new sap.m.VBox({
			visible: boMenuVisible,
			alignItems : sap.m.FlexAlignItems.Center,
			justifyContent : sap.m.FlexJustifyContent.Start ,
		}).addStyleClass("Desktop_Menu_Container");
		
	}
	
	function VIEWS_Create_SubHeader_Title( ){
		
	}
	
};

function VIEWS_Create_Root ( obView ){
	
	obView.div_root = new sap.m.HBox({
		alignItems : sap.m.FlexAlignItems.Start,
		justifyContent : sap.m.FlexJustifyContent.Center ,
		items:[
			// obView.div_menu,
			obView.div_content
		]
	}).addStyleClass("Axxiome_Page_content");
	
	try {
		obView.thePage.setShowFooter( AXX_SETTINGS_getProperty( "FOOTER.visible" ) );
	} catch(e){}
	
}


function VIEWS_Add_Main_Content( obView, arItems ){

	obView.div_all = new sap.m.VBox({
		alignItems : sap.m.FlexAlignItems.Center,
	}).addStyleClass("PAGE_Content_DIV_ALL");
	
	
	if ( myCurrentData.IS_MOBILE ){
		
		$.each( arItems, function(index, theItem){
			obView.div_all.addItem( theItem );
		})
		
	} else {
		
		this.scrollContainer = 	new sap.m.ScrollContainer({
			horizontal: false,
			vertical: true,
			content:[ arItems ] 
		});
		
		obView.div_all.addItem( this.scrollContainer );
		
	}

	/* ------------- FINALLY --------------- */
	obView.div_content = new sap.m.VBox({
		alignItems : sap.m.FlexAlignItems.Center,
		justifyContent : sap.m.FlexJustifyContent.Start ,
		items:[
		    obView.div_all,
		]
	}).addStyleClass("PAGE_Content_DIV");
	
}


function apply_Header_Title_CSS( obView, parameters ){
	
	console_log( obView );
	console_log( parameters );

    // SUB HEADER
    var subHeader_TEXT = sap.ui.core.Fragment.byId( obView.getId() + "_SubHeader", "SubHeader_TEXT" );
	subHeader_TEXT.setText( parameters.subHeader_TEXT ); 
	var subHeader_IMG = sap.ui.core.Fragment.byId( obView.getId() + "_SubHeader", "SubHeader_IMG" );
	subHeader_IMG.setSrc( parameters.subHeader_IMG );

    // TITLE
	try {
		var Title_TEXT = sap.ui.core.Fragment.byId( obView.getId() + "_Title", "Title_TEXT");
		Title_TEXT.setText( parameters.title_TEXT );
	} catch(e){}
	
	// CSS Removing any, if necessary
	if ( parameters.arCSS_REM ){
		$.each( parameters.arCSS_REM, function( i, thisItem ){
			obView.subHeader.removeStyleClass( thisItem );
		});
	}
	if ( parameters.arTitleCSS_REM ){
		$.each( parameters.arTitleCSS_REM, function( i, thisItem ){
			obView.title.removeStyleClass( thisItem );
		});
	}
	
	// CSS Adding
	if ( parameters.arCSS_ADD ){
		$.each( parameters.arCSS_ADD, function( i, thisItem ){
			obView.subHeader.addStyleClass( thisItem );
		});
	}
	if ( parameters.arTitleCSS_ADD ){
		$.each( parameters.arTitleCSS_ADD, function( i, thisItem ){
			obView.title.addStyleClass( thisItem );
		});
	}
    
};




function VIEWS_Add_Navigation_SecondaryViews(){
	
	var arViews = [
	               
	    sap.ui.jsview( Common.Navigations.PAGE_HOME, "view.Home"),
	    
	];
	
	$.each( arViews, function(index, theItem){
		
		try{
			homeNavContainer.addPage(theItem);
		} catch(e){
			console_log([ ">%>% ERROR LOADING THE PAGE", theItem.getId() ]);
		}
		
	});

}


function VIEWS_Create_Buttons_Container( arButtons ){
	
	console_log(">> VIEWS_Create_Buttons_Container");

	try{

		if ( !isEmpty(arButtons) ){

			var divContainer = new sap.m.HBox({
				alignItems : sap.m.FlexAlignItems.Center,
				justifyContent : sap.m.FlexJustifyContent.Center
			}).addStyleClass("PAGE_Buttons_DIV");

			if ( arButtons.length === 1 ){

				divContainer.addItem(
					new sap.m.VBox({
						items:[
							arButtons[0]
						]
					}).addStyleClass("Page_Buttons_DIV_V")
				);

				return divContainer;

			} else if ( arButtons.length === 2 ){

				divContainer.addItem(
					new sap.m.VBox({
						items:[
							arButtons[0]
						]
					}).addStyleClass("Page_Buttons_DIV_H")
				);

				divContainer.addItem(
					new sap.m.VBox({
						items:[
							arButtons[1]
						]
					}).addStyleClass("Page_Buttons_DIV_H")
				);

				return divContainer;

			} else {

				throw "ERROR: Wrong number of buttons. It should be 1 or 2";

			}
		} else {
			throw "ERROR: No Buttons added to the container";
		}

		return divContainer;

	} catch(e){
		console_log( e );
		return null;
	} finally{
		
	}

}

var VIEWS = {

	update_Current_Header: function(){

		var obView = app.getCurrentView();

		if ( myCurrentData.isLoggedIn ){
			obView.thePage.setCustomHeader( sap.ui.jsfragment( "Header", "fragment.headers.desktop.APP_HEADER_SESSION", obView.getController() ) );
		} else {
			obView.thePage.setCustomHeader( sap.ui.jsfragment( "Header", "fragment.headers.desktop.APP_HEADER_SIGNIN", obView.getController() ) );
		}

	},

	update_Current_Content: function(){

		var oCon = app.getCurrentController();

		VIEWS.update_Current_Header();

		if ( !isEmpty(oCon.reload_Session) && ( typeof oCon.reload_Session === "function" ) ){
			oCon.reload_Session();
		}


	}

}