UTILS_ORDER = {

	Update_Order : function( OrderData ){
		console_log(" >> Update_Order");

		myCurrentData.TEMP.OrderData = OrderData;
		app.setCurrentViewBusy(true);
		
		obDataOrder = {
			CustomerID : OrderData.UserData.CustomerID,
			StatusID : OrderData.StatusID,
			OrderID : OrderData.OrderID
		};
		
		var wsParameter = new WSCaller();
		wsParameter.setCustomData( obDataOrder );
		var wsService = new WS( ORDERS_WS.Update_Order , wsParameter );
		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();
	},

	Create_Order : function( OrderData ){
		/*
			The obData will have 2 sets:
			- PizzaData
			- DeliveryData
			And with it, we call the proper POST methods accordingly
		*/

		console_log(" >> Create_Order");

		myCurrentData.TEMP.OrderData = OrderData;

		// ORDER

		/* ================================================================= */
		/* -------          		CALL ODATA SERVICE 			------------ */
		/* ================================================================= */
		app.setCurrentViewBusy(true);

		
		obDataOrder = {
			CustomerID : myCurrentData.INFO.get_User_ID(),
			OrderID : OrderData.OrderID
		};
		
		var wsParameter = new WSCaller().setSuccessCallback( UTILS_ORDER.Create_Pizza_Entry );
		wsParameter.setCustomData( obDataOrder );
		var wsService = new WS( ORDERS_WS.Create_Order , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();
		
		
	},

	Create_Pizza_Entry : function( jsonData ){

		console_log(" >> Create_Pizza_Entry");

		thisOrder = myCurrentData.TEMP.OrderData;
		thisOrder.OrderDataTEMP = myCurrentData.TEMP.OrderDataTEMP;

		// PIZZA
		obDataPizza = {
			OrderID : thisOrder.OrderDataTEMP.OrderID,
			StandardPizzaID : ( thisOrder.PizzaData.isStandard === true ? thisOrder.PizzaData.StandardPizzaID : null ),
			CrustTypeID : thisOrder.PizzaData.Crust,
			SauceTypeID : thisOrder.PizzaData.Sauce,
			Size		: thisOrder.PizzaData.Size
		};
		
		var fn_CallBackSuccess = null;
		if ( thisOrder.PizzaData.isStandard === true ){
			fn_CallBackSuccess = UTILS_ORDER.Order_Created;
		} else {
			fn_CallBackSuccess = UTILS_ORDER.Create_Toppings_Entry;
		}

		var wsParameter = new WSCaller().setSuccessCallback( fn_CallBackSuccess );
		wsParameter.setCustomData( obDataPizza );
		var wsService = new WS( PIZZAS_WS.Create_Pizza , wsParameter );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();
		
	},

	Create_Toppings_Entry : function (jsonData ){
		
		thisOrder = myCurrentData.TEMP.OrderData;

		var arToppings = thisOrder.PizzaData.arToppings;
		if ( isEmpty( arToppings ) ){

			UTILS_ORDER.Order_Created();

		} else {

			myCurrentData.TEMP.ToppingsData = {
				CountDONE : 0,
				Count_TO_DO : arToppings.length,
			}


			$.each( arToppings, function(i, item){

				console_log([ " CREATING TOPPING [" + i + "] " , item ]);

				obDataTopping = {
					PizzaID			: myCurrentData.TEMP.PizzaData.PizzaID,
					ToppingTypeID	: item
				};

				var fn_CallBackSuccess = UTILS_ORDER.Order_Created_Verify_TopicsCount;

				var wsParameter = new WSCaller().setSuccessCallback( fn_CallBackSuccess );
				wsParameter.setCustomData( obDataTopping );
				var wsService = new WS( PIZZAS_WS.Create_Topping , wsParameter );

				var wsWrapper = new WSPackageWrapper().setOperation(wsService);
				wsWrapper.startWS();
				
				
			});

		}

		// TOPPING if custom
		// if StandardPizzaID is null
		// parse toppings (comma delimited)
		//for each topping
		// obDataTopping = {
		// 		thisPizza.PizzaID,
		// 		//individual ToppingTypeID from parsing
		// }

		// var fn_CallBackSuccess = UTILS_ORDER.Order_Created();
		// //var wsParameter = new WSCaller().setSuccessCallback(); No callback needed?

		// wsParameter.setCustomData( obDataTopping );
		// var wsService = new WS( PIZZAS_WS.Create_Topping , wsParameter );

		// var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		// wsWrapper.startWS();

	},

	Order_Created_Verify_TopicsCount : function(){

		myCurrentData.TEMP.ToppingsData.CountDONE ++;

		if ( myCurrentData.TEMP.ToppingsData.CountDONE === myCurrentData.TEMP.ToppingsData.Count_TO_DO ){
			UTILS_ORDER.Order_Created();
		}

	},

	Order_Created : function(){

		app.setCurrentViewBusy(false);

		var obParameters = {
			fn_callBack_YES : function(){
				if(myCurrentData.isStaff){
					Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_STAFF_ORDER_HISTORY });
				} else if ( myCurrentData.isLoggedIn ){
					Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_ORDER_HISTORY });
				} else {
					Navigation_GoHome();
				}
			}
		};

		DISPLAY_MESSAGE_SUCCESS(
			"Order Confirmed",
			"Your pizza order has been submitted!",
			obParameters
		);

	},
	
	Update_Offer : function( OfferData ){
		console_log(" >> Update_Offer");

		myCurrentData.TEMP.OfferData = OfferData;
			
		obDataOffer = {
			OfferID : OfferData.SelectedOrder.OfferID,
			CustomerID : OfferData.UserData.CustomerID,
			RedeemOrderID : OfferData.SelectedOrder.OrderID,
			RewardOrderID : OfferData.SelectedOrder.OrderID
		};
		
		var wsParameter = new WSCaller();
		wsParameter.setCustomData( obDataOffer );
		var wsService = new WS( ORDERS_WS.Redeem_Offer , wsParameter );
		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();
	}

 
};