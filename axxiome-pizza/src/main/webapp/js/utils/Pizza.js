var UTILS_PIZZA = {

	Translate_Toppings : function ( arToppingsINITIAL ){

		var stToppings = "";

		if ( typeof sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_TOPPINGS ) === "undefined" ){

			console_log(" !! >> Please, load the Toppings Data first ");

		} else {

			var arToppings = [];

			if ( typeof arToppingsINITIAL === "object" ){
				console_log("object");
				$.each( arToppingsINITIAL, function(i, item){
					arToppings.push( parseInt( item ) );
				});
			} else {
				console_log("not object");
				arToppings.push( parseInt( arToppingsINITIAL ) );
			}

			var myData = sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_TOPPINGS ).getData();
			$.each( myData, function( i, currentTopping ){

				if ( arToppings.indexOf( currentTopping.ToppingTypeID ) >= 0 ){
					stToppings = stToppings + currentTopping.ToppingName + ", ";
				}

			});

			stToppings = stToppings.slice(0, -2);

		}

		return stToppings;

	},

	Translate_Sauces : function ( arToppingsINITIAL ){

		var stToppings = "";

		if ( typeof sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_SAUCES ) === "undefined" ){

			console_log(" !! >> Please, load the Toppings Data first ");

		} else {

			var arToppings = [];

			if ( typeof arToppingsINITIAL === "object" ){
				$.each( arToppingsINITIAL, function(i, item){
					arToppings.push( item );
				});
			} else {
				arToppings.push( arToppingsINITIAL );
			}

			var myData = sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_SAUCES ).getData();
			$.each( myData, function( i, currentTopping ){

				if ( arToppings.indexOf( currentTopping.SauceTypeID ) >= 0 ){
					stToppings = stToppings + currentTopping.SauceName + ", ";
				}

			});

			stToppings = stToppings.slice(0, -2);

		}

		return stToppings;

	},

	

	Translate_Crusts : function ( arToppingsINITIAL ){

		var stToppings = "";

		if ( typeof sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_CRUSTS ) === "undefined" ){

			console_log(" !! >> Please, load the Toppings Data first ");

		} else {

			var arToppings = [];

			if ( typeof arToppingsINITIAL === "object" ){
				$.each( arToppingsINITIAL, function(i, item){
					arToppings.push( item );
				});
			} else {
				arToppings.push( arToppingsINITIAL );
			}

			var myData = sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_CRUSTS ).getData();
			$.each( myData, function( i, currentTopping ){

				if ( arToppings.indexOf( currentTopping.CrustTypeID ) >= 0 ){
					stToppings = stToppings + currentTopping.CrustName + ", ";
				}

			});

			stToppings = stToppings.slice(0, -2);

		}

		return stToppings;

	},

	Translate_Sizes : function( stSizeInitial ){

		var stString = "Small";
		switch ( stSizeInitial ){
			case "S":
				stString = "Small";
			break;
			case "M":
				stString = "Medium";
			break;
			case "L":
			default:
				stString = "Large";
			break;

		}

		return stString;

	}


}