function isEmpty(value){
	return typeof value == "undefined" || value == undefined || value == null || $.trim(value) == '';
}
function isValidEmail(stEmail){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return ( regex.test(stEmail) );
}

function isValidPaymentAmount(paymentDueAmount){
	var regex = /^(\d*\.\d{1,2}|\d+)$/;
	return ( regex.test(paymentDueAmount) );
}

var UUIDv4 = function b(a){
    return a?(a^Math.random()*16>>a/4).toString(16):([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,b);
}

function parseDate( stValue ){
	var today = new Date(parseInt(jsonData.customer.dateOfBirth.toString()));
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    var today = dd+'/'+mm+'/'+yyyy;
    
    return today;
}


function clearValidationErrorIfNotEmpty(){
    var value = this.getValue();

    if(!isEmpty(value)){
        this.setValueState(sap.ui.core.ValueState.None);
    }
}

Date.prototype.toLocal = function() {
	
    var local = new Date(this);
    local.setHours( this.getHours()+(this.getTimezoneOffset()/-60) );
    var obDateTime = local.toJSON().split("T");
    
    //Date
    var stDate = obDateTime[0].split("-");
	stDate[3] = stDate[0]; stDate[0] = stDate[2]; stDate[2] = stDate[3];
	stDate.pop();
	obDateTime[0] = stDate.join("/");
    
	//Time
    obDateTime[1] = obDateTime[1].split(".")[0];
    
    return obDateTime;
    
}


function replaceURLWithHTMLLinks(text) {
    if ( !isEmpty(text) ){
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i;
        return text.replace(exp,"<a href='$1' target='_blank'>$1</a>");
    } else {
        return "";
    }
}

function extractCookieFromHeader(str){

    if(typeof str === 'string' || str instanceof String){
        //In android response is a string
        if(isEmpty(str)){
            return null;
        }

        var cutString = replaceCaseInsensitive(str,'set-cookie=[MOBILISER_REMEMBER_ME_COOKIE="";','');

        var re = /MOBILISER_REMEMBER_ME_COOKIE=(.*?);/;
        var m;

        if ((m = re.exec(cutString)) !== null) {
            if (m.index === re.lastIndex) {
                re.lastIndex++;
            }

            return m[1] == "" ? null : m[1];
        }

        return m;
    } else {
        //In IOS response is an object
        var iosHeaderCookie = str["Set-Cookie"];

        if(isEmpty(iosHeaderCookie)){
            return null;
        }

        var finalHeader = iosHeaderCookie.replace('MOBILISER_REMEMBER_ME_COOKIE="";','');

        var re = /MOBILISER_REMEMBER_ME_COOKIE=(.*?);/;
        var m;

        if ((m = re.exec(finalHeader)) !== null) {
            if (m.index === re.lastIndex) {
                re.lastIndex++;
            }

            return m[1] == "" ? null : m[1];
        }

        return m;
    }
}

function isEven(n) {
    return n % 2 == 0;
}

function isOdd(n) {
    return Math.abs(n % 2) == 1;
}

function isAllStringInArrayNonEmpty(arr){
    if(arr == null){
        return false;
    }

    return arr.some(function (currentValue) {
        return !isEmpty(currentValue);
    });
}

function replaceCaseInsensitive( data, search, replaceCharacter ) {
    return data.replace( new RegExp( "(" + preg_quote( search ) + ")" , 'gi' ), replaceCharacter );
}

function preg_quote( str ) {
    return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
}

function replaceNewLinesWithSpace(inputString){
    var str = inputString
    str = str.replace(/\n/g, " ");
    return str;
}