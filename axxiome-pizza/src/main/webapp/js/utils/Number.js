function isNumeric(value){
	return $.isNumeric(value);
}

function isAmount(value){
	
	value = $.trim(value);
	
	return !isEmpty(value) && $.isNumeric(value); 
}

function textToNumber( stValue, inDecimalPlaces ){

	if ( isEmpty(stValue) || isNaN(stValue) ){
		return;
	} else {
		stValue = stValue + "";
	}
	

	var myNewNumber = stValue.replace( Common.CurrentyFormat.THOUSAND_SEPARATOR, "").replace( Common.CurrentyFormat.THOUSAND_SEPARATOR , "").replace( Common.CurrentyFormat.THOUSAND_SEPARATOR, "").replace( Common.CurrentyFormat.THOUSAND_SEPARATOR, "");
	myNewNumber = myNewNumber.replace( Common.CurrentyFormat.DECIMAL_SEPARATOR, "." );
	myNewNumber = parseFloat( myNewNumber ).toFixed(inDecimalPlaces);
	
	var arNumber = myNewNumber.split( Common.CurrentyFormat.DECIMAL_SEPARATOR );
	var countSplit = arNumber.length;
	
	var numberA = parseInt( arNumber[0] );
	
	try {
		var numberB = parseInt( arNumber[1] );
		numberB = numberB / 100;
	} catch(e){}
	finally{
		myNewNumber = numberA + numberB;
	}
	
	return myNewNumber;
	
}

function numberToText( dcValue, inDecimalPlaces ){
	
	var arNumber = (dcValue+"").split( Common.CurrentyFormat.DECIMAL_SEPARATOR );
	var arThousands = [];
	var stDecimals = ( arNumber.length > 1 ) ? arNumber[1] : "00";
	
	var leftSide = "";
	var count = arNumber[0].length;
	var quant =  arNumber[0].length;
	if ( quant > 3 ) {

		while ( count > 0 ){

			leftSide = arNumber[0][count-1] + "" + leftSide;
			if ( leftSide.length % 3 == 0 ){
				arThousands.push( leftSide );
				leftSide = "";
			}
			if ( count == 1 && leftSide != "" ){
				arThousands.push( leftSide );
			}
			count--;

		}
		
		arThousands.reverse();
		
		leftSide = arThousands.join( Common.CurrentyFormat.THOUSAND_SEPARATOR );
		
	} else {
		leftSide = arNumber[0];
	}

	return leftSide + "." + stDecimals;
	
}

function currencyToText( dcValue, inDecimalPlaces ){
	
	var arNumber = (dcValue+"").split( Common.CurrentyFormat.DECIMAL_SEPARATOR );
	var arThousands = [];
	var stDecimals = ( arNumber.length > 1 ) ? arNumber[1] : "00";
	
	var leftSide = "";
	var count = arNumber[0].length;
	var quant =  arNumber[0].length;
	if ( quant > 3 ) {

		while ( count > 0 ){

			leftSide = arNumber[0][count-1] + "" + leftSide;
			if ( leftSide.length % 3 == 0 ){
				arThousands.push( leftSide );
				leftSide = "";
			}
			if ( count == 1 && leftSide != "" ){
				arThousands.push( leftSide );
			}
			count--;

		}
		
		arThousands.reverse();
		
		leftSide = arThousands.join( Common.CurrentyFormat.THOUSAND_SEPARATOR );
		
	} else {
		leftSide = arNumber[0];
	}

	return leftSide + "." + stDecimals;
	
}


function validateInputTypeNumber( theValue, evt ){

	var regexKey = /[\d]|\./;
	var regexValue = /[\d]{1,9}|[\d]{1,9}(\.[\d]{1,2}){1}/;

    var theEvent = evt || window.event;

    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);

    //First REGEX validation, in the character only
    var boRegex = regexKey.test(key);
    if ( !boRegex ) {

        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();

    } else {
    	
    	var valueTemp = theValue + "" + key;

    	//Second REGEX Validation, in the whole value
    	var boRegexValue = regexValue.test( valueTemp );

    	if ( !boRegexValue ){

    		theEvent.returnValue = false;
       		if (theEvent.preventDefault) theEvent.preventDefault();

    	}

    }

}

function generateRandom( min, max ){
	if ( isEmpty(min) ){ min = 1; }
	if ( isEmpty(max) ){ max = 9999; }
	return Math.floor(Math.random() * (max - min + 1) + min);
}