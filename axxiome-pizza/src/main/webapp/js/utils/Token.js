function TOKEN_Register_Services( boRegister ){
	
	myCurrentData.TOKEN.ACTIVE = boRegister;
	
}

function TOKEN_Is_Service_Active(  ){
	
	try {
		return myCurrentData.TOKEN.ACTIVE;
	} catch(e){
		AXX_MESSAGE_WARNING(
			fn_APP_CONF_GET_TEXT( "messageFile", "token.register.register.fail_title" ),
			fn_APP_CONF_GET_TEXT( "messageFile", "token.register.register.fail_message" )
		);
		return false;
	}
	
}