var Navigation_Invoke_Page = function( thisData ){
	
	console_log(" ------- NAVIGATING --------");
	console_log( "FROM PAGE [" + homeNavContainer.getPreviousPage() + "]" );
	console_log( "INVOKING PAGE [" + thisData.PageTo + "]" );

	var stType = typeof thisData.PageTo;
	var stViewName = "";
	var stViewPath = "";

	if ( stType == "object" ){
		stViewName = thisData.PageTo.name;
		stViewPath = thisData.PageTo.path;
	} else {
		stViewName = thisData.PageTo;
		
	}
	
	if ( new AXX_CONN().hasNetworkConnection() == false && stViewName != Common.Navigations.PAGE_SIGN_IN ) {

		displayNoConnectionError();
		
	} else {
		
		if ( ( stViewName == Common.Navigations.PAGE_HOME ) 
			&& ( myCurrentData.INFO == null || myCurrentData.INFO.get_Session_ID() == null ) ){
			return;
		} else {

			if ( typeof thisData == 'object' && stViewName.toUpperCase() != "BACK" ){
				
				try{
					myCurrentData.INFO.renewSessionCounter( "KEEPING SESSION - NAVIGATION " );
				} catch(e){ console_log("KEEPING SESSION - NAVIGATION [ no session ] "); }
				
				if ( app.loadView( thisData.PageTo ) ){

					myCurrentData.stView = stViewName;
					
					Navigation_HandleSamePage( stViewName, {data: thisData} );
					
					homeNavContainer.to( stViewName, thisData );

					VIEWS.update_Current_Header();

				} else {
					console_log("Page navigation error. Please contact the administrator.");
				}
				
			} else {
				app.onBackKeyPress();
			}

		}
		
	}
	
};


var Navigation_GoHome = function( isFromPopUp ){
	if ( isFromPopUp){
		swal.close();
	}
	Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_SIGN_IN });
};

/*********************************************************************
 * 			TRIGGERED WHEN AN ICON IN MEU IS CLICKED
 * */
var Menu_Navigation_GoTo = function( inLink ) {
	
	//console.clear();
//	console_log("[[ menuNavigation TRIGGERED ]] ... => " );
//	console_log( inLink );
//	console_log( typeof inLink );
	
	var myOption = "";
	
	if ( typeof inLink == "string" ){
		myOption = inLink;
	} else {
		myOption = inLink.iconHome_Clicked;
	}
	myOption = myOption.replace("_Desktop", "") 
	
	console_log(" My Navigation [ " + myOption + " ]");
	
	switch( myOption ){

		case "menuHome":

			if (myCurrentData.IS_MOBILE) {
				var editViewData = {PageTo: Common.Navigations.PAGE_HOME};
				Navigation_Invoke_Page(editViewData);
			} else {
				this.Navigation_get_Accounts_View_Data("Savings");
			}
			break;
			
		case "menuAccountSavings":
		case "Home_Icon_SAVINGS":
			this.Navigation_get_Accounts_View_Data("Savings");
			//Navigation_Invoke_Page( editViewData );n
			break;
		case "menuAccountLoans":
		case "Home_Icon_LOANS":
			this.Navigation_get_Accounts_View_Data("Loans");
			//Navigation_Invoke_Page( editViewData );
			break;
		case "menuAccountInsurances":
		case "Home_Icon_INSURANCES":
			this.Navigation_get_Accounts_View_Data("Insurances");
			break;
		case "menuCards":
		case "Home_Icon_CARDS":
			var editViewData = { PageTo : Common.Navigations.PAGE_TDD_LIST }; 
			Navigation_Invoke_Page( editViewData );
			break;
		case "menuContact":
		case "Home_Icon_CONTACT":
            Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_CONTACT });
            break;
		case "menuSettings":
		case "Home_Icon_ADMINISTRATION":
			Navigation_Invoke_Page({ PageTo : Common.Navigations.PAGE_SETTINGS_MAIN });
			break;
	    case "menuTransfers":
		case "Home_Icon_TRANSFERS":
			Navigation_GoToTransfers();
			break;		
		case "menuMobileMoney":
		case "Home_Icon_MOBILE_MONEY":
			Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_MOBILE_MONEY_PREPARATION });
			break;		
		case "menuPayments":
		case "Home_Icon_PAYMENTS":
			this.Navigation_get_Accounts_View_Data("Loans");
			break;		
		default:
			Navigation_Invoke_Page({ PageTo : inLink });					
			break;
		
    }
};


/**
 * Handles appropriate form initialization when user switches from Edit to Add view.
 * Since we use the same view for both edit and add operations, the view needs to be setup/initialized appropriate to the operation. 
 */
var Navigation_HandleSamePage = function( viewId, data ){
	
	var currentController = sap.ui.getCore().byId(viewId).getController();
	
	//Check if the user is already on the view which he/she wants to navigate to.
	
//	console_log(" ");
//	console_log( "getCurrentPage().getId() >> " + homeNavContainer.getCurrentPage().getId() );
//	console_log( "viewId >> " + viewId );
//	console_log( "Common.Navigations.PAGE_ACCOUNTS_LIST_GENERAL >> " + Common.Navigations.PAGE_ACCOUNTS_LIST_GENERAL );
//	console_log(" data ");
//	console_log( data );
	
		
	var controller = sap.ui.getCore().byId(viewId).getController();
		
		//Initialize the form for add operation only if we are currently in edit mode.
	if( typeof controller.isEditMode == 'function' && controller.isEditMode() )
	{
//		console_log("A1");
		//show confirmation dialog for data loss.
		var header = fn_APP_CONF_GET_TEXT( "i18N", "common.dialog.ConfirmNavigationHeader" );
		var confirmText =  fn_APP_CONF_GET_TEXT( "i18N", "common.dialog.EditViewDataLossConfirmText" );

		var onOk = function(oAction)
		{
			if(oAction === sap.m.MessageBox.Action.OK)
			{	//initialize the form for add operation.
				controller.onBeforeShow(data);
			}
		};
		
		//Show confirm box
		jQuery.sap.require("sap.m.MessageBox");
		sap.m.MessageBox.confirm(confirmText, onOk, header, "editViewDataLossConfirmDlg");
			
		
	}  else 	{
//		console_log("B1");
		if ( homeNavContainer.getCurrentPage().getId() == viewId ){
			controller.onBeforeShow(data);
		}
	} 
	
};




var Navigation_get_Accounts_View_Data = function(stType, query){
		
	var editViewData = { PageTo: Common.Navigations.PAGE_ACCOUNTS_LIST_GENERAL	};
	var boCall = false;
	// Here, there will be a validation against the COUNT of this ACCOUNT CATEGORY
	// IF only 1, goes directly to DETAILS PAGE
	switch(stType){
		case "Savings":
			editViewData.productCategory = 'Savings';
			try{
				if ( sap.ui.getCore().getModel( Common.ModelNames.CUSTOMER_WALLET ).getData()[Common.ModelNames.ACCOUNTS_SAVINGS] == 1 ){
					boCall =  true;
					editViewData.PageTo = Common.Navigations.PAGE_ACCOUNTS_DETAILS_SAVINGS;
				}
			} catch(e){}
		break;
		
		case "Loans":
			editViewData.productCategory = 'Loans' ;
//			try{
//				if ( sap.ui.getCore().getModel( Common.ModelNames.CUSTOMER_WALLET ).getData().customerWallet[Common.ModelNames.ACCOUNTS_LOANS] == 1 ){
//					boCall = true;
//					editViewData.PageTo = Common.Navigations.PAGE_ACCOUNTS_DETAILS_LOANS;
//				}
//			} catch(e){}
		break;
		
		case "Insurances":
			editViewData.productCategory = 'Insurances' ;
			try{
				// boCall = true;
				// editViewData.PageTo = Common.Navigations.PAGE_ACCOUNTS_INSURANCES;
			} catch(e){}
		break;
	}

	if (!boCall){

		if(query != null){
			editViewData.query = query;
		}
		editViewData.oneAccount = false;
		Navigation_Invoke_Page( editViewData );
		
	} else {

		var nonce = generateNonce();
		var encryptedBpId = getEncryptedBpId(nonce);

		var customData = {
			bpId : encryptedBpId,
			optData :  toHex(nonce),
			editViewData: editViewData,
			productCategory: editViewData.productCategory
		};

		var wsParameterGetAccounts = new WSCaller().setSuccessCallback(accountsByCategorySuccessCallback).setCustomData(customData);
		var wsGetAccounts = new WS(ACCOUNTS_Get_by_Category, wsParameterGetAccounts);

		var wsWrapper = new WSPackageWrapper().setWsArray([wsGetAccounts]);
		wsWrapper.startWS();

	}

};

var accountsByCategorySuccessCallback = function (jsonData, customData) {
	var editViewData = customData.editViewData;
	editViewData.data = jsonData;
	editViewData.oneAccount = true;
	Navigation_Invoke_Page( editViewData );
};


var NavigationSetSource = function(evt, controller){
	if ( evt.data.setSource && evt.data.setSource == true && evt.isBack == false ){
		controller.sourceNavigation = homeNavContainer.getCurrentPage().getId();
		controller.setSource = evt.data.setSource;
		evt.data.setSource = false;
		controller.MT_Source = evt.data.moduleSource;
		myCurrentData.TEMP.MT_Source = !isEmpty(evt.data.moduleSource) ? evt.data.moduleSource : myCurrentData.TEMP.MT_Source;
	}
}

function clearView( obView ){
	$("#" + obView.getId() + " input[type=text]").val('');
	$("#" + obView.getId() + " input[type=number]").val('');
}

var NAVIGATION = {

	goTo_GoogleMaps: function(){
	 	sap.m.URLHelper.redirect( "https://www.google.ca/maps/search/atb+Alberta/@52.6975804,-116.9677824,7z/data=!3m1!4b1" , true );
	}
	
}
