var TDD_Get_Card_Image_By_CardNumber = function( stCardNumber ){

	stCardNumber = ( stCardNumber + "" ).replace( "-", "" );
	var myType = stCardNumber.slice(6,8);
	
	var arTDDs = [];

	$.each( IMAGES.TDD.CODES, function( index, theTDD ){
		arTDDs.push(index);
	});

	if ( $.inArray( myType, arTDDs ) < 0 ){
		myType = "01";
	}
	
	return IMAGES.TDD[IMAGES.TDD.CODES[myType]]; 

}