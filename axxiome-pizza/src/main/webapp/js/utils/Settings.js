function AXX_SETTINGS_getRelease(){

	var theSettings = null;
	
	$.each( AXX_SETTINGS.RELEASE, function(i, thisItem){ 
	    if ( thisItem == true ) {
	        theSettings = AXX_SETTINGS[i];
	        return false;
	    }
	});
	
	return theSettings;

}

function AXX_SETTINGS_getProperty( myModule ){
	
	var mySettings = AXX_SETTINGS_getRelease();
	
//	console_log(" $$$ READING SETTINGS [" + myModule + "]" );
	
	var arBranches = myModule.split(".");
	try {
		$.each( arBranches, function( index, item ){
			mySettings = mySettings[item];
		});
	} catch(e){
		console_log(" $$$ ERROR READING SETTINGS [" + myModule + "]" );
	} 
	
	return mySettings;
	
}

function AXX_SETTINGS_getConnectionID( ){

	var theID = 0;
	
	$.each(arENVS, function(index, theEnv){
	    if ( theEnv[1] == AXX_SETTINGS_getProperty("ID") ) { 
	    	theID = theEnv[0];
	    }
	}); 
	
	return theID;
	
}

function console_log( obText, obParameters ){

	var boLog = false;

	if ( !isEmpty(obParameters) && !isEmpty(obParameters.type) ){
		
		try{
			boLog = AXX_SETTINGS_getProperty( obParameters.type );
		} catch(e){
			boLog = false;
		}

	} else {

		boLog = AXX_SETTINGS_getProperty( "DEBUG_MODE" );

	}

	if ( boLog ){

		switch ( typeof obText ){
			case "string":
				console.log( obText );
			break;
			case "object":
				
				if ( $.isArray(obText) ){
					
					$.each( obText, function(index, value){
						console.log( value );
					});
					
				} else {
					console.log( obText );
				}
				
			break;
		}

	}
	
}