var CPTMS_session_Keep = function( stText ){
	if ( myCurrentData.INFO ){
		myCurrentData.INFO.session_Keep( stText );
	} else {
		console_log(">$>$> SESSION EXPIRED from [ CPTMS_session_Keep ]");
		new AXX_SESSION().logOut_Force( fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , "03_F23") );
	}
};

var CPTMS_logOut = function(){
	try{
		if ( new AXX_CONN().hasNetworkConnection() === false  ){
			clearTimeout(wsTimeOutTimer);
			myCurrentData.activeWebService = null; 
			SESSION_clear_TimeOut_Clock();
			Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_SIGN_IN, action: "logout", mode: "forced" } );
		} else {
			myCurrentData.INFO.logOut();
		}
	} catch(e){ new AXX_SESSION().logOut_Force(); }
	finally{
		SESSION_clear_TimeOut_Clock();
	}
}

var SESSION_clear_TimeOut = function( boDisplay ){
	try{
		if ( timeoutHandle ){
			clearTimeout( timeoutHandle );
			timeoutHandle = null;
		}
	} catch(e){}
	finally{ if ( boDisplay == true ){ console_log("timeoutHandle cleared"); } }
}

var SESSION_clear_TimeOut_Clock = function( boDisplay ){
	
	try{
		if ( timeoutHandleClock ){
			clearTimeout( timeoutHandleClock );
			timeoutHandleClock = null;
		}
	} catch(e){}
	finally{ if ( boDisplay == true ){  console_log("timeoutHandleClock cleared"); } }
}

