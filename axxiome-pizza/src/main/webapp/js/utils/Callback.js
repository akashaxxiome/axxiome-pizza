function GENERIC_WEB_SERVICE_FAILURE(jsonData, methodName, customFunctions ){
	
	var obView = sap.ui.getCore().byId( homeNavContainer.getCurrentPage().sId );
	app.setBusy( obView, false );
	
	var myStatus = ( jsonData.Status ) ? jsonData.Status.code : jsonData.status;
	
	if( myStatus && ( $.inArray( myStatus, arERROR_CODES ) > -1 ) ){
		AXX_EXCEPTION_CATCH( jsonData, methodName );
	} else {
			
		var obErrorTEMP = { title: "", message: "" };
		
		if ( myStatus == 9999 ){
			AXX_MESSAGE_WARNING(
				"",
				fn_APP_CONF_GET_TEXT("messageFile", "03_B35")
			);
			boMessageDisplayed = true;
		} else {
		
			if ( myStatus ){
				
				try {
					obErrorTEMP.title = "Error";
					obErrorTEMP.message = (jsonData.Status.value) ? jsonData.Status.value : jsonData.value;
				} catch(e){
					obErrorTEMP = AXX_EXCEPTION_Translate_ERROR("I");
				}
				
				AXX_EXCEPTION_APP( 
					obErrorTEMP.title, 
					obErrorTEMP.message, 
					true, null, null
				);
				
			} else {
				if ( swal ){
					obErrorTEMP = AXX_EXCEPTION_Translate_ERROR("I");
					AXX_EXCEPTION_APP( 
						obErrorTEMP.title, 
						obErrorTEMP.message, 
						true, null, null
					);
				}
				
			}
		}
		
	}
	
	var boExecuteCustom = true;
	
	if ( typeof customFunctions == "undefined"  ){
		boExecuteCustom = false;
	} else {
		if ( !isEmpty(jsonData) && $.inArray( myStatus, arERROR_CODES_SESSION) > -1 ){
			boExecuteCustom = false
		} 
	}
	
	if ( isEmpty(myCurrentData.boCallBackError_Stop) && myCurrentData.boCallBackError_Stop == true ){
		if ( boExecuteCustom ){
			customFunctions( jsonData );
		}
	} else {
		//myCurrentData.boCallBackError_Stop = true;
	}
}

/**
 * 
 * @param mapOfFieldsOnView e.g. [oldPassword => txt_oldPassword, newPassword => txt_newPassword]
 */
function GENERIC_WEB_SERVICE_VALIDATION_FAILURE(jsonData, methodName, mapOfFieldsOnView, fnCallBack_Function ){
	
	//TODO mapOfFieldsOnView - needs to contain list of fields which should be marked as invalid - not map
	var currentPageById = sap.ui.getCore().byId( homeNavContainer.getCurrentPage().sId );
	
	var arFields_Invalid = [];
	
	app.setBusy( homeNavContainer.getCurrentPage(), false );
	
	if (currentPageById != undefined){
		//Soon we will know which object isn't valid in the response, so we can do something like this:
		//oCon.getView()[mapOfFieldsOnView[jsonData.Status.invalidField]] - mark as invalid ;
		
		//arFields_Invalid.push( element1 );
	}
	console_log( jsonData );
	
	var boMessageDisplayed = false;
	
	var myStatus = ( jsonData.Status ) ? jsonData.Status.code : jsonData.status;


	if ( $.inArray( myStatus, arERROR_CODES_SESSION ) > -1 ){

		if ( myStatus == 352 ){

			app.setCurrentViewBusy(false);

			SESSION_clear_TimeOut_Clock();
			Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_SIGN_IN });

			app.setCurrentViewBusy(false);

			try{
				myCurrentData.INFO.logOut_Force( "", false );
			} catch(e) {
				new AXX_SESSION().logOut_Force(  "", false );
			}

		} else if ( myCurrentData.TEMP.action != "logoutForced" ) {

			console_log("LogOut triggered");
			try{
				myCurrentData.INFO.logOut_Force( );
			} catch(e) {
				new AXX_SESSION().logOut_Force( );
			}

		}
	}
	else {

		if (myStatus == 9999 || isEmpty(jsonData.Status.value)) {
			AXX_MESSAGE_WARNING(
				"",
				fn_APP_CONF_GET_TEXT("messageFile", "03_B35")
			);
			boMessageDisplayed = true;
		}


		if (boMessageDisplayed == false) {
			AXX_EXCEPTION_USER(
				"Error"
				, jsonData.Status.value
				, true
				, "error"
				, mapOfFieldsOnView
			);
		}

		if (typeof fnCallBack_Function != 'undefined' && fnCallBack_Function != null) {
			fnCallBack_Function(jsonData);
		}
	}
}

function GENERIC_WEB_SERVICE_VALIDATION_FAILURE_MOBILE(jsonData, fnCallBack_Function ){

	app.setBusy( homeNavContainer.getCurrentPage(), false );
	console_log( jsonData );

		AXX_MESSAGE_WARNING(
			"",
			fn_APP_CONF_GET_TEXT("messageFile", "03_B35")
		);

	var myStatus = jsonData.status;
	var boExecuteCustom = true;

	if ( typeof fnCallBack_Function == "undefined"  ){
		boExecuteCustom = false;
	} else {
		if ( !isEmpty(jsonData) && $.inArray( myStatus, arERROR_CODES_SESSION) > -1 ){
			boExecuteCustom = false
		}
	}

	if ( boExecuteCustom ){
		fnCallBack_Function( jsonData );
	}
}

function displayNoConnectionError(){
	AXX_MESSAGE_WARNING(
		null,
		"No puede navegar sin conexión.\n\nSi su conexión no se restablece, \nla sesión se cerrará pronto.",
		function(){
		}
	);
}