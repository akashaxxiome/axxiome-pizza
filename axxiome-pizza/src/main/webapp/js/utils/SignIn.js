function SIGNIN_execute_pos_login (oCon, jsonData){

	try {

		/**
		 * 	WHERE THE CUSTOMER OBJECT IS CREATED
		 * 	IN THE SESSION
		 */
		try{
			if ( jsonData.lastLogin != null ){

				var myDate = jsonData.lastLogin.lastLoginDate;
				var myHour = jsonData.lastLogin.lastLoginHour;

				jsonData.lastLogin.DateTime = myDate + " " + myHour;
				jsonData.lastLogin.DateLabel = "Último Acceso:";

				$(".FRAG_Header_User div.FRAG_Header_User_LastLogin ").css("display", "flex");
				
			} else {
				throw 1;
			}
		} catch(e){
			jsonData.customer.lastLoginDate = "";
			jsonData.customer.lastLoginDateLabel = "";
		}

			
		// Set Model User Preferences and Information
		createUserInfoModel( jsonData );

		// Create Local Session and timeouts
		createLocalSession( jsonData );



	} catch(e){
		DISPLAY_MESSAGE_ERROR("", fn_APP_CONF_GET_TEXT("messageFile", "ERR_INTERNAL.GENERIC.MESSAGE") );
	} finally {
		app.setCurrentViewBusy(false);
	}
	
	
	function createLocalSession (jsonData){
		
		/** SESSION INFORMATION ****************************************************/
		myCurrentData.INFO = new AXX_SESSION(
			jsonData.sessionId
			, ( jsonData.sessionTimeout.timeout * 1000 )
			, jsonData.bpId
			, jsonData.customer.displayName
			, jsonData.customer.id
		);
		myCurrentData.INFO.set_User_LastSession( jsonData.lastLogin );
		myCurrentData.INFO.set_Session_LastAction( Date.now() );
		
		if( myCurrentData.IS_MOBILE ){
	//		Common.Session.APP.POPUP_IN_PERIOD = ( jsonData.sessionTimeout.timeout - jsonData.sessionTimeout.popUp ) * 1000;
			Common.Session.APP.POPUP_IN_PERIOD = jsonData.sessionTimeout.popUp * 1000;
			Common.Session.APP.POPUP_COUNTER = jsonData.sessionTimeout.count;
		} else {
	//		Common.Session.WEB.POPUP_IN_PERIOD = ( jsonData.sessionTimeout.timeout - jsonData.sessionTimeout.popUp ) * 1000;
			Common.Session.WEB.POPUP_IN_PERIOD = jsonData.sessionTimeout.popUp * 1000;
			Common.Session.WEB.POPUP_COUNTER = jsonData.sessionTimeout.count;
		}
		
		console_log("Setting TIMEOUT .. ");
		var period_for_checking = myCurrentData.IS_MOBILE ? Common.Session.APP.CHECK_PERIOD : Common.Session.WEB.CHECK_PERIOD;
		timeoutHandle = setTimeout( myCurrentData.INFO.session_Check, period_for_checking );
		console_log("Setting TIMEOUT .. [" + timeoutHandle + "] ");

	};

	function createUserInfoModel (jsonData){
		var userInfoModel = new sap.ui.model.json.JSONModel();
		userInfoModel.setData(jsonData.customer);
		sap.ui.getCore().setModel( userInfoModel, Common.ModelNames.USER_INFO );
	};

};


function SIGNIN_Set_New_User( oCon ){
	
	var obView = oCon.getView();
	oCon.isRegisteredUser = false;
	
	if ( myCurrentData.IS_MOBILE ) {
		
		obView.div_label_Enjoy.setVisible(true);

		obView.divCredentials.setVisible(false);
	
		obView.div_Login.setVisible(false);
		obView.div_Forgot_Password.setVisible(false);
		
		obView.div_Btn_Mock.setVisible( false );
		obView.div_Btn_Token.setVisible( false );
		
		obView.div_Enroll.setVisible(true);
		
		obView.div_Enroll.addStyleClass("margin-top-2");
	
	} else {
		obView.div_label_Enjoy.setVisible(false);
	}
	
	obView.div_label_Welcome.setVisible(true);
	obView.div_label_Below_Logo.setVisible(true);

	oCon.display_controls();
	
}

function SIGNIN_Set_Registered_User( oCon ){
	
	var obView = oCon.getView();
	oCon.isRegisteredUser = true;
	
	if ( myCurrentData.IS_MOBILE ) {
		
		obView.divCredentials.setVisible(true);
		obView.div_Login.setVisible(true);
		obView.div_Enroll.setVisible( false );
		obView.div_Forgot_Password.setVisible(true);
		obView.div_Btn_Token.setVisible(true);

		obView.div_label_Below_Logo.setVisible(false);
		
		obView.div_Enroll.removeStyleClass("margin-top-2");
		
		obView.div_label_Welcome.setVisible(false);
		
	} else {
		
		obView.div_label_Below_Logo.setVisible(true);

	}
	
	obView.div_Btn_Mock.setVisible( AXX_SETTINGS_getProperty("LOGIN.BUTTONS.ENV.visible") );
	
	obView.div_label_Enjoy.setVisible(false);

	oCon.display_controls();
	
}

function loadMODULES(){

}

var SIGNIN = {

	stPopup_LOGIN_STAFF : '<div data-role="popup" id="myPopup" class="ui-content" style="min-width:250px; text-align: initial;">' +
    	'<div>' +
      		// '<h3>Login information</h3>' +
      		'<label for="usrnm" class="ui-hidden-accessible">E-mail:</label><br>' +
      		'<input type="text" name="user" id="usrnm" placeholder="email@provider.com"><br>' +
      		'<label for="pswd" class="ui-hidden-accessible">Password:</label><br>' +
      		'<input type="password" name="passw" id="pswd" placeholder="Password"><br>' +
      		
      		'<div class="width-100 centered" style="display: table">' +
      			'<div class="width-20  centered" style="display: table-cell;">' +
          			'<input type="checkbox" name="login" id="log" value="1" data-mini="true" style="height: 30px;">' +
          		'</div>' +
          		'<div class="width-80 align_left" style="display: table-cell; vertical-align: middle;">' +
          			'<label style="" for="log">Keep me logged in</label>' +
          		'</div>' +
      		'</div>' +

    	'</div>' +
   	'</div>',

	stPopup_LOGIN : '<div data-role="popup" id="myPopup" class="ui-content" style="min-width:250px; text-align: initial;">' +
    	'<div>' +
      		// '<h3>Login information</h3>' +
      		'<label for="usrnm" class="ui-hidden-accessible">E-mail:</label><br>' +
      		'<input type="text" name="user" id="usrnm" placeholder="email@provider.com"><br>' +
      		'<label for="pswd" class="ui-hidden-accessible">Password:</label><br>' +
      		'<input type="password" name="passw" id="pswd" placeholder="Password"><br>' +
      		
      		'<div class="width-100 centered" style="display: table">' +
      			'<div class="width-20  centered" style="display: table-cell;">' +
          			'<input type="checkbox" name="login" id="log" value="1" data-mini="true" style="height: 30px;">' +
          		'</div>' +
          		'<div class="width-80 align_left" style="display: table-cell; vertical-align: middle;">' +
          			'<label style="" for="log">Keep me logged in</label>' +
          		'</div>' +
      		'</div>' +

      		'<div class="width-100 centered" style="display: table">' +
      			'<div class="width-80 padding-top-1 centered" style="display: table-cell;">' +
      				'<a href="#" class="text_navy" onclick="SIGNIN.Navigate_To_Register(true);">Not a user yet ? Click here to create one!</a>' +
      			'</div>' +
      		'</div>' +

    	'</div>' +
   	'</div>',

   	stPopup_SIGNUP : '<div data-role="popup" id="myPopup" class="ui-content" style="min-width:250px; text-align: initial;">' +
    	'<div>' +
      		// '<h3>Login information</h3>' +
      		'<div class="width-100 centered" style="display: table">' +
      			'<div class="social_login">' +
                    '<div class="">' +
                        '<a href="#" class="social_box ">' +
                            '<span class="icon">' +
                            	'<img src="img/social/face.png" />' +
                            '</span>' +
                            '<span class="icon_title">Connect with Facebook</span>' +
                        '</a>' +
                        '<a href="#" class="social_box ">' +
                            '<span class="icon">' +
                            	'<img src="img/social/twitter.png" />' +
                            '</span>' +
                            '<span class="icon_title">Connect with Google</span>' +
                        '</a>' +
                    '</div>' +
                '</div>' +
      		'</div>' +
    	'</div>' +
   	'</div>',


	authenticate_tap_PopUp : function(){

		$("#myPopup #usrnm").removeClass( "Token_Control_Input_Error" );
		$("#myPopup #pswd").removeClass( "Token_Control_Input_Error" );

		var obError = { error: false, title : [], message: [], component: [] };


		if ( $("#myPopup #usrnm").val() === "" ) {

			obError.error = true;
			obError.message.push( "The e-mail was not provided." );
			obError.component.push( $("#myPopup #usrnm") );

		} else if ( $("#myPopup #pswd").val() === "" ){

			obError.error = true;
			obError.message.push( "The password was not provided." );
			obError.component.push( $("#myPopup #pswd") );

		} else if ( !isValidEmail( $("#myPopup #usrnm").val() ) ){

			obError.error = true;
			obError.message.push( "The e-mail provided is invalid" );
			obError.component.push(	$("#myPopup #usrnm") );

		}

		if ( obError.error === true ){

			swal.showInputError( obError.message );
			obError.component[0].removeClass( "Token_Control_Input" ).addClass( "Token_Control_Input_Error" ).focus();

		} else {

			var obParameters = {
				Email: $("#myPopup #usrnm").val(),
				Password: $("#myPopup #pswd").val()
			}
			myCurrentData.isStaff = false;
			SIGNIN.authenticate( obParameters );

		}

	},

	authenticate_tap_PopUp_Staff : function(){

		$("#myPopup #usrnm").removeClass( "Token_Control_Input_Error" );
		$("#myPopup #pswd").removeClass( "Token_Control_Input_Error" );

		var obError = { error: false, title : [], message: [], component: [] };


		if ( $("#myPopup #usrnm").val() === "" ) {

			obError.error = true;
			obError.message.push( "The e-mail was not provided." );
			obError.component.push( $("#myPopup #usrnm") );

		} else if ( $("#myPopup #pswd").val() === "" ){

			obError.error = true;
			obError.message.push( "The password was not provided." );
			obError.component.push( $("#myPopup #pswd") );

		} else if ( !isValidEmail( $("#myPopup #usrnm").val() ) ){

			obError.error = true;
			obError.message.push( "The e-mail provided is invalid" );
			obError.component.push(	$("#myPopup #usrnm") );

		}

		if ( obError.error === true ){

			swal.showInputError( obError.message );
			obError.component[0].removeClass( "Token_Control_Input" ).addClass( "Token_Control_Input_Error" ).focus();

		} else {

			var obParameters = {
				Email: $("#myPopup #usrnm").val(),
				Password: $("#myPopup #pswd").val()
			}
			myCurrentData.isStaff = true;
			SIGNIN.authenticateStaff( obParameters );

		}

	},

	authenticate : function( obParameters ){

		// var wsParameters = new WSCaller().setSuccessCallback( SIGNIN.authenticate_SUCCESS );
		var wsParameters = new WSCaller().setSuccessCallback( SIGNIN.authenticate_SUCCESS_MOCK );
		wsParameters.setFailureCallback( SIGNIN.authenticate_FAILURE );
		wsParameters.setCustomData( obParameters );

		// var wsService = new WS( AUTHENTICATION_MOCK.login , wsParameters );
		var wsService = new WS( AUTHENTICATION_WS.Authenticate , wsParameters );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

	},

	authenticateStaff : function( obParameters ){

		// var wsParameters = new WSCaller().setSuccessCallback( SIGNIN.authenticate_SUCCESS );
		var wsParameters = new WSCaller().setSuccessCallback( SIGNIN.authenticate_SUCCESS_MOCK );
		wsParameters.setFailureCallback( SIGNIN.authenticate_FAILURE );
		wsParameters.setCustomData( obParameters );

		// var wsService = new WS( AUTHENTICATION_MOCK.login , wsParameters );
		var wsService = new WS( AUTHENTICATION_WS.AuthenticateStaff , wsParameters );

		var wsWrapper = new WSPackageWrapper().setOperation(wsService);
		wsWrapper.startWS();

	},

	authenticate_SUCCESS : function( jsonData ){

		console_log([ "authenticate_SUCCESS", jsonData ]);

		swal.close();
		myCurrentData.isLoggedIn = true;

		SIGNIN.execute_Pos_Login( jsonData );

	},

	authenticate_SUCCESS_MOCK : function( jsonData ){
		
		console_log([ "authenticate_SUCCESS_MOCK", jsonData ]);

		if ( isEmpty( jsonData ) ){
			
			swal.showInputError( "Incorrect Credentials" );

		} else {

			swal.close();

			var NEWjsonData = AUTHENTICATION_MOCK.Authenticate_with_MOCK_info(jsonData);

			SIGNIN.execute_Pos_Login( NEWjsonData );
		}

	},

	authenticate_FAILURE : function( jsonData ){

		console_log([ ">> authenticate_FAILURE", jsonData ]);

		myCurrentData.isLoggedIn = false;

		var obParameters = {
			closeAfterOK : false,
			closeAfterCancel : true,
			fn_callBack_YES: function(){
				 DISPLAY_MESSAGE_HTML( "LOG IN", SIGNIN.stPopup_LOGIN, obParameters );
			}
		}
		DISPLAY_MESSAGE_CONFIRM(
			"Oops",
			"Could not found the user/password provided." //" /n Would you like to try again ?",
			// obParameters
		);

	},




	execute_Pos_Login : function( jsonData ){

		try {

			/**
			 * 	WHERE THE CUSTOMER OBJECT IS CREATED
			 * 	IN THE SESSION
			 */
			try{
				if ( jsonData.lastLogin != null ){

					var myDate = jsonData.lastLogin.lastLoginDate;
					var myHour = jsonData.lastLogin.lastLoginHour;

					jsonData.lastLogin.lastLoginDateTime = myDate + " " + myHour;
					jsonData.lastLogin.lastLoginDateLabel = "Last Login: ";

				} else {
					throw 1;
				}
			} catch(e){
				jsonData.lastLogin.lastLoginDate = "";
				jsonData.lastLogin.lastLoginDateLabel = "";
			}

				
			// Set Model User Preferences and Information
			createUserInfoModel( jsonData );

			// Create Local Session and timeouts
			createLocalSession( jsonData );

			VIEWS.update_Current_Header();

			myCurrentData.isLoggedIn = true;
			
			VIEWS.update_Current_Content();

			// Login Page - Orders button - visible = true
			viewSignin.div_buttons_line1.getItems()[0].getItems()[0].setVisible(true);

			// Login Page - Orders button - adapting for staff users
			viewSignin.div_buttons_line1.getItems()[0].getItems()[0].setText("My Previous Orders");
			if(myCurrentData.isStaff){
				viewSignin.div_buttons_line1.getItems()[0].getItems()[0].setText("Today's order queue");
				// Login Page - Create Orders button - not available for staff
				viewSignin.div_buttons_line1.getItems()[1].getItems()[0].setVisible(false);
				// Navigate to Staff Orders View
				SIGNIN.staffInitialPage();
			}else{
				viewSignin.div_buttons_line1.getItems()[1].getItems()[0].setVisible(true);
			}

		} catch(e){
			myCurrentData.isLoggedIn = false;
			DISPLAY_MESSAGE_ERROR("", fn_APP_CONF_GET_TEXT("messageFile", "ERR_INTERNAL.GENERIC.MESSAGE") );
		} finally {
			app.setCurrentViewBusy(false);
		}
		
		
		function createLocalSession (jsonData){
			
			/** SESSION INFORMATION ****************************************************/
			myCurrentData.INFO = new AXX_SESSION(
				jsonData.sessionId
				, ( jsonData.sessionTimeout.timeout * 1000 )
				, jsonData.customer.CustomerID
				, jsonData.customer.DisplayName
				, jsonData.customer.CustomerID
			);
		// 	myCurrentData.INFO.set_User_LastSession( jsonData.lastLogin );
		// 	myCurrentData.INFO.set_Session_LastAction( Date.now() );
			
		// 	if( myCurrentData.IS_MOBILE ){
		// //		Common.Session.APP.POPUP_IN_PERIOD = ( jsonData.sessionTimeout.timeout - jsonData.sessionTimeout.popUp ) * 1000;
		// 		Common.Session.APP.POPUP_IN_PERIOD = jsonData.sessionTimeout.popUp * 1000;
		// 		Common.Session.APP.POPUP_COUNTER = jsonData.sessionTimeout.count;
		// 	} else {
		// //		Common.Session.WEB.POPUP_IN_PERIOD = ( jsonData.sessionTimeout.timeout - jsonData.sessionTimeout.popUp ) * 1000;
		// 		Common.Session.WEB.POPUP_IN_PERIOD = jsonData.sessionTimeout.popUp * 1000;
		// 		Common.Session.WEB.POPUP_COUNTER = jsonData.sessionTimeout.count;
		// 	}
			
		// 	console_log("Setting TIMEOUT .. ");
		// 	var period_for_checking = myCurrentData.IS_MOBILE ? Common.Session.APP.CHECK_PERIOD : Common.Session.WEB.CHECK_PERIOD;
		// 	timeoutHandle = setTimeout( myCurrentData.INFO.session_Check, period_for_checking );
		// 	console_log("Setting TIMEOUT .. [" + timeoutHandle + "] ");

		};

		function createUserInfoModel (jsonData){
			var userInfoModel = new sap.ui.model.json.JSONModel();
			userInfoModel.setData(jsonData);
			sap.ui.getCore().setModel(userInfoModel, Common.ModelNames.USER_INFO);
		};

	},

	Navigate_To_Register : function( isFromPopUp ){
		if ( isFromPopUp ){
			swal.close();
		}
		Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_REGISTRATION });
	},

	show_PopUp_Authentication(){

		if ( myCurrentData.isLoggedIn === false ){
			var obParameters = {
		   		allowOutsideClick : true,
		   		closeAfterOK : false,
		   		arButtonTexts : [
		   			"Cancel",
		   			"Log in"
		   		],
		   		fn_callBack_YES : function(){
		   			swal.resetInputError();
		   			SIGNIN.authenticate_tap_PopUp();
		   		},
		   	};

	        DISPLAY_MESSAGE_HTML( "LOG IN", SIGNIN.stPopup_LOGIN, obParameters );

	    }

	},

	staffInitialPage(){
		Navigation_Invoke_Page({ PageTo: Common.Views.PAGE_STAFF_ORDER_HISTORY });
	}
}