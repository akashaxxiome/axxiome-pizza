function addPlaceholderToModel(model){
    if (myCurrentData.IS_MOBILE && placeHolderNeeded(model)){
        model.unshift(
            {
                isPlaceholder : true
            }
        );
    }
}

function placeHolderNeeded(model) {
    return model != null && model[0] != null && model[0].isPlaceholder == null;
}

function validateInputLiveChangeValue(value, pattern){
    var newString = "";

    for (var i =0; i < value.length; i++){

        if (pattern.test(value[i]))
            newString += value[i];
    }

    return newString;
}

function validateNummericFieldByPressEvent(event){
    var code = event.charCode;

    //46 - comma
    if (code == 46 || (code >= 48 && code <=57)  ){
        //ok
    }else {
        event.preventDefault();
    }

    // if (code == 46 && value.indexOf('.') > -1){
    //     event.preventDefault();
    // }
}

function validateOnlyNummericFieldByPressEvent(event){
    var code = event.charCode;

    if (code >= 48 && code <=57){
        //ok
    }else {
        event.preventDefault();
    }
}

function removeMultipleDotsFromValue(value){

    var newValue = '';
    var isDot = false;

    for (var i =0; i < value.length; i++){
        var sign = value[i];

        if (sign != '.' || !isDot){
            newValue += sign;
        }

        if (sign == '.'){
            isDot = true;
        }
    }

    return newValue;
}