jQuery.sap.declare("js.utils.legacy.Formatter");
jQuery.sap.require("sap.ui.core.format.NumberFormat");
jQuery.sap.require("Constants");

//var util = {};

util.Formatter = {
	currencyFormatter : sap.ui.core.format.NumberFormat.getCurrencyInstance({
             maxFractionDigits: 2,
             groupingEnabled: true,
             groupingSeparator: ",",
             decimalSeparator: ".",
             currencyCode: false
     }),
	
    decimalFormat : sap.ui.core.format.NumberFormat.getFloatInstance({
         maxFractionDigits: 2,
         groupingEnabled: true,
         groupingSeparator: ",",
         decimalSeparator: "."
     }),
		
	convertToNumber :  function(value) {
		var numberOutput = parseFloat(value);
        return numberOutput;
	},
    
	//TODO RWA formatting accountnumber
	getFormattedAccountNumberWithBalance : function(accountNumber, balance, currencyCode) {
		accountNumber = (String(accountNumber)).slice(-4); //Get only last 4 digits of account number
		
		console_log("getFormattedAccountNumberWithBalance: " + accountNumber + "," + balance + "," + currencyCode);
    	if( typeof balance != 'undefined'){ 
    		return accountNumber + ", " + this.currencyFormatter.format(balance, currencyCode);
    	}
    	return "00.00";
    },
    
	getFormattedCurrency : function(value) {

        if( typeof value != 'undefined'){
        	//return "$ " + parseFloat(value).toFixed(2);
            return "$ " + currencyToText( parseFloat(value).toFixed(2) );
        }
        return "$ 0.00"

    },
    
    roundToDecimal : function(value) {
        return this.decimalFormat.format(value);
    },
    
    formatDate : function(dateStr) {
        var date = new Date(dateStr);
        var mthNames = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        var formattedDate = date.getDate() +" "+ mthNames[date.getMonth()] +" "+ date.getFullYear();
        return formattedDate;
    },
    
    formatDateSimple : function(dateStr) {
    	var date = this.getJavascriptDate(dateStr);
    	var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({pattern : Common.DateFormat.SHORT });
	    return dateFormat.format(date);
    },
    
    getJavascriptDate : function(dateStr){
    	var dateObj = new Date();
    	if(typeof dateStr != 'undefined'){
    		if(typeof dateStr === 'object'){
    			dateObj = new Date(dateStr);
	    	} else {
	    		dateObj = new Date(eval(dateStr.replace("/Date(", "").replace(")/", "")));	    		
	    	}
    	}	
	    return	dateObj;
    },
    
    /*formatDateTest : function(dateStr) {
    	var date = null;
    	var formattedDate = "";
    	if(typeof dateStr != 'undefined'){
    		var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({pattern : "MM/dd/yyyy" });
	    	if(typeof dateStr === 'object'){
	    		date = new Date(dateStr);
	    	} else {
	    		date = new Date(eval(dateStr.replace("/Date(", "").replace(")/", "")));	    		
	    	}	        
	        return date;
    	}
        return formattedDate;        
    },*/
    
    getDateForDays : function(days){
    	var todaysDate = new Date();
    	if(days){
    		todaysDate = todaysDate.setDate(todaysDate.getDate() + parseInt(days));
    	}
    	return new Date(todaysDate);
    },
    
    parseDate : function(dateStr, dateFormat){
    	var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({pattern : dateFormat });
    	return dateFormat.parse(dateStr);
    },
    
    convertToDateTimeString : function(d) {
        //YYYY-MM-DDThh:mm:ss
        var date = d;
        var day         = (date.getDate() < 10) ? "0"+date.getDate() : date.getDate();
        var month       = ((date.getMonth() + 1) < 10) ? "0"+(date.getMonth() + 1) : (date.getMonth() + 1);
        var hours       = (date.getHours() < 10) ? "0"+date.getHours() : date.getHours();
        var minutes     = (date.getMinutes() < 10) ? "0"+date.getMinutes() : date.getMinutes();
        var seconds     = (date.getSeconds() < 10) ? "0"+date.getSeconds() : date.getSeconds();
        var fDate = date.getFullYear() + "-" + month + "-" + day + "T" + hours + ":" + minutes + ":" + seconds;
        return fDate;
    },
    
    addDecimal : function(value) {
        var fValue
        if(value.toString().indexOf(".") == -1) {
            var fValue = value.toString().substring(0,value.toString().length-2) + '.' + value.toString().substring(value.toString().length - 2);
        } else {
            var fValue = value;
        }
        return fValue;
    },
    
    getCurrencySymbol : function(code) {
        var currencyCodes = sap.ui.getCore().byId("Login").getController().getCurrencyCodes();
        for(var i = 0; i < currencyCodes.length; i++) {
            if(currencyCodes[i].code.toString() == code) {
                return currencyCodes[i].symbol;
            }
        }
        return currencyCodes[0].symbol;
    },
    
    generateWithdrawalCode : function() {
        return Math.floor(Math.random() * 900000) + 100000;
    },
    
    getFrequencyString: function(freqValue){
		var resourceModel = sap.ui.getCore().getModel("resourceFile");
		var propertyName = "/Frequencies/" + freqValue + "/Label";
		return resourceModel.getProperty(propertyName);
    }
    
 };