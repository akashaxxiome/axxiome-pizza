jQuery.sap.declare("js.utils.legacy.ErrorHandler");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("sap.m.MessageToast");

//jQuery.sap.require("sap.banking.ui.services.ServiceConstants");
jQuery.sap.require("sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants");

//jQuery.sap.require("util.EntitlementHandler");
var util = {};

util.ErrorHandler = {
		
	mfaStarted : false,
	
	userSessionTimedOut:false,
	
	entitlementRefreshed: false,
	
	bServerDownMesageShown: false,
	
	/**
	 * Function to be called after tapping on control with 
	 * wrong value. This function will set validation message in error label.
	 * @param e
	 */
	captureElementFocus:function(e){
		console_log("***************On focus..");
	
		var id = e.currentTarget.id;
		var oController = null, view = null;
		if(id.indexOf('-') != -1)
			oController = sap.ui.getCore().byId(id.split("-")[0]);
		
		if(oController != undefined && oController != null && oController.getController != undefined)
			view = oController.getController().getView();
		
		if(view!=undefined && view!= null){			
			var title = e.currentTarget.title;
			if(title && title.length > 0){
				//Show error Messages using MessageToast.
				//TODO: Uncomment the MessageToast
				/*
				sap.m.MessageToast.show(e.currentTarget.title, {
					autoClose: false,
					duration : 800,
					my:sap.ui.core.Popup.Dock.RightBottom,
					at:sap.ui.core.Popup.Dock.RightTop,
					of:e.currentTarget
				});
				*/
			}
		}
	},

	/**
	 * This function will set validation message as tool tip of the control.
	 * Then it will be shown in error section, when user taps on the control.
	 * @param validationResult :- Validation result received from server.
	 * @param controlIdPrefix :- Any prefix given to the control id.
	 * @param view :- View object for the input form.
	 */
	showValidationError : function(validationResult, controlIdPrefix, view){
		var control = null;
		var container = view ? view : sap.ui.getCore();
		for ( var prop in validationResult) {
			//Get control id using prefix provided
			var controlId = prop;
			control = container.byId(controlId);
			if(!control){
				if(controlIdPrefix){
					controlId = controlIdPrefix + prop.substring(0,1).toUpperCase() + prop.substring(1,prop.length);
					control = container.byId(controlId);
				}
			}	
			if(control){
				//Set Error CSS and tooltip
				if(control && control != null){
					control.setTooltip(validationResult[prop].Desc);
					if(control.getMetadata().getName() == "sap.m.Select") {
						control.addStyleClass("validationErrorSelect");						
					}
					else{
						control.addStyleClass("validationError");
					}
				}
			}
		}
	},
	
	/**
	 * Function to clear validation error / class from input control. 
	 */
	clearValidationError : function(view){
		//Remove background color from the control which has wrong value
		var errorControls1 = jQuery(".validationError");
		var errorControls2 = jQuery(".validationErrorSelect");
		var counter = 0, id = null, control= null;
		for(counter = 0; counter < errorControls1.length; counter++){
			id = errorControls1[counter].id;
			control = sap.ui.getCore().byId(id);
			if(control){
				control.removeStyleClass("validationError");
				control.setTooltip("");
			}
			/*if(typeof control.getValueState === 'function'){
					control.setValueState(sap.ui.core.ValueState.Success);
			}*/
		}
		for(counter = 0; counter < errorControls2.length; counter++){
			id = errorControls2[counter].id;
			control = sap.ui.getCore().byId(id);
			if(control){
				control.removeStyleClass("validationErrorSelect");
				control.setTooltip("");
			}
		}	
	},	
	
	markMFAComplete: function(){
		mfaStarted = false;
	},
	
	/**
	 * This function will be called from all the drivers to show validation errors as well as,
	 * show error dialog for Business or Service error. 
	 * @param oError :- Error object received 
	 * @param controlNamePrefix :- prefix to be added before control Id
	 * @param view :- View object for the input form.
	 * @param callBack :- callBack if any after handling error.
	 */
	handleError : function(oError, controlNamePrefix, view, callBack){
		this.view = view;
		var errorResponse = {};
		if(oError.response && oError.response.body && oError.response.body.length){
			try{
	    		errorResponse = JSON.parse(oError.response.body);
				
	    		if(typeof errorResponse.error == 'undefined')
    			{
	    			if(errorResponse.code == Common.ErrorTypes.HTTP_ERROR)
    				{
	    				sap.m.MessageBox.show(oError.message + "\nPlease verify Connection Settings.",
							sap.m.MessageBox.Icon.ERROR,
							"ERROR",
							sap.m.MessageBox.Action.OK,
							null
						);
    				}
    			}
	    		else
	    		{
					//Handle Validation Error
					if(errorResponse.error.code === Common.ErrorTypes.VALIDATION_ERROR){
						var validationErrorRes = JSON.parse(errorResponse.error.message.value);
						util.ErrorHandler.showValidationError(validationErrorRes.VE, controlNamePrefix, view);
					} else if(errorResponse.error.code === Common.ErrorTypes.MFA_ERROR){
						//MFA Handler
						//util.ErrorHandler.getMFA(view);
						if(!this.mfaStarted){
							mfaStarted = true;
							jQuery.sap.require("view.MFAController");
							this.showMFADialogFragment = sap.ui.jsfragment("mfaFragment","fragment.MFAList", mfaController);
							this.showMFADialogFragment.open();
						}
					} else if(errorResponse.error.code === Common.ErrorTypes.UAF_ERROR){ //session timeout
						if(!this.userSessionTimedOut){
							this.userSessionTimedOut = true;
							var validationErrorRes = JSON.parse(errorResponse.error.message.value);
							/*
							sap.m.MessageBox.show(validationErrorRes.UAF,
								sap.m.MessageBox.Icon.ERROR,
								"ERROR",
								sap.m.MessageBox.Action.OK,
								function(){homeNavContainer.to("Signin",{data:{action: "timeout"}});}
							);
							*/
							var callBack = function(){
								Navigation_Invoke_Page({
									action: "logout", 
									PageTo: Common.Navigations.PAGE_SIGN_IN
								});
							};
							this.showMessage( "ERROR", validationErrorRes.UAF, "OK", "ERROR", callBack, null );
						}
					//Temporary solution.	
					} else if(errorResponse.error.code == "" || !errorResponse.error.code){
						/*
						sap.m.MessageBox.show( "Unable to complete request due to server failure!",
								sap.m.MessageBox.Icon.ERROR,
								"ERROR",
								sap.m.MessageBox.Action.OK,
								callBack
						);
						*/
						this.showMessage( "ERROR", "Unable to complete request due to server failure!", "OK", "ERROR", callBack, null );
						
					} else {
						//Handle Service / Business / OData Error. Show Message in Dialog.
						var errorMsg = errorResponse.error.message.value;						
						if(errorMsg && errorMsg === "Not entitled to perform the specified action."){
							if(!this.entitlementRefreshed){
								this.entitlementRefreshed = true;
								this.handleEntitlementChange();
							}	
						} else {
							if(!errorMsg || errorMsg == "")
							{
								errorMsg = sap.ui.getCore().getModel("i18N").getResourceBundle().getText("common.error.message"); 
							}
							/*
							sap.m.MessageBox.show(errorMsg,
								sap.m.MessageBox.Icon.ERROR,
								"ERROR",
								sap.m.MessageBox.Action.OK,
								callBack
							);
							*/
							this.showMessage( "ERROR", errorMsg, "OK", "ERROR", callBack, null );
						}	
					}
	    		}
			}catch(err){
				//TODO: Uncomment alert
				/*
				sap.m.MessageBox.show(oError.message,
						sap.m.MessageBox.Icon.ERROR,
						"ERROR",
						sap.m.MessageBox.Action.OK,
						callBack
					);
				*/
			}
    	} else {
    		var oResourceBundle = sap.ui.getCore().getModel("i18N").getResourceBundle();    
    		var sErrorLabel = oResourceBundle.getText("common.label.error");		
    		//Check if server returns 404, which indicates backend services are unavailable
    		var scope = this;
    		var waitForConsecutiveErrorCallbacks = function(){
    			var dfd = new $.Deferred();
    			setTimeout(dfd.resolve,8000);
    			return dfd.promise();
    		};

    		if(oError.response && oError.response.statusCode === 404){
    			if(this.bServerDownMesageShown === false){    				
    				var sServerDownMessage = oResourceBundle.getText("server.down.message");
	    			
					//TODO:Have one more attempt to test services and then navigate?
	    			/*
					sap.m.MessageBox.show(sServerDownMessage,
						sap.m.MessageBox.Icon.ERROR,
						sErrorLabel,
						sap.m.MessageBox.Action.OK,
						function(){						
							//Navigate to signin page in case of server is down
							homeNavContainer.to("Signin",{data:{action: "timeout"}});
							if(homeNavContainer.getCurrentPage().getId() === "Signin"){
								scope.bServerDownMesageShown = false; //Reset the server down message shown flag			
							}
						}
					);
					*/
					swal({   
						title: sErrorLabel,   
						text: sServerDownMessage,   
						type: "error",   
						confirmButtonText: "OK",
						}, function(){
								//Navigate to signin page in case of server is down
							Navigation_Invoke_Page({action: "timeout", PageTo: Common.Navigations.PAGE_SIGN_IN});
							if(homeNavContainer.getCurrentPage().getId() === "Signin"){
								scope.bServerDownMesageShown = false; //Reset the server down message shown flag			
							}
						}
					);

	    			this.bServerDownMesageShown = true;

	    			//Wait for consecutive error responses and then reset this flag
	    			waitForConsecutiveErrorCallbacks().then(function(){
	    				scope.bServerDownMesageShown = false;	
	    			});					
    			}    			
    		} else if(oError.response && oError.response.statusCode === Common.ErrorTypes.NETWORK_OFFLINE_ERROR){
    			
    			// NETWORK_Went_Offline( oError );
    			
    			//window.navigator.onLine === false
//    			var sNetWorkOffilineMessage = oResourceBundle.getText("network.offline.message");
//    			var obMessage = AXX_EXCEPTION_Translate_ERROR( "USER_EXIT_APP_NETWORK" );
				/*
    			sap.m.MessageBox.show(sNetWorkOffilineMessage,
					sap.m.MessageBox.Icon.ERROR,
					sErrorLabel,
					sap.m.MessageBox.Action.OK,
					callBack
				);
				*/
//				this.showMessage( 
//					null, 
//					obMessage.meesage, 
//					"OK", 
//					"ERROR", 
//					function(){ 
//						callBack();
//					},
//					function(){ 
//						navigator.app.exitApp(); 
//					} 
//				);
				
    			//TODO:Show offline message after ajax hypermedia head ping
    		}else{
    			var httpRequestFailed = oResourceBundle.getText("http.request.failed.message");
				/*
    			sap.m.MessageBox.show(httpRequestFailed,
					sap.m.MessageBox.Icon.ERROR,
					sErrorLabel,
					sap.m.MessageBox.Action.OK,
					callBack
				);
				*/
				this.showMessage( sErrorLabel, httpRequestFailed, "OK", "ERROR", callBack, null );
			}
    	}
    	
	},
	
	
	/**
	 * This function will handle entitlement related change 
	 */
	handleEntitlementChange : function(){
		// Create dialog to show to user
		var entitlementChangeDialog = new sap.m.BusyDialog({
			title:sap.ui.getCore().getModel("i18N").getResourceBundle().getText("entitlementChange.dialog.title"),
			text:sap.ui.getCore().getModel("i18N").getResourceBundle().getText("entitlementChange.dialog.text")
		});
		entitlementChangeDialog.open();
		
		// Define failure / Success callback
		var timeOutId = 0, scope = this;
		var successCallBack = function(jsonData){
			var oHomeController = sap.ui.getCore().byId("Home").getController();
			oHomeController.globalEntitlementsObj = jsonData.Entitlements.results;;
			Navigation_Invoke_Page({PageTo: Common.Navigations.PAGE_HOME});
			entitlementChangeDialog.close();
			timeOutId = setTimeout(function(){
				scope.entitlementRefreshed = false;
			}, 10000);
		};
		
		var failureCallBack = function(oError){
			entitlementRefreshed = false;
			entitlementChangeDialog.close();
			clearTimeout(timeOutId);
			scope.handleError(oError);
		};	
		
		// Update Entitlement by making server request.
		var entitlementHandler = new util.EntitlementHandler();
		entitlementHandler.FetchEntitlementData(successCallBack, failureCallBack);
	},
	
	showMessage : function( stTitle, stMessage, stButtonText, stErrorType, callBackSuccess, callBackError ){
		
//		AXX_MESSAGE_CONFIRM(
//			stTitle, 
//			stMessage,
//			null,
//			callBackSuccess,
//			callBackError
//		);
		
	}
	
};
