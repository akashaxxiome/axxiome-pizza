function throwValidationError(obError){
    AXX_EXCEPTION_USER(
        obError.title
        , obError.message
        , true
        , "error"
        , obError.component
    );    
}

function createValidationObject(){
    return {error: false, title : [], message: [], component: []};    
}