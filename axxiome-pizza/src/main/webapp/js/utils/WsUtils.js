
// var wsPackageWrapper = null;

// var activeWebService = null;

//[id] => wrapper

var wsTimeOutTimer = null;


function generateWSId(){
    // var random() * 1000000000;

    return 123;
}

// function WSParameters(name, callMethod, onSuccess, onFail, customData){
//     this.name = name;
//     this.callMethod = callMethod;
//     this.onSuccess = onSuccess;
//     this.onFail = onFail;
//     this.customData = customData;
//     this.ready = false;
// }
//
// WSParameters.prototype.setName = function(name){
//     this.name = name;
//
//     return this;
// }
//
// WSParameters.prototype.setCallMethod = function(callMethod){
//     this.callMethod = callMethod;
//
//     return this;
// }
//
// WSParameters.prototype.setOnSuccess = function(onSuccess){
//     this.onSuccess = onSuccess;
//
//     return this;
// }
//
// WSParameters.prototype.setCustomData = function(customData){
//     this.customData = customData;
//
//     return this;
// }

function WSParameters(name,callMethod, wsData){
    this.name = name;
    this.wsData = wsData;
    this.callMethod = callMethod;
}

function WSPackageWrapper(onAllWsSucess, wSArray, controller, timeOut, actionOnFail){
    this.onAllWsSucess = onAllWsSucess;
    this.wSArray = wSArray;
    this.id = generateWSId();
    this.timeOut = timeOut;
    this.controller = controller;
    this.actionOnFail = actionOnFail;
    myCurrentData.activeWebService = this;
}

WSPackageWrapper.prototype.setAllCallBackSuccess = function(onAllWsSucess){
    this.onAllWsSucess = onAllWsSucess;

    myCurrentData.activeWebService = this;
    
    return this;
}

WSPackageWrapper.prototype.setWsArray = function(wSArray){
    this.wSArray = wSArray;

    return this;
}

WSPackageWrapper.prototype.setOperation = function(wsOperation){
    this.wSArray = [wsOperation];

    return this;
}


WSPackageWrapper.prototype.setController = function(controller){
    this.controller = controller;

    return this;
}


WSPackageWrapper.prototype.setActionOnFail = function(actionOnFail){
    this.actionOnFail = actionOnFail;

    return this;
}



/**
 * Identifies a web service call - one method
 * @param wsParameters
 * @constructor
 */

//temporary solution
function WS(wsParameters, wsData) {

    if (typeof wsParameters =='object') {

        this.name = wsParameters.name;
        this.ready = wsParameters.ready;
        this.callMethod = wsParameters.callMethod;
        this.wsData = wsParameters.wsData;//parameters for calling WS, like success, validation, mapOfFieldsOnView
    }
    else {
        this.ready = false;
        this.callMethod = wsParameters;
        this.wsData = wsData;
    }

    if (isEmpty(this.wsData))
        this.wsData = {dataModelName:null}; //modelToUpdate in sap.ui.setModel
    else
        this.wsData.dataModelName = null;


    this.ajaxCall = null;//variable storing AJAX connection
    myCurrentData.activeWebService = null;

    // this.callMethod = wsParameters.callMethod;
    // this.onSuccess = wsParameters.onSuccess;
    // this.onFail = wsParameters.onFail;
    // this.customData = wsParameters.customData;
    // this.ready = wsParameters.ready;
}

/**
 * Container for WSs
 * @param onAllWsSucess
 * @param wSArray
 * @param id
 */
WSPackageWrapper.prototype.startWS = function(){

    console_log("startWS: " , this.wSArray );

    var id = this.id;

    //TODO: DIEGO -> Can we clear this parameter before execution ?
    // myCurrentData.activeWebService = null;

    this.wSArray.forEach(function(ws,i) {
        console_log("Iterating over WS array current iteration: " + i)
        ws.wsData.parentId = id;
        // ws.wsData.operationName = ws.name;
        ws.wsData.operationIndex = i;
        ws.callMethod(ws.wsData);
        ws.ready = false; //for reattempts
    });
    
    //wsTimeOutTimer = setTimeout(checkWSTimeOut, this.timeOut == undefined ? Common.WebServices.TIME_OUT : this.timeOut);
    console_log('set timeout checkWSTimeOut_NEW ' + (this.timeOut == undefined ? Common.WebServices.TIME_OUT : this.timeOut))
    wsTimeOutTimer = setTimeout( checkWSTimeOut_NEW, this.timeOut == undefined ? Common.WebServices.TIME_OUT : this.timeOut);

}

WSPackageWrapper.prototype.executeActionOnAllWSReady = function(operationIndex){
    // this.updateWsArrayStatusByWsName(wsName);

    this.wSArray[operationIndex].ready = true;

    if(this.isAllWsDone()){

        // myCurrentData.activeWebService = null;
        clearTimeout(wsTimeOutTimer);
        
        console_log('clearTimeout(wsTimeOutTimer)');
        
        if (!isEmpty(this.onAllWsSucess)) {
            this.onAllWsSucess();
        } else {
            if (this.controller != null){
                // app.setBusy(this.controller.getView(), false);
                app.setCurrentViewBusy(false);
            }
        }

    }
    
}


WSPackageWrapper.prototype.wsFailed = function(operationIndex, res){
    if(myCurrentData.IS_MOBILE){
        GENERIC_WEB_SERVICE_VALIDATION_FAILURE_MOBILE(res, this.wSArray[operationIndex].wsData.failure_callback );
    }else {
    clearTimeout(wsTimeOutTimer);
    console_log('clearTimeout(wsTimeOutTimer)');
    GENERIC_WEB_SERVICE_FAILURE( res, "", this.wSArray[operationIndex].wsData.failure_callback );
    }
}

WSPackageWrapper.prototype.executeActionOnAllWSReadyFailure = function(wsName){
    this.updateWsArrayStatusByWsName(wsName);

    if(this.isAllWsDone()){
        // myCurrentData.activeWebService = null;
        console_log('clearTimeout(wsTimeOutTimer)');
        clearTimeout(wsTimeOutTimer);
        app.setCurrentViewBusy(false);
    }
}


WSPackageWrapper.prototype.updateWsArrayStatusByWsName = function(wsName) {

    for (var i = 0; i < this.wSArray.length; i++) {

        if (this.wSArray[i]['name'] == wsName) {
            this.wSArray[i].ready = true;
        }
    }
}

WSPackageWrapper.prototype.isAllWsDone = function(){

    for (var i = 0; i < this.wSArray.length; i++) {
        if(!this.wSArray[i].ready){
            return false;
        }
    }

    return true;
}

WSPackageWrapper.prototype.killAllAjaxCalls = function(){
    for (var i = 0; i < this.wSArray.length; i++) {
        if (!this.wSArray[i].ready && this.wSArray[i].ajaxCall != null) {
            this.wSArray[i].ajaxCall.abort();
        }
    }
}

function killCurrentWebservice(){
    //myCurrentData.activeWebService
}



function checkWSTimeOut(){

    if (myCurrentData.activeWebService != null){

        if (!myCurrentData.activeWebService.isAllWsDone()){
            //TODO fail

            AXX_MESSAGE_CONFIRM("Error","Error de conexión");

            app.setCurrentViewBusy(false);
        }

        myCurrentData.activeWebService = null;

    }
    console_log('checkWSTimeOut -> clearTimeout(wsTimeOutTimer)');
    clearTimeout(wsTimeOutTimer);
}


function checkWSTimeOut_NEW(){

    console_log('checkWSTimeOut_NEW');

    //TODO: KK 24.08 -> This wsTimeOutTimer if cleared in another execution, cannot execute this method
    console_log('checkWSTimeOut_NEW -> clearTimeout(wsTimeOutTimer)');

    clearTimeout(wsTimeOutTimer);

    console_log(myCurrentData.activeWebService);
    console_log(myCurrentData.WS_CALL);

    if (myCurrentData.activeWebService != null){

        if (!myCurrentData.activeWebService.isAllWsDone()){

            // ajaxCall.abort();
            myCurrentData.activeWebService.killAllAjaxCalls();
            
            myCurrentData.boCallBackError_Stop = true;

            var obError = fn_APP_GET_Translation_By_ErrorCode( WSExceptions.TIMEOUT_ERROR );

            var NETWORK_TIMEOUT_HANDLER__obParameters = myCurrentData.TEMP.obParameters;

            NETWORK_TIMEOUT_HANDLER__obParameters.obError = obError;
            NETWORK_TIMEOUT_HANDLER__obParameters.obError.res = null;

            try {
                NETWORK_TIMEOUT_HANDLER__obParameters.obError.operationIndex = myCurrentData.WS_CALL.parameters.operationIndex;
            }catch(e){}
            

            NETWORK_TIMEOUT_HANDLER__obParameters.newTentative = true;


            NETWORK_TIMEOUT_HANDLER( NETWORK_TIMEOUT_HANDLER__obParameters );

        } else{
            myCurrentData.activeWebService = null;
        }

    } else{
        //TODO: DBC -> added it here now INTEGRALES test. check if it is necessary
        myCurrentData.activeWebService = null;
    }


}
