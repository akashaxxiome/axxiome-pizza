

	// /* Customization constants */
    // var CACHE_OTHER_IDENTIFICATIONS = true; // Set this to cache other identifications and avoid repeated calls to server //
    //
    // var CACHE_ALERT_NOTIFICATION_MSG_IDS = true; // Set this to avoid caching of alert notification message ids //
    //
    // var MSISDN = '+919036404940';
    // var PIN = '3456';
    // var CID = '500011352'; //Customer ID
	// //var currentAccountId = "500011054";
	//
	//
	// var isSignedIn = false;
	var appHasJustStarted = true;
	var isVIEWS_After_Login_Loaded = false;
	
	var oApp = new Application({root : "content"});
	var ios7;
	
	try {
    	sessionStorage.clear();
    } catch(e){ console_log(e); }

    //console_log("IS MOBILE TEST ----------------");
	myCurrentData.DEVICE = new COMPARTAMOS_DEVICE({});
	
	
	$("body").removeClass("AXX_Mobile");
	$("body").removeClass("AXX_Desktop");
	myCurrentData.IS_MOBILE = false;
	if ( (/iPhone|iPod|iPad|Android|BlackBerry/).test(navigator.userAgent) ){
		myCurrentData.IS_MOBILE = true;
		$("body").addClass("AXX_Mobile");
	} else {
		myCurrentData.DEVICE.set_DEVICE();
		$("body").addClass("AXX_Desktop");
	}
	//console_log("IS MOBILE [" + myCurrentData.IS_MOBILE + "]");
	//console_log("IS MOBILE TEST ---------------- END");
	//$("#event_received").html("<br>Loading modules ..");
	
	
	document.addEventListener( 
		"deviceready", 
		function(){
			$("#event_received").html("Device is Ready");
			myCurrentData.DEVICE.set_DEVICE();
		}, 
		false 
	);
	
	$(document.body).on("click", ".sapMDialogScroll:not(sapMDialogScrollCont)", function(evt){
		var myTarget = evt.target.className;
		if ( myTarget == "sapMDialogScroll" ){
			var oCon = sap.ui.getCore().byId( homeNavContainer.getCurrentPage().getId() ).getController(); 
			oCon.clearView();
		}
	});
	