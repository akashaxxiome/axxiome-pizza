var isPinningSet = false;

function CORDOVA_verify_PINNING() {

	// console_log(cordovaHTTP);

	// if(isPinningSet){
	// 	return isPinningSet;
	// }

	// window.cordovaHTTP.enableSSLPinning( 
	// 	AXX_SETTINGS.ENABLE_PINNING, 
	// 	function () {
	// 		window.cordovaHTTP.setHeader("Content-Type", "application/json");
	// 		window.cordovaHTTP.setHeader("Accept", "application/json");
	// 		console_log('CORDOVA_verify_PINNING ... SUCCESS!');
	// 		isPinningSet = true;
	// 	}, 
	// 	function () {
	// 		showConnectionUnavailable();
	// 		isPinningSet = true;
	// 		console_log('CORDOVA_verify_PINNING ... ERROR :(');
	// 	}
	// );

	// window.cordovaHTTP.acceptAllCerts(
	// 	AXX_SETTINGS.ACCEPT_ALL_CERTIFICATES,
	// 	function(res) {console_log('Accept all certs: ' + res)},
	// 	function(err) {console_log('Accept all certs: ' + err)}
	// );

	// window.cordovaHTTP.validateDomainName(
	// 	AXX_SETTINGS.VALIDATE_DOMAIN_NAME, 
	// 	function() { console_log('success! validatedomain name ');	}, 
	// 	function() { console_log('error validate domain name (');	}
	// );

	// return isPinningSet;

}


var showConnectionUnavailable = function () {
		var obMessage = AXX_EXCEPTION_Translate_ERROR("USER_EXIT_APP_NETWORK");
		AXX_MESSAGE_CONFIRM(
			obMessage.title,
			obMessage.message,
			null,
			function (){
				try{
					navigator.app.exitApp();
				} catch(e){
					swal.close();
				}
			}, null, true
		);
	};

	function verify_STORAGE(){
		
		console_log("CordovaTest :: verify_STORAGE ...");
  
		jQuery.sap.require("jquery.sap.storage");  
		oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);  
		
		var oData = oStorage.get("COMPARTAMOS_DATA");  
		
		//Create Model
		var oModel = new sap.ui.model.json.JSONModel();  
		oModel.setData(oData); 
		  
		/*
		var oNew_CompartamosDATA = {  
			UUID : $.now()
			TESTE: "FLU" 
		};  
		var oDataNEW = oModel.getData();  
		oDataNEW.Collection.push(oNew_CompartamosDATA);  
		oModel.setData(oDataNEW); 
		oModel.refresh(true);  
		oStorage.put("COMPARTAMOS_DATA", oDataNEW);  
  
		if (oStorage.get("COMPARTAMOS_DATA")) {  
			console_log("Data is from Storage!");  
		} else {
			console_log("No Data found in STORAGE");
			//oStorage.put("DIEGO_DATA", "FLU");
		}
		/*
		if( jQuery.sap.storage.isSupported() ){
				
			console_log( "STORAGE IS SUPPORTED !" );
			
		}
		*/
		console_log("CordovaTest :: verify_STORAGE ...");
	}
	
	function verify_language(  ){
		console_log("CordovaTest :: verify_LANGUAGE ...");
		
		try{
		var userLang = navigator.language || navigator.userLanguage; 
		console_log("The language is: " + userLang);
		} catch (e){
//			console_log( "ERROR: " + e.message );
			AXX_EXCEPTION_APP( "Not able to get the BROWSER LANGUAGE", "DEVICE" );
		}
		console_log("CordovaTest :: verify_LANGUAGE ... done");
	}
	
	function verify_is_Mobile(){
		
		console_log("CordovaTest :: verify_is_Mobile ...");
		
		//var myCurrentData.IS_MOBILE = false; //initiate as false
		// device detection
		if ( (/iPhone|iPod|iPad|Android|BlackBerry/).test(navigator.userAgent) ){
			myCurrentData.IS_MOBILE = true;
		}
		console_log ( "IS MOBILE [" + myCurrentData.IS_MOBILE + "]" );
		console_log( navigator.userAgent );
		//sessionStorage.setItem("UserDevice", navigator.userAgent);
	
		console_log("CordovaTest :: verify_is_Mobile ... done");
		
	}
	
	
	function getJSessionId(){
		var jsId = document.cookie.match(/JSESSIONID=[^;]+/);
		if(jsId != null) {
			if (jsId instanceof Array)
				jsId = jsId[0].substring(11);
			else
				jsId = jsId.substring(11);
		}
		return jsId;
	}
	
	
	function getDeviceInfo(){
		
		try{ 
			var element = document.getElementById('deviceProperties');
			if ( typeof element != "undefined" ){
				try {
					if ( typeof device != "undefined" ){
						try {
							var myOutput = '<span class="info_title">Device:</span> '    + navigator.userAgent    + 
									'<br /><br />' +
									//'<span class="info_title">Device Name:</span> '     + device.name     + '<br />' +
									'<span class="info_title">Device Model:</span> '    + device.model    + '<br />' +
									'<span class="info_title">Device Cordova:</span> '  + device.cordova  + '<br />' +
									'<span class="info_title">Device Platform:</span> ' + device.platform + '<br />' +
									'<span class="info_title">Device UUID:</span> '     + device.uuid     + '<br />' +
									'<span class="info_title">Device Version:</span> '  + device.version  + '<br />';
							console_log( myOutput );
							element.innerHTML = myOutput;
						} catch (e){
							throw "Device Module not loaded";	
						}
					} else {
						throw "Device Module not loaded";
					}
				} catch (e) {
					element.innerHTML = e;
					throw e;
				}
			} else { throw "Device Module not ready"; }
		} catch (e){
			AXX_EXCEPTION_APP( e, "APP DEVICE" );
		}
	};
	
	
	// onSuccess Callback
	//   This method accepts a `Position` object, which contains
	//   the current GPS coordinates
	//
	var onSuccessGPS = function(position) {
		console_log('Latitude: '          + position.coords.latitude          + '\n' +
			  'Longitude: '         + position.coords.longitude         + '\n' +
			  'Altitude: '          + position.coords.altitude          + '\n' +
			  'Accuracy: '          + position.coords.accuracy          + '\n' +
			  'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
			  'Heading: '           + position.coords.heading           + '\n' +
			  'Speed: '             + position.coords.speed             + '\n' +
			  'Timestamp: '         + position.timestamp                + '\n');
	};
	
	// onError Callback receives a PositionError object
	//
	function onError(error) {
		console_log('code: '    + error.code    + '\n' +
			  'message: ' + error.message + '\n');
	};
	
	
	
	var authenticate = function(){
		
		console_log("CordovaTest :: HTTP PINNING ...");
		
		try { 
			
			//console_log( cordovaHTTP );
			
			cordovaHTTP.setHeader("MyUser", "tbowman", function() {
				console_log('SetHeader 1 success!');
			}, function() {
				console_log('SetHeader 1 error :(');
			});
			
			cordovaHTTP.setHeader("MyPin", "Pass1234", function() {
				console_log('SetHeader 2 success!');
			}, function() {
				console_log('SetHeader 2 error :(');
			});
			
			cordovaHTTP.enableSSLPinning(true, function() {
				console_log('enableSSLPinning success!');
			}, function() {
				console_log('enableSSLPinning error :(');
			});
			
			
			console_log('>> cordovaHTTP GET .. starting');
			cordovaHTTP.get( 
				"https://connections.axxiome.com/xcc/main", 
				{
				//cordovaHTTP.get("https://localhost:8443", {
					id: 12,
					message: "test"
				}, { Authorization: "OAuth2: token" }, function(response) {
					console_log('cordovaHTTP NEW GET success!');
					console_log(response.status);
					console_log(xinspect(response));
					
					//console_log(response);
				}, function(response) {
					console_log('cordovaHTTP GET error!');
					console.error(response.error);
					console_log(xinspect(response));
				}
			);

		} catch (error){
			AXX_EXCEPTION_APP( "HTTP Pinning error", "DEVICE");
		}
		
		console_log("CordovaTest :: authenticate ... done");
		
	}
	
	function xinspect(o,i){
		if(typeof i=='undefined')i='';
		if(i.length>50)return '[MAX ITERATIONS]';
		var r=[];
		for(var p in o){
			var t=typeof o[p];
			r.push(i+'"'+p+'" ('+t+') => '+(t=='object' ? 'object:'+xinspect(o[p],i+'  ') : o[p]+''));
		}
		return r.join(i+'\n');
	}
