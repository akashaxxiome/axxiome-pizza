jQuery.sap.declare("js.settings.DeviceSettings");

/** Dummy controller required for Device Setting dialog */
var deviceSettingController = {
	
	deviceSettingsDialog : null,
	
	/**
	* This function initializes the connection settings model, before opening connection setting dialog.
	*/
	getSavedDeviceSetting : function(event){
		this.deviceSettingsDialog = event.getSource();
		var oSettings  = this.getStoredDeviceSettingsHostURI();
		var oSettingsModel = new sap.ui.model.json.JSONModel(oSettings);
		deviceSettingsDialog.setModel(oSettingsModel);
	},
		
	/**
	 * This function will check if there is any connection setting available in HTML5 local storage.
	 * If available, it returns URI with host and port.
	 * If not available, it returns default values. 
	 */
	getStoredDeviceSettingsHostURI : function(){
		
		//Get Device Settings from HTML 5 - local storage.
		var oSettings = null, sDeviceSettings  = window.localStorage.getItem(Authentication.Device.STORAGE_KEY);
		try{
			oSettings = JSON.parse(sDeviceSettings);
		}catch(e){
			// left empty on purpose
		}
		
		//default settings
		if(!oSettings){
			
			//TODO: Need to call CORDOVA METHOD
			if( typeof device == "object" ) {
				oSettings = {
					uuid: device.uuid,
					manufacturer: device.manufacturer,
					platform: device.platform,
					model: device.model,
					version: device.version,
				};
			} else {
				AXX_EXCEPTION_APP("Not able to load DEVICE INFORMATION", "CORDOVA PLUGIN");
				oSettings = {
					uuid: "N/A",
					manufacturer: "N/A",
					platform: "N/A",
					model: "N/A",
					version: "N/A",
				};
			}
			this.onSaveDeviceSettings(oSettings);
		}
		
		return oSettings;
		
	},

	/**
	 * Event Handlers for saving connection settings
	 */
	onSaveDeviceSettings: function( oSettings ){
		//Save it to HTML 5 local storage.
		//var oSettings = this.getDataFromScreen();
		var sDeviceSettings = JSON.stringify(oSettings);
		//console_log(" >>> onSaveDeviceSettings ... ");
		//console_log(oSettings);
		window.localStorage.setItem(Authentication.Device.STORAGE_KEY, sDeviceSettings);
		
		//this.deviceSettingsDialog.getModel().refresh();
		//var host = this.getHostURI(oSettings);
		//sap.banking.ui.services.ServiceConstants.changeHost(host);
		//this.deviceSettingsDialog.close();
		
		//Reset all Services and their ODataModels
		//sap.banking.ui.services.ServiceFactory.unInitializeAllServices(true);
		console_log(" >>> onSaveDeviceSettings ... done");
	},
	
	/**
	 * Event Handlers for testing connection settings
	 */
	onTestDevice : function(){
		
		//deviceSettingsDialog.setBusy(true);
		//var oSettings = this.getDataFromScreen();
		//var host = this.getHostURI(oSettings);
		
		/*
		//Create Sample OdataModel to perform $metadata request.
		var serviceUrl = sap.banking.ui.services.ServiceConstants.AUTHENTICATIONSERVICE.NONSECURE_URI;
		serviceUrl = serviceUrl.substring(serviceUrl.lastIndexOf(':'), serviceUrl.length);
		serviceUrl = serviceUrl.substring(serviceUrl.indexOf('/') + 1, serviceUrl.length);
		serviceUrl = host + serviceUrl;
		this.oModel = new sap.banking.ui.model.OCBODataModel(serviceUrl,true,null,null,null,null,null,true)
		.attachMetadataLoaded(null, this.successMetaDataLoaded, this).attachMetadataFailed(null, this.failureMeataDataLoaded, this);
		*/
		
		this.onSaveDeviceSettings();
	},
	
	getDataFromScreen : function(){
		var oSettings = this.deviceSettingsDialog.getModel().getData();
		for ( var prop in oSettings) {
			if(oSettings[prop]){
				oSettings[prop] = oSettings[prop].trim();
			}
		}
		return oSettings;
	},
	
	successMetaDataLoaded : function(){
		/*
		//deviceSettingsDialog.setBusy(false);
		sap.m.MessageBox.show(sap.ui.getCore().getModel("i18N").getResourceBundle().getText("connectionSettings.msg.connectionSuccess"),
				sap.m.MessageBox.Icon.INFORMATION,
				"SUCCESS",
				sap.ui.getCore().getModel("i18N").getResourceBundle().getText("common.button.Save"),
				jQuery.proxy(this.onSaveDeviceSettings, this) 
			);
		
		//Destroy OData model
		arguments[0].oSource.destroy();
		*/
	},
	
	failureMeataDataLoaded : function(){
		deviceSettingsDialog.setBusy(false);
		sap.m.MessageBox.show(sap.ui.getCore().getModel("i18N").getResourceBundle().getText("connectionSettings.msg.connectionFailure"),
				sap.m.MessageBox.Icon.ERROR,
				"ERROR",
				sap.m.MessageBox.Action.OK,
				null
			);
		//Destroy OData model
		arguments[0].oSource.destroy();
	},
	
	/**
	 * This function will form Host URL in a fixed format
	 */
	getHostURI : function(jsondeviceSettings){
		return jsondeviceSettings.protocol + "://" + jsondeviceSettings.server + ":" + jsondeviceSettings.port + "/";
	},
	
	/**
	 * This function will fetch connection settings from HTML5 localstorage and 
	 * update it in Constant files so that ODataModel can refer it.
	 */
	initializeDeviceSettings : function(){
		var deviceSettingsJson = this.getStoredDeviceSettingsHostURI();
	}	
};