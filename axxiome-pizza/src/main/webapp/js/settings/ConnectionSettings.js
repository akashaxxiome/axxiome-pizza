jQuery.sap.declare("js.settings.ConnectionSettings");

var arENVS = [];
var contArENVS = 0;
$.each( AXX_SETTINGS.RELEASE, function( index, theItem ) {
    arENVS.push([ contArENVS, index ]);
    contArENVS++;
});
myCurrentData.URL = arENVS[0];

var TimeOutSSL = null;

/** Dummy controller required for Connection Setting dialog */
var connectionSettingController = {

	conSettingsDialog: null,

	afterConnectionSettingsClose: function (event) {
		// removing selected state class from settings icon in non-secure menu
		$(".connSettings").removeClass("menuSelectedItem");
		event.getSource().destroy();
	},

	/**
	 * This function initializes the connection settings model, before opening connection setting dialog.
	 */
	getSavedConnectionSetting: function (event) {
		this.conSettingsDialog = event.getSource();
		var oConSettings = this.getStoredConnectionSettingsHostURI();
		console_log("_");
		console_log("oConSettings data");
		console_log(oConSettings);
		console_log("_");
		var oConSettingsModel = new sap.ui.model.json.JSONModel(oConSettings);
		conSettingsDialog.setModel(oConSettingsModel);
	},

	/**
	 * This function will check if there is any connection setting available in HTML5 local storage.
	 * If available, it returns URI with host and port.
	 * If not available, it returns default values.
	 */
	getStoredConnectionSettingsHostURI: function () {
		//Get Connection Settings from HTML 5 - local storage.
		var oConSettings = null;
		sConSettings = null;
		try {
			sConSettings = window.localStorage.getItem(Authentication.Connection.STORAGE_KEY);
			oConSettings = JSON.parse(sConSettings);
		} catch (e) {
			// left empty on purpose
		}

		if (!oConSettings) {

			var myProtocol = ( AXX_SETTINGS[myCurrentData.URL[1]].CONNECTION_SETTINGS.HTTPS == false ) ? "http" : "https";
			oConSettings = {
				protocol: myProtocol,
				server: AXX_SETTINGS[myCurrentData.URL[1]].CONNECTION_SETTINGS.IP,
				port: AXX_SETTINGS[myCurrentData.URL[1]].CONNECTION_SETTINGS.PORT
			};


		}

		return oConSettings;

	},

	getConnection_URL: function (typeConnection) {

		var myConnection = this.getStoredConnectionSettingsHostURI();
		var stRequest = myConnection.protocol + "://" + myConnection.server ;
		if ( myConnection.port ){
			stRequest = stRequest + ":" + myConnection.port ;
		}
		// stRequest = stRequest + "/";

		//console_log(stRequest);
		if (typeConnection == null || typeConnection == "" || typeConnection.toLowerCase() == "rest") {
			stRequest += "mobiliser/rest/smartphone";
		} else if (typeConnection.toLowerCase() == "odata") {
			stRequest += "atbpizza";
		}

		return stRequest;

	},


	/**
	 * Event Handlers for saving connection settings
	 */
	onSaveConnectionSettings: function (oEvent) {
		//Save it to HTML 5 local storage.
		var oConSettings = this.getDataFromScreen();
		var sConSettings = JSON.stringify(oConSettings);
		window.localStorage.setItem(Authentication.Connection.STORAGE_KEY, sConSettings);

		this.conSettingsDialog.getModel().refresh();
		var host = this.getHostURI(oConSettings);
		sap.banking.ui.services.ServiceConstants.changeHost(host);
		this.conSettingsDialog.close();

		//Reset all Services and their ODataModels
		sap.banking.ui.services.ServiceFactory.unInitializeAllServices(true);
	},

	/**
	 * Event Handlers for testing connection settings
	 */
	onTestConnection: function () {
		conSettingsDialog.setBusy(true);
		var oConSettings = this.getDataFromScreen();
		var host = this.getHostURI(oConSettings);

		//Create Sample OdataModel to perform $metadata request.
		var serviceUrl = sap.banking.ui.services.ServiceConstants.AUTHENTICATIONSERVICE.NONSECURE_URI;
		serviceUrl = serviceUrl.substring(serviceUrl.lastIndexOf(':'), serviceUrl.length);
		serviceUrl = serviceUrl.substring(serviceUrl.indexOf('/') + 1, serviceUrl.length);
		serviceUrl = host + serviceUrl;
		this.oModel = new sap.banking.ui.model.OCBODataModel(serviceUrl, true, null, null, null, null, null, true)
			.attachMetadataLoaded(null, this.successMeataDataLoaded, this).attachMetadataFailed(null, this.failureMeataDataLoaded, this);
	},

	getDataFromScreen: function () {
		var oConSettings = this.conSettingsDialog.getModel().getData();
		for (var prop in oConSettings) {
			if (oConSettings[prop]) {
				oConSettings[prop] = oConSettings[prop].trim();
			}
		}
		return oConSettings;
	},

	successMeataDataLoaded: function () {

		conSettingsDialog.setBusy(false);
		sap.m.MessageBox.show(
			sap.ui.getCore().getModel("i18N").getResourceBundle().getText("connectionSettings.msg.connectionSuccess"),
			sap.m.MessageBox.Icon.INFORMATION,
			"SUCCESS",
			sap.ui.getCore().getModel("i18N").getResourceBundle().getText("common.button.Save"),
			jQuery.proxy(this.onSaveConnectionSettings, this)
		);

		//Destroy OData model
		arguments[0].oSource.destroy();
	},

	failureMeataDataLoaded: function () {

		conSettingsDialog.setBusy(false);
		/*
		 sap.m.MessageBox.show(sap.ui.getCore().getModel("i18N").getResourceBundle().getText("connectionSettings.msg.connectionFailure"),
		 sap.m.MessageBox.Icon.ERROR,
		 "ERROR",
		 sap.m.MessageBox.Action.OK,
		 null
		 );
		 */
		var stErrorTitle = "ERROR";
		var stErrorMessage = sap.ui.getCore().getModel("i18N").getResourceBundle().getText("connectionSettings.msg.connectionFailure");
		/*
		 showMessage( stErrorTitle, stErrorMessage, "OK", "error", null, null );
		 */

		swal({
				title: stErrorTitle,
				text: stErrorMessage,
				type: "error",
				confirmButtonText: "OK",
			}
		);

		//Destroy OData model
		arguments[0].oSource.destroy();
	},

	/**
	 * This function will form Host URL in a fixed format
	 */
	getHostURI: function (jsonConSettings) {
		return jsonConSettings.protocol + "://" + jsonConSettings.server + ":" + jsonConSettings.port + "/";
	},


	/**
	 * This function will fetch connection settings from HTML5 localstorage and
	 * update it in Constant files so that ODataModel can refer it.
	 */
	initializeConnectionSettings: function () {
		var conSettingsJson = this.getStoredConnectionSettingsHostURI();
		console_log("_");
		console_log("conSettingsJson data");
		console_log(conSettingsJson);
		console_log("_");
		if (conSettingsJson) {
			var conSettingsURI = this.getHostURI(conSettingsJson);
			sap.banking.ui.services.ServiceConstants.changeHost(conSettingsURI);
		}
	},

	initiateSSL: function (successCallback) {

		var obView = sap.ui.getCore().byId( Common.Navigations.PAGE_SIGN_IN );
		
		app.setBusy( obView, false );
		swal.close();

		TimeOutSSL = null;
		clearTimeout( TimeOutSSL );
		
		if ( new AXX_CONN().hasNetworkConnection() == false ){

			var obMessage = AXX_EXCEPTION_Translate_ERROR( "NETWORK" );
			AXX_MESSAGE_CONFIRM( 
				obMessage.title, 
				obMessage.message, 
				[], 
				function(){
					swal.close();
					app.setBusy( obView, true );
					TimeOutSSL = setTimeout( 
						function(){
							connectionSettingController.initiateSSL( successCallback );
						}, 
						2000
					);
				},
				function(){
					
					//What to do when the user don't want to try again.
					var obParameters = {};
					var obMessage = AXX_EXCEPTION_Translate_ERROR( "USER_EXIT_APP_NETWORK" );
					obParameters.fn_callBack_YES = function(){
						try{
							navigator.app.exitApp();
						} catch(e){
							swal.close();
						}
					};
					DISPLAY_MESSAGE_WARNING(
						obMessage.title,
						obMessage.message,
						obParameters
					);
					
				}, false, null, null, { closeAfterCancel: false }
			);

		} else {

			app.setBusy( obView, true );
			
			var myConnection = connectionSettingController.getStoredConnectionSettingsHostURI();
			var URL = myConnection.protocol + "://" + myConnection.server + ":" + myConnection.port + "/";
			URL += 'mobiliser/catalog';

			try{
				cordovaHTTP.get(URL,
					{},
					{},
					function (response) {

						app.setBusy( obView, false );
						console_log("Initialize SSL Connection response: " + response.status);
						if (response.status == 200) {
							console_log('Status : ' + response.status)
							myCurrentData.IS_SSL = true;
							successCallback();
						} else {
							myCurrentData.IS_SSL = false;
						}



					}, function (response) {

						app.setBusy( obView, false );

						console_log("Initialize SSL Connection response error:  " + response.error);
						myCurrentData.IS_SSL = false;
						connectionSettingController.showConnectionErrorMessage(successCallback);
					});
				} catch(e){
				console_log(e);
				connectionSettingController.showConnectionErrorMessage(successCallback);
			}

		}
		
	},


	showConnectionErrorMessage : function (successCallback) {

		var myNewTimeout = null;
		if (myCurrentData.IS_MOBILE && myCurrentData.IS_SSL) {
			
			SESSION_clear_TimeOut();
			SESSION_clear_TimeOut_Clock();
			connectionSettingController.initiateSSL(successCallback);
			
		} else {
			
			var obView = sap.ui.getCore().byId( Common.Navigations.PAGE_SIGN_IN );
			
			swal.close();
			app.setBusy( obView, true );
			TimeOutSSL = setTimeout( 
				function(){
					AXX_MESSAGE_CONFIRM(
						"",
						fn_APP_CONF_GET_TEXT("messageFile", "connection.security.noSSL"),
						[],
						function(){
							swal.close();
							connectionSettingController.initiateSSL(successCallback)	
						},
						function(){
							//What to do when the user don't want to try again.
							var obParameters = {};
							obParameters.fn_callBack_YES = function(){
								try{
									navigator.app.exitApp();
								} catch(e){
									swal.close();
								}
							};

							var stTitle = fn_APP_CONF_GET_TEXT("messageFile", "MSG_USER_EXIT_APP_NETWORK_TITLE");
							var stMessage = fn_APP_CONF_GET_TEXT("messageFile", "MSG_USER_EXIT_APP_NETWORK_MSG");
							DISPLAY_MESSAGE_WARNING(
								stTitle,
								stMessage,
								obParameters
							);

						},
						false, null, null, { closeAfterCancel: false }
					)
				}, 
				2000
			);
					
//			AXX_MESSAGE_WARNING(
//				"",
//				fn_APP_CONF_GET_TEXT("messageFile", "03_B35"),
//				connectionSettingController.initiateSSL
//			)
//			myNewTimeout = setTimeout(this.showConnectionErrorMessage, 1000);
			
			
		}
		
	}
}