/** SETTINGS ****************************/
AXX_SETTINGS = {

    RELEASE: {
        DEV     : true,
        QA      : false, 
        PROD    : false
    },

    /** DEV ENVIRONMENTS *****************************************************
     *    Includes DEV QA and PROD
     *  */

    DEV: {

        ID: "DEV",
        NAME: "Development",

        DEBUG_MODE: false,

        CONNECTION_SETTINGS: {
            IP: "trial.apim1.hanatrial.ondemand.com/p1942272953trial/v1/",
            // PORT: "8080",
            HTTPS: true,
        },

        MODULES: {
            ORDERS: {
                visible: true,
                ITEMS: {
                    HISTORY: {
                        visible: true
                    },
                    NEW_ORDER: {
                        visible: true
                    },
                    CUSTOM_ORDER: {
                        visible: true
                    },
                }
            },
            LOGIN: {
                visible: true,
            },
            SUPPORT: {
                visible: false,
            },
        },

        APP_INFO: {
            VERSION: {
                SHOW_IN_FOOTER: false,
                SHOW_IN_LOGIN: false,
            }
        },
        SESSION: {
            DEBUG_MODE: false,
            DEBUG_MODE_TIME: false
        },

    },

    QA: {

        ID: "QA",
        NAME: "Quality",

        DEBUG_MODE: false,

        CONNECTION_SETTINGS: {
            IP: "trial.apim1.hanatrial.ondemand.com/p15245012trial/v1/",
            // PORT: "8080",
            HTTPS: true,
        },

        MODULES: {
            ORDERS: {
                visible: true,
                ITEMS: {
                    HISTORY: {
                        visible: true
                    },
                    NEW_ORDER: {
                        visible: true
                    },
                    CUSTOM_ORDER: {
                        visible: true
                    },
                }
            },
            LOGIN: {
                visible: true,
            },
            SUPPORT: {
                visible: false,
            },
        },

        APP_INFO: {
            VERSION: {
                SHOW_IN_FOOTER: false,
                SHOW_IN_LOGIN: false,
            }
        },
        SESSION: {
            DEBUG_MODE: false,
            DEBUG_MODE_TIME: false
        },

    },

    PROD: {

        ID: "PROD",
        NAME: "Production",

        DEBUG_MODE: false,

        CONNECTION_SETTINGS: {
            IP: "trial.apim1.hanatrial.ondemand.com/p1942277145trial/v1/",
            // PORT: "8080",
            HTTPS: true,
        },

        MODULES: {
            ORDERS: {
                visible: true,
                ITEMS: {
                    HISTORY: {
                        visible: true
                    },
                    NEW_ORDER: {
                        visible: true
                    },
                    CUSTOM_ORDER: {
                        visible: true
                    },
                }
            },
            LOGIN: {
                visible: true,
            },
            SUPPORT: {
                visible: false,
            },
        },

        APP_INFO: {
            VERSION: {
                SHOW_IN_FOOTER: false,
                SHOW_IN_LOGIN: false,
            }
        },
        SESSION: {
            DEBUG_MODE: false,
            DEBUG_MODE_TIME: false
        },

    },
    
}