var browserSodium = window.sodium;

// Asymetric encryption wrappers
function createKeyPair() {
    sodiumCreateKeyPair();
}

function getEncryptedBpId(nonce) {
    var bpId = ( myCurrentData.INFO == null ) ? null : myCurrentData.INFO.get_User_ID();
    if(isEmpty(bpId)){
        return "";
    } else {
        return encryptMessage(bpId,nonce);
    }
}

function encryptMessage(message, nonce) {
    if(isEmpty(message)){
        return "";
    }

    if(isNumeric(message)){
        message = String(message);
    }


    return sodiumEncryptMessage(message, nonce);
}

function decryptMessage(cipher, nonce) {
    return sodiumDecryptMessage(cipher, nonce);
}

//Assymetric encryption BROWSER: every method here is synchronous
function sodiumCreateKeyPair() {
    var keyPairCryptoBoxResponse = browserSodium.crypto_box_keypair();
    saveClientPrivateKey(keyPairCryptoBoxResponse.privateKey);
    saveClientPublicKey(keyPairCryptoBoxResponse.publicKey);
}

function sodiumEncryptMessage(message, nonce) {
    var encryptedMessage = toHex(browserSodium.crypto_box_easy(message, nonce, fromHex(getServerPublicKey()), fromHex(getClientPrivateKey())));
    if(isEven(encryptedMessage.length)){
        return encryptedMessage;
    } else {
        console_log("sodiumEncryptMessage: the characters numbers is not even: " + encryptedMessage.length);
        return "";
    }
}

function sodiumDecryptMessage(crypto, nonce) {
    return toStringFromBytes(browserSodium.crypto_box_open_easy(fromHex(crypto), nonce, fromHex(getServerPublicKey()), fromHex(getClientPrivateKey())));
}

//Browser and Cordova encryption common methods:
function generateNonce() {
    var nonce = window.secureRandom.randomUint8Array(Encryption.NONCE_SIZE_BYTES);
    if(nonce.length == Encryption.NONCE_SIZE_BYTES){
        return nonce;
    } else {
        console_log("NONCE: incorrect  size!: " + nonce.length);
    }
}

function joinValuesByPipeSeparator(arr) {
        return (arr == null || arr == undefined) ? "" : arr.join("|");
}

function splitValuesByPipeSeparator(str) {
    return isEmpty(str) ? [] : str.split("|");
}

function toHex(bytes) {
    return browserSodium.to_hex(bytes);
}

function fromHex(hex) {
    return browserSodium.from_hex(hex);
}

function toStringFromBytes(hex) {
    return browserSodium.to_string(hex);
}

function toBytesFromString(text) {
    return browserSodium.from_string(text);
}

function saveClientPrivateKey(key) {
    if(key!=null && key.length == Encryption.PRIVATE_KEY_SIZE_BYTES){
        myCurrentData.DEVICE.saveClientSecretKey(key);
    } else{
        console_log("saveClientPrivateKey: incorrect key size: " + key.length);
    }
}

function saveClientPublicKey(key) {
    if(key!=null && key.length == Encryption.PUBLIC_KEY_SIZE_BYTES){
        myCurrentData.DEVICE.saveClientPublicKey(key);
    } else{
        console_log("saveClientPublicKey: incorrect key size: " + key.length);
    }
}

function saveServerPublicKey(key) {
    if(key!=null && key.length == Encryption.PUBLIC_KEY_SIZE_HEX){
        myCurrentData.DEVICE.saveServerPublicKey(key);;
    } else{
        console_log("saveServerPublicKey: incorrect key size: ");
    }
}

function saveMessageNonce(nonce) {
    myCurrentData.DEVICE.saveMessageNonce(nonce);
}

function getClientPublicKey() {
    var clientPublicKey = myCurrentData.DEVICE.getClientPublicKey();
    if(!isEmpty(clientPublicKey) && clientPublicKey.length == Encryption.PUBLIC_KEY_SIZE_HEX){
        return clientPublicKey;
    } else {
        console_log("getClientPublicKey: incorrect size!: ");
        return "";
    }
}

function getClientPrivateKey() {
    var clientPrivateKey = myCurrentData.DEVICE.getClientSecretKey();
    if(!isEmpty(clientPrivateKey) && clientPrivateKey.length == Encryption.PRIVATE_KEY_SIZE_HEX){
        return clientPrivateKey;
    } else {
        console_log("getClientPrivateKey: incorrect  key size!: ");
        return "";
    }
}

// function getServerPublicKey() {
//     var serverPublicKey = myCurrentData.DEVICE.getServerPublicKey();
//     if(!isEmpty(serverPublicKey) && serverPublicKey.length == Encryption.PRIVATE_KEY_SIZE_HEX){
//         return serverPublicKey;
//     } else {
//         console_log("getServerPublicKey: incorrect  size!: ");
//         return "";
//     }
// };

function getServerPublicKey() {
    // var serverPublicKey = myCurrentData.DEVICE.getServerPublicKey();
    // if(!isEmpty(serverPublicKey) && serverPublicKey.length == Encryption.PRIVATE_KEY_SIZE_HEX){
    //     return serverPublicKey;
    // } else {
    //     console_log("getServerPublicKey: incorrect  size!: ");
    //     return "";
    // }

    return "093dfa10fdfc816db08856d5fd158231b81bbaceac3223dbaec73045f48b094a";
};

function isServerPublicKeyAvailable() {
    return !isEmpty(getServerPublicKey());
}

function isClientSetOfKeysReady() {
    return !isEmpty(getClientPrivateKey()) && !isEmpty(getClientPublicKey());
}
//------------------------------------------------------------------------------

function decryptAccounts(accounts,accountType, nonce) {

        for(var i=0; i>accounts[accountType].length;i++){
            accounts[accountType].accountAvailableBalance =  decryptMessage(accountType[accountType].accountAvailableBalance,nonce);
            accounts[accountType].accountBalance =  decryptMessage(accountType[accountType].accountBalance,nonce);
            accounts[accountType].accountNumber =  decryptMessage(accountType[accountType].accountNumber,nonce);
        }

        return accounts;
}