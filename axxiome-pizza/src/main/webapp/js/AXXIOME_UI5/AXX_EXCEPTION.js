var arERROR_CODES = [ 401, 403, 404, 500, 1222];
var arERROR_CODES_SESSION = [ 401, 403, 1222, 352 ];
var arERROR_CODES_STOP = [ 404 ];

arERROR_CODES.push( 352 ); // Check Session ERror -> To logout
arERROR_CODES_SESSION.push( 352 ); // Check Session ERror -> To logout

var fnLog_Exception = function(  boAlert ){

	if (isEmpty(this.element))
		this.element = fn_APP_CONF_GET_TEXT( "i18N", "MESSAGE.ALERT.MESSAGE");	
	
	var thisLog = "!! ERROR :: ";
	( this.element ) ? thisLog += "[" + this.element + "] " : null; 
	thisLog += this.message;
	
	if ( this.type == "HTML" ){
		swal({   title: this.title,   text: this.message,   confirmButtonText: "OK", html: true,  });
	} else if ( boAlert == true ){
		console_log( thisLog );

		var stTitle = this.title || fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , "MESSAGE.ALERT.TITLE" );

		// swal({ 
		// 	title: this.title || fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , "MESSAGE.ALERT.TITLE" ), 
		// 	text: this.message, 
		// 	type: this.type, 
		// 	confirmButtonText: "Aceptar",
		// 	confirmButtonColor: Common.Branding.BUTTON_COLOR
		// });

		DISPLAY_MESSAGE_ERROR( stTitle, this.message );

	} 
	
	var currentPage = sap.ui.getCore().byId( homeNavContainer.getCurrentPage().sId );
	currentPage.setBusy(false);
	
	return thisLog;
};



var AXX_CLEAR_EXCEPTIONS = function (theElements ) {

	if ( theElements != null && Array.isArray(theElements) ){
		$.each( theElements, function( i, theCurrentElement ){
			try{
				theCurrentElement.setValueState( sap.ui.core.ValueState.None );
			}
			catch (e){
				//IN the catch, we will apply for those components that don't have the valueState property
				try{
					theCurrentElement.markValid();
				}
				catch (e1){}
			}
		});
	}

};

var AXX_EXCEPTION_USER = function ( theTitle, theMessage, boAlert, theType, theElements ) {
	
	this.name = "Exception_User";
	this.source = "USER";
	this.title = ( typeof theTitle == "string" ) ? theTitle : theTitle != null ? theTitle[0] : "Error";
	this.message = ( typeof theMessage == "string" ) ? theMessage : theMessage != null ? theMessage[0] : "Internal Error";
	this.type = (theType) ? theType : "error";
	this.toString = fnLog_Exception( boAlert );
	
	if ( theElements != null && Array.isArray(theElements) ){
		$.each( theElements, function( i, theCurrentElement ){
			try{
				theCurrentElement.setValueState( sap.ui.core.ValueState.Error );
			}
			catch (e){
				//IN the catch, we will apply for those components that don't have the valueState property 
				try{
					theCurrentElement.markValidationError();
				}
				catch (e1){}
			}
		});
	}
	
};

var AXX_EXCEPTION_APP = function (theTitle, theMessage, boAlert, theType, theElement ) {
   
	this.title = theTitle;
	this.message = theMessage;
	this.name = "Exception_App";
	this.source = "APP";
	this.type = "error";
	this.element = theElement;
    this.toString = fnLog_Exception( boAlert );
    
};



/***********************************************************
 * 
 * FUNCTION: 	AXX_EXCEPTION_Translate_ERROR
 * DESCRIPTION:	 
 * PARAMETERS:	errorCode	:	Code of the ERROR returned from the Mobiliser Backend APP
 * RETURN:		Array		: [ TITLE, MESSAGE DESCRIPTION ]
 * TAGS: 		messages
 * 
 * ----------------   */
var AXX_EXCEPTION_CATCH = function( data, stDummy_Title ){
	
	console_log(" ");
	console_log(">> " + stDummy_Title);
	console_log( data );
	console_log(data.stack);

	var obMessage = { title: null, message: null };
	var errorCode = null;
	var errorText = null;
	
	if ( data == null ){
		errorCode = "I";
		//errorText = "";
	} else {

		errorCode = (data.Status != null) ? data.Status.code : ( ( data.status != null ) ? data.status : (errorCode != null) ? errorCode : null );
		errorText = (data.Status != null) ? data.Status.value : ( data.message != null) ? data.message : (errorText != null) ? errorText : ( data.statusText ) ? data.statusText : null ;

	} 
	
	obMessage = AXX_EXCEPTION_Translate_ERROR( errorCode, errorText );
	AXX_EXCEPTION_APP( obMessage.title, obMessage.message, true );
	
	if ( $.inArray( errorCode, arERROR_CODES_SESSION ) > -1 ){
		
		if ( errorCode == 352 ){

			SESSION_clear_TimeOut_Clock();
			Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_SIGN_IN });

		} else if ( myCurrentData.TEMP.action != "logoutForced" ) {
			
			console_log("LogOut triggered");
			try{
				myCurrentData.INFO.logOut_Force( );
			} catch(e) {
				new AXX_SESSION().logOut_Force( );
			}
			
		}
	} 
	
};


/***********************************************************
 * 
 * FUNCTION: 	AXX_EXCEPTION_Translate_ERROR
 * DESCRIPTION:	 
 * PARAMETERS:	errorCode	:	Code of the ERROR returned from the Mobiliser Backend APP
 * RETURN:		Array		: [ TITLE, MESSAGE DESCRIPTION ]
 * TAGS: 		messages
 * 
 * ----------------   */
var AXX_EXCEPTION_Translate_ERROR = function( errorCode, errorText ){
	
	console_log(" ");
	console_log(" AXX_EXCEPTION_Translate_ERROR ");
	console_log("ERROR CODE [" + errorCode + "]");
	console_log("ERROR TEXT [" + errorText + "]");

	var obMessage = new Object({ title: "", message: "" });
	
	try{ //fetch_Translation_By_ErrorCode
		if ( errorCode != null ){
			
			//console_log(" -> validating error code from enrollment-register");
			if ( $.inArray( errorCode, arERROR_CODES ) > -1 
				&& (homeNavContainer.getCurrentPage().sId == Common.Navigations.PAGE_SIGN_IN 
						|| homeNavContainer.getCurrentPage().sId == Common.Navigations.PAGE_ENROLLMENT_REGISTER) ) {
				errorCode = 500;
			}
			
			var obMessage_Mapped = fn_APP_GET_Translation_By_ErrorCode(errorCode);
			
			obMessage.title = fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , obMessage_Mapped.title_alias, errorCode );
			obMessage.message = fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , obMessage_Mapped.message_alias, errorCode );
			
		} else {
			throw "MISSING_ERROR_CODE";
		}
	} catch(e){

		console_log(" AXX_EXCEPTION_Translate_ERROR - CATCH ");
		console_log("ERROR CODE [" + errorCode + "]");
		console_log("ERROR TEXT [" + errorText + "]");
		
		if ( errorCode != null && errorText != null ){
			
			console_log("But we still have the code and TEXT: " + errorCode + " [" + errorText + "]");
			obMessage.title = (obMessage.title)? obMessage.title : "Error [" + errorCode + "]";
			obMessage.message = (obMessage.message)? obMessage.message : errorText + "\n { MISSING ERROR TRANSLATION }";
			
		} else if ( errorCode == null && errorText != null ){ 
			
			errorCode = -1; 
			console_log("EXCEPTION [ " + errorText + " ]");
			obMessage = AXX_EXCEPTION_Translate_ERROR("I");
			
		} else {
						
			switch( e.errorType ){
				case "MISSING_ERROR_CODE":
					AXX_EXCEPTION_APP("Message", "Lo sentimos .. Error interno [errorCode not provided]", true);
					break;
				case "MAPPING_NOT_FOUND":
					AXX_EXCEPTION_APP("Message", "Lo sentimos .. Error interno \n [Translation for the message not provided]", true);
					break;
			}
			
		}
		
	} finally{
		sap.ui.getCore().byId( homeNavContainer.getCurrentPage().sId ).setBusy(false);
		return obMessage;
	}
	
};