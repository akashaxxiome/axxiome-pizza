/*************************************************************************	
 * @function	fn_APP_CONF_GET_TEXT
 * @param fileCategory	- 	Which file of resources will be used
 * @param messageAlias	- 	The identification of the resource
 * @param errorCode		- 	If any errorCode, it will be added to the Text of the error
 * @returns {String}	- 	The text of the resource
 */
function fn_APP_CONF_GET_TEXT( fileCategory, messageAlias, errorCode ){

	//console_log( ">> fn_APP_CONF_GET_TEXT ( " + fileCategory + ", " + messageAlias + ", " + errorCode + " )" );
	var stText = "";
	try {
		stText = sap.ui.getCore().getModel( fileCategory ).getResourceBundle().getText( messageAlias );
		if( stText == messageAlias ){
			throw "TRANSLATION_NOT_FOUND";
		}

	} catch(e){
		//console_log(e);
		//console_log(" "  + messageAlias );
		
		switch(e.errorType){
			
			case "TRANSLATION_NOT_FOUND":
				if ( messageAlias.toLowerCase().indexOf("title") >= 0 ){
					stText = "Exception Title";
				} else {
					stText = "Message not mapped";
					stText += ( (errorCode == null) ? "" : " [errorCode " + errorCode + "]" );
				}
				break;
			default:
				stText = messageAlias;
				break;
		}
		
	}
	
	return stText;
	
};


/*************************************************************************
 * @function	fn_APP_GET_Translation_By_ErrorCode
 * @param inErrorCode
 * @returns {___anonymous1516_1522}
 */
function fn_APP_GET_Translation_By_ErrorCode(inErrorCode){
	
	console_log("fn_APP_GET_Translation_By_ErrorCode");
	var obError = new Object();
	try{

		
		
		var arItem = tb_MAP_ERROR[inErrorCode];
		//console_log("tb_MAP_ERROR"); console_log(arItem);
		if ( !arItem || arItem.length < 2 ){
			console_log("excecao");
			throw "";
		}
		
		obError.title_alias = arItem[0];
		obError.message_alias = arItem[1];
		
		console_log(" MAP OK "); console_log( obError ); console_log("  ");
		
		if ( $.inArray( inErrorCode, arERROR_CODES_STOP ) > -1  ){
			myCurrentData.boCallBackError_Stop = true;
			myCurrentData.WS_CALL = { obError : obError };
		} 

		return obError;
		
	} catch(e){
		throw new Error({ errorType: "MAPPING_NOT_FOUND", errorCode: inErrorCode, errorText : "" });
	}
	
};