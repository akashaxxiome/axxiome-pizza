function AXX_CONN( typeCONN ) {


	//PROPERTIES SECTION
	// TODO: Improve and Structure the system to load this info from device and settings files
	var myTypeComm = (typeCONN) ? typeCONN : "REST";

	//TODO : optimize and reuse for each call ?
	var obData = new Object();
	var boError = false;



	/*	=============================================================================================
	 *	AUXILIARY METHODS	SECTION
	 */
	this.hasNetworkConnection = function(){

		if( myCurrentData.IS_MOBILE && ( (/ios|android/).test(sap.ui.Device.os.name.toLowerCase()) ) && navigator.platform.toLowerCase() != "win32" ){
			try{
				var networkState = navigator.connection.type;

				var states = {};
				// states[Connection.UNKNOWN]  = 'Unknown connection';
				// states[Connection.ETHERNET] = 'Ethernet connection';
				// states[Connection.WIFI]     = 'WiFi connection';
				// states[Connection.CELL_2G]  = 'Cell 2G connection';
				// states[Connection.CELL_3G]  = 'Cell 3G connection';
				// states[Connection.CELL_4G]  = 'Cell 4G connection';
				// states[Connection.CELL]     = 'Cell generic connection';
				// states[Connection.NONE]     = 'No network connection';

				var myConnection = networkState;

				switch( myConnection ){
					case "none":
						return false;
					default:
						return true;
				}

			}catch(e){
				console_log("DEVICE.CONNECTION information not available");
				return null;
			}
		} else {
			return navigator.onLine;
		}


	}

	// var UUIDv4 = function b(a){
	// 	return a?(a^Math.random()*16>>a/4).toString(16):([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,b)
	// };

	var generateNewObject_Rest = function( moreData ){

		var sessionId = ( myCurrentData.INFO ) ?  myCurrentData.INFO.get_Session_ID() : "";

		// obData = new Object();
		// obData.origin =  myCurrentData.IS_MOBILE ? "MAPP" : "WAPP";
		// obData.traceNo = UUIDv4();
		// obData.AuditData = {
		// 	"deviceId": ( myCurrentData.DEVICE ) ? myCurrentData.DEVICE.get_Device_UUID() : "",
		// 	"otherDeviceId": ( myCurrentData.INFO ) ?  myCurrentData.INFO.get_Session_ID() : "" ,
		// 	"application": (myCurrentData.IS_MOBILE) ? "MAPP" : "WAPP",
		// 	"applicationVersion": "1.0.2",
		// }; //setting.appinfo;

		// if(!isEmpty(sessionId)){
		// 	obData.sessionId = sessionId;
		// }

		// if ( moreData != null ){

		// 	console_log([ " ", "THERE ARE MORE DATA TO SEND..", moreData ]);

		// 	$.each( moreData, function(thisData, value){
		// 		//console_log( thisData + " | " + value );
		// 		obData[thisData] = value;
		// 	});

		// }

		obData = toJSON(obData);
		console_log([ "CONN obData FINAL" , obData ]);

	};

	var generateNewObject_OData = function( moreData, stMethod ){

		console_log([ " >> generateNewObject_OData " , moreData ]);

		var sessionId = ( myCurrentData.INFO ) ?  myCurrentData.INFO.get_Session_ID() : "";

		if ( stMethod === "PUT" ) {
			var stQuery = "";

			if ( moreData != null ){
                if ( !isEmpty( moreData.OrderID ) ){
                    stQuery = stQuery + "(" + moreData.OrderID + ")";
                }
                if ( !isEmpty( moreData.OfferID ) ){
                    stQuery = stQuery + "(" + moreData.OfferID + ")";
                }
            }

            // obData = toJSON(obData);
            obData = toJSON(moreData);
            console_log([ "CONN obData FINAL" , obData ]);

			return stQuery;
			
		} else if ( stMethod !== "POST" ) {
			var stQuery = "";

			if ( moreData != null ){
				if ( !isEmpty( moreData.filter ) ){

					stQuery = stQuery + "&$filter=";

					$.each( moreData.filter, function(thisData, arValue){

						console_log( thisData + " : " + arValue );
						if ( arValue.length === 3 ){

							var stTemp = " " + arValue[0] + " " + arValue[1];
							if ( typeof arValue[2] === "string" ){
								stTemp = stTemp + " '" + arValue[2] + "' and ";
							} else {
								stTemp = stTemp + " " + arValue[2] + " and ";
							}

							stQuery += stTemp;
						}
						
					});

					stQuery = stQuery.slice( 0, -4 );

				}
				if ( !isEmpty( moreData.orderby ) ){

					stQuery = stQuery + "&$orderby=" + moreData.orderby;

				}
			}

			// obData = toJSON(obData);
			console_log([ "CONN obData FINAL" , obData ]);

			return stQuery;
			
		} else {

			//TODO : Check if need anything to make the POST object
			obData = toJSON(moreData);

		}

	};


	//Call a method from the DEVICE object to grab this information
	var getMobileNumber = function(){
		mobileNumber = myCurrentData.DEVICE.get_Device_Mobile_Number();
	}.bind(this);


	/*
	 *--------------------------------------------------------------------------*/
	var toJSON = function(object) {

		var type = typeof object;
		switch (type) {
			case 'undefined':
			case 'function':
			case 'unknown': return;
			case 'object': break;
			default: return '"' + object.toString() + '"';
		}
		if (object === null) {
			return 'null';
		}
		if (object.ownerDocument === document) {
			return;
		}

		var results = [];
		if (object.length) { //array
			for (var i=0; i<object.length; i++) {
				var value = toJSON(object[i]);
				if (value !== undefined)
					results.push(value);
			}
			return '[' + results.join(',') + ']';
		} else { //object
			for (var property in object) {
				var value = toJSON(object[property]);
				if (value !== undefined) {
					property = (property.split('_')[0] == "pseudo") ? '@'+property.split('_')[1] : property;
					results.push('"' + property + '"' + ':' + value);
				}
			}
			return '{' + results.join(',') + '}';
		}
	};



	/* ================================================================================================= */
	/* ========================  					FUNCTIONS					======================== */
	/* ================================================================================================= */

	this.get = function( inData, methodName, callBackFunctionSuccess, callBackFunction_Error ){


	}


	/***********************************************************
	 *
	 * FUNCTION: 	POST
	 * DESCRIPTION:	Call the services in the BackEnd and triggers success of failure callBack Function
	 * PARAMETERS:	inData	:	Code of the ERROR returned from the Mobiliser Backend APP
	 * 				methodName:
	 * 				callBack_Success:
	 * 				callBack_Error:
	 * RETURN:
	 * TODO:		Add handleSSLInit of Connection
	 * 				Add room for LOGGING
	 *
	 * ----------------   */
	this.POST_New = function( inData, methodName, parameters, parentId, operationName ){

		maxTentatives = isEmpty(parameters.maxTentatives) ? Common.NETWORK_TIMEOUT_HANDLER.MAX_TENTATIVES : parameters.maxTentatives;

		var obParameters = {

			//obView :
			tryAgain : true,
			maxTentatives: maxTentatives,
			isTransaction: true,
			methodName: methodName,
			fn_Main : function( ){

				console_log(">> EXECUTING [" + methodName + "] FUNCTION");
				myCurrentData.TEMP.obParameters = obParameters;

				try{
					new AXX_CONN().POST_WITHOUT_PINNING_New( inData, methodName, parameters, obParameters );
				} catch(e){
					if (e.code == "5"){
						NETWORK_TIMEOUT_HANDLER( obParameters );
					}
				}

			},

			fn_MainOnRetry : function( obParameters ){

				// renewSessionCounter(methodName);

				console_log(">> RETRYING [" + methodName + "] FUNCTION");
				try{

					obParameters.newTentative = true;

					if ( myCurrentData.activeWebService != null ) {

						myCurrentData.TEMP.obParameters = obParameters;

						myCurrentData.activeWebService.startWS();

					} else {

						new AXX_CONN().POST_WITHOUT_PINNING_New( inData, methodName, parameters, obParameters );

					}


				} catch(e){
					if (e.code == "5"){
						throw 1; //NETWORK_TIMEOUT_HANDLER( obParameters );
					}
				}

				// NETWORK_TIMEOUT_HANDLER( obParameters );

			},


			fn_Tentatives_Finished : function(){
				console_log(">> EXECUTING AFTER TENTATIVES ENDED");
				// obParameters.fn_Option_Negative();
				clearWS_Execution();

			},

			fn_Option_Negative : function( ){
				console_log(">> EXECUTING OPTION NEGATIVE");
				clearWS_Execution();
			}



		};

		// try{
		// 	myCurrentData.WS_CALL.obError = {};
		// 	myCurrentData.WS_CALL.parameters = obParameters;
		// } catch(e){
		// 	myCurrentData.WS_CALL = { parameters : obParameters, obError : {} };
		// }
		if ( isEmpty(parameters) ){
			if ( isEmpty(obParameters) || ( isEmpty(obParameters.currentTentative) && !isEmpty(myCurrentData.TEMP.obParameters) ) ) {
				obParameters = myCurrentData.TEMP.obParameters;
			}
		}

		NETWORK_TIMEOUT_HANDLER( obParameters );

	};


	

	this.POST_WITHOUT_PINNING_New = function( inData, methodName, parameters, obParameters ){

		try{
			myCurrentData.INFO.renewSessionCounter( "POST_WITHOUT_PINNING_New :: " + methodName );
		} catch(e){ console_log(	"POST_WITHOUT_PINNING_New :: " + methodName + " [ no session ] "); }


		function handleError( res, data ){

			if ( !isEmpty(myCurrentData.TEMP) && myCurrentData.TEMP.obParameters.methodName == "/logout" ){

				try{
					myCurrentData.INFO.logOut_Force( );
				} catch(e) {
					new AXX_SESSION().logOut_Force( );
				}

			} else 	if (!isEmpty(myCurrentData.activeWebService)) {

				var errorCode = ( isEmpty(res.status) ? res.Status.code : res.status );

				errorCode = ( errorCode == 400 || errorCode == 404 ) ? 9999 : errorCode;

				var obError = fn_APP_GET_Translation_By_ErrorCode( errorCode );
				obError.errorCode = ( isEmpty(res.status) ? res.Status.code : res.status );

				res.status = errorCode;

				if ( isEmpty(obParameters) ){
					obParameters = myCurrentData.TEMP.obParameters;
				}

				obParameters.obError = obError;
				obParameters.obError.res = res;
				obParameters.obError.operationIndex = parameters.operationIndex;

				obParameters.newTentative = true;
				NETWORK_TIMEOUT_HANDLER( obParameters );

			}
		};

		function handleSuccess( methodName ){
			
			try {
				// myCurrentData.INFO.renewSessionCounter( "WS response [ " + NETWORK_TIMEOUT_HANDLER__obParameters.methodName + "]" );
				myCurrentData.INFO.renewSessionCounter( "WS response [ " + methodName + " ]" );
			} catch(e){}

			myCurrentData.TEMP.obParameters = null;

		};

		var stTypeWSCall = "odata";

		var parentId = parameters.parentId;
		var operationName = methodName;
		var operationIndex = parameters.operationIndex;

		var stQuery = "";
		if ( stTypeWSCall === "odata" ){
			stQuery = generateNewObject_OData( inData, parameters.METHOD );
		} else {
			generateNewObject_Rest( inData );
		}

		var stMethod = ( isEmpty( parameters.METHOD ) ? "GET" : parameters.METHOD);
		
		var myURL = connectionSettingController.getConnection_URL( stTypeWSCall );
		myURL = myURL + "/" + methodName.replace("/","");

		if ( stTypeWSCall === "odata" && stMethod === "GET" ){
			myURL = myURL + "?";
		}

		if ( !isEmpty(stQuery) ){
			myURL = myURL + stQuery;
		}
		if ( stTypeWSCall === "odata" && stMethod === "GET" ){
			myURL = myURL + "&$format=json";
		}


		//console_log(" ========== post_B ========== ");
		console_log(" URL: " + myURL );

		try {

			var operationIndex = parameters.operationIndex;

			myCurrentData.activeWebService.wSArray[operationIndex].ajaxCall =  $.ajax({
				url: myURL,
				type: stMethod,
				dataType: "json",
				data: obData,
				//contentType: "text/xml; charset=\"utf-8\"",
				beforeSend: function(xhr){
					xhr.setRequestHeader( "Content-Type", "application/json" );
					xhr.setRequestHeader( "Accept", "application/json" );
					xhr.setRequestHeader( "Access-Control-Allow-Origin", "*");
				},
				success: function(rawData, result, response){

					// wsTimeOutTimer = null;
					clearTimeout(wsTimeOutTimer);
					wsTimeOutTimer = null;

					//console_log(" [ " + methodName + " ] ----> SUCCESS");
					//data.result = result;
					//data.response = response;
					var data = rawData;
					if ( stTypeWSCall.toLowerCase() === "odata" ){
						 data = rawData.d.results;
					} 

					try{

						console_log('POST_WITHOUT_PINNING_New SUCCESS');
						// console_log([ parameters, myCurrentData.activeWebService ]);

						//TODO move to WSUtils

						if (!isEmpty(parameters.dataModelName)){
							var theModel = new sap.ui.model.json.JSONModel( data );
							theModel.setSizeLimit(500);
							//RWa refresh the model
							// theModel.refresh(true);
							sap.ui.getCore().setModel( theModel, parameters.dataModelName );
						}

						//is parentId empty - for old approach
						//checks if the WS is still needed - timeout, etc
						if (!isEmpty(myCurrentData.activeWebService) && (isEmpty(parentId) || myCurrentData.activeWebService.id == parentId)) {

							// if (data.Status.code == 0) {
							if ( response.status === 200 || response.status === 201 ) {

								try {

									try{
										handleSuccess( methodName );
									} catch(e){}

									try {
										if ( !isEmpty( parameters.success_callback ) ) {
											parameters.success_callback(data, parameters.customData);
										}
									} catch (e) {
										console_log("CATCH [ " + methodName + " ]");
										// AXX_EXCEPTION_CATCH(e, methodName);
									} finally {
										parameters.isSuccess = true;
									}

								} catch (e) {
									console_log("CATCH [ " + methodName + " ]");
									AXX_EXCEPTION_CATCH(e, methodName);
								} finally {
									// myCurrentData.activeWebService = null;
									// myCurrentData.TEMP.obParameters = null;
								}

							} else {

								if ( ( $.inArray( data.Status.code, [ 400 ] ) >= 0 ) ) {

									handleError(data);

								} else {

									if (isEmpty(parameters.validation_callback)){
										// GENERIC_WEB_SERVICE_VALIDATION_FAILURE(data, methodName, mapOfFieldsOnView, callBack_Error);
										GENERIC_WEB_SERVICE_VALIDATION_FAILURE(data, methodName, null, parameters.failure_callback );
									} else {
										parameters.validation_callback(data, parameters.customData);
									}

								}

							}

						}

						//check if the call should be exeuted - maybe it should be interrupted
						var theError = null;
						var boHasInternalError = false;

						try{

							var operationIndex = parameters.operationIndex;

							if (myCurrentData.activeWebService != null){
								myCurrentData.activeWebService.executeActionOnAllWSReady(operationIndex);
							}

						} catch(e){

							theError = e;
							boHasInternalError = true;
							DISPLAY_MESSAGE_ERROR(
								"",
								fn_APP_CONF_GET_TEXT("messageFile", "ERR_INTERNAL.GENERIC.MESSAGE")
							);

						} finally{

							if ( !isEmpty( obParameters ) ){
								obParameters = null;
							}
							if ( !isEmpty( myCurrentData.TEMP.obParameters ) ){
								myCurrentData.TEMP.obParameters = null;
							}

							// app.setCurrentViewBusy( false );

							// if ( boHasInternalError ){
							// 	throw theError;
							// }

						}

					} catch (e){
						console_log("CATCH " + methodName);
						console_log(e.stack);
						AXX_EXCEPTION_CATCH ( e, methodName );
					}

				},
				error: function(res, x, y){
					//console_log(" [ " + methodName + " ] ----> FAILURE");

					console_log('POST_WITHOUT_PINNING_New FAIL');
					// console_log(parameters);
					// console_log(myCurrentData.activeWebService);

					clearTimeout(wsTimeOutTimer);
					wsTimeOutTimer = null;

					if (res.statusText != 'abort'){

						if ( ( $.inArray( res.status, [ 400, 404 ] ) >= 0 ) || ( res.status == 0 && res.statusText == "error" ) ) {

							handleError(res);

						} else {

							if (!isEmpty(myCurrentData.activeWebService)) {

								var operationIndex = parameters.operationIndex;

								if (myCurrentData.activeWebService != null)
									myCurrentData.activeWebService.executeActionOnAllWSReadyFailure(operationIndex);

								myCurrentData.activeWebService.wsFailed(operationIndex, res);
							}

						}
					}

				},

			});
		} catch(e){
			AXX_EXCEPTION_CATCH ( e, methodName );
		}



	};

};


/**
 *	NETWORK_TIMEOUT_HANDLER
 *	Function to handle timeout and Network bad connection
 */
function NETWORK_TIMEOUT_HANDLER( obParameters ){

	var TimeOutBlock = null;
	var obView = sap.ui.getCore().byId( myCurrentData.stView );

	if ( isEmpty(obParameters.currentTentative) || typeof obParameters.currentTentative == "undefined" ){
		obParameters.currentTentative = 0;
	}


	if ( isEmpty(obParameters.maxTentatives) ){
		obParameters.maxTentatives = Common.NETWORK_TIMEOUT_HANDLER.MAX_TENTATIVES;
	}

	var newTentative = false || obParameters.newTentative;

	var boExecute = false;
	var boHasNetwork = isNetworkAvailable();




	if ( !boHasNetwork ){

		boHasNetwork = false;

		obParameters.tryAgain = false;
		boExecute = false;

		fnStandardNegative();
		// var obMessage = AXX_EXCEPTION_Translate_ERROR( "NETWORK" );

		// DISPLAY_MESSAGE_WARNING(
		// 	obMessage.title,
		// 	obMessage.message,
		// 	obMessageParameters
		// );

	} else {


		if ( obParameters.tryAgain == true ){

			if ( obParameters.methodName == "/logout" && obParameters.currentTentative > 0 ){

				try{
					myCurrentData.INFO.logOut_Force( "", false );
				} catch(e) {
					new AXX_SESSION().logOut_Force( "", false );
				}

			} else {

				if ( obParameters.currentTentative == 0 ){

					boExecute = true;

				} else if ( ( obParameters.newTentative == true && obParameters.currentTentative == obParameters.maxTentatives ) ){

					boExecute = false;

					clearTimeout( TimeOutBlock );

					fnStandardTentativesFailed();

				} else {
					boExecute = false;
					displayPopUp( obParameters.obError );
				}

			}

		} else {
			boExecute = true;
		}

		if ( boExecute == true ){
			obParameters.currentTentative ++;
			obParameters.fn_Main( obParameters );
		}

	}

	function displayPopUp( obMessage ){

		// myCurrentData.activeWebService = null;

		var obMessage = readErrorMessage( obMessage );

		obMessage.message = obMessage.message + fn_APP_CONF_GET_TEXT("messageFile", "ERR_TIMEOUT.tentatives.newTry.message" );

		obParametersPopUp = {
			fn_callBack_YES : function(){
				tryAgain( obParameters );
			},
			fn_callBack_NO : function(){
				clearTimeout( TimeOutBlock );
				console_log("End of Tentatives");

				fnStandardNegative();
			},
			closeAfterOk : false,
			closeAfterCancel: false
		}

		renewSessionCounter( "DISPLAY POPUP" + obParameters.methodName);

		DISPLAY_MESSAGE_CONFIRM(
			obMessage.title,
			obMessage.message,
			obParametersPopUp
		);

	};

	// Functions to be executed before calling the main method once more
	function tryAgain(obParameters){

		clearTimeout( TimeOutBlock );

		var obView = sap.ui.getCore().byId( myCurrentData.stView );
		app.setBusy( obView, true );

		obParameters.currentTentative++;

		swal.close();

		TimeOutBlock = setTimeout(
			function(){

				clearTimeout( TimeOutBlock );

				if ( typeof obParameters.fn_MainOnRetry == "undefined"){
					obParameters.fn_Main( obParameters );	
				} else {
					try{
						obParameters.fn_MainOnRetry ( obParameters );
					} catch(e){
						obParameters.fn_Main( obParameters );	
					}
				}
			},
			1000
		);

	};


	function fnStandardTentativesFailed( parameters ){

		clearWS_Execution();

		if ( obParameters.isTransaction == true ){
			console_log("executing fnStandardTentativesFailed isTransaction");
		} else {
			console_log("executing fnStandardTentativesFailed no transaction");
		}

	
		// obFinishParameters = {
		// 	callBack_YES : function(){
		// 		myTempTimeout = setTimeout(function() {
		// 			try{
		// 				app.setCurrentViewBusy(false);
		// 			}catch(e){} finally{
		// 				myTempTimeout = null; navigateBack();
		// 			}
		// 		}, 500);
		// 	},
		// 	closeAfterOk : false
		// };

		var obMessageParameters = {};
		obMessageParameters.fn_callBack_YES = function(){
			
			// obParameters.fn_Option_Negative();

			navigateBack();

		};

		var obMessage = {
			title: "",
			message: fn_APP_CONF_GET_TEXT("messageFile", "ERR_TIMEOUT.negative.finished.message" ),
			
		}

		if ( myCurrentData.stView == Common.Navigations.PAGE_SIGN_IN  ) {

			if ( myCurrentData.IS_MOBILE && !isNetworkAvailable() ){

				var obMessage = AXX_EXCEPTION_Translate_ERROR( "USER_EXIT_APP_NETWORK" );

				// DISPLAY_MESSAGE_WARNING(
				// 	obMessage.title,
				// 	obMessage.message,
				// 	obMessageParameters
				// );

			} 

		} else {
		

		}

		obMessage.message = obMessage.message + "\n\n\n" + fn_APP_CONF_GET_TEXT("messageFile", "ERR_TIMEOUT.tentatives.finished.message" );

		DISPLAY_MESSAGE_WARNING(
			obMessage.title,
			obMessage.message,
			obMessageParameters
		);

		// clearWS_Execution();
		console_log("All tentatives finished unsucessfuly");

	};




	function fnStandardNegative(){

		clearWS_Execution();

		if ( obParameters.isTransaction  == true ){
			console_log("fnStandardNegative isTransaction");
		} else {
			console_log("All tentatives finished unsucessfuly");
		}


		if ( myCurrentData.stView == Common.Navigations.PAGE_SIGN_IN  ) {

			var obMessageParameters = {};
			obMessageParameters.fn_callBack_YES = function(){
				
				obParameters.fn_Option_Negative();

				navigateBack();

			};

			if ( myCurrentData.IS_MOBILE && !isNetworkAvailable() ){

				var obMessage = AXX_EXCEPTION_Translate_ERROR( "NETWORK" );

				DISPLAY_MESSAGE_WARNING(
					obMessage.title,
					obMessage.message,
					obMessageParameters
				);

			} else {

				swal.close();
				navigateBack();

			}

		} else {

			navigateBack();

		}

	};







	function navigateBack(){

		myCurrentData.activeWebService = null;

		swal.close();
		var oCon = sap.ui.getCore().byId( myCurrentData.stView ).getController();
		app.setBusy( oCon.getView() , false );

		if ( myCurrentData.stView == Common.Navigations.PAGE_SIGN_IN  ) {

			try{

				// if ( myCurrentData.IS_MOBILE && !isNetworkAvailable() ){

				// 	navigator.app.exitApp();

				// } else {

					viewSignin.getController().session_timeout_failure();

				// }

			} catch(e){

			}

		} else {

			// if( myCurrentData.boCallBackError_Stop == true ){

			myCurrentData.boCallBackError_Stop = false;

			var oCon = sap.ui.getCore().byId( myCurrentData.stView ).getController();

			app.setBusy( oCon.getView() , false );

			// if ( myCurrentData.INFO == null ){
			// 	Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_SIGN_IN });
			// } else {
			// 	Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_HOME });
			// }

		}

	};

	

	function readErrorMessage( obMessage ){

		var obErrorMessage = {};

		try{
			if ( isEmpty(obMessage) ){
				obErrorMessage.title = fn_APP_CONF_GET_TEXT( "messageFile", myCurrentData.WS_CALL.obError.title_alias );
				obErrorMessage.message = fn_APP_CONF_GET_TEXT( "messageFile", myCurrentData.WS_CALL.obError.message_alias );
			} else {
				if ( isEmpty(obMessage.message) ){
					obErrorMessage.title = fn_APP_CONF_GET_TEXT( "messageFile", obMessage.title_alias );
					obErrorMessage.message = fn_APP_CONF_GET_TEXT( "messageFile", obMessage.message_alias );
				} else {
					obErrorMessage = obMessage;
				}
			}
		} catch(e){

			obErrorMessage.title = "";
			obErrorMessage.message = "No message defined for this error";

		}

		return obErrorMessage;

	};


	

	function isNetworkAvailable(){
		return new AXX_CONN().hasNetworkConnection();
	};

	// function clearTimeout( obTimeOut ){
	// 	obTimeOut = null;
	// 	wsTimeOutTimer = null;
	// };

	function renewSessionCounter( methodName ){
		try {
			console_log( "Keeping the session by [" + methodName + "]" );
			myCurrentData.INFO.set_Session_LastAction( Date.now() );
		} catch(e){
			console_log(" Method [" + methodName + "] There is no session yet.");
		}
	}

};

function clearWS_Execution(){

	console_log("clearWS_Execution");

	myCurrentData.activeWebService = null;

	try{
		// if (!isEmpty(myCurrentData.activeWebService)) {

		// 	var operationIndex = obParameters.obError.operationIndex;
		// 	var res = obParameters.obError.res;

		// 	if (myCurrentData.activeWebService != null){
		// 		myCurrentData.activeWebService.executeActionOnAllWSReadyFailure(operationIndex);
		// 	}

		// 	if ( res ){
		// 		myCurrentData.activeWebService.wsFailed(operationIndex, res);
		// 	}

		// }
	} catch(e){

	} finally{
		myCurrentData.TEMP.obParameters = null;
	}

};
