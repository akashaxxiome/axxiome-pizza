function COMPARTAMOS_DEVICE ( stDevice_Number ){
	
	// List here all the device properties
	var DEVICE_INFO = new Object({
		manufacturer: null,
		model: null,
		cordova: null,
		platform: null,
		version: null,
		uuid: null,
		serial: null,
    	number: "", //is it possible to retrieve this information from the device ?
    });

	// List here all the desktop properties
	var SCREEN_INFO = new Object({
    	screen_width: "",
		menu_width : "22%",
		content_width : "78%",
    });
	
	
    
	/****************************************
     *  DEVICE METHODS
     * */
	var set_Device_Manufacturer = function(stValue){
		DEVICE_INFO.manufacturer = stValue;
	};
	var set_Device_Model = function( stValue ){
    	DEVICE_INFO.model = stValue;
    };
    var set_Device_Platform = function( stValue ){
    	DEVICE_INFO.platform = stValue;
    };
    var set_Device_Version = function( stValue ){
    	DEVICE_INFO.version = stValue;
    };
	var set_Device_UUID = function(stValue){
		DEVICE_INFO.uuid = stValue;
	};
	var set_Device_Serial = function(stValue){
		DEVICE_INFO.serial = stValue;
	};
	var set_Device_Cordova = function( stValue ){
    	DEVICE_INFO.cordova = stValue;
    };
    var set_Device_Number = function( stValue ){
    	DEVICE_INFO.number = stValue;
    };
    
    
    this.get_Device_Manufacturer = function(){
    	return DEVICE_INFO.manufacturer;
    }
    this.get_Device_Platform = function(){
    	return DEVICE_INFO.platform + " " + DEVICE_INFO.version;
    }
    this.get_Device_UUID = function(){
    	
    	if ( DEVICE_INFO.uuid == null ) {
    		if ( myCurrentData.IS_MOBILE && typeof cordova != 'undefined' ){
    			DEVICE_INFO.uuid = device.uuid;
    		} else {
    			DEVICE_INFO.uuid = null;
    		}
    	}
    	return DEVICE_INFO.uuid;
    	// return "5e3562c0af6dd7d6"; //DEVICE_INFO.uuid;

    }
    this.get_Device_Serial = function(){
    	return DEVICE_INFO.serial;
    }
    this.get_Device_Mobile_Number = function(){
    	return DEVICE_INFO.number;
    }
    
    
    /****************************************
     *  SCREEN METHODS
     * */
    this.set_Screen_Menu_Width = function( stValue ){
    	Common.ScreenWidths.DESKTOP_MENU = stValue + "%";
    	Common.ScreenWidths.DESKTOP_CONTENT = ( 100 - stValue ) + "%";
    	
    	try{
	    	var arPages = homeNavContainer.getPages();
	    	$.each( arPages, function( i, theView ){
	    		if ( theView.div_menu ){
					 myCurrentData.DEVICE.adjust_View_Width(theView);
	    		}
			});
    	}
	    catch(e){}
    	
    };
    
    this.adjust_View_Width = function( obView ){
    	try{
	    	obView.div_menu.setWidth( ( myCurrentData.IS_MOBILE ) ? Common.ScreenWidths.MOBILE_MENU : Common.ScreenWidths.DESKTOP_MENU );
			obView.div_content.setWidth( ( myCurrentData.IS_MOBILE ) ? Common.ScreenWidths.MOBILE_CONTENT : Common.ScreenWidths.DESKTOP_CONTENT );
	    	obView.div_menu.setVisible( !myCurrentData.IS_MOBILE );
	    	//obView.thePage.removeStyleClass("sapMPageScroll");
			//obView.thePage.setShowFooter( !myCurrentData.IS_MOBILE );
    	} catch(e){
    		
    	}
    };
    
    this.get_Screen_Menu_Width = function(){
    	return SCREEN_INFO.menu_width;
    };
    this.get_Screen_Content_Width = function(){
    	return SCREEN_INFO.content_width;
    };
    
    
    
    this.set_DEVICE = function(){
    	if ( myCurrentData.IS_MOBILE && ( typeof device != 'undefined' ) ){
	    	set_Device_Manufacturer( device.manufacturer );
	    	set_Device_Model( device.model );
	    	set_Device_Platform( device.platform );
	    	set_Device_Version( device.version );
	    	set_Device_UUID( device.uuid );
	    	set_Device_Serial( device.serial );
	    	set_Device_Cordova( device.cordova );
    	} else {
    		set_Device_Manufacturer( navigator.vendor );
	    	set_Device_Model( navigator.appCodeName );
	    	set_Device_Platform( navigator.platform );
	    	set_Device_Version( navigator.appVersion.substr(0,3) );
	    	set_Device_UUID( "" );
	    	set_Device_Serial( "" );
	    	set_Device_Cordova( "" );
    	}
    };
    
    var register_local_storage = function( stKey, stValue ){
    	try {
    		window.localStorage.setItem( stKey, stValue );
    	} catch(e){
    		 console_log("##### ERROR REGISTERING IN LOCAL STORAGE");
    	} finally{
    		return false;
    	}
	};

	this.setCalculatedSeed = function (calculatedSeed) {
		register_local_storage( "CalculatedSeed", calculatedSeed );
	},

	this.getCalculatedSeed = function () {
		var calculatedSeed = '';
		try {
			calculatedSeed = window.localStorage.getItem( "CalculatedSeed" );
			return calculatedSeed;
		} catch(e) {
			console_log("##### ERROR RETRIEVING CALCULATED SEED FROM LOCAL STORAGE");
			return calculatedSeed;
		}
	},

	this.setCookie = function (cookie) {
		register_local_storage( "cookie", cookie);
	},

	this.getCookie = function () {
		return window.localStorage.getItem("cookie");
	},

	this.removeCookie = function () {
		window.localStorage.removeItem("cookie");
	},

	this.saveMessageNonce = function (nonce) {
		register_local_storage( "messageNonce", toHex(nonce));
	},

	this.getMessageNonce = function () {
		return fromHex(window.localStorage.getItem("messageNonce"));
	},

	this.saveClientPublicKey = function (key) {
		register_local_storage( "clientPublicKey", toHex(key));
	},

	this.saveClientSecretKey = function (key) {
		register_local_storage( "clientSecretKey", toHex(key));
	},

	this.saveServerPublicKey = function (key) {
		register_local_storage( "serverPublicKey", key);
	},

	this.getClientPublicKey = function () {
		return window.localStorage.getItem("clientPublicKey");
	},

	this.getClientSecretKey = function () {
		return window.localStorage.getItem("clientSecretKey");
	},

	this.getServerPublicKey = function () {
		return window.localStorage.getItem("serverPublicKey");
	},

	this.set_registered_user = function( stValueA ){
			register_local_storage( "CompartamosUserReg", stValueA );
	};
	this.get_registered_user = function( ){
		var bpId = "";
		try {
			if(bpId == null){
				bpId = ""
			} else{
				bpId = window.localStorage.getItem( "CompartamosUserReg" );
			}
			return bpId;
		} catch(e) {
			console_log("##### ERROR RETRIEVING FROM LOCAL STORAGE");
			return bpId;
		} finally {
			
		}
	};

	this.set_Enroll_DOB_Format = function( stValue ){
		register_local_storage( "CompartamosEnrollDateFormat", stValue );
	}
	this.get_Enroll_DOB_Format = function (){
		try {
			return window.localStorage.getItem( "CompartamosEnrollDateFormat" );
		} catch(e) {
			return null;
		}
	},

	this.removeEnrollDOBFormat = function () {
		window.localStorage.removeItem('CompartamosEnrollDateFormat');
	},

	this.set_Enroll_DOB_UUID = function( stValue ){
		register_local_storage( "CompartamosEnrollDateUUID", stValue );
	},

	this.get_Enroll_DOB_UUID = function (){
		try {
			var UUID = window.localStorage.getItem( "CompartamosEnrollDateUUID" );
			return UUID;
		} catch(e) {
			return null;
	}
	},

	this.removeEnrollDobUUID = function () {
		window.localStorage.removeItem('CompartamosEnrollDateUUID');
	}

	this.set_RememberMe = function( stValue ){
		register_local_storage( "RememberMe", stValue );
	}
	this.get_RememberMe = function( stValue ){
		var myValue = isEmpty(window.localStorage.getItem("RememberMe")) ? "false" : window.localStorage.getItem("RememberMe");
		return ( myValue === "true" );
	}

}