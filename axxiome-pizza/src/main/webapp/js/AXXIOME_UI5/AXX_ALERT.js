function DISPLAY_MESSAGE(){
	
	this.btnCancel_Regular = fn_APP_CONF_GET_TEXT( "i18N", "common.button.Cancel");
	this.btnOk_Regular = fn_APP_CONF_GET_TEXT( "i18N", "common.button.Ok");

	this.Show_NEW = function( stTitle, stMessage, obParameters ){

		var stType = null;
		var isHtml = ( isEmpty(obParameters.isHtml ) ? false : obParameters.isHtml );

		if ( isHtml ){
			stMessage = "<div style='margin-bottom: 2em'>" + stMessage + "</div>";
		}

		var stTitle = ( stTitle ) ? stTitle : fn_APP_CONF_GET_TEXT( "messageFile", "MESSAGE.ALERT.TITLE") ;

		var closeAfterOK = ( isEmpty(obParameters.closeAfterOK )  ? true : obParameters.closeAfterOK );
		var closeAfterCancel = ( isEmpty( obParameters.closeAfterCancel ) ) ? true : obParameters.closeAfterCancel ;

		var fn_callBack_YES = obParameters.fn_callBack_YES;
		var fn_callBack_NO = obParameters.fn_callBack_NO;
		var parameters_Yes = obParameters.parameters_Yes;
		var parameters_No = obParameters.parameters_No;


		var boShowCancelButton = false;
		var txtCancelButton = fn_APP_CONF_GET_TEXT( "i18N", "common.button.Cancel");
		if ( boShowCancelButton || ( $.isArray( obParameters.arButtonTexts ) && obParameters.arButtonTexts.length > 1 ) ){
			txtCancelButton = obParameters.arButtonTexts[0] ;
		}
		boShowCancelButton = ( !isEmpty( obParameters.boShowCancelButton ) && obParameters.boShowCancelButton === false ) ? false : obParameters.boShowCancelButton;

		var txtOkButton = fn_APP_CONF_GET_TEXT( "i18N", "common.button.Ok");
		if ( $.isArray( obParameters.arButtonTexts ) && obParameters.arButtonTexts.length > 1 ){
			txtOkButton = obParameters.arButtonTexts[1] ;
		}
		var boShowConfirmButton = isEmpty(obParameters.showConfirmButton) ? true : obParameters.showConfirmButton;

		var customImage = ( isEmpty( obParameters.customImage ) ) ? null : obParameters.customImage;
		if ( customImage != null ){
			//isHtml = true;
			// stType = "html";
		}

		myCurrentData.POPUP = { ACTIVE : false, NAME: null, CLOSE_CANCEL : closeAfterCancel, CLOSE_OK : closeAfterOK };
		if( !isEmpty( obParameters.name ) ){
			myCurrentData.POPUP.ACTIVE = true;
			myCurrentData.POPUP.NAME = obParameters.name;
		}


		swal({

			title: stTitle, 
			text: stMessage, 
			source: "USER",
			type: stType,
			html: isHtml,
			showCancelButton: boShowCancelButton,
			showConfirmButton: boShowConfirmButton,
			confirmButtonColor: Common.Branding.BUTTON_COLOR,
			cancelButtonText: txtCancelButton,
			confirmButtonText: txtOkButton,   
			closeOnConfirm: closeAfterOK,
			closeOnCancel: closeAfterCancel,
			allowOutsideClick: isEmpty(obParameters.allowOutsideClick) ? false : obParameters.allowOutsideClick,
			imageUrl: customImage

		}, function( isConfirm ){
			


			console_log( "ANSWER IS [" + isConfirm + "]" );
			if ( isConfirm ){
				
				if ( myCurrentData.POPUP.CLOSE_OK ){
					myCurrentData.POPUP.ACTIVE = false;
				}

				if ( typeof fn_callBack_YES == 'function' ){
					var myArgs = (typeof parameters_Yes == 'undefined') ? null : parameters_Yes;
					fn_callBack_YES( myArgs );
				}

			} else{
				
				if ( myCurrentData.POPUP.CLOSE_CANCEL ){
					myCurrentData.POPUP.ACTIVE = false;
				}

				if ( fn_callBack_NO != null && fn_callBack_NO != undefined){
					var myArgs = (typeof parameters_No == 'undefined') ? null : parameters_No;
					fn_callBack_NO( myArgs );
				}
			}
			
		});
		
		if (stType && stType.toUpperCase() == "HTML") {
			$(".sweet-alert .sa-p-div").css("margin-bottom", "2em");
		} else {
			$(".sweet-alert .sa-p-div").css("margin-bottom", "0em");
		}
			
	}
	
	
};
	
	


/**
 * 	TIP: 	Pass null to arButtonTexts to have only 1 button, the [OK] 
 */

 var AXX_MESSAGE_HTML_CONFIRM = function ( stTitle, stMessage, arButtonTexts, closeAfterOK, fn_callBack_YES, fn_callBack_NO, obParameters  ) {
	
	// new DISPLAY_MESSAGE().Show( "HTML", stTitle, stMessage, arButtonTexts, fn_callBack_YES, fn_callBack_NO, closeAfterOK, null, null, obParameters );
	

	if ( isEmpty(obParameters) ){
		obParameters = {};
	}

	obParameters.fn_callBack_YES = fn_callBack_YES;
	obParameters.fn_callBack_NO = fn_callBack_NO;
	
	obParameters.parameters_Yes = null;
	obParameters.parameters_No = null;

	obParameters.closeAfterOK = closeAfterOK;

	//obParameters.customImage = IMAGES.POPUP.WARNING;
	obParameters.isHtml = true;
	obParameters.arButtonTexts = [];
	obParameters.boShowCancelButton = true;
	
	DISPLAY_MESSAGE_CONFIRM( stTitle, stMessage, obParameters );

};

var AXX_MESSAGE_CONFIRM = function( stTitle, stMessage, arButtonTexts, fn_callBack_YES, fn_callBack_NO, closeAfterOK, parameters_Yes, parameters_No, obParameters ){
	
	// new DISPLAY_MESSAGE().Show( "warning", stTitle, stMessage, arButtonTexts, fn_callBack_YES, fn_callBack_NO, closeAfterOK, parameters_Yes, parameters_No, obParameters );
	
	console_log(" >>> METHOD AXX_MESSAGE_CONFIRM deprecated. Use the DISPLAY_MESSAGE_CONFIRM ");

	if ( isEmpty(obParameters) ){
		obParameters = {};
	}

	obParameters.fn_callBack_YES = fn_callBack_YES;
	obParameters.fn_callBack_NO = fn_callBack_NO;
	
	obParameters.parameters_Yes = parameters_Yes;
	obParameters.parameters_No = parameters_No;

	obParameters.closeAfterOK = closeAfterOK;

	//obParameters.customImage = IMAGES.POPUP.WARNING;
	obParameters.stType = "warning";
	obParameters.arButtonTexts = [];
	obParameters.boShowCancelButton = true;
	
	DISPLAY_MESSAGE_CONFIRM( stTitle, stMessage, obParameters );

};

var AXX_MESSAGE_WARNING = function( stTitle, stMessage, fn_callBack_YES, fn_callBack_NO, closeAfterOK, parameters_Yes, parameters_No, obParameters ){
	
	//new DISPLAY_MESSAGE().Show( "warning", stTitle, stMessage, null, fn_callBack_YES, fn_callBack_NO, closeAfterOK, parameters_Yes, parameters_No, obParameters );

	console_log(" >>> METHOD AXX_MESSAGE_WARNING deprecated. Use the DISPLAY_MESSAGE_WARNING ");

	if ( isEmpty(obParameters) ){
		obParameters = {};
	}

	obParameters.fn_callBack_YES = fn_callBack_YES;
	obParameters.fn_callBack_NO = fn_callBack_NO;
	
	obParameters.parameters_Yes = parameters_Yes;
	obParameters.parameters_No = parameters_No;

	obParameters.closeAfterOK = closeAfterOK;

	//obParameters.customImage = IMAGES.POPUP.WARNING;
	obParameters.stType = "warning";
	obParameters.arButtonTexts = null;
	
	DISPLAY_MESSAGE_WARNING( stTitle, stMessage, obParameters );
	
};



var AXX_MESSAGE_SUCCESS = function( stTitle, stMessage, fn_callBack_YES, parameters_Yes, obParameters ){
	
	// new DISPLAY_MESSAGE().Show( "success", stTitle, stMessage, null, fn_callBack_YES, null, true, parameters_Yes, null, obParameters );
	console_log(" >>> METHOD AXX_MESSAGE_SUCCESS deprecated. Use the DISPLAY_MESSAGE_SUCCESS ");
	if ( isEmpty(obParameters) ){
		obParameters = {};
	}

	obParameters.fn_callBack_YES = fn_callBack_YES;
	obParameters.parameters_Yes = parameters_Yes;
	
	DISPLAY_MESSAGE_SUCCESS( stTitle, stMessage, obParameters );
	
};



/** NEW METHODS -> Use these ones. The others are deprecated. */


var DISPLAY_MESSAGE_ERROR = function( stTitle, stMessage, obParameters ){
	
	if ( isEmpty(obParameters) ){
		obParameters = {};
	}

	obParameters.customImage = IMAGES.POPUP.ERROR;
	AXX_MESSAGE_IMG_CUSTOM( stTitle, stMessage, obParameters );
	
};

var DISPLAY_MESSAGE_WARNING = function( stTitle, stMessage, obParameters ){
	
	if ( isEmpty(obParameters) ){
		obParameters = {};
	}

	obParameters.customImage = IMAGES.POPUP.WARNING;
	obParameters.stType = "";
	obParameters.arButtonTexts = null;
	AXX_MESSAGE_IMG_CUSTOM( stTitle, stMessage, obParameters );
	
};

var DISPLAY_MESSAGE_SUCCESS = function( stTitle, stMessage, obParameters ){
	
	if ( isEmpty(obParameters) ){
		obParameters = {};
	}
	if ( isEmpty(stTitle) ){
		stTitle = "Ok";
	}

	obParameters.customImage = IMAGES.POPUP.SUCCESS;
	// obParameters.stType = "success";
	AXX_MESSAGE_IMG_CUSTOM( stTitle, stMessage, obParameters );
	
};

var DISPLAY_MESSAGE_CONFIRM = function( stTitle, stMessage, obParameters ){
	
	if ( isEmpty(obParameters) ){
		obParameters = { };
	}

	if ( isEmpty( obParameters.stType ) ){
		obParameters.stType = "";
	}

	if ( isEmpty( obParameters.arButtonTexts ) ){
		obParameters.arButtonTexts = [];
	}

	obParameters.boShowCancelButton = true ;
	obParameters.customImage = IMAGES.POPUP.WARNING;

	new DISPLAY_MESSAGE().Show_NEW( stTitle, stMessage, obParameters );
	
};

var DISPLAY_MESSAGE_HTML = function( stTitle, stMessage, obParameters ){
	
	if ( isEmpty(obParameters) ){
		obParameters = {
			boShowCancelButton : false
		};
	}
	if ( isEmpty( obParameters.arButtonTexts ) ){
		obParameters.arButtonTexts = [];
	}

	obParameters.customImage = "";
	obParameters.isHtml = true;

	new DISPLAY_MESSAGE().Show_NEW( stTitle, stMessage, obParameters );

	$(".sweet-alert").addClass("sweet-alert-html");
	
};

var AXX_MESSAGE_IMG_CUSTOM = function ( stTitle, stMessage, obParameters  ) {
	
	var arButtonTexts = ( isEmpty(obParameters.arButtonTexts) ? null : obParameters.arButtonTexts );
	var closeAfterOK = ( isEmpty( obParameters.closeAfterOK ) ? true : obParameters.closeAfterOK );
	var fn_callBack_YES = ( isEmpty(obParameters.fn_callBack_YES) ? null : obParameters.fn_callBack_YES ); 
	var fn_callBack_NO = ( isEmpty(obParameters.fn_callBack_NO) ? null : obParameters.fn_callBack_NO ); 
	
	new DISPLAY_MESSAGE().Show_NEW( stTitle, stMessage, obParameters );
	
};

var AXX_MESSAGE_PROMPT = function( stTitle, stMessage, fn_callBack_YES, theMessage_Example, theMessage_Empty ){
	
	swal({   
		title: theTitle,   
		text: theMessage,   
		type: "input",   
		showCancelButton: true,
		closeOnConfirm: false,   
		inputPlaceholder: theMessage_Example,
		allowOutsideClick: false,
	}, function(inputValue){   
		if (inputValue === false) return false;      
		if (inputValue === "") {     
			swal.showInputError( (theMessage_Empty != null) ? theMessage_Empty : fn_APP_CONF_GET_TEXT( "i18N", "common.field.isEmpty" ) );
			return false   
		} else {
			fn_callBack_YES(inputValue);
		}
	});
	
}

