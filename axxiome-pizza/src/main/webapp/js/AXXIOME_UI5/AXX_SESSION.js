function AXX_SESSION ( inSessionID, inTimeOut, stUserID, inUserName, inCustomerID ){
	
	var session_Clock_TimeLeft = myCurrentData.IS_MOBILE ? Common.Session.APP.POPUP_COUNTER : Common.Session.WEB.POPUP_COUNTER;
	
    var SESSION_INFO = new Object({
    	session_id : (inSessionID) ? inSessionID : null,
    	time_out: (inTimeOut) ? inTimeOut : null,
    	last_check: null,
    	last_action: null,
    	popUp_user: false
    });
    var USER_INFO = new Object({ 
    	id :  (inTimeOut) ? stUserID : null,
    	customer_id : (inTimeOut) ? inCustomerID : null,
    	display_name:  (inTimeOut) ? inUserName : null, 
    	last_session: null,
    	mobile_number: null,
    });
	
	/*	=============================================================================================
	 *	AUXILIARY METHODS	SECTION 
	 */
    /*
    this.set_SessionID = function( inSessionID ){
    	SESSION_ID = inSessionID;
    }
    */
    
	this.set_User_LastSession = function( stValue ){
    	USER_INFO.last_session = stValue;
    };
	this.set_Session_TimeOut = function( stValue ){
    	SESSION_INFO.time_out = stValue;
    };
    this.set_Session_LastCheck = function( stValue ){
    	SESSION_INFO.last_check = stValue;
    };
    this.set_Session_LastAction = function( stValue ){
    	SESSION_INFO.last_action = stValue;
    };
    
    //REMOVE THIS GET AFTER WS IS BACK
    this.set_User_ID = function( stValue ){
    	USER_INFO.id = stValue;
    }
    this.set_Session_ID = function( stValue ){
    	USER_INFO.session_id = stValue;
    };
    
    this.get_User_ID = function(){
    	return USER_INFO.id;
    };
    this.get_User_InternalID = function(){
    	return USER_INFO.customer_id;
    }
    this.get_User_DisplayName = function(){
    	return USER_INFO.display_name;
    };
    this.get_User_LastSession = function(){
    	return USER_INFO.last_session;
    };
    this.get_Session_ID = function(){
    	return SESSION_INFO.session_id;
    };
    this.get_Session_TimeOut = function(){
    	return SESSION_INFO.time_out;
    };
    this.get_Session_LastAction = function(){
    	return SESSION_INFO.last_action;
    };
    this.get_Mobile_Number = function(){
    	return SESSION_INFO.mobile_number;
    }
    
	this.CLEAR = function(){
		SESSION_INFO.session_id = null;
		SESSION_INFO.time_out = null;
		USER_INFO.id = null;
		USER_INFO.display_name = null;
		USER_INFO.last_session = null;
		USER_INFO.mobile_number = null;
		USER_INFO.ACCOUNTS = [];
	};
	
	
	this.logOut = function(){

		if ( new AXX_CONN().hasNetworkConnection() == false ){

			// DISPLAY_MESSAGE_WARNING("","No hay conexión");
			myCurrentData.INFO.logOut_Force( fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , "03_F23") );

		} else {

			//AUTHENTICATION_Logout_ASK( this.logout_Success, this.logout_Error );
			// swal({   
			// 	title: fn_APP_CONF_GET_TEXT( "i18N", "general.dialog.Logout.ConfirmTitle" ), 
			// 	text: fn_APP_CONF_GET_TEXT( "i18N", "general.dialog.Logout.ConfirmText" ), 
			// 	type: "warning",
			// 	//animation: "slide-from-top",
			// 	showCancelButton: true,   
			// 	confirmButtonColor: Common.Branding.BUTTON_COLOR,
			// 	confirmButtonText: fn_APP_CONF_GET_TEXT( "i18N", "general.dialog.Logout.ButtonConfirm" ), 
			// 	cancelButtonText: fn_APP_CONF_GET_TEXT( "i18N", "general.dialog.Logout.ButtonCancel"), 
			// 	closeOnConfirm: true,   
			// 	closeOnCancel: true
			
			// }, function(isConfirm){
			// 	if(isConfirm){
			// 		logOutSession();
			// 	} else {
			// 		null;
			// 	}
			// });

			var obParameters = {
				fn_callBack_YES: function(){
					logOutSession();
				},
				fn_callBack_NO : function(){

					try{
						myCurrentData.INFO.renewSessionCounter( "User PROMPT -> Continue " );
					} catch(e){}
					// myCurrentData.INFO.session_Check("Renew");
					
				}
			};

			DISPLAY_MESSAGE_CONFIRM(
				"Session Logout",
				"Are you sure you want to close your session?",
				obParameters
			);
			
		}
	
	};
	
	
	
	this.logOut_Force = function( stMessage, boExecuteWS ){

		try{

			var oCon = sap.ui.getCore().byId( myCurrentData.stView ).getController();

			myCurrentData.INFO.set_User_ID(null);
			myCurrentData.INFO.set_Session_ID(null);
			myCurrentData.DEVICE.removeCookie();

			console_log("logOut_Force");
			myCurrentData.TEMP.action = "logoutForced";
			myCurrentData.TEMP.actionMessage = stMessage;
			console_log('Clearing the view if method available')

			if( oCon!=null && typeof oCon.clearView == 'function' ){
				oCon.clearView();
			}

			// if ( new AXX_CONN().hasNetworkConnection() == true && boExecuteWS != false ){
			if ( new AXX_CONN().hasNetworkConnection() == true ){

				var wsLogout = new WSCaller().setSuccessCallback(this.logout_Success).setFailureCallback(this.logout_Error);
				var wsLogout = new WS(AUTHENTICATION_Logout, wsLogout );

				var wsWrapper = new WSPackageWrapper().setOperation(wsLogout);
				wsWrapper.startWS();

			} else {
				throw 1;
			}

		} catch(e){

			// SESSION_clear_TimeOut( true );
			// SESSION_clear_TimeOut_Clock( true );
			clearTimeout(wsTimeOutTimer);
			myCurrentData.activeWebService = null; 
			Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_SIGN_IN, action: "logout", mode: "forced" } );

		} finally {

		}
		
	};
	
	this.logout_Success = function(data){

		console_log(">> Logout_Success"); console_log(data);
		
		SESSION_clear_TimeOut( true );
		SESSION_clear_TimeOut_Clock( true );

		myCurrentData.activeWebService = null; 
		myCurrentData.INFO.set_User_ID(null);
		myCurrentData.INFO.set_Session_ID(null);
		
		myCurrentData.isLoggedIn = false;

		Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_SIGN_IN, action: "logout" } );
		
	};
	
	this.logout_Error = function( data ){
		
		console_log("CALL BACK ERROR"); console_log(data);
		obErrorTEMP = AXX_EXCEPTION_Translate_ERROR("I");
		AXX_EXCEPTION_APP( obErrorTEMP.title, obErrorTEMP.message, true );
		
		SESSION_clear_TimeOut( true );
		SESSION_clear_TimeOut_Clock( true );

		myCurrentData.INFO.set_User_ID(null);
		myCurrentData.INFO.set_Session_ID(null);
		myCurrentData.activeWebService = null; 
		
		Navigation_Invoke_Page({ PageTo: Common.Navigations.PAGE_SIGN_IN, action: "logout", mode: "forced" } );
		
	};
	
	
	
	
	
	
	this.session_Check = function( stText ){
		
		var timeIdle = ( Date.now() - myCurrentData.INFO.get_Session_LastAction() );
		var willExpireAt = myCurrentData.INFO.get_Session_TimeOut() + myCurrentData.INFO.get_Session_LastAction();
		var timeToExpire = willExpireAt - Date.now();
		var sinceLastCheck = ( Date.now() - SESSION_INFO.last_check );
		
		SESSION_clear_TimeOut_Clock();
		var period_for_checking = myCurrentData.IS_MOBILE ? Common.Session.APP.CHECK_PERIOD : Common.Session.WEB.CHECK_PERIOD;
		
		if ( timeoutHandle || stText == "Renew" ){
			session_Check_Calc( session_Check_Calc_SUCCESS );
		}
		
	};
	
	
	var session_Check_Calc = function(callBack){
		
		// console_log(">$>$> CHECKING IF SESSION IS ALIVE", { type: "SESSION.DEBUG_MODE_TIME" });
		
		var jsonData = { 
			Status : { code : 0, value: "" }, 
			isAboutToExpire: false, 
			isExpired: false, 
			lastAction: myCurrentData.TEMP.lastAction 
		};
		
		var isAboutToExpire = false;
		var isExpired = false;
		var timeOut = myCurrentData.INFO.get_Session_TimeOut();
		var timeIdle = ( Date.now() - myCurrentData.INFO.get_Session_LastAction() );
		
		var popUpPeriod = myCurrentData.IS_MOBILE ? Common.Session.APP.POPUP_IN_PERIOD : Common.Session.WEB.POPUP_IN_PERIOD;
		var timeToPopUp = popUpPeriod;
		
		if ( timeIdle > timeToPopUp && timeIdle < timeOut ) {
			isAboutToExpire = true;
			jsonData.Status.code = 1;
			jsonData.Status.code = "Session is about to Expire";
			
		} else if ( timeIdle > timeOut ) {
			
			jsonData.Status.code = 2;
			isExpired = true;
			
		} else {
//			console_log("C");
		}
		
	    jsonData.isAboutToExpire = isAboutToExpire;
	    jsonData.isExpired = isExpired;
	    
	    callBack(jsonData);
	    
	};
	
	
	session_Check_Calc_SUCCESS = function( jsonData ){
		
		var timeIdle = ( Date.now() - myCurrentData.INFO.get_Session_LastAction() );
		var timeToExpire = myCurrentData.INFO.get_Session_TimeOut() + myCurrentData.INFO.get_Session_LastAction() - Date.now();
		var willExpireAt = myCurrentData.INFO.get_Session_TimeOut() + myCurrentData.INFO.get_Session_LastAction();
		var period_for_checking = myCurrentData.IS_MOBILE ? Common.Session.APP.CHECK_PERIOD : Common.Session.WEB.CHECK_PERIOD;
		
		//SUCCESS CALLBACK
		if ( jsonData.Status.code == 0 ){
			
			// console_log(">$>$> SESSION IS OK", { type: "SESSION.DEBUG_MODE_TIME" } );
			console_log(">$>$> IDLE[" + timeIdle + "] :: EXPIRE IN[" + timeToExpire + "] :: WILL EXPIRE["+ willExpireAt +"]", { type: "SESSION.DEBUG_MODE_TIME"} );
						
			SESSION_INFO.last_check = Date.now();
			SESSION_clear_TimeOut();
			timeoutHandle = setTimeout( myCurrentData.INFO.session_Check, period_for_checking  );
			
		} else {
			if ( jsonData.isAboutToExpire && jsonData.isAboutToExpire == true ){
				
				console_log(">$>$> POP UP CHECK USER");
				
				if ( typeof timeoutHandleClock == 'undefined' || timeoutHandleClock == null ){
					
					session_Clock_TimeLeft = myCurrentData.IS_MOBILE ? Common.Session.APP.POPUP_COUNTER : Common.Session.WEB.POPUP_COUNTER;
					
					SESSION_clear_TimeOut( );
					SESSION_clear_TimeOut_Clock( );
					
					timeoutHandleClock = setTimeout( session_Clock, 1000 );
				
					var stMessage = fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , "03_F22" ) + "<br><br><span class='sweet-alert_clock'></span>";
					
					window.focus();
					AXX_MESSAGE_HTML_CONFIRM( 
						fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , "MESSAGE.ALERT.TITLE"), 
						stMessage,
						[ 
						  fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.I18N , "session.alert.btn.finish"), 
						  fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.I18N , "session.alert.btn.continue")
						],
						true,
						function(){

							SESSION_clear_TimeOut(  );
							SESSION_clear_TimeOut_Clock(  );
							
							myCurrentData.INFO.renewSessionCounter( "USER PROMPT" );

							console_log("SESSION [YES]");
							CPTMS_session_Keep("USER PROMPT");

							try{
								app.setBusy( sap.ui.getCore().byId( myCurrentData.stView ), false);
							} catch(e){}
						}, 
						function(){
							console_log("SESSION [NO]");
							SESSION_clear_TimeOut_Clock(  );
							sap.ui.getCore().byId( homeNavContainer.getCurrentPage().sId ).setBusy(false);
							myCurrentData.INFO.logOut_Force("", true);
						}
					);
					
				}
				
			} else if ( jsonData.isExpired && jsonData.isExpired == true ){
				console_log(">$>$> SESSION EXPIRED from [ session_Check_Calc ]", { type: "SESSION.DEBUG_MODE"});
				swal.close();
				SESSION_clear_TimeOut_Clock(  );
				myCurrentData.INFO.logOut_Force( fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , "03_F23") );
			}
		}
	
	};
	
	
	this.session_Clock_Kill = function(){
		SESSION_clear_TimeOut(  );
		SESSION_clear_TimeOut_Clock(  );
	}
	
	var session_Clock = function(){
		
		$(".sweet-alert .sweet-alert_clock").html( session_Clock_TimeLeft );
		
		if ( timeoutHandleClock ){
			
			if ( session_Clock_TimeLeft == -1 ){

				$(".sweet-alert .sweet-alert_clock").html( "Su sesión ha finalizado." );
				myCurrentData.INFO.session_Clock_Kill();

				console_log(">$>$> SESSION EXPIRED from [ session_Clock ]", { type: "SESSION.DEBUG_MODE"});
				myCurrentData.INFO.logOut_Force( fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , "03_F23") );

			} else {
				session_Clock_TimeLeft -= 1;
				timeoutHandleClock = setTimeout( session_Clock, 1000 );
			}
			
		}
		
	}
	
	
	
	
	
	
	this.session_Keep = function( stText ){

		var nonce = generateNonce();
		var encryptedBpId = getEncryptedBpId(nonce);

		var customData = {
			stText:"session_Keep [" + stText + "]",
			bpId : encryptedBpId,
			optData : toHex(nonce)
		};

		var successCallback = function(jsonData, customData){

			var stText = customData.stText;

			if ( jsonData.isValidSession == true ){
				
				//myCurrentData.INFO.set_Session_TimeOut(   );
				myCurrentData.INFO.renewSessionCounter( "session_Keep [" + stText + "]" );
				myCurrentData.INFO.session_Check("Renew");

			} else {
			
				timeoutHandle = null;
				myCurrentData.INFO.session_Clock_Kill();

				console_log(">$>$> SESSION EXPIRED from [ session_Keep ]", { type: "SESSION.DEBUG_MODE"});
				myCurrentData.INFO.logOut_Force( fn_APP_CONF_GET_TEXT( Common.GlobalModelNames.MESSAGE_FILE , "03_F23") );

			}

		}



		var wsParameterCheckSession = new WSCaller().setSuccessCallback(successCallback).setCustomData(customData);
		var wsCheckSession = new WS(AUTHENTICATION_Check_Session, wsParameterCheckSession );

		var wsWrapper = new WSPackageWrapper().setOperation(wsCheckSession);
		wsWrapper.startWS();

	};

	this.renewSessionCounter = function( methodName ){
		try {
			
			console_log( "Keeping the session [" + methodName + "]", { type: "SESSION.DEBUG_MODE" } );
			
			myCurrentData.INFO.set_Session_LastAction( Date.now() );

		} catch(e){
			console_log(" Method [" + methodName + "] There is no session yet.", { type: "SESSION.DEBUG_MODE" });
		}
	};
	
	
	this.check_Connection = function() {
		
	    if ( navigator.onLine !== 'undefined' ) {
	        if (window.addEventListener) {
	            window.addEventListener("online", function() { return false; }, false);
	            window.addEventListener("offline", function() { return true; }, false);
	            if (navigator.onLine) {
	                window.addEventListener("offline", function() { return true; }, false);
	                return false;
	            } else {
	                window.addEventListener("online", function() { return false; }, false);
	                return true;
	            }
	        }
	        else {
	            document.body.attachEvent("onoffline", function() { return true; });
	            document.body.attachEvent("ononline", function() { return false; });
	        }
	    } else {
	        return false;
	    }
	    
	};
	
};

function logOutSession(){
	
	// var mySuccess = null;
	try{
		myCurrentData.INFO.logout_Success();
	} catch(e){
		new AXX_SESSION().logout_Success();
	} finally{

		//clearing data
		myCurrentData.IS_SSL = false;
		myCurrentData.DEVICE.removeCookie();

		// Login Page - Orders button - visible = false
		viewSignin.div_buttons_line1.getItems()[0].getItems()[0].setVisible(false);
		// Login Page - Create Orders button - visible = true
		viewSignin.div_buttons_line1.getItems()[1].getItems()[0].setVisible(true);

		// var oCon = sap.ui.getCore().byId( homeNavContainer.getCurrentPage().getId() ).getController();
		
		// if(oCon!=null && typeof oCon.clearView == 'function'){
		// 	oCon.clearView();
		// }

		// var wsLogout = new WSCaller().setSuccessCallback(mySuccess).setFailureCallback(new AXX_SESSION().logout_Error);
		// var wsLogout = new WS(AUTHENTICATION_Logout, wsLogout );

		// var wsWrapper = new WSPackageWrapper().setOperation(wsLogout);
		// wsWrapper.startWS();
		
	}
	
}