/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    
    closeApp : function(){
    	try {
    		navigator.app.exitApp();
    	} catch(e){ console_log(">> Could not EXIT APP .."); }
    },
    
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    
    
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
		document.addEventListener( "backbutton", app.onBackKeyPress, true);
		document.addEventListener( "pause", app.onPause, true);
    },
    
    
    // Update DOM on a Received Event
    receivedEvent: function(id) {
    	
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console_log('Received Event: ' + id);
        
//        ios7 = (device.platform == 'iOS' && parseInt(device.version) >= 7);
//        if (ios7){
//            $("<link/>", {
//                   rel: "stylesheet",
//                   type: "text/css",
//                   href: "css/iOS7.css"
//                }).appendTo("head");
//					
//			document.body.style.height = screen.availHeight + 'px';
//			var nonSecureMenu = $(".nonSecureMenuCls");
//			nonSecureMenu.css({ "top": nonSecureMenu.position().top, "bottom": "auto"});
//        }
        
       
        
    },
    
	//Handle the back key press and prevent the default behavior of exiting the application.
	onBackKeyPress: function(e) {		
        
		try{
			e.preventDefault();
		} catch(er){}
		
		console_log('Received backbutton event');
		try{

			var thisView = myCurrentData.stView || homeNavContainer.getCurrentPage().getId();
			
			if( thisView == Common.Navigations.PAGE_HOME )
			{
				myCurrentData.INFO.logOut();
			} else {
				var fnBack = homeNavContainer.getCurrentPage().getController().backTap;
				if ( typeof fnBack == 'function' ){
					try{
						fnBack();
					} catch(e){
						Navigation_Invoke_Page({ PageTo: homeNavContainer.getPreviousPage().sId });
					}
				} else {
					Navigation_Invoke_Page({ PageTo: homeNavContainer.getPreviousPage().sId,  }); 
					// myCurrentData.stViewName = homeNavContainer.getPreviousPage().sId;
					// homeNavContainer.back();
				}
			}
		} catch(e){}
    },

	onPause: function () {

		try{
			if ( myCurrentData.INFO != null && myCurrentData.INFO.get_User_ID() != null ){
				logOutSession();
			}
		} catch(e){}

	},
    
    onMenuKeyDown :function(e){
    	applicationMenu.toggleSecureMenu();    	
    },
    
    setBusy : function( obView, bBusy ){
	
		try {
			if ( 1 == 1 ){
				throw 1;
			} else {
				
				// TODO: DBC Wait for user experience utilization and feedback
				// We can add the possibility of having only the div_content for DESKTOP being loading
				
				obView.setBusy(bBusy);
				
			}
		} catch(e){
			obView.setBusy(bBusy);
		}
		
	},

	setCurrentViewBusy: function(bBusy){
		try {
			app.setBusy( app.getCurrentView(), bBusy);
		} catch(e){
			var obView = homeNavContainer.getCurrentPage();
			obView.setBusy(bBusy);
		}
	},

	loadView : function(stView){

		var stType = typeof stView;
		var stViewName = "";
		var stViewPath = "";

		if ( stType == "object" ){
			stViewName = stView.name;
			stViewPath = stView.path;
		} else {

			stViewName = stView;
			$.each( Common.Views, function(index, item) {
			   // console.log(item.name);
				if ( item.name === stViewName ){
			   		stViewPath = item.path;
			   		return false;
			   	}
			});

		}

		try{

			if ( typeof sap.ui.getCore().byId( stViewName ) === "undefined"  ){
					
				var thePage = sap.ui.jsview( stViewName, stViewPath );
		 		homeNavContainer.addPage( thePage );

		 		console_log(" View [ " + stViewName + " ] ADDED");

			} else {
				console_log(" View [ " + stViewName + " ] already loaded");
			}

			return true;

		}
		catch(e){
			try{
				console_log(e);
				homeNavContainer.removePage( stViewName );
			} catch(e){}
			return false;
		}

	},

	getCurrentView : function(){
		return sap.ui.getCore().byId( myCurrentData.stView );
	},

	getCurrentController : function(){
		return app.getCurrentView().getController();
	},

	getSpecificController : function( stControllerName ){
		try{
			return sap.ui.getCore().byId( stControllerName ).getController(); 
		} catch(e){
			console_log(">> VIEW CONTROLLER not available yet !");
			return false;
		}
	}

};