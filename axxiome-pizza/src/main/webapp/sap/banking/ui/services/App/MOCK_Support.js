jQuery.sap.require("sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants");
/**
 @description CONTACT_Get_Contact_Email function
 @purpose Get the e-mail for sending message via Mobile
*/
var CONTACT_Get_Contact_Email_MOCK = function( callBack_Success, callBack_Failure ) {
    
    console_log("CONTACT_Get_Contact_Email");
    var jsonData = { 
    	"contactEmailAddress" : "servicioalcliente@compartamos.com", 
    	"contactSubject" : "Compartamos Mobile APP Contact" 
    };
    callBack_Success(jsonData);
	
};
