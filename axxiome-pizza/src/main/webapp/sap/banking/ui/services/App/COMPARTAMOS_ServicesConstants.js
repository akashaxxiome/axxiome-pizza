jQuery.sap.declare("sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants");
jQuery.sap.require("js.settings.ConnectionSettings");

//var HOST = connectionSettingController.getConnection_URL("REST");
var HOST= "";

sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants = {
		
	/** Constants used for Authentication service */
	ACCOUNT_SERVICE : {
		NAME: "Account_Service", //Class Name
		URI: HOST,
		PORT: "",	
		ENTITIES:{
			GET_WALLET : "/getWallet",
			GET_ACCOUNTS_BY_CATEGORY: "/getAccountsByCategory",
			GET_ACCOUNTS_SAVINGS: "/getCustomerAccounts",
			GET_ACCOUNTS_LOANS: "/getCustomerLoans",
			GET_ACCOUNTS_INSURANCES: "/getInsurance",
			CHANGE_ALIAS : "",
			CHECK_IS_ENOUGH_FUNDS: "",
			CHECK_IS_LOAN_PAYMENT_SUCCESFULL : "",
			GET_TRANSACTIONS: "/getTransactionsByAccountId",
			GET_CUSTOMER_ACCOUNTS_FOR_TRANSFER : "/getCustomerAccountsForTransfer",
			PAY_CREDIT : "/payCredit"
		}
	},

	BENEFICIENT_SERVICE : {
		NAME: "BENEFICIENT_Service",
		URI: HOST,
		PORT: "",
		ENTITIES:{
			GET_BENEFICIARY_ACCOUNT_TYPES: "/getBeneficiaryAccountTypes",
			GET_MOBILE_CARRIES : "/getMobileCarriers",
			GET_MOBILE_BENEFICIARIES: "/getMobileBeneficiaries",
			GET_TRANSFERS_BENEFICIARIES: "/getTransfersBeneficiaries",
			GET_LIST_BENEFICIARIES: "listBeneficiaries",
			CREATE_BENEFICIARY : "createBeneficiary",
			UPDATE_BENEFICIARY : "updateBeneficiary",
			DELETE_BENEFICIARY : "deleteBeneficiary"
		}
	},

	TOKEN_SERVICE : {
		NAME: "TOKEN_SERVICE",
		URI: HOST,
		PORT: "",
		ENTITIES: {
			GET_MOBILE_Token_RegisterMENT: "/getMobileTokenEnrollment", //reissueMobileTokenforEnrollment
			VERIFY_MOBILE_Token_RegisterMENT: "/verifyMobileTokenEnrollment",
			GET_MOBILE_TOKEN: "/getMobileToken",
			VERIFY_MOBILE_TOKEN: "/verifyMobileToken"
		}
	},

	TRANSFERS_SERVICE: {
		NAME: "TRANSFERS_Service",
		URI: HOST,
		PORT: "",
		ENTITIES:{
			GENERATE_TRANSACTION_ID: "/generateTransactionId",
			GENERATE_REFERENCE_NUMBER: "/generateReferenceNumber",
			VALIDATE_TRANSFER_AMOUNT: "/validateTransferAmount",
			EXECUTE_TRANSFER: "/executeTransaction",
			EXECUTE_MOBILE_TRANSFER: "/sendMobileMoney",
			GET_MOBILE_TRANSFERS: "/getMobileMoneyTransactions",
			CANCEL_MOBILE_TRANSFER:"/cancelMobileMoneyTransaction",
			IS_ACCOUNT_VALID_FOR_SPEI:"/isAccountValidForSPEI",
		}
	},

	/** Constants used for Authentication service */
	AUTHENTICATION_SERVICE : {
		NAME: "Authentication_Service", //Class Name
		URI: HOST,
		PORT: "",	
		ENTITIES:{
			OLD_LOGIN : "/loginCustomer",
			OLD_LOGOUT : "/logout",
			LOGIN : "/customLoginCustomer",
			LOGOUT : "/authenticationLogout",
			CHECK_SESSION: "/checkSession",
			GET_SECOND_AUTH_FACTOR : "/getSecondAuthFactor",
			GET_SERVER_PUBLIC_KEY : "/getServerPublicKey"
		}
	},
	
	/** Constants used for Credentials service */
	CREDENTIAL_SERVICE : {
		NAME: "Authentication_Service", //Class Name
		URI: HOST,
		PORT: "",	
		ENTITIES:{
			PIN_CHANGE: "/changeCredential", 
			PIN_CONFIRMATION: "/checkCredential", // Will be deprecated with the new WS
			PIN_VALID: "/validateCredential",
		}
	},
	
	/** Constants used for Customer Info service */
	CUSTOMER_SERVICES : {
		NAME: "Customer_Service", //Class Name
		URI: HOST,
		PORT: "",
		ENTITIES:{
			GET_INFO : "getCustomerOtherIndentificationByCustomer",
			SET_SECURITY_QUESTION : "/setCustomerSecurityQuestion",
			GET_EMAIL : "",
			GET_CUSTOMER_DEVICE_ACTIVATION : "/validateDeviceId",
		}
	},
	
	/** Constants used for Customer Info service */
	SUPPORT_SERVICES : {
		NAME: "Support_Service", //Class Name
		URI: HOST,
		PORT: "",
		ENTITIES:{
			GET_CONTACT_EMAIL: "/getContactEmailAddress",
			SEND_CONTACT : "/sendContactEmail",
		}
	},
	
	/** Constants used for Cards services */
	CARDS_SERVICES : {
		TDD:{
			ENTITIES:{
				GET_CARDS: "/customerCards",
				ACTIVATE_TDD: "/activateCard",
				CHANGE_TDD_NIP: "/changeNip",
			}
		}
	},
	
	/** Constants used for Enrollment services */
	ENROLLMENT_SERVICES : {
		ENTITIES:{
			CHECK_CUSTOMER: "/CheckCustomer",
			CHECK_ACTIVATION: "/validateActivationCode",
			CHECK_ONLINE_BANKING: "/checkCustHasMobileBank",

			CHECK_CUSTOMER_PASSWORD: "/validateMobileBankPassword",
			
			CREATE_CUSTOMER_PASSWORD: "/completeEnrolmentProcess",
			GENERATE_ACTIVATION_CODE: "/generateActivationCode",

			FORMAT_BIRTH_DATE: "/formatBirthDate",

			SEND_CUSTOMER_EMAIL: "",
			ACTIVATE_SERVICE: "/activateService",
		}
	},
	
	/** Constants used for General services */
	GENERAL_SERVICES : {
		ENTITIES:{
			GET_BANK_CATALOG: "getBankCatalog",
		}
	},
	
	/** Constants used for Authentication service */
	LOAN_SERVICE : {
		NAME: "Loan_Service", //Class Name
		URI: HOST,
		PORT: "",	
		ENTITIES:{
			GET_SUGGESTED_PAYMENT_AMOUNT : "/getSuggestedPaymentAmount",
		}
	},
	
	/** Constants used for Authentication service */
	SETTINGS_SERVICES : {
		NAME: "Settings_Service", //Class Name
		URI: HOST,
		PORT: "",	
		ENTITIES:{
			HALT_SERVICE : "/createBlockedUser",
		}
	},
	
};