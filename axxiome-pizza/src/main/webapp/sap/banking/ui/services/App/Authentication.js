jQuery.sap.require("sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants");

/**
 @description AUTHENTICATION_Login function
 @param username The username should normally be the msisdn in addition to its country code i.e. +18881234567
 @param password The user password
*/
var AUTHENTICATION_Login = function(parameters){
	
	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.AUTHENTICATION_SERVICE;
	var stURL_Service = CONST_AUTH.ENTITIES.LOGIN;

	var obData = new Object();
	obData.identification = parameters.customData.username;
	obData.credential = parameters.customData.userpin;
	obData.optData=parameters.customData.optData;

	obData.identificationType = 5;
	obData.credentialType = 1;

	var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
};

/**
 @description AUTHENTICATION_Logout function
*/
var AUTHENTICATION_Logout = function(parameters) {
    
	console_log("AUTHENTICATION_Logout");
	
	var obView = sap.ui.getCore().byId( homeNavContainer.getCurrentPage().sId );
	obView.setBusy( true );
	
	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.AUTHENTICATION_SERVICE;
	var stURL_Service = CONST_AUTH.ENTITIES.OLD_LOGOUT;

	var nonce = generateNonce();
	var encryptedBpId = getEncryptedBpId(nonce);

	var obData = new Object();
	try {
		obData.sessionId = myCurrentData.INFO.get_Session_ID();
		// obData.bpId = encryptedBpId;
		// obData.optData = toHex(nonce);
	} catch(e){console_log(e)}

	parameters.customData = {
		obView : obView,
		success_callback: parameters.success_callback
	};

	var successCallback = function(json, customData){
		customData.obView.setBusy( false );
		myCurrentData.INFO.CLEAR();

		customData.success_callback(json, customData);
	}

	parameters.success_callback = successCallback;
	
	var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
};

/**
 @description AUTHENTICATION_Check_Session function
*/
var AUTHENTICATION_Check_Session = function( parameters ) {
	console_log("AUTHENTICATION_Check_Session");
	
	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.AUTHENTICATION_SERVICE;
	var stURL_Service = CONST_AUTH.ENTITIES.CHECK_SESSION;
	
	var obData = new Object();
	obData.sessionId = myCurrentData.INFO.get_Session_ID();
	obData.bpId = parameters.customData.bpId;
    obData.optData = parameters.customData.optData;

	var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
}

var AUTHENTICATION_Get_Second_Factor_Auth = function(parameters){

	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.AUTHENTICATION_SERVICE;
	var stURL_Service = CONST_AUTH.ENTITIES.GET_SECOND_AUTH_FACTOR;

	var obData = new Object();
	obData.customerId = parameters.customData.username;
	obData.publicKey = parameters.customData.publicKey;

	var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
};

var AUTHENTICATION_Get_Server_Public_Key = function(parameters){

	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.AUTHENTICATION_SERVICE;
	var stURL_Service = CONST_AUTH.ENTITIES.GET_SERVER_PUBLIC_KEY;

	var obData = new Object();

	var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
};