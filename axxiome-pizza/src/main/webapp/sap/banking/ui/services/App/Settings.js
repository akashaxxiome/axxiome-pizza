jQuery.sap.require("sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants");

/**
 @description SETTINGS_Halt_Service function
 @param 
 @param 
 @param 
*/
var SETTINGS_Halt_Service = function( parameters ){
	console_log("SETTINGS_Halt_Service");
	
	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.SETTINGS_SERVICES;
	var stURL_Service = CONST_AUTH.ENTITIES.HALT_SERVICE;
	
	var obData = new Object();
	obData.BlockedUser = {
		bpId : ( myCurrentData.INFO == null ) ? "" : myCurrentData.INFO.get_User_ID(),
		operation : "suspend"
	}

	var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
    
}