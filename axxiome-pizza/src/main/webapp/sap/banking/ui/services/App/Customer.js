var CUSTOMER_Get_Device_Activation = function( parameters ) {
    
    console_log("CUSTOMER_Get_Device_Activation");
	
	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.CUSTOMER_SERVICES;
	var stURL_Service = CONST_AUTH.ENTITIES.GET_CUSTOMER_DEVICE_ACTIVATION;
	
	var obData = new Object();
    obData.bpId = parameters.customData.bpId;
    obData.deviceId = parameters.customData.deviceId;
	obData.optData = parameters.customData.optData;
	obData.publicKey = parameters.customData.publicKey;

    var myConn = new AXX_CONN();
    myConn.POST_New( obData
        , stURL_Service
        , parameters
    );

};

var CUSTOMER_GetInfo = function( parameters) {

    console_log("CUSTOMER_GetInfo");
	
	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.CUSTOMER_SERVICES;
	var stURL_Service = CONST_AUTH.ENTITIES.GET_INFO;

	var obData = new Object();
	obData.customerId = parameters.customData.inCustomer_ID;

	var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
};



var CUSTOMER_Set_Security_Question = function( parameters ) {

	console_log("CUSTOMER_Set_Security_Question");
	
	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.CUSTOMER_SERVICES;
	var stURL_Service = CONST_AUTH.ENTITIES.SET_SECURITY_QUESTION;

	var obData = new Object();
	obData.customerId = parameters.customData.customerId;
	obData.email = parameters.customData.email;
	obData.security_question = parameters.customData.security_question;
	obData.security_answer = parameters.customData.security_answer;

	var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
};