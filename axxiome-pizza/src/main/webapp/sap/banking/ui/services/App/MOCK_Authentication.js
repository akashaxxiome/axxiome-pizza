jQuery.sap.require("sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants");


/**
 @description AUTHENTICATION_Check_Session function
*/
var AUTHENTICATION_Check_Session_MOCK = function( callBack_Success, callBack_Failure ) {
	
	console_log("AUTHENTICATION_Check_Session_MOCK");
	var jsonData = { 
		Status : { code : 0, value: "" }, 
		isAboutToExpire: false, 
		isExpired: false, 
		lastAction: myCurrentData.TEMP.lastAction 
	};
	
	var isAboutToExpire =  false;
	var isExpired = false;
	var timeOut = myCurrentData.INFO.get_Session_TimeOut();
	var timespan = Date.now() - myCurrentData.INFO.get_Session_LastAction();
	var oneMinuteBefore = timeOut - Common.Session.POPUP_IN_PERIOD;
	
	if ( timespan > oneMinuteBefore && timespan < timeOut ) {
		console_log("A");
		isAboutToExpire = true;
		jsonData.Status.code = 1;
		jsonData.Status.code = "Session is about to Expire";
	} else if ( timespan > timeOut ) {
		jsonData.Status.code = 2;
		console_log("B");
		isExpired = true;
	} else {
		console_log("C");
	}
	console_log( "SESSION Since last check [" + timespan + "]" );
	
    jsonData.isAboutToExpire = isAboutToExpire;
    jsonData.isExpired = isExpired;
    
    callBack_Success(jsonData);
	
}


var AUTHENTICATION_Set_Session_Alive_MOCK = function( methodName, callBack_Success, callBack_Failure ) {
	
	console_log("AUTHENTICATION_Set_Session_Alive_MOCK");
	try {
		myCurrentData.INFO.set_Session_LastAction( Date.now() );
	} catch(e){
		console_log(" Method [" + methodName + "] There is no session yet.");
	}
    var jsonData = { Status : { code : 0, value: 0 } };
    callBack_Success(jsonData);
	
}






