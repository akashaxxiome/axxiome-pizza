jQuery.sap.require("sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants");
/**
 @description CONTACT_Get_Contact_Email function
 @purpose Get the e-mail for sending message via Mobile
*/
var CONTACT_Get_Contact_Email = function(parameters) {
    
    console_log("CONTACT_Get_Contact_Email");
	
	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.SUPPORT_SERVICES;
	var stURL_Service = CONST_AUTH.ENTITIES.GET_CONTACT_EMAIL;
	
	var obData = new Object();
	
    var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
};

/**
 @description CONTACT_Send_Contact function
 @purpose Send an e-mail contact to the Bank Customer Center
 @param stEmail 
 @param stText 
*/
var CONTACT_Send_Contact = function( parameters ) {
    
    console_log("CONTACT_Send_Comments");
	
	var CONST_AUTH = sap.banking.ui.services.App.COMPARTAMOS_ServicesConstants.SUPPORT_SERVICES;
	var stURL_Service = CONST_AUTH.ENTITIES.SEND_CONTACT;
	
	var obData = new Object();
	obData.contactEmail = {
		bpidSender : ( myCurrentData.INFO == null ) ? null : myCurrentData.INFO.get_User_ID(),
		emailText : parameters.customData.stText
	};

	var myConn = new AXX_CONN();
	myConn.POST_New( obData
		, stURL_Service
		, parameters
	);
	
};