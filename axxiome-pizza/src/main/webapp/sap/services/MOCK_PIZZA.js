var PIZZAS_MOCK = {

    Get_Options : function( obParameters ) {
    
        console_log("PIZZA_Get_Options");
        
        var jsonData = { Status : { code : 0, value: 0} };
        
        $.getJSON( "mockdata/StandardPizzas.json", function(jsonData) {

            var oModel = new sap.ui.model.json.JSONModel();
            oModel.setData(jsonData);
            sap.ui.getCore().setModel( oModel, Common.ModelNames.PIZZAS_STANDARD );

            //note: for you to read the model, just use:
            // sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_STANDARD );        
           
            var newjsonData = {
                "Status" : {
                    code: 0,
                    value: 0
                },
                "StandarPizzas" : jsonData
            };
            
            console.log(newjsonData);

            myCurrentData.activeWebService = null; 
            
            if ( !isEmpty( obParameters.success_callback ) && typeof obParameters.success_callback == 'function' ){
                obParameters.success_callback(newjsonData);
            }
            
        });
        
    },

    Get_Crusts : function( obParameters ) {
    
        console_log("Get_Crusts");
        
        var jsonData = { Status : { code : 0, value: 0} };
        
        $.getJSON( "mockdata/CrustTypes.json", function(jsonData) {

            // var oModel = new sap.ui.model.json.JSONModel();
            // oModel.setData(jsonData);
            // sap.ui.getCore().setModel( oModel, Common.ModelNames.PIZZAS_STANDARD );

            //note: for you to read the model, just use:
            // sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_STANDARD );        
           
            var newjsonData = {
                "Status" : {
                    code: 0,
                    value: 0
                },
                "CrustTypes" : jsonData
            };
            
            console.log(newjsonData);

            myCurrentData.activeWebService = null; 
            
            if ( !isEmpty( obParameters.success_callback ) && typeof obParameters.success_callback == 'function' ){
                obParameters.success_callback(newjsonData);
            }
            
        });
        
    },

    Get_Sauces : function( obParameters ) {
    
        console_log("Get_Sauces");
        
        var jsonData = { Status : { code : 0, value: 0} };
        
        $.getJSON( "mockdata/SauceTypes.json", function(jsonData) {

            // var oModel = new sap.ui.model.json.JSONModel();
            // oModel.setData(jsonData);
            // sap.ui.getCore().setModel( oModel, Common.ModelNames.PIZZAS_STANDARD );

            //note: for you to read the model, just use:
            // sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_STANDARD );        
           
            var newjsonData = {
                "Status" : {
                    code: 0,
                    value: 0
                },
                "SauceTypes" : jsonData
            };
            
            console.log(newjsonData);

            myCurrentData.activeWebService = null; 
            
            if ( !isEmpty( obParameters.success_callback ) && typeof obParameters.success_callback == 'function' ){
                obParameters.success_callback(newjsonData);
            }
            
        });
        
    },

    Get_Toppings : function( obParameters ) {
    
        console_log("Get_Toppings");
        
        var jsonData = { Status : { code : 0, value: 0} };
        
        $.getJSON( "mockdata/ToppingTypes.json", function(jsonData) {

            // var oModel = new sap.ui.model.json.JSONModel();
            // oModel.setData(jsonData);
            // sap.ui.getCore().setModel( oModel, Common.ModelNames.PIZZAS_STANDARD );

            //note: for you to read the model, just use:
            // sap.ui.getCore().getModel( Common.ModelNames.PIZZAS_STANDARD );        
           
            var newjsonData = {
                "Status" : {
                    code: 0,
                    value: 0
                },
                "ToppingTypes" : jsonData
            };
            
            console.log(newjsonData);

            myCurrentData.activeWebService = null; 
            
            if ( !isEmpty( obParameters.success_callback ) && typeof obParameters.success_callback == 'function' ){
                obParameters.success_callback(newjsonData);
            }
            
        });
        
    },

};