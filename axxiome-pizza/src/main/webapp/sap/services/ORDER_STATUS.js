jQuery.sap.require("sap.services.ServicesConstants");
/**
 * @description ORDER_STATUS Get_Status function
 * @purpose Get the list of orders status
 */

var ORDER_STATUS_WS = {

	Get_Status : function(parameters) {

		console_log("ORDER_STATUS_WS > Get_Status");

		var CONST_AUTH = sap.services.ServicesConstants.MENU_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.GET_ORDERSTATUS;

		parameters.dataModelName = Common.ModelNames.ORDERS_STATUS;

		var obData = new Object();
		obData.filter = [];

		if (!isEmpty(parameters.customData)) {
			if (parameters.customData.StatusID) {
				obData.filter.push([ "StatusID", "eq", parameters.customData.StatusID ]);
			}
		}

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	}
	
}