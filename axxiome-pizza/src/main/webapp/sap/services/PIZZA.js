jQuery.sap.require("sap.services.ServicesConstants");
/**
 * @description PIZZA_Get_Options function
 * @purpose Get the list of standard pizzas
 */

var PIZZAS_WS = {

	Get_Pizzas : function(parameters) {

		console_log("PIZZA_Get_Options");

		var CONST_AUTH = sap.services.ServicesConstants.MENU_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.GET_PIZZAS_STANDARD;

		parameters.dataModelName = Common.ModelNames.PIZZAS_STANDARDS;

		var obData = new Object();
		obData.filter = [];

		if (!isEmpty(parameters.customData)) {
			if (parameters.customData.PizzaID) {
				obData.filter.push([ "StandardPizzaID", "eq", parameters.customData.PizzaID ]);
			}
		}

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	},

	Get_Pizzas_Details : function(parameters) {

		console_log("PIZZA_Get_Options");

		var CONST_AUTH = sap.services.ServicesConstants.MENU_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.GET_PIZZAS_STANDARD_DETAILS;

		// parameters.dataModelName = Common.ModelNames.PIZZAS_STANDARDS;

		var obData = new Object();
		obData.filter = [];

		if (!isEmpty(parameters.customData)) {
			if (parameters.customData.StandardPizzaID) {
				obData.filter.push([ "StandardPizzaID ", "eq", parameters.customData.StandardPizzaID ]);
			}
		}

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	},
	

	Get_Crusts : function(parameters) {

		console_log("Get_Crusts");

		var CONST_AUTH = sap.services.ServicesConstants.MENU_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.GET_CRUSTS;

		parameters.dataModelName = Common.ModelNames.PIZZAS_CRUSTS;

		var obData = new Object();
		obData.filter = [];

		if (!isEmpty(parameters.customData)) {
			if (parameters.customData.CrustTypeID) {
				obData.filter.push([ "CrustTypeID", "eq", parameters.customData.CrustTypeID ]);
			}
		}

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	},

	Get_Sauces : function(parameters) {

		console_log("Get_Crusts");

		var CONST_AUTH = sap.services.ServicesConstants.MENU_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.GET_SAUCES;

		parameters.dataModelName = Common.ModelNames.PIZZAS_SAUCES;

		var obData = new Object();
		obData.filter = [];

		if (!isEmpty(parameters.customData)) {
			if (parameters.customData.SauceTypeID) {
				obData.filter.push([ "SauceTypeID", "eq", parameters.customData.SauceTypeID ]);
			}
		}

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	},

	Get_Toppings : function(parameters) {

		console_log("Get_Crusts");

		var CONST_AUTH = sap.services.ServicesConstants.MENU_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.GET_TOPPINGS;

		parameters.dataModelName = Common.ModelNames.PIZZAS_TOPPINGS;

		var obData = new Object();
		obData.filter = [];

		if (!isEmpty(parameters.customData)) {
			if (parameters.customData.ToppingTypeID) {
				obData.filter.push([ "ToppingTypeID", "eq",
						parameters.customData.ToppingTypeID ]);
			}
		}

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	},

	Create_Pizza : function(parameters) {

		console_log("Create_Order");

		var CONST_AUTH = sap.services.ServicesConstants.PIZZA_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.CREATE_PIZZA;

		parameters.METHOD = "POST";

		var obData = new Object();
		obData.PizzaID = generateRandom();
		obData.OrderID = parameters.customData.OrderID;
		obData.StandardPizzaID = parameters.customData.StandardPizzaID;
		obData.CrustTypeID = parameters.customData.CrustTypeID;
		obData.SauceTypeID = parameters.customData.SauceTypeID;
		obData.Size = parameters.customData.Size;

		myCurrentData.TEMP.PizzaData = obData;

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	},
	
	
	Create_Topping : function(parameters) {

		console_log("Create_Topping");

		var CONST_AUTH = sap.services.ServicesConstants.PIZZA_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.CREATE_TOPPING;

		parameters.METHOD = "POST";

		var obData = new Object();
		obData.PizzaID = parameters.customData.PizzaID;
		obData.ToppingTypeID = parameters.customData.ToppingTypeID;

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	}

}