var AUTHENTICATION_MOCK = {

	login: function( obParameters ) {
    
	    console_log("PIZZA_Get_Options");
	    
	    var jsonData = { Status : { code : 0, value: 0} };
	    
	    $.getJSON( "mockdata/Customers.json", function(jsonData) {

	    	var pos = 0;
	    	var boFound = false;
	    	$.each( jsonData, function( i, item){

	    		if ( obParameters.customData.username === item.Email ){
	    			boFound = true;
	    		} else {
	    			pos++;
	    		}

	    	});

	    	/* Just to mock data purposes */
	    	boFound = true;
	    	pos = 0;
	    	/* Just to mock data purposes */

	    	if ( boFound === true ){

		    	var jsonData_Customer = {
		    		"CustomerID": 	jsonData[pos].CustomerID,
		    		"FirstName" : 	jsonData[pos].FirstName,
		    		"LastName" 	: 	jsonData[pos].LastName,
		    		"Street"	: 	jsonData[pos].Street,
		    		"City"		: 	jsonData[pos].City,
		    		"Province"	: 	jsonData[pos].Province,
		    		"PostalCode": 	jsonData[pos].PostalCode,

		    		"DisplayName":  jsonData[pos].FirstName + " " + jsonData[pos].LastName
		    	};

		    	var jsonData_LastLogin = {
		    		"lastLoginDate" : "09/25/2016",
		    		"lastLoginHour" : "10:46" 
		    	};

		    	var jsonData_Session = {
		    		"timeout" : 360
		    	}

		        var newjsonData = {
		            "Status" : {
		                code: 0,
		                value: 0
		            },
		            "sessionId": UUIDv4(),
		            "customer" : jsonData_Customer,
		            "lastLogin": jsonData_LastLogin,
		            "sessionTimeout" : jsonData_Session
		        };
		        
		        console.log(newjsonData);

		        myCurrentData.activeWebService = null; 
		        
		        if ( !isEmpty( obParameters.success_callback ) && typeof obParameters.success_callback == 'function' ){
		            obParameters.success_callback(newjsonData);
		        }
		    
			} else {

				if ( !isEmpty( obParameters.failure_callback ) && typeof obParameters.failure_callback == 'function' ){
		            obParameters.failure_callback(jsonData);
		        }

		    }
	        
	    });

	},

	Authenticate_with_MOCK_info : function( jsonData ){

		var pos = 0;

    	var jsonData_Customer = {
    		"CustomerID": 	jsonData[pos].CustomerID,
    		"FirstName" : 	jsonData[pos].FirstName,
    		"LastName" 	: 	jsonData[pos].LastName,
    		"Street"	: 	jsonData[pos].Street,
    		"City"		: 	jsonData[pos].City,
    		"Province"	: 	jsonData[pos].Province,
    		"PostalCode": 	jsonData[pos].PostalCode,

    		"DisplayName":  jsonData[pos].FirstName + " " + jsonData[pos].LastName
    	};

    	var jsonData_LastLogin = {
    		"lastLoginDate" : "09/25/2016",
    		"lastLoginHour" : "10:46" 
    	};

    	var jsonData_Session = {
    		"timeout" : 360
    	}

        var newjsonData = {
            "Status" : {
                code: 0,
                value: 0
            },
            "sessionId": UUIDv4(),
            "customer" : jsonData_Customer,
            "lastLogin": jsonData_LastLogin,
            "sessionTimeout" : jsonData_Session
        };
        
        console.log(newjsonData);

        myCurrentData.activeWebService = null; 

       	return newjsonData;

    }



	    
};