jQuery.sap.require("sap.services.ServicesConstants");
/**
 * @description PIZZA_Get_Options function
 * @purpose Get the list of standard pizzas
 */

var ORDERS_WS = {

	Get_Orders : function(parameters) {

		console_log("Get_Orders");

		var CONST_AUTH = sap.services.ServicesConstants.ORDER_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.GET_ORDERS;

		var obData = new Object();
		obData.filter = [];
		obData.orderby = [ "OrderTstmp desc" ];

		if (!isEmpty(parameters.customData)) {
			if (parameters.customData.OrderID) {
				obData.filter.push([ "OrderID", "eq",
						parameters.customData.OrderID ]);
			}
			if (parameters.customData.CustomerID) {
				obData.filter.push([ "CustomerID", "eq",
						parameters.customData.CustomerID ]);
			}
			if (parameters.customData.PizzaID) {
				obData.filter.push([ "PizzaID", "eq",
						parameters.customData.PizzaID ]);
			}
			if (parameters.customData.StatusID) {
				obData.filter.push([ "StatusID", "eq",
						parameters.customData.StatusID ]);
			}
		}

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	},

	Create_Order : function(parameters) {

		console_log("Create_Order");

		var CONST_AUTH = sap.services.ServicesConstants.ORDER_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.CREATE_ORDER;
		var obData = new Object();

        var dateObj = new Date();
        var day = dateObj.getUTCDate();
        var appender = day + "";
        obData.OrderID = appender + generateRandom();
		parameters.METHOD = "POST";
		obData.StatusID = 1;

		if (!isEmpty(parameters.customData.CustomerID)) {
			obData.CustomerID = parameters.customData.CustomerID;
		} else {
			obData.CustomerID = myCurrentData.INFO.get_User_ID();
		}

		myCurrentData.TEMP.OrderDataTEMP = obData;

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	},

	Update_Order : function(parameters) {
		console_log("Update_Order");

		var CONST_AUTH = sap.services.ServicesConstants.ORDER_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.CREATE_ORDER;
		var obData = new Object();
		parameters.METHOD = "PUT";

		if (!isEmpty(parameters.customData.Id)) {
			obData.Id = parameters.customData.Id;
		}

		if (!isEmpty(parameters.customData.OrderID)) {
			obData.OrderID = parameters.customData.OrderID;
		}

		if (!isEmpty(parameters.customData.StatusID)) {
			obData.StatusID = parameters.customData.StatusID;
		}

		if (!isEmpty(parameters.customData.CustomerID)) {
			obData.CustomerID = parameters.customData.CustomerID;
		} else {
			obData.CustomerID = myCurrentData.INFO.get_User_ID();
		}

		myCurrentData.TEMP.OrderDataTEMP = obData;

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

		if (parameters.customData.StatusID == 5) {

			var CONST_OFFER_AUTH = sap.services.ServicesConstants.OFFER_SERVICES;
			var stURL_Offer_Service = CONST_OFFER_AUTH.ENTITIES.CREATE_OFFER;
			var obOfferData = new Object();

			if (!isEmpty(parameters.customData.OrderID)) {
				obOfferData.RewardOrderID = parameters.customData.OrderID;
			}

			var dateObj = new Date();
			var mins = dateObj.getMinutes();
			var appender = mins + "";
			obOfferData.OfferID = appender + generateRandom(1, 99999);

			if (!isEmpty(parameters.customData.CustomerID)) {
				obOfferData.CustomerID = parameters.customData.CustomerID;
			}

			offerParameters = parameters;
			offerParameters.METHOD = "POST";

			var myConn = new AXX_CONN();
			myConn.POST_New(obOfferData, stURL_Offer_Service, offerParameters);

		}
	},

	Get_Offers : function(parameters) {

		console_log("Get_Offers");

		var CONST_AUTH = sap.services.ServicesConstants.OFFER_SERVICES;
		var stURL_Service = CONST_AUTH.ENTITIES.GET_OFFERS;

		var obData = new Object();
		obData.filter = [];

		if (!isEmpty(parameters.customData)) {
			if (parameters.customData.CustomerID) {
				obData.filter.push([ "CustomerID", "eq",
						parameters.customData.CustomerID ]);
			}
		}

		var myConn = new AXX_CONN();
		myConn.POST_New(obData, stURL_Service, parameters);

	},
	
	Redeem_Offer : function(parameters) {
		console_log("Update_Offer");

		var CONST_OFFER_AUTH = sap.services.ServicesConstants.OFFER_SERVICES;
		var stURL_Offer_Service = CONST_OFFER_AUTH.ENTITIES.CREATE_OFFER;
		var obOfferData = new Object();
		parameters.METHOD = "PUT";

		if (!isEmpty(parameters.customData.OfferID)) {
			obOfferData.OfferID = parameters.customData.OfferID;
		}
		if (!isEmpty(parameters.customData.CustomerID)) {
			obOfferData.CustomerID = parameters.customData.CustomerID;
		}
		if (!isEmpty(parameters.customData.RedeemOrderID)) {
			obOfferData.RedeemOrderID = parameters.customData.RedeemOrderID;
		}
		if (!isEmpty(parameters.customData.RewardOrderID)) {
			obOfferData.RewardOrderID = parameters.customData.RewardOrderID;
		}
		
		myCurrentData.TEMP.OfferDataTEMP = obOfferData;

		var myConn = new AXX_CONN();
		myConn.POST_New(obOfferData, stURL_Offer_Service, parameters);

	}
}