jQuery.sap.declare("sap.services.ServicesConstants");
// jQuery.sap.require("js.settings.ConnectionSettings");

//var HOST = connectionSettingController.getConnection_URL("REST");
var HOST= "";

sap.services.ServicesConstants = {
		
	/** Constants used for Authentication service */
	ACCOUNT_SERVICE : {
		NAME: "Account_Service", //Class Name
		URI: HOST,
		PORT: "",	
		ENTITIES:{
			// GET_CUSTOMER_ACCOUNTS_FOR_TRANSFER : "/getCustomerAccountsForTransfer",
		}
	},

	/** Constants used for Authentication service */
	AUTHENTICATION_SERVICE : {
		ENTITIES:{
			LOGIN : "/customLoginCustomer",
			LOGOUT : "/authenticationLogout",
			CHECK_SESSION: "/checkSession",
			CUSTOMERS : "/Customer",
			STAFF : "/StaffUsers"
		}
	},
	
	/** Constants used for General services */
	MENU_SERVICES : {
		ENTITIES:{
			GET_PIZZAS_STANDARD: "/StandardPizza",
			GET_PIZZAS_STANDARD_DETAILS: "/StandardPizzaDetails",
			GET_CRUSTS: "/CrustType",
			GET_SAUCES: "/SauceType",
			GET_TOPPINGS: "/ToppingType",
			GET_ORDERSTATUS: "/OrderStatus",

			CREATE_PIZZA: "/Pizza",
			CREATE_TOPPING: "/Topping"
		}
	},

	ORDER_SERVICES : {
		ENTITIES:{
			GET_ORDERS: "/OrderPizzaDetails6",
			CREATE_ORDER : "/Order"
		}
	},
	
	PIZZA_SERVICES : {
		ENTITIES:{
			CREATE_PIZZA : "/Pizza",
			CREATE_TOPPING : "/Topping"
		}
	},
	
	OFFER_SERVICES : {
		ENTITIES:{
			GET_OFFERS: "/Offer",
			CREATE_OFFER : "/Offer"
		}
	}
	
};