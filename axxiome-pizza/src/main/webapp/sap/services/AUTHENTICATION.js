jQuery.sap.require("sap.services.ServicesConstants");

/**
 @description AUTHENTICATION WS functions
 @purpose Get the list of customer
*/

AUTHENTICATION_WS = {

	Authenticate : function(parameters) {
	    
	    console_log("Authenticate");
		
		var CONST_AUTH = sap.services.ServicesConstants.AUTHENTICATION_SERVICE;
		var stURL_Service = CONST_AUTH.ENTITIES.CUSTOMERS;
		
		var obData = new Object();
		obData.filter = [];

		if ( !isEmpty( parameters.customData ) ) {
			if ( parameters.customData.Email ){
				obData.filter.push( [ "Email", "eq", parameters.customData.Email ] );
			}
			if ( parameters.customData.Password ){
				var myNewPass = SECURITY.MD5( parameters.customData.Password  );
				obData.filter.push( [ "Password", "eq", myNewPass] );
			}
		}
		
	    var myConn = new AXX_CONN();
		myConn.POST_New( obData
			, stURL_Service
			, parameters
		);

	},

	AuthenticateStaff : function(parameters) {
	    
	    console_log("Authenticate");
		
		var CONST_AUTH = sap.services.ServicesConstants.AUTHENTICATION_SERVICE;
		var stURL_Service = CONST_AUTH.ENTITIES.STAFF;
		
		var obData = new Object();
		obData.filter = [];

		if ( !isEmpty( parameters.customData ) ) {
			if ( parameters.customData.Email ){
				obData.filter.push( [ "Email", "eq", parameters.customData.Email ] );
			}
			if ( parameters.customData.Password ){
				//var myNewPass = SECURITY.MD5( parameters.customData.Password  );
				obData.filter.push( [ "Password", "eq", parameters.customData.Password] );
			}
		}
		
	    var myConn = new AXX_CONN();
		myConn.POST_New( obData
			, stURL_Service
			, parameters
		);

	},

	Create_User: function( parameters ) {
	    
	    console_log("Create_Order");
		
		var CONST_AUTH = sap.services.ServicesConstants.AUTHENTICATION_SERVICE;
		var stURL_Service = CONST_AUTH.ENTITIES.CUSTOMERS;
		
		parameters.METHOD = "POST";

		var myNewPass = SECURITY.MD5( parameters.customData.Password  );

		var obData = {};
		// obData.CustomerId 	= parameters.CustomerId;
		obData.CustomerID 	= generateRandom( 1, 100 );
		obData.FirstName 	= parameters.customData.FirstName;
		obData.LastName 	= parameters.customData.LastName;
		obData.Email 		= parameters.customData.Email;
		obData.Password  	= myNewPass;
		obData.Street  		= parameters.customData.Street ;
		obData.City  		= parameters.customData.City;
		obData.Province  	= parameters.customData.Province;
		obData.PostalCode   = parameters.customData.PostalCode;

		myCurrentData.TEMP.customer = obData;
		
	    var myConn = new AXX_CONN();
		myConn.POST_New( obData
			, stURL_Service
			, parameters
		);

	},

}